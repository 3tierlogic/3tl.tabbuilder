<?php

	session_start();

	require_once('config.php');
	require_once('functions.php');

	require_once('facebook/config.php');
	require_once('facebook/fb_sdk/facebook.php');

	// Create our Application instance
	$facebook = new Facebook(array(
		'appId'  => $fbconfig['appid'],
		'secret' => $fbconfig['secret']
	));

	$logoutUrl = $facebook->getLogoutUrl( array( 'next' => DEV_WEBSITE_URL .'index.php' ) );

	// if user unauthorized, go to the home page
	ars_is_user_authorized();

?><!-- saved from url=(0014)about:internet -->
<!doctype html>
<html lang="en-US">
<head>
<meta charset="UTF-8" />
<title>Dashboard</title>
<link rel="stylesheet" href="../style.css" />
<link rel="stylesheet" href="css/jquery-ui-1.8.16.css" />
<link rel="stylesheet" href="css/jquery.fancybox-1.3.4.css" />
<link rel="stylesheet" href="css/colorpicker.css" />
<link rel="stylesheet" href="css/style.css" />
<script src="js/jquery.1.7.js"></script>
<script src="js/jquery-ui-1.8.16.js"></script>
<script src="js/jquery.fancybox-1.3.4.pack.js"></script>
<script src="js/colorpicker.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="js/site.js"></script>
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if IE 6]>
<script src="../js/belatedPNG.js"></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
</head>



<body class="normalpage">

<!-- ajax auxillary elements -->
<img id="ajaxLoader" src="images/ajax-loader.gif" alt="" />

<section id="page">
<div id="bodywrap">
<section id="top">
<nav>
<h1 id="sitename">
  <a href="#">3 Tier Logic Inc | Tab Builder Dashboard </a></h1>
  
  <ul id="sitenav">
    <? if (ars_is_user_superuser()) { ?>
    	<li><a href="../admin/">Admin</a></li>
    <? } ?>
	<li><a href="../report/main.php">Report</a></li>
    <li class="current"><a href="dashboard.php">Dashboard</a></li>
    <li><a href="../help">Help</a></li>
    <li><a href="http://platform.3tierlogic.com/tabbuilder/">Logout</a></li>
  </ul>

</nav>

<header id="normalheader">
	<div class="buttons">
		<a class="bigButton orange prev hidden" href="dashboard.php">Return to Dashboard<span></span></a>
		<a href="" class="bigButton blue next createTab">Create a New Tab<span></span></a>
	</div><!-- / .buttons -->
</header>

</section>

<section id="contentwrap">
<div id="contents" class="normalcontents">
<section id="normalpage">



	<section id="left">

		<div id="includes">
			<div class="logoutUrl"><?php echo $logoutUrl; ?></div>
		</div><!-- / .includes -->

		<?php
		// searching tabs
		if ( isset( $_REQUEST['s'] ) ) {
			$query = ars_get_allowed_tabs( $_SESSION['user_pages'], 0, strtolower( $_REQUEST['s'] ) );
		}
		// show all tabs from one page (tabs, created by user or where he's admin)
		elseif ( isset( $_REQUEST['id'] ) ) {
			$query = ars_get_allowed_tabs( $_SESSION['user_pages'], $_REQUEST['id'] );
		}
		// show all tabs from all pages (tabs, created by user or where he's admin)
		else {
			$query = ars_get_allowed_tabs( $_SESSION['user_pages'] );
		}


		// search returns no tabs
		if ( mysql_num_rows( $query ) == 0 && isset( $_REQUEST['s'] ) ) {
			echo '<p>No tabs found. Try to change the keyword.</p>';
		}
		// if no tabs found -  show the menu for creating new tabs
		elseif ( mysql_num_rows( $query ) == 0 && ! isset( $_REQUEST['s'] ) ) {
			$showTabMenu = 1;
		}
		// if at least one tab - show it
		else {
			$showTabInfo = 1;
		}


		require_once('create-tab.php');


		# TAB INFO AND MANAGING LINKS
		// if there's at least one created tab - this block will be shown
		if ( $showTabInfo ) : ?>

			<!-- list of all user tabs -->
			<div class="myTabs">
				<ul>
					<?php while ( $tab = mysql_fetch_assoc( $query ) ) : ?>
						<li>

							<div class="heading">
								<div class="status">
									<div class="label">Status</div>
									<span>
										<?php if ( $tab['saved'] == '1' ) echo "Saved"; else echo "Not Saved"; ?>,
										<?php if ( $tab['published'] == '1' ) echo "Published"; else echo "Not Published"; ?>
									</span>
								</div><!-- / .status -->
								Fan Page:
								<span class="pageName">
									<?php echo ars_get_page_name_by_id( $tab['fan_page_id'] ); ?>
								</span>
							</div><!-- / .heading -->

							<div class="tabName">
								<div class="label">Selected Tab:</div>
								<span class="name"><?php echo $tab['name']; ?></span>
								<?php $contestDescription = getContestDescription( $tab['id'] );
									if ( $contestDescription ) echo '<span class="description">' . $contestDescription . '</span>'; ?>
							</div><!-- / .tabName -->

							<div class="options">

								<a class="rename" href="<?php echo $tab['id']; ?>"
									data-fbid="<?php echo $tab['fan_page_id']; ?>"
									data-appid="<?php echo $tab['app_id']; ?>"
									data-fbuser="<?php echo $tab['user_id']; ?>">Rename Tab</a>

								<?php // check if an app is added to a fan page
									$info = $facebook->api( "/".$tab['fan_page_id']."/tabs/".$tab['app_id'] );
									if ( empty( $info['data'] ) ) : ?>
										<a class="reinstall" href="<?php echo $tab['fan_page_id']; ?>"
											data-appid="<?php echo $tab['app_id']; ?>">Re-Install Tab</a>
									<?php endif; ?>

								<a class="delete" href="<?php echo $tab['id']; ?>"
									data-fbid="<?php echo $tab['fan_page_id']; ?>"
									data-appid="<?php echo $tab['app_id']; ?>">Delete Tab</a>

							</div><!-- / .options -->

							<div class="bigButtons">
								<a class="bigButton orange edit" href="<?php echo EDITOR; ?>?id=<?php echo $tab['id']; ?>">Edit this Tab<span></span></a>
								<a class="bigButton blue next" href="https://www.facebook.com/profile.php?id=<?php echo $tab['fan_page_id']; ?>&sk=app_<?php echo $tab['app_id']; ?>" target="_blank">View Tab on Facebook<span></span></a>
							</div><!-- / .bigButtons -->

						</li>
					<?php endwhile; ?>
				</ul>
			</div><!-- / .myTabs -->

		<?php endif; ?>

	</section><!-- / #left -->



	<section id="sidebar">
		<h2>My Current Tabs</h2>

		<form method="post" action="dashboard.php">
			<div class="searchTabs">
				<?php $searchWord = isset( $_REQUEST['s'] ) ? $_REQUEST['s'] : ''; ?>
				<input type="text" name="s" value="<?php echo $searchWord;  ?>" />
				<input type="submit" value="Search" />
			</div>
		</form>

		<?php
			if ( isset( $_REQUEST['id'] ) )
				ars_show_user_pages( $_SESSION['user_pages'], $_REQUEST['id'] );
			else
				ars_show_user_pages( $_SESSION['user_pages'] );
		?>
	</section><!-- / #sidebar -->



	<div class="clear"></div>
</section><!-- / #normalpage -->
</div><!--  / #contents -->
</section><!-- / #contentwrap -->
</div>



<footer id="pagefooter">
<div id="bottom">
<div class="block1">
<h2>Meet The Team</h2>

<div class="teamimg"><? include("team.php")?></div>

</div>
<div class="block2">
<h2>Latest Project</h2>
<div class="projectthmb"><? include("latestproject.php")?></div>

</div>
<div class="block3">

<h2>More About</h2>
<p>3 Tier Logic is a full service digital marketing and technology services company that enables organizations to grow their customer and prospect communities through anywhere-anytime communications. </p>

</div>
<div class="clear"></div>
</div>
<div id="credits">
<p>
<? include("copyright.html")?>
</p>
</div>
</footer>

</section>
</body>
</html>
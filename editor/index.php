<?php
/**
 * Copyright 2011 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

session_start();

//*** Quit if the user has not logged in properly
if ( ! isset ( $_SESSION['companyid'] ) ) {

	header ( 'Location: http://platform.3tierlogic.com/tabbuilder/index1_3.php?id='. $_SESSION['companyid'] );
	exit();
}


require_once('config.php');
require_once('functions.php');

require_once('facebook/config.php');
require_once('facebook/fb_sdk/facebook.php');

// Create our Application instance
$facebook = new Facebook(array(
	'appId'  => $fbconfig['appid'],
	'secret' => $fbconfig['secret']
));


// If a user landed from facebook invitation
if ( ! empty($_REQUEST['request_ids']) ) {
	// get app token
	$token_url = "https://graph.facebook.com/oauth/access_token?grant_type=client_credentials" .
								"&client_id=" . $fbconfig['appid'] . "&client_secret=" . $fbconfig['secret'];
	$app_token = file_get_contents( $token_url );
	$app_token = substr( $app_token, strpos( $app_token, '=' ) + 1 );
	// we may have more than one request, so it's better to loop
	$requests = explode( ',', $_REQUEST['request_ids'] );
	foreach ( $requests as $request_id ) {
		$request = $facebook->api("/$request_id?$app_token");
		$json = json_decode( $request['data'] );
		$appLink = $json->{'appLink'};
		if ( strpos( $appLink, 'http' ) !== false ) {
			echo "<script>top.location.href = '" . $appLink . "';</script>";
			exit();
		}
	}
}

// Get User ID
$user = $facebook->getUser();

// We may or may not have this data based on whether the user is logged in.
//
// If we have a $user id here, it means we know the user is logged into
// Facebook, but we don't know if the access token is valid. An access
// token is invalid if the user logged out of Facebook.

if ( $user ) {
	try {
		// Proceed knowing you have a logged in user who's authenticated.
		$user_info = $facebook->api('/me');
		// if we have no records about the user, add it
		ars_save_new_user_info( $user_info );
		// save all required info in session
		$_SESSION['user_info'] = $user_info;
		$_SESSION['user_pages'] = ars_filter_apps_from_pages( $facebook->api("/$user/accounts") );
		// is user the admin?
		$_SESSION['admin'] = ars_is_admin( $user_info['id'] );
	} catch ( FacebookApiException $e ) {
		error_log($e);
		$user = null;
	}
}

// Login or logout url will be needed depending on current user state.
if ( $user ) {
	$logoutUrl = $facebook->getLogoutUrl();
} else {
	$loginUrl   = $facebook->getLoginUrl(array('scope' => 'publish_actions,manage_pages'));
}

?><!doctype html>
<html lang="en-US" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
<meta charset="UTF-8" />
<title>Connect With Facebook to Continue</title>
<link rel="stylesheet" href="css/style.css" />
<style type="text/css">
<!--
 body {
  text-align: center;
  }
#container {
  margin: 0 auto;
  width: 900px;
  text-align: justify;
  }
  
  #containerLogo {
  margin: 0 auto;
  width: 900px;
  text-align: center;
  }
 -->
</style>
</head>

<body>
<div id="container">
	<div id="containerLogo"><img src="images/3tllogo.png"><br><br></div>
    
    <p  style="font-family:Arial, Helvetica, sans-serif; font-style:normal; font-size:16px; font-weight:bold;">Welcome to the beta version of 3 Tier Logic's Digital Marketing Platform!</p>
	<p  style="font-family:Arial, Helvetica, sans-serif; font-style:normal; font-size:12px;">Beta software is software that is tested by real people in real applications. By beta testing our Platform in a broad testing environment, we 
increase the overall quality of the Platform. We invite you to begin using the Platform and welcome your feedback, which you can send 
to  <a href="mailto:feedback@3tierlogic.com">feedback@3tierlogic.com</a>.
<br><br>
The latest version of the Platform includes two main updates:</p>
<ul style="font-family:Verdana, Geneva, sans-serif; font-style:normal; font-size:12px;">
<li><font color="#0099FF"><b>Photo Contest</b></font> (also known as a User Generated Contest application): Inspire customers to upload unique and authentic 
            photo content for your brand and share with their social networks. The workflow can include a a Submission and/or a Voting phase. </li>

<li><font color="#0099FF"><b>Tab Builder:</b></font> Design, upload and manage content on static Facebook tabs. Use the open palette layout to drag and drop widgets 
            from a wide menu to fully customize your tabs.</li>
</ul>
<p  style="font-family:Arial, Helvetica, sans-serif; font-style:normal; font-size:12px;">We look forward to receiving your feedback and suggestions and hope you enjoy the Platform. </p>
<p  style="font-family:Arial, Helvetica, sans-serif; font-style:normal; font-size:10px;"><font color="#CC0000"><b>Supported versions:</b></font> IE 9+, Firefox 17+, Chrome23+, Safari5+ </p>
<div id="loginFB">
	<?php if ( ! $user ) : ?>
		<h1>Please Connect With Facebook to Continue</h1>
		<a href="<?php echo $loginUrl; ?>"><img src="images/fbconnect.gif" /></a>
	<?php else: ?>
		<meta http-equiv="refresh" content="0; url=dashboard.php" />
	<?php endif; ?>
</div><!-- / #loginFB -->
</div><!-- / #container -->
</body>
</html>
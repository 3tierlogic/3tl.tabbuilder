<!-- create new tab -->
<div class="createNewTab <?php if ( $showTabMenu ) echo "show"; ?>">

	<!-- steps -->
	<div class="steps">
		<div class="li b1 active">Step 1<span class="arrow"></span></div>
		<div class="li b2">Step 2<span class="arrow"></span></div>
		<div class="li b3">Step 3<span class="arrow"></span></div>
	</div><!-- / .steps -->

	<!-- STEP 1 -->
	<div class="step">
		<h2>Select a Fan Page</h2>
		<p>The tab will be installed onto the page you select.</p>
		<?php // available pages
		$records = count( $_SESSION['user_pages'] );
		if ( $records ) : ?>
			<select name="fan_page_id">
				<?php
				for ( $i = 0; $i < $records; $i++ ) {
					$id = $_SESSION['user_pages'][$i]['id'];
					$name = $_SESSION['user_pages'][$i]['name'];
					echo '<option value="'.$id.'">'.$name.'</option>';
				} ?>
			</select>
			<input type="hidden" name="user_id" value="<?php echo $_SESSION['user_info']['id']; ?>" />
            <input type="hidden" name="partner_id" value="<?php echo $_SESSION['companyid']; ?>" />
			<br /><br />
			<a class="aButton done" id="fan_page_id_submit" href="">Continue</a>
		<?php else : ?>
			<p>You have no created pages on Facebook.</p>
		<?php endif; ?>
	</div><!-- / .step -->

	<!-- STEP 2 -->
	<form method="post" action="">
		<div class="step">
			<h2>Name Your Tab</h2>
			<p>The name entered will be displayed on your Facebook Page underneath the tab icon. You are able to change this tab name on the Dashboard.</p>
			<input type="text" name="tab_name" class="required" maxlength="50" value="" />
            <input type="hidden" name="partner_id" value="<?php echo $_SESSION['companyid']; ?>" />
			<br /><br />
			<a class="aButton done" id="tab_name_submit" href="">Continue</a>
			<img class="ajaxLoader" src="images/ajax-loader.gif" alt="" />
		</div><!-- / .step -->
	</form>

	<!-- STEP 3 -->
	<div class="step">
		<a class="aButton done" id="tab_edit_in_editor" href="">Start building tab</a>
		<!--<a class="aButton done" id="tab_add_another" href="">Add another</a>-->
	</div><!-- / .step -->

</div><!-- / .createNewTab -->


<?php // документ UTF-8 ?>
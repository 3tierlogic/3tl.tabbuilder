<?php

	// this file is responsible for displaying "contest form" on facebook

	//facebook application
	//@author: Muhammad Nouman

	header('P3P: CP="CAO PSA OUR"');  // this is IE session issue fix

	// includes
	require_once('config.php');
	require_once('functions.php');

	session_start();

	$appId = $_SESSION['appId'];
	$secret = $_SESSION['appSecret'];
	$page_id = $_SESSION['page_id'];
	$app_link = $_SESSION['appLink'];

	if ( ! isset( $appId ) || ! isset( $secret ) || ! isset( $page_id ) ) exit();


	// get values from the sting and then search the db to get correct app secret
	$fbconfig['appid' ] = $appId;
	$fbconfig['api'] = $appId;
	$fbconfig['secret'] = $secret;


	//try to include the facebook base file
	try{
		include_once "facebook/fb_sdk/facebook.php";
	}
	catch ( Exception $o ) {
		error_log($o);
	}
	$user = null; //facebook user uid


	// Create our Application instance.
	$facebook = new Facebook(array(
		'appId'  => $fbconfig['appid'],
		'secret' => $fbconfig['secret'],
		'cookie' => true
	));


	// If the user is from safari and session is not valid, this the session bug, fixed here
	if ( ! count($_COOKIE) > 0 && strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') && empty($data) )
	{
		$signed_var = $_GET["request_var"];
		$secret = $fbconfig['secret'];  //get app secret from db
		include_once('facebook/fb_sdk/sig_control.php');
		$data=parse_signed_request($signed_var, $secret);
		$page_id=$data["page"]["id"];
		$page_data = json_decode(file_get_contents("https://graph.facebook.com/$page_id"));
		$page_link=$page_data->link;
		echo $page_id;
		echo $page_link;
		$fbconfig['pageUrl']    =   $page_data->link;
		$fbconfig['appPageUrl'] =   "{$fbconfig['pageUrl']}?sk=app_{$fbconfig['appid' ]}&app_data=true";
		$loginUrl   = $facebook->getLoginUrl(
				array(
					'scope'         => 'user_location,user_birthday,email,publish_actions',
					'redirect_uri'  => $fbconfig['appPageUrl'],
				)
		); 
		// if the user session is not valid on safari then we will redirect to get the authorization
		echo "<script type='text/javascript'>top.location.href = '$loginUrl';</script>";
	}


	// We get user info from API using this code
	$user = $facebook->getUser();
	if ( $user ) {
		try {
			$user_info = $facebook->api('/me');
		}
		catch ( FacebookApiException $e ) {
			//print_r( $user_info );
			$user = null;
		}
	}


	// get tab to show
	$query = "SELECT id FROM pages WHERE fan_page_id = '" . $page_id . "' AND app_id = '" . $appId . "'
							AND published = '1' LIMIT 1 ";
	$query = mysql_query( $query ) or exit( mysql_error() );
	if ( mysql_num_rows( $query ) > 0 ) {
		$tabId = mysql_result( $query, 0 );
		$tab = ars_getPageInfo( $tabId );
	}


?><!doctype html>
<html lang="en-US" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
<meta charset="UTF-8" />
<title><?php echo $tab['name']; ?></title>
<link rel="stylesheet" href="css/style.css?v=1.01" />
<script src="js/jquery.1.7.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/fb.js"></script>
<script src="js/both.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="https://widgets.twimg.com/j/2/widget.js"></script>
<script src="https://apis.google.com/js/plusone.js"></script>
<script src="https://maps.google.com/maps/api/js?sensor=false"></script>
<style>* { margin: 0; padding: 0; }</style>
<script>
$(window).load( function() {

	// DISPLAY WIDGETS
	displayWidgetsOnload( $('.pageBody') );

});
</script>
</head>



<body class="fb-view">

	<!-- include Javascript SDK -->
	<div id="fb-root"></div>
	<script>jsSDK();</script>


	<iframe id="ajaxResult" name="upload_target"></iframe>
	<img id="ajaxLoader" src="images/ajax-loader.gif" alt="" />


	<div id="includes">
		<div class="appId"><?php echo $appId; ?></div>
		<div class="pageId"><?php echo $page_id; ?></div>
		<div class="signedRequest"><?php echo $signed_request; ?></div>
		<div class="appLink"><?php echo $_SESSION['appLink']; ?></div>
	</div><!-- / .includes -->


	<form method="post" action="action.php" data-action="view-thank.php" enctype="multipart/form-data" target="upload_target">
		<?php echo $tab['content_contest_ugc_form']; ?>
		<input type="hidden" name="pageID" value="<?php echo $tab['id']; ?>" />
		<input type="hidden" name="act" value="contest_ugc_image" />
		<input type="hidden" name="save-ugc" value="1" />
		<input type="hidden" name="ugc_photo_path" />
		<input type="hidden" name="ugc_photo_name" />
		<input type="hidden" name="ugc_original_photo_name" />
		<?php foreach ( $_POST as $key => $value )
			echo '<input type="hidden" name="'.$key.'" value="'.$value.'" />'; ?>
	</form>


	<script>

		window.fbAsyncInit = function() {
			FB.init({
				appId  : $appId,
				status : true,
				cookie : true,
				xfbml  : true
			});
		};

		(function() {
			var e = document.createElement('script');
			e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?php echo $appId; ?>';
			e.async = true;
			document.getElementById('fb-root').appendChild(e);
		}());

	</script>

</body>
</html>
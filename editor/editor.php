<?php

	// includes
	require_once('config.php');
	require_once('functions.php');

	session_start();

	// if user unauthorized, go to the home page
	ars_is_user_authorized();

	// if there's no tab id, return to dashboard
	if ( ! isset( $_REQUEST['id'] ) ) header( 'Location: dashboard.php' );

	// if user attempts to edit another's page
	ars_is_it_my_page( $_REQUEST['id'], $_SESSION['user_info']['id'] );

	// get all info about the tab
	$tab = ars_getPageInfo( $_REQUEST['id'] );
	
	// Get all the widgets available for this company
	$widgets = ars_getWidgetsInfo( $_SESSION['companyid'] );
	/*echo "<pre>";
	print_r($widgets);
	echo "</pre>";*/

?><!doctype html>
<html lang="en-US" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
<meta charset="UTF-8" />
<title>Editor | <?php echo ars_get_page_name_by_id( $tab['fan_page_id'] ) . ' | ' . $tab['name']; ?></title>
<link rel="stylesheet" href="../style.css" />
<link rel="stylesheet" href="css/jquery-ui-1.8.16.css" />
<link rel="stylesheet" href="css/jquery.fancybox-1.3.4.css" />
<link rel="stylesheet" href="css/colorpicker.css" />
<link rel="stylesheet" href="plugins/cleditor/jquery.cleditor.css" />
<link rel="stylesheet" href="css/style.css" />
<script src="js/jquery.1.7.js"></script>
<script src="js/jquery-ui-1.8.16.js"></script>
<script src="js/jquery.timepicker.js"></script>
<script src="js/jquery.fancybox-1.3.4.pack.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/colorpicker.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="plugins/cleditor/jquery.cleditor.js"></script>
<script src="http://widgets.twimg.com/j/2/widget.js"></script>
<script src="https://apis.google.com/js/plusone.js">{parsetags: 'explicit'}</script>
<script src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script src="js/fb.js"></script>
<script src="js/date.js"></script>
<script src="js/both.js"></script>
<script src="js/site.js"></script>
<script>
$(function () {
	<?php // if page content is empty, load blank tab
	$blankTab = false;
	$contentArr = array(
		$tab['content_nonfans'],
		$tab['content_fans'],
		$tab['content_contest_fans'],
		$tab['content_contest_form'],
		$tab['content_contest_ugc_form'],
		$tab['content_contest_ugc_gallery'],
		$tab['content_contest_thank']
	);
	for ( $i = 0; $i < count($contentArr); $i++ ) if ( $contentArr[$i] == '' ) $blankTab = true;
	if ( $blankTab ) : ?> blankPage();
	<?php // if content exists, load it
	else : ?> ajaxLoadPage( <?php echo $tab['id']; ?> );
	<?php endif; ?>

});
</script>
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if IE 6]>
<script src="../js/belatedPNG.js"></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
</head>



<body class="normalpage editorPage">

<!-- include Javascript SDK -->
<div id="fb-root"></div>
<script>jsSDK();</script>

<iframe id="ajaxResult" name="upload_target"></iframe>
<img id="ajaxLoader" src="images/ajax-loader.gif" alt="" />
<div id="ajaxMessage"></div>
<div id="includes"></div>

<section id="page">
<div id="bodywrap">
<section id="top">
<nav>
	<h1 id="sitename"><a href="#">Portfolio | Premium CSS Template </a></h1>
  
	<ul id="sitenav">
		<? if (ars_is_user_superuser()) { ?>
    		<li><a href="../admin/">Admin</a></li>
    	<? } ?>
		<li><a href="../report/main.php">Report</a></li>
    	<li class="current"><a href="dashboard.php">Dashboard</a></li>
    	<li><a href="../help">Help</a></li>
    	<li><a href="http://platform.3tierlogic.com/tabbuilder/">Logout</a></li>
	</ul>
  
</nav>

<header id="normalheader">

	<div class="buttons">
		<a class="bigButton orange prev" href="dashboard.php">Return to Dashboard<span></span></a>
		<a href="" class="bigButton blue next createTab">Create a New Tab<span></span></a>
	</div><!-- / .buttons -->

	<div class="tabSteps">
		<ul class="<?php if ( $tab['contest'] == '0' ) echo 'noContest'; if ( $tab['ugc'] == '0' ) echo ' noUGC'; ?>">
			<li class="a1 active" data-switch="nonfans">
				<span class="number">1</span> Fan Gate
				<span class="arrow"></span></li>
			<li class="a2" data-switch="fans">
				<span class="number">2</span> Campaign
				<span class="arrow"></span></li>
			<li class="a3" data-switch="contest-fans">
				<span class="number">3</span> Splash
				<span class="arrow"></span></li>
			<li class="a4" data-switch="contest-form">
				<span class="number">4</span> Contest Form
				<span class="arrow"></span></li>
			<li class="a5" data-switch="contest-ugc-form">
				<span class="number">5</span> UGC Form
				<span class="arrow"></span></li>
			<li class="a6" data-switch="contest-ugc-gallery">
				<span class="number">6</span> UGC Gallery
				<span class="arrow"></span></li>
			<li class="a7" data-switch="contest-thank">
				<span class="number">7</span> Thanks
				<span class="arrow"></span></li>
			<li class="a8" data-switch="previewAndPublish">
				<span class="number">8</span> Publish
				<span class="arrow"></span></li>
		</ul>
	</div><!-- / .tabSteps -->

</header>

</section>

<section id="contentwrap">
<div id="contents" class="normalcontents">

	<?php require_once('create-tab.php'); ?>


	<div class="editorContent">

		<p class="editorTip">Select a widget, that you would like to include within your tab, then simply drag and drop it into the frame below.</p>


		<!-- this block changes its position to fixed, when page is scrolled down -->
		<div class="editorScrollingWidgets">
			<div class="in">

				<!-- widgets -->
				<div class="widgetsSlider">
					<div class="frame">
						<ul>
                        	<? if ($widgets['w_text'] == '1') { ?>
							<li data-cat="Basic Tools"><a href="" title="<b>Text Widget</b> - Add text to your tab with our Rich Text Editor. If you would like to include text, that is not included within the editor, we would recommend uploading an image of the text, through the <i>Image Widget</i>.">
								<span class="iconText"></span></a></li>
							<? } ?>
                            
                            <? if ($widgets['w_link'] == '1') { ?>
							<li data-cat="Basic Tools"><a href="" title="<b>Link Widget</b> - Create hot spots of clickable sections anywhere within your tab, linking to an external link.">
								<span class="iconHotspot"></span></a></li>
							<? } ?>
                            
                            <? if ($widgets['w_countdown'] == '1') { ?>
							<li data-cat="Basic Tools"><a href="" title="<b>Countdown Widget</b> - Highlight an important date and allow for the days, hours, minutes and seconds to be shown to build up hype on your tab.">
								<span class="iconCountdown"></span></a></li>
							<? } ?>
                            
                            <? if ($widgets['w_contact_form'] == '1') { ?>
							<li data-cat="Basic Tools"><a href="" title="<b>Contact Form Widget</b> - Collect contact info, such as Name, Phone Number, Email Address, through a very basic form from your users.">
								<span class="iconContact"></span></a></li>
							<? } ?>
                            
                            <? if ($widgets['w_email'] == '1') { ?>
							<li data-cat="Basic Tools"><a href="" title="<b>Email Widget</b> - Allow your users to send an email your way through a click of a button.">
								<span class="iconEmail"></span></a></li>
							<? } ?>
                            
                            <? if ($widgets['w_like'] == '1') { ?>
							<li data-cat="Facebook Tools"><a href="" title="<b>Like Widget</b> - Incorporate a Facebook Like button to any URL of your choice.">
								<span class="iconLike"></span></a></li>
							<? } ?>
                            
                            <? if ($widgets['w_comment'] == '1') { ?>
							<li data-cat="Facebook Tools"><a href="" title="<b>Comment Widget</b> - Allows you to encourage fan engagement by including a Facebook Comment Box.">
								<span class="iconComment"></span></a></li>
							<? } ?>
                            
                            <? if ($widgets['w_share'] == '1') { ?>
							<li data-cat="Facebook Tools"><a href="" title="<b>Share Widget</b> - Create an opportunity for your users to share content within their Facebook timeline. This post can be customized with a picture, a link and a description of where the post will link to.">
								<span class="iconFacebookPostToFeed"></span></a></li>
							<? } ?>
                            
                            <? if ($widgets['w_send'] == '1') { ?>
							<li data-cat="Facebook Tools"><a href="" title="<b>Send Widget</b> - Allow your users to send a link to their friends' Facebook Message Inbox.">
								<span class="iconFacebookSend"></span></a></li>
							<? } ?>
                            
                            <? if ($widgets['w_invitefriend'] == '1') { ?>
							<li data-cat="Facebook Tools"><a href="" title="<b>Invite Button Widget</b> - Facebook send application request.">
								<span class="iconFacebookInvite"></span></a></li>
							<? } ?>
                            
                            <? if ($widgets['w_twitter_stream'] == '1') { ?>
							<li data-cat="Social Integration"><a href="" title="<b>Twitter Stream Widget</b> - Display the latest tweets from a specific Twitter account.">
								<span class="iconTwitter"></span></a></li>
                            <? } ?>
	
    						<? if ($widgets['w_sharetwitter'] == '1') { ?>
							<li data-cat="Social Integration"><a href="" title="<b>Share on Twitter Widget</b>">
								<span class="iconTwitterShare"></span></a></li>
                            <? } ?>

							<? if ($widgets['w_pinterestpinit'] == '1') { ?>
							<li data-cat="Social Integration"><a href="" title="<b>Pinterest Pin It Widget</b>">
								<span class="iconPinterestPinIt"></span></a></li>
							<? } ?>
                            
                            <? if ($widgets['w_pinterestfollow'] == '1') { ?>
							<li data-cat="Social Integration"><a href="" title="<b>Pinterest Follow Widget</b>">
								<span class="iconPinterestFollow"></span></a></li>
                            <? } ?>

							<? if ($widgets['w_google_plus'] == '1') { ?>
							<li data-cat="Social Integration"><a href="" title="<b>Google +1 Widget</b>">
								<span class="iconGoogle1"></span></a></li>
							<? } ?>
                            
                            <? if ($widgets['w_rss'] == '1') { ?>
							<li data-cat="Social Integration"><a href="" title="<b>RSS Widget</b> - Show content from your RSS feed within your tab.">
								<span class="iconRSS"></span></a></li>
                            <? } ?>

							<? if ($widgets['w_google_maps'] == '1') { ?>
							<li data-cat="Social Integration"><a href="" title="<b>Google Maps Widget</b> - Include a Google map of an address of your choice.">
								<span class="iconGoogleMaps"></span></a></li>
							 <? } ?>

							<? if ($widgets['w_youtube'] == '1') { ?>
							<li data-cat="Interactive Media"><a href="" title="<b>YouTube Widget</b> - Integrate a YouTube video.">
								<span class="iconYoutube"></span></a></li>
							<? } ?>

							<? if ($widgets['w_vimeo'] == '1') { ?>
							<li data-cat="Interactive Media"><a href="" title="<b>Vimeo Widget</b> - Integrate a Vimeo video.">
								<span class="iconVimeo"></span></a></li>
							<? } ?>

							<? if ($widgets['w_flickr'] == '1') { ?>
							<li data-cat="Interactive Media"><a href="" title="<b>Flickr Widget</b> - Integrate a set of pictures from your Flickr account.">
								<span class="iconFlickr"></span></a></li>
							<? } ?>

							<? if ($widgets['w_image'] == '1') { ?>
							<li data-cat="Interactive Media"><a href="" title="<b>Image Widget</b> - Include an image within your tab. You can also make this image clickable to link to an external site, or using the <i>Link Widget</i>, you can make multiple parts of the image clickable.">
								<span class="iconImage"></span></a></li>
							<? } ?>

							<? if ($widgets['w_imageslider'] == '1') { ?>
							<li data-cat="Interactive Media"><a href="" title="<b>Photo/Video Gallery Widget</b> - Allows you to build a slider with images and/or videos.">
								<span class="iconImagesSlider"></span></a></li>
							<? } ?>

							<? if ($widgets['w_html'] == '1') { ?>
							<li data-cat="Advanced Tools"><a href="" title="<b>HTML Widget</b> - Add HTML directly.">
								<span class="iconHtml"></span></a></li>
							<? } ?>

							<? if ($widgets['w_iframe'] == '1') { ?>
							<li data-cat="Advanced Tools"><a href="" title="<b>iFrame Widget</b> - Allows you to display an external website within your tab.">
								<span class="iconIframe"></span></a></li>
							<? } ?>

							<? if ($widgets['w_contest'] == '1') { ?>
							<li data-cat="Contest Features" class="contest_text_input"><a href="" title="<b>Text Input Widget</b>">
								<span class="iconContestText"></span></a></li>

							<li data-cat="Contest Features" class="contest_textarea"><a href="" title="<b>Text Area Widget</b>">
								<span class="iconContestTextarea"></span></a></li>

							<li data-cat="Contest Features"><a href="" title="<b>Dropdown Widget</b>">
								<span class="iconContestSelect"></span></a></li>

							<li data-cat="Contest Features"><a href="" title="<b>Radio Buttons Widget</b>">
								<span class="iconContestRadio"></span></a></li>

							<li data-cat="Contest Features"><a href="" title="<b>Checkbox Widget</b>">
								<span class="iconContestCheckbox"></span></a></li>

							<li data-cat="Contest Features" class="contest_button"><a href="" title="<b>Contest Button Widget</b> - Allow your users to create different types of contest buttons.">
								<span class="iconContestBtn"></span></a></li>
							<? } ?>
                            
                            <? if ($widgets['w_ugc'] == '1') { ?>
							<li data-cat="Contest Features" class="ugc_upload_area"><a href="" title="<b>UGC Upload Area Widget</b>">
								<span class="iconContestUGCUploadArea"></span></a></li>

							<li data-cat="Contest Features" class="ugc_gallery"><a href="" title="<b>UGC Gallery Widget</b>">
								<span class="iconContestUGCGallery"></span></a></li>
                            <? } ?>
						</ul>
					</div><!-- / .frame -->
				</div><!-- / .widgetsSlider -->


				<div class="filterWidgetsBy">
					<em>Filter By:</em>
				</div>

			</div><!-- / .in -->
		</div><!-- / .editorScrollingWidgets -->


		<div class="editorButtons">
			<a class="bigButton orange next" data-step="next">Next<span></span></a>
			<a class="bigButton blue next save" href="<?php echo $tab['id']; ?>">Save Tab<span></span></a>
			<a class="bigButton blue next publish" href="<?php echo $tab['id']; ?>"
				data-published="<?php echo $tab['published']; ?>"><i><?php
				if ( $tab['published'] == '0' ) echo 'Publish'; else echo 'Unpublish'; ?></i><span></span></a>
		</div><!-- / .editorButtons -->


		<!-- facebbok area -->
		<div class="fbArea">
			<div class="header"></div>

			<!-- information about tab and content switch -->
			<div class="panel">
				<div class="tabName">
					<?php echo shortName( ars_get_page_name_by_id( $tab['fan_page_id'] ) ) . ' | ' . shortName( $tab['name'] ); ?><span></span>
				</div>

				<div class="contentTypeSwitch forPreview">
					<a class="nonfans">Fan Gate<span></span></a>
					<a class="fans">Campaign<span></span></a>
					<a class="contest-fans">Splash<span></span></a>
					<a class="contest-form">Contest Form<span></span></a>
					<a class="contest-ugc-form">UGC Form<span></span></a>
					<a class="contest-ugc-gallery">UGC Gallery<span></span></a>
					<a class="contest-thank">Thanks<span></span></a>
				</div>
			</div><!-- / .panel -->


			<!-- content of a tab -->
			<div class="contentInner">

				<div class="editorInstruments">
					<a href="" class="contest <?php if ( $tab['contest'] != '0' ) echo 'disable'; ?>" title="Contest Tool"></a>
					<span class="divider"></span>
					<span class="contestButtons <?php if ( $tab['contest'] == '0' ) echo 'hidden'; ?>">
						<a href="" class="ugc <?php if ( $tab['ugc'] != '0' ) echo 'disable'; ?>" title="UGC Tool"></a>
						<span class="divider"></span>
						<a href="#popup_contest_properties" class="contestProperties" title="Contest Properties"></a>
						<span class="divider"></span>
						<a href="#popup_contest_form_design" class="contestFormDesign" title="Contest Form Design"></a>
						<span class="divider"></span>
					</span>
					<a href="#popup_pageHeight" class="height" title="Page Height"></a>
					<span class="divider"></span>
					<a href="#popup_backgroundSettings" class="background" title="Background Settings"></a>
					<span class="divider"></span>
					<a href="#popup_analytics" class="analytics" title="Google Analytics"></a>
					<span class="divider"></span>
					<a href="" class="ruler" title="Ruler"></a>
					<span class="divider"></span>
					<a href="#popup_gridSettings" class="grid" title="Grid View"></a>
					<span class="divider"></span>
					<a href="" class="clear" title="Clear Content"></a>
				</div><!-- / .editorInstruments -->

				<!-- block for creating pages -->
				<div class="pageBodyOuter">
					<div class="pageBodyInner"><!-- created elements will be appended here --></div>
					<div class="pageBodyPreview"><!-- preview is loaded here --></div>
				</div><!-- / .pageBodyOuter -->

				<div class="editorButtons">
					<a class="bigButton orange next" data-step="next">Next<span></span></a>
					<a class="bigButton blue next save" href="<?php echo $tab['id']; ?>">Save Tab<span></span></a>
					<a class="bigButton blue next publish" href="<?php echo $tab['id']; ?>"
						data-published="<?php echo $tab['published']; ?>"><i><?php
						if ( $tab['published'] == '0' ) echo 'Publish'; else echo 'Unpublish'; ?></i><span></span></a>
				</div><!-- / .editorButtons -->

			</div><!-- / .contentInner -->

		</div><!-- / .fbArea -->


	</div><!-- / .editorContent -->

</div><!--  / #contents -->
</section><!-- / #contentwrap -->
</div>



<footer id="pagefooter">
<div id="bottom">
<div class="block1">
<h2>Meet The Team</h2>

<div class="teamimg"><? include("team.php")?></div>

</div>
<div class="block2">
<h2>Latest Project</h2>
<div class="projectthmb"><? include("latestproject.php")?></div>

</div>
<div class="block3">

<h2>More About</h2>
<p>3 Tier Logic is a full service digital marketing and technology services company that enables organizations to grow their customer and prospect communities through anywhere-anytime communications. </p>

</div>
<div class="clear"></div>
</div>
<div id="credits">
<p>
<? include("copyright.html")?>
</p>
</div>
</footer>

</section>


<!-- POPUP WINDOWS -->
<div class="hiddenForms">


	<!-- contest properties -->
	<div id="popup_contest_properties" class="unrelatedToWidgets">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Contest Properties</div>
				<div class="inner">

					<div class="field">
						<span>1. Description:</span>
						<input type="text" name="contestDescr" maxlength="300" />
					</div><!-- / .field -->

					<div class="field">
						<span>2. Start Date:</span>
						<input type="text" name="contestStart" class="datePicker required" maxlength="10" />
					</div><!-- / .field -->

					<div class="field">
						<span>3. End Date:</span>
						<input type="text" name="contestEnd" class="datePicker required" maxlength="10" />
					</div><!-- / .field -->

					<div class="field">
						<span>4. Voting Start Date (optional):</span>
						<input type="text" name="contestVotingStart" class="datePicker" maxlength="10" />
					</div><!-- / .field -->

					<div class="field">
						<span>5. Voting End Date (optional):</span>
						<input type="text" name="contestVotingEnd" class="datePicker" maxlength="10" />
					</div><!-- / .field -->

					<div class="field">
						<span>6. Number of times a user can vote (only applicable for UGC contests):</span>
						<label><input type="radio" name="contestVotingTimes" value="-1" checked="checked" />
							Unlimited</label>
						<label><input type="radio" name="contestVotingTimes" value="0" />
							Once for the entire duration of the campaign</label>
						<label><input type="radio" name="contestVotingTimes" value="1" />
							Or enter the number of votes <b>per day</b></label>
						<input type="text" name="contestVotingPerDay" class="required digits" maxlength="3" disabled="disabled" />
					</div><!-- / .field -->

					<div class="field">
						<span>7. Number of user entries:</span>
						<input type="text" name="contestEntries" maxlength="1" />
					</div><!-- / .field -->

					<div class="field" style="display: none;">
						<span>8. Age restriction (people over 18 only):</span>
						<input type="checkbox" name="contestAge" />
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_contest_properties -->



	<!-- contest form design -->
	<div id="popup_contest_form_design" class="unrelatedToWidgets">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Contest Form Design</div>
				<div class="inner">

					<table cellspacing="0">
						<tr>
							<th>&nbsp;</th>
							<th>Width</th>
							<th>Bold</th>
							<th>Italic</th>
							<th>Text Align</th>
							<th>Font Size</th>
							<th>Font Color</th>
							<th>Background Color</th>
							<th>Border Color</th>
							<th>Border Size</th>
						</tr>
						<tr>
							<td>Label</td>
							<td><input id="contestLabelWidth" type="text" /></td>
							<td><input id="contestLabelBold" type="checkbox" /></td>
							<td><input id="contestLabelItalic" type="checkbox" /></td>
							<td><select id="contestLabelAlign"></select></td>
							<td><select id="contestLabelFontSize"></select></td>
							<td><span class="uniPicker" id="contestLabelColor"></span></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>Input</td>
							<td><input id="contestInputWidth" type="text" /></td>
							<td><input id="contestInputBold" type="checkbox" /></td>
							<td><input id="contestInputItalic" type="checkbox" /></td>
							<td><select id="contestInputAlign"></select></td>
							<td><select id="contestInputFontSize"></select></td>
							<td><span class="uniPicker" id="contestInputColor"></span></td>
							<td><span class="uniPicker" id="contestBackground"></span></td>
							<td><span class="uniPicker" id="contestBorderColor"></span></td>
							<td><select id="contestInputBorderSize"></select></td>
						</tr>
					</table>

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_contest_form_design -->



	<!-- page background settings -->
	<div id="popup_backgroundSettings" class="unrelatedToWidgets">
		<form method="post" action="action.php" enctype="multipart/form-data" target="upload_target">
			<div class="form">
			  <div class="headline">Background Settings</div>
				<div class="inner">

					<div class="field">
						<span>Select a background color:</span>
						<span class="uniPicker bgPicker"></span>
					</div><!-- / .field -->

					<div class="field">
						<span>Please provide the URL of the background image you would like to apply:</span>
						<input type="text" name="bgURL" class="url" maxlength="300" value="" />
					</div><!-- / .field -->

					<div class="field">
						<span>OR select an image to upload:</span>
						<input type="file" name="bgImage" class="uploadFile" />
					</div><!-- / .field -->

					<div class="field">
						<span>Select the repeat pattern of your background image:</span>
						<select id="bgRepeat">
							<option value="no-repeat" selected="selected">Single Image</option>
							<option value="repeat">Repeat</option>
							<option value="repeat-x">Repeat Horizontal</option>
							<option value="repeat-y">Repeat Vertical</option>
						</select>
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<input type="hidden" name="pageID" value="<?php echo $tab['id']; ?>" />
					<input type="hidden" name="bgToUpdate" value="" />
					<input type="hidden" name="act" value="save_bg" />
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
			  </div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_backgroundSettings -->



	<!-- google analytics -->
	<div id="popup_analytics" class="unrelatedToWidgets">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Google Analytics</div>
				<div class="inner">

					<div class="field">
						<span>UA Code:</span>
						<input type="text" name="googleUAcode" class="required" maxlength="20" value="" />
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_analytics -->



	<!-- page height -->
	<div id="popup_pageHeight" class="unrelatedToWidgets">
		<form method="post" action="">
			<div class="form">
			  <div class="headline">Page Height</div>
				<div class="inner">

					<div class="field">
						<span>Enter the page height:</span>
						<!-- value defines #pageBody height when loading a blank page -->
						<input type="text" name="pageHeight" class="integer required" maxlength="4" value="500" />
						<a class="aButton blue plus">Increase</a>
						<a class="aButton blue minus">Decrease</a>
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done">Done</a>
					<a class="aButton cancel">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_pageHeight -->



	<!-- grid settings -->
	<div id="popup_gridSettings" class="unrelatedToWidgets">
		<form method="post" action="">
			<div class="form">
			  <div class="headline">Grid View</div>
				<div class="inner">

					<div class="field">
						<span>1. Please enter the size of the grid to help with placement of your widgets:</span>
						<input type="text" name="gridSize" maxlength="3" />
					</div><!-- / .field -->

					<div class="field">
						<span>2. Enable Grid View:</span>
						<input type="checkbox" id="gridVisible" />
					</div><!-- / .field -->

					<div class="field">
						<span>3. Enable Snap to Grid:</span>
						<input type="checkbox" id="gridSnap" />
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done">Done</a>
					<a class="aButton cancel">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_gridSettings -->



	<!-- iframe -->
	<a href="#popup_iconIframe"></a>
	<div id="popup_iconIframe">
		<form method="post" action="">
			<div class="form">
				<div class="headline">iFrame</div>
				<div class="inner">

					<div class="field">
						<span>1. iFrame URL:</span>
						<input type="text" name="iFrameURL" class="required url" maxlength="300" value="" />
					</div><!-- / .field -->

					<div class="field">
						<span>2. Display Scroll Bars:</span>
						<input type="checkbox" id="iFrameScrolling" />
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconIframe -->



	<!-- youtube -->
	<a href="#popup_iconYoutube"></a>
	<div id="popup_iconYoutube">
		<form method="post" action="">
			<div class="form">
				<div class="headline">YouTube:</div>
				<div class="inner">

					<div class="field">
						<span>1. Please provide the URL of the YouTube video you would like to include:</span>
						<input type="text" name="youtubeURL" class="required url" maxlength="300" value="" />
					</div><!-- / .field -->

				</div><!-- / .inner -->
			  <div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconYoutube -->



	<!-- vimeo -->
	<a href="#popup_iconVimeo"></a>
	<div id="popup_iconVimeo">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Vimeo</div>
				<div class="inner">

					<div class="field">
						<span>1. Please provide the URL of the Vimeo video you would like to include:</span>
						<input type="text" name="vimeoURL" class="required url" maxlength="300" value="" />
					</div><!-- / .field -->

				</div><!-- / .inner -->
			  <div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconVimeo -->



	<!-- image -->
	<a href="#popup_iconImage"></a>
	<div id="popup_iconImage">
		<form method="post" action="action.php" enctype="multipart/form-data" target="upload_target">
			<div class="form">
				<div class="headline">Image</div>
				<div class="inner">

					<div class="field">
						<span>1. Please provide the URL of the image you would like to include:</span>
						<input type="text" name="imageURL" class="url" maxlength="300" value="" />
					</div><!-- / .field -->

					<div class="field">
						<span>Or select an image to upload:</span>
						<input type="file" name="contentImage" class="uploadFile" />
					</div><!-- / .field -->

					<div class="field">
						<span>Add a hyperlink to your image by providing the URL (optional):</span>
						<input type="text" name="imageLinkURL" class="url" maxlength="300" value="" />
					</div><!-- / .field -->

				</div><!-- / .inner -->
			  <div class="footline">
					<input type="hidden" name="pageID" value="<?php echo $tab['id']; ?>" />
					<input type="hidden" name="imageToUpdate" value="" />
					<input type="hidden" name="act" value="content_image" />
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconImage -->



	<!-- slider (gallery) widget -->
	<a href="#popup_iconCoolSlider"></a>
	<div id="popup_iconCoolSlider">
		<form method="post" action="action.php" enctype="multipart/form-data" target="upload_target">
			<div class="form">
				<div class="headline">Photo/Video Gallery</div>
				<div class="inner">

					<div class="field">
						<span>1. Please choose a slider background color:</span>
						<span class="uniPicker coolSliderBgColor"></span>
					</div><!-- / .field -->

					<div class="field">
						<span>2. Please choose a slider border color:</span>
						<span class="uniPicker coolSliderBorderColor"></span>
					</div><!-- / .field -->

					<div class="field">
						<span>3. Please provide the width of a slide (px):</span>
						<input type="text" name="coolSliderWidth" class="required" maxlength="3" />
					</div><!-- / .field -->

					<div class="field">
						<span>4. Please provide the height of a slide (px):</span>
						<input type="text" name="coolSliderHeight" class="required" maxlength="3" />
					</div><!-- / .field -->

					<div class="field">
						<span>5. Please provide the number of visible slides:</span>
						<input type="text" name="coolSliderVisible" class="required" maxlength="3" />
					</div><!-- / .field -->

					<div class="field">
						<span>6. Please provide the URL of an image or a video (YouTube, Vimeo) you want to include:</span>
						<input type="text" name="coolSliderURL" class="url" maxlength="300" />
						<a class="aButton blue coolSliderAddItem" href="">Add</a>
					</div><!-- / .field -->

					<div class="field">
						<span>Or select an image to upload:</span>
						<input type="file" name="coolSliderImage" class="uploadFile" />
						<a class="aButton blue coolSliderUploadImage" href="">Upload</a>
					</div><!-- / .field -->

					<div class="field">
						<span>7. Change the order of slides if needed:</span>
						<div class="coolSliderPreview">
							<ul></ul>
						</div><!-- / .coolSliderPreview -->
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<input type="hidden" name="pageID" value="<?php echo $tab['id']; ?>" />
					<input type="hidden" name="act" value="content_coolslider_image" />
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconCoolSlider -->



	<!-- hotspot -->
	<a href="#popup_iconHotspot"></a>
	<div id="popup_iconHotspot">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Link to</div>
				<div class="inner">

					<div class="field">
						<span>1. Please provide the URL that you would like to include a link for:</span>
						<input type="text" name="hotspotURL" class="required url" maxlength="300" value="" />
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconHotspot -->



	<!-- html -->
	<a href="#popup_iconHtml"></a>
	<div id="popup_iconHtml">
		<form method="post" action="">
			<div class="form">
				<div class="headline">HTML</div>
				<div class="inner">

					<div class="field">
						<span>1. Please provide the HTML code you would like to include:</span>
						<textarea cols="70" rows="20" name="html" class="required html"></textarea>
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconHtml -->



	<!-- text -->
	<a href="#popup_iconText"></a>
	<div id="popup_iconText">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Text</div>
				<div class="inner">

					<div class="field">
						<textarea cols="40" rows="10" name="text" maxlength="1500"></textarea>
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconText -->



	<!-- FB like -->
	<a href="#popup_iconLike"></a>
	<div id="popup_iconLike">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Facebook Like Button</div>
				<div class="inner">

					<div class="field">
						<span>1. Please provide the URL that you would like to users to &quot;Like&quot;:</span>
						<input type="text" name="likeURL" class="required url" maxlength="300" value="" />
					</div><!-- / .field -->

				</div><!-- / .inner -->
			  <div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconLike -->



	<!-- FB send -->
	<a href="#popup_iconFacebookSend"></a>
	<div id="popup_iconFacebookSend">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Facebook Send Button</div>
				<div class="inner">

					<div class="field">
						<span>1. Please provide the URL that you would like to users to &quot;Send&quot; to their friends:</span>
						<input type="text" name="sendURL" class="required url" maxlength="300" value="" />
					</div><!-- / .field -->

				</div><!-- / .inner -->
			  <div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconFacebookSend -->



	<!-- FB comment -->
	<a href="#popup_iconComment"></a>
	<div id="popup_iconComment">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Facebook Comment Stream</div>
				<div class="inner">

					<div class="field">
						<span>1. Please provide the URL to your Facebook fan page. If the user chooses to &quot;Post to Facebook&quot;, a story appears on their friends' News Feed, indicating, that they've made a comment on your website, which will also link back to your site.</span>
						<input type="text" name="commentsURL" class="required url" maxlength="300" value="" />
					</div><!-- / .field -->

					<div class="field">
						<span>2. Select the maximum number of Posts you would like to display below the comment box:</span>
						<select name="commentsNumber">
							<option value="1" selected="selected">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
						</select>
					</div><!-- / .field -->

				</div><!-- / .inner -->
			  <div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconComment -->



	<!-- FB invite -->
	<a href="#popup_iconFacebookInvite"></a>
	<div id="popup_iconFacebookInvite">
		<form method="post" action="action.php" enctype="multipart/form-data" target="upload_target">
			<div class="form">
			  <div class="headline">Invite Button:</div>
				<div class="inner">

					<div class="field">
						<span>1. Enter a title:</span>
						<input type="text" name="inviteTitle" class="required" maxlength="100" value="" />
					</div><!-- / .field -->

					<div class="field">
						<span>2. Enter an invite message to your friend(s):</span>
						<textarea cols="40" rows="2" name="inviteMessage" class="required" maxlength="500"></textarea>
					</div><!-- / .field -->

					<div class="field">
						<span>3. Please provide the URL of the image of the button you would like to include:</span>
						<input type="text" name="inviteBtnURL" class="url" maxlength="300" />
					</div><!-- / .field -->

					<div class="field">
						<span>OR select an image to upload:</span>
						<input type="file" name="inviteBtnImage" class="uploadFile" />
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<input type="hidden" name="pageID" value="<?php echo $tab['id']; ?>" />
					<input type="hidden" name="inviteToUpdate" />
					<input type="hidden" name="act" value="content_invite" />
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconFacebookInvite -->



	<!-- FB post to feed -->
	<a href="#popup_iconFacebookPostToFeed"></a>
	<div id="popup_iconFacebookPostToFeed">
		<form method="post" action="action.php" enctype="multipart/form-data" target="upload_target">
			<div class="form">
				<div class="headline">Share on Facebook</div>
				<div class="inner">

					<div class="field">
						<span>1. Please provide the URL that will be linked to the post of Facebook:</span>
						<label><input type="radio" name="postFeedLinkSwitch"
							value="0" checked="checked" />App url</label>
						<label><input type="radio" name="postFeedLinkSwitch"
							value="1" />Custom url</label>
						<input type="text" name="postFeedLink" class="required url" maxlength="300" disabled="disabled" />
					</div><!-- / .field -->

					<div class="field">
						<span>2. Enter the Post Title:</span>
						<input type="text" name="postFeedName" class="required" maxlength="300" />
					</div><!-- / .field -->

					<div class="field">
						<span>3. Enter the Caption Name:</span>
						<input type="text" name="postFeedCaption" class="required" maxlength="300" />
					</div><!-- / .field -->

					<div class="field">
						<span>4. Enter the Post Description:</span>
						<textarea cols="40" rows="2" name="postFeedDescr" class="required" maxlength="500"></textarea>
					</div><!-- / .field -->

					<div class="field">
						<span>5. Please provide the URL of the image you would like to include within your post:</span>
						<input type="text" name="postFeedImgURL" class="url" maxlength="300" />
					</div><!-- / .field -->

					<div class="field">
						<span>OR select an image to upload:</span>
						<input type="file" name="postFeedImgImage" class="uploadFile" />
					</div><!-- / .field -->

					<div class="field">
						<span>6. Please provide the URL of the image of the button you would like to include:</span>
						<input type="text" name="postFeedBtnURL" class="url" maxlength="300" />
					</div><!-- / .field -->

					<div class="field">
						<span>OR select an image to upload:</span>
						<input type="file" name="postFeedBtnImage" class="uploadFile" />
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<input type="hidden" name="pageID" value="<?php echo $tab['id']; ?>" />
					<input type="hidden" name="postFeedImgToUpdate" value="" />
					<input type="hidden" name="postFeedBtnToUpdate" value="" />
					<input type="hidden" name="act" value="content_fb_post_feed" />
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconFacebookPostToFeed -->



	<!-- google plus -->
	<a href="#popup_iconGoogle1"></a>
	<div id="popup_iconGoogle1">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Google Plus</div>
				<div class="inner">

					<div class="field">
						<span>1. Please provide the URL that you would like to recommend to your circles and drive traffic to:</span>
						<input type="text" name="google1URL" class="required url" maxlength="300" value="" />
					</div><!-- / .field -->

				</div><!-- / .inner -->
			  <div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconGoogle1 -->



	<!-- twitter stream -->
	<a href="#popup_iconTwitter"></a>
	<div id="popup_iconTwitter">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Twitter</div>
				<div class="inner">

					<div class="field">
						<span>1. Please provide Twitter Username that you would like to stream Tweets from:</span>
						<input type="text" name="twitterName" class="required" maxlength="50" value="" />
					</div><!-- / .field -->

					<div class="field">
						<span>2. Display Properties:</span>
						Links: <span class="uniPicker twitter link"></span>
						Tweet Text Color: <span class="uniPicker twitter text"></span>
					</div><!-- / .field -->

					<div class="field">
						<span>3. Number of Tweets to display:</span>
						<select name="tweetsNumber">
							<option value="1" selected="selected">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
						</select>
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconTwitter -->



	<!-- twitter share -->
	<a href="#popup_iconTwitterShare"></a>
	<div id="popup_iconTwitterShare">
		<form method="post" action="action.php" enctype="multipart/form-data" target="upload_target">
			<div class="form">
				<div class="headline">Share on Twitter</div>
				<div class="inner">

					<div class="field">
						<span>1. Please provide a url that you would like to share:</span>
						<label><input type="radio" name="twitterShareURLSwitch"
							value="0" checked="checked" />App url</label>
						<label><input type="radio" name="twitterShareURLSwitch"
							value="1" />Custom url</label>
						<input type="text" name="twitterShareURL" class="required url" maxlength="300" disabled="disabled" />
					</div><!-- / .field -->

					<div class="field">
						<span>2. Please provide a message that you would like to share:</span>
						<textarea cols="40" rows="2" name="twitterShareText" class="required" maxlength="500"></textarea>
					</div><!-- / .field -->

					<div class="field">
						<span>3. Please provide your Twitter nickname:</span>
						<input type="text" name="twitterShareAccount" class="required" maxlength="100" />
					</div><!-- / .field -->

					<div class="field">
						<span>4. Please provide the URL of the image of the button you would like to include:</span>
						<input type="text" name="twitterShareButtonURL" class="url" maxlength="300" />
					</div><!-- / .field -->

					<div class="field">
						<span>OR select an image to upload:</span>
						<input type="file" name="twitterShareButtonImage" class="uploadFile" />
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<input type="hidden" name="pageID" value="<?php echo $tab['id']; ?>" />
					<input type="hidden" name="twitterShareToUpdate" />
					<input type="hidden" name="act" value="content_twitter_share" />
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconTwitterShare -->



	<!-- pinterest pin it -->
	<a href="#popup_iconPinterestPinIt"></a>
	<div id="popup_iconPinterestPinIt">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Pinterest Pin It</div>
				<div class="inner">

					<div class="field">
						<span>1. Please provide the URL of the page to pin:</span>
						<input type="text" name="pinItPageURL" class="required url" maxlength="300" value="" />
					</div><!-- / .field -->

					<div class="field">
						<span>2. Please provide the URL of the image to pin:</span>
						<input type="text" name="pinItImageURL" class="required url" maxlength="300" value="" />
					</div><!-- / .field -->

					<div class="field">
						<span>3. Please provide the description of the page to be pinned:</span>
						<textarea cols="70" rows="3" name="pinItDescr"></textarea>
					</div><!-- / .field -->

					<div class="field">
						<span>4. Pease choose pin count location:</span>
						<select name="pinItCount">
							<option value="horizontal" selected="selected">Horizontal</option>
							<option value="vertical">Vertical</option>
							<option value="none">No Count</option>
						</select>
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconPinterestPinIt -->



	<!-- pinterest follow -->
	<a href="#popup_iconPinterestFollow"></a>
	<div id="popup_iconPinterestFollow">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Pinterest Follow</div>
				<div class="inner">

					<div class="field">
						<span>1. Please provide your Pinterest account name:</span>
						<input type="text" name="pinFollowUser" class="required" maxlength="30" />
					</div><!-- / .field -->

					<div class="field">
						<span>2. Pease choose button type:</span>
						<select name="pinFollowBtnType">
							<option value="follow-me-on-pinterest" selected="selected">Follow me on Pinterest</option>
							<option value="pinterest">Pinterest</option>
							<option value="big-p">Big logo</option>
							<option value="small-p">Small logo</option>
						</select>
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconPinterestFollow -->



	<!-- rss -->
	<a href="#popup_iconRSS"></a>
	<div id="popup_iconRSS">
		<form method="post" action="">
			<div class="form">
				<div class="headline">RSS Feed</div>
				<div class="inner">

					<div class="field">
						<span>1. Please provide the RSS Feed link you would like to stream posts from:</span>
						<input type="text" name="rssURL" class="required url" maxlength="300" value="" />
					</div><!-- / .field -->

					<div class="field">
						<span>2. Display Properties:</span>
						Links: <span class="uniPicker rss link"></span>
						Post Text Color: <span class="uniPicker rss text"></span>
					</div><!-- / .field -->

					<div class="field">
						<span>3. Number of Posts to display:</span>
						<select name="rssNumber">
							<option value="1" selected="selected">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
						</select>
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconRSS -->



	<!-- google maps -->
	<a href="#popup_iconGoogleMaps"></a>
	<div id="popup_iconGoogleMaps">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Google Maps</div>
				<div class="inner">

					<div class="field">
						<span>1. Please provide the full address you would like to display on the map:</span>
						<input type="text" name="gmAddress" class="required" maxlength="300" value="" />
					</div><!-- / .field -->

					<div class="field">
						<span>2. Zoom level:</span>
						<input type="text" name="gmZoom" class="small" maxlength="3" value="" /> %
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconGoogleMaps -->



	<!-- countdown -->
	<a href="#popup_iconCountdown"></a>
	<div id="popup_iconCountdown">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Countdown Tool</div>
				<div class="inner">

					<div class="field">
						<span>1. Please select date that you would like to countdown to:</span>
						<input type="text" name="countdownDate" class="required dateTimePicker" maxlength="16" value="" />
					</div><!-- / .field -->

					<div class="field">
						<span>2. Display Properties:</span>
						Text Color: <span class="uniPicker countdown text"></span>
					</div><!-- / .field -->

					<div class="field">
						<span>3. Text Size:</span>
						<input type="text" name="countdownSize" class="small" maxlength="2" value="" /> px
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconCountdown -->



	<!-- contact form -->
	<a href="#popup_iconContact"></a>
	<div id="popup_iconContact">
		<form method="post" action="action.php" enctype="multipart/form-data" target="upload_target">
			<div class="form">
				<div class="headline">Contact Form</div>
				<div class="inner">

					<div class="field">
						<span>1. Please provide the email address that you would like to send the submitted data to:</span>
						<input type="text" name="contactAddress" class="email required" maxlength="300" value="" />
					</div><!-- / .field -->

					<div class="field">
						<span>2. Select the data that you would like to capture within the Contact Form:</span>
						<input type="checkbox" id="contactName" />Name
						<input type="checkbox" id="contactPhone" />Phone
					</div><!-- / .field -->

					<div class="field">
						<span>3. Please provide the URL of the image of the button you would like to include:</span>
						<input type="text" name="contactButtonURL" class="url" maxlength="300" value="" />
					</div><!-- / .field -->

					<div class="field">
						<span>OR select an image to upload:</span>
						<input type="file" name="contactButtonImage" class="uploadFile" />
					</div><!-- / .field -->

				</div><!-- / .inner -->
			  <div class="footline">
					<input type="hidden" name="pageID" value="<?php echo $tab['id']; ?>" />
					<input type="hidden" name="contactButtonToUpdate" value="" />
					<input type="hidden" name="act" value="content_contact_form" />
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconContact -->



	<!-- email -->
	<a href="#popup_iconEmail"></a>
	<div id="popup_iconEmail">
		<form method="post" action="action.php" enctype="multipart/form-data" target="upload_target">
			<div class="form">
				<div class="headline">Email</div>
				<div class="inner">

					<div class="field">
						<span>1. Please provide the email address that you would like to send the submitted data to:</span>
						<input type="text" name="emailAddress" class="email required" maxlength="300" value="" />
					</div><!-- / .field -->

					<div class="field">
						<span>2. Please provide the URL of the image of the button you would like to include:</span>
						<input type="text" name="emailButtonURL" class="url" maxlength="300" value="" />
					</div><!-- / .field -->

					<div class="field">
						<span>OR select an image to upload:</span>
						<input type="file" name="emailButtonImage" class="uploadFile" />
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<input type="hidden" name="pageID" value="<?php echo $tab['id']; ?>" />
					<input type="hidden" name="emailToUpdate" value="" />
					<input type="hidden" name="act" value="content_email" />
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconEmail -->



	<!-- flickr -->
	<a href="#popup_iconFlickr"></a>
	<div id="popup_iconFlickr">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Flickr</div>
				<div class="inner">

					<div class="field">
						<span>1. Please provide your Flickr ID you would like to stream from (<a href="http://idgettr.com" target="_blank">Get Flickr ID</a>):</span>
						<input type="text" name="flickrId" class="required" maxlength="20" value="" />
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconFlickr -->



	<!-- contest - input text -->
	<a href="#popup_iconContestText"></a>
	<div id="popup_iconContestText">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Contest Text Input</div>
				<div class="inner">

					<div class="field">
						<span>1. Please choose type of field:</span>
						<select id="contestTextFieldType" class="required"></select>
					</div><!-- / .field -->

					<div class="hiddenPopupFields">

						<input type="hidden" name="contestTextName" />

						<div class="field">
							<span>2. Label:</span>
							<input type="text" name="contestTextLabel" class="required" />
						</div><!-- / .field -->

						<div class="field">
							<span>3. Required:</span>
							<input type="checkbox" name="contestTextRequired" />
						</div><!-- / .field -->

						<div class="field">
							<span>4. Error message:</span>
							<input type="text" name="contestTextError" />
						</div><!-- / .field -->

						<div class="field">
							<span>5. Validation:</span>
							<select name="contestTextValidation">
								<option value="">None</option>
								<option value="url">Url</option>
								<option value="email">Email</option>
								<option value="digits">Digits</option>
								<option value="zip">Zip Code</option>
							</select>
						</div><!-- / .field -->

					</div><!-- / .hiddenPopupFields -->

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconContestText -->



	<!-- contest - textarea -->
	<a href="#popup_iconContestTextarea"></a>
	<div id="popup_iconContestTextarea">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Contest Text Area</div>
				<div class="inner">

					<div class="field">
						<span>1. Please choose type of field:</span>
						<select id="contestTextareaFieldType" class="required"></select>
					</div><!-- / .field -->

					<div class="hiddenPopupFields">

						<input type="hidden" name="contestTextareaName" />

						<div class="field">
							<span>2. Label:</span>
							<input type="text" name="contestTextareaLabel" class="required" />
						</div><!-- / .field -->

						<div class="field">
							<span>3. Required:</span>
							<input type="checkbox" name="contestTextareaRequired" />
						</div><!-- / .field -->

						<div class="field">
							<span>4. Error message:</span>
							<input type="text" name="contestTextareaError" />
						</div><!-- / .field -->

					</div><!-- / .hiddenPopupFields -->

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconContestTextarea -->



	<!-- contest - select -->
	<a href="#popup_iconContestSelect"></a>
	<div id="popup_iconContestSelect">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Contest Dropdown</div>
				<div class="inner">

					<div class="field">
						<span>1. Please choose type of field:</span>
						<select id="contestSelectFieldType" class="required"></select>
					</div><!-- / .field -->

					<div class="hiddenPopupFields">

						<input type="hidden" name="contestSelectName" />

						<div class="field">
							<span>2. Label:</span>
							<input type="text" name="contestSelectLabel" class="required" />
						</div><!-- / .field -->

						<div class="field">
							<span>3. Options:</span>
							<input type="text" name="contestSelectOptionText" maxlength="50" title="Text" />
							<input type="text" name="contestSelectOptionValue" maxlength="50" title="Value" />
							<a class="addContestSelectOption" href="">Add option</a>
						</div><!-- / .field -->

						<div class="field">
							<span>4. Default Value:</span>
							<select name="contestSelectDefault" class="required"></select>
						</div><!-- / .field -->

					</div><!-- / .hiddenPopupFields -->

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconContestSelect -->



	<!-- contest - radio -->
	<a href="#popup_iconContestRadio"></a>
	<div id="popup_iconContestRadio">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Contest Radio Buttons</div>
				<div class="inner">

					<div class="field">
						<span>1. Please choose type of field:</span>
						<select id="contestRadioFieldType" class="required"></select>
					</div><!-- / .field -->

					<div class="hiddenPopupFields">

						<input type="hidden" name="contestRadioName" />

						<div class="field">
							<span>2. Label:</span>
							<input type="text" name="contestRadioLabel" class="required" />
						</div><!-- / .field -->

						<div class="field">
							<span>3. Options:</span>
							<input type="text" name="contestRadioOptionText" maxlength="50" title="Text" />
							<input type="text" name="contestRadioOptionValue" maxlength="50" title="Value" />
							<a class="addContestRadioOption" href="">Add option</a>
						</div><!-- / .field -->

						<div class="field">
							<span>4. Default Value:</span>
							<select name="contestRadioDefault" class="required"></select>
						</div><!-- / .field -->

					</div><!-- / .hiddenPopupFields -->

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconContestRadio -->



	<!-- contest - checkbox -->
	<a href="#popup_iconContestCheckbox"></a>
	<div id="popup_iconContestCheckbox">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Contest Checkbox</div>
				<div class="inner">

					<div class="field">
						<span>1. Please choose type of field:</span>
						<select id="contestCheckboxFieldType" class="required"></select>
					</div><!-- / .field -->

					<div class="hiddenPopupFields">

						<input type="hidden" name="contestCheckboxName" />

						<div class="field">
							<span>2. Label:</span>
							<input type="text" name="contestCheckboxLabel" class="required" />
						</div><!-- / .field -->

						<div class="field">
							<span>3. Value:</span>
							<input type="text" name="contestCheckboxValue" class="required" />
						</div><!-- / .field -->

						<div class="field">
							<span>4. Required:</span>
							<input type="checkbox" name="contestCheckboxRequired" />
						</div><!-- / .field -->

						<div class="field">
							<span>5. Error message:</span>
							<input type="text" name="contestCheckboxError" />
						</div><!-- / .field -->

					</div><!-- / .hiddenPopupFields -->

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconContestCheckbox -->



	<!-- contest - button -->
	<a href="#popup_iconContestBtn"></a>
	<div id="popup_iconContestBtn">
		<form method="post" action="action.php" enctype="multipart/form-data" target="upload_target">
			<div class="form">
				<div class="headline">Contest Button</div>
				<div class="inner">

					<div class="field">
						<span>1. Type of the button you want to add:</span>
						<select name="contestBtnType" class="required">
							<option value=""></option>
							<option value="typeHome">Home</option>
							<option value="typeContestEnter">Enter Contest</option>
							<option value="typeContestSubmit">Submit</option>
							<option value="typeUgcView">View Entries</option>
							<option value="typeUgcPreview">Preview Entry</option>
							<option value="typeUgcUpload">Photo Upload</option>
						</select>
					</div><!-- / .field -->

					<div class="field">
						<span>2. Custom Button Image (optional):<br /> Paste Your Image URL:</span>
						<input type="text" name="contestBtnImageURL" class="url" maxlength="300" />
					</div><!-- / .field -->

					<div class="field">
						<span>Or upload Image:</span>
						<input type="file" name="contestBtnImage" class="uploadFile" />
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<input type="hidden" name="pageID" value="<?php echo $tab['id']; ?>" />
					<input type="hidden" name="contestBtnImageToUpdate" value="" />
					<input type="hidden" name="act" value="contest_button" />
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconContestBtn -->



	<!-- contest - ugc - upload area -->
	<a href="#popup_iconContestUGCUploadArea"></a>
	<div id="popup_iconContestUGCUploadArea">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Contest UGC Upload Area</div>
				<div class="inner">

					<div class="field">
						<span>1. Please choose a background color for the area:</span>
						<span class="uniPicker ugcUploadAreaBgColor"></span>
					</div><!-- / .field -->

					<div class="field">
						<span>2. Error message:</span>
						<input type="text" name="ugcUploadAreaError" class="required" />
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconContestUGCUploadArea -->



	<!-- contest - ugc - gallery -->
	<a href="#popup_iconContestUGCGallery"></a>
	<div id="popup_iconContestUGCGallery">
		<form method="post" action="">
			<div class="form">
				<div class="headline">Contest UGC Gallery</div>
				<div class="inner">

					<div class="field">
						<span>1. Please choose layout grid:</span>
						<select name="ugcGalleryLayout">
							<option value="5x2">5x2</option>
							<option value="5x3">5x3</option>
						</select>
					</div><!-- / .field -->

					<div class="field">
						<span>2. Please choose a gallery background color:</span>
						<span class="uniPicker ugcGalleryBgColor"></span>
					</div><!-- / .field -->

					<div class="field">
						<span>3. Please choose a gallery border color:</span>
						<span class="uniPicker ugcGalleryBorderColor"></span>
					</div><!-- / .field -->

					<div class="field">
						<span>4. Please choose a gallery text color:</span>
						<span class="uniPicker ugcGalleryTextColor"></span>
					</div><!-- / .field -->

					<div class="field">
						<span>5. Please choose a gallery buttons color:</span>
						<span class="uniPicker ugcGalleryBtnColor"></span>
					</div><!-- / .field -->

					<div class="field">
						<span>6. Please choose a gallery buttons text color:</span>
						<span class="uniPicker ugcGalleryBtnTextColor"></span>
					</div><!-- / .field -->

					<div class="field">
						<span>Unsure about how to customize your gallery? Please look at the
							<a target="_blank" href="images/galleryimage.jpg">example</a>.</span>
					</div><!-- / .field -->

				</div><!-- / .inner -->
				<div class="footline">
					<a class="aButton done" href="">Done</a>
					<a class="aButton cancel" href="">Cancel</a>
				</div><!-- / .footline -->
			</div><!-- / .form -->
		</form>
	</div><!-- / #popup_iconContestUGCGallery -->


</div><!-- / .hiddenForms -->

</body>
</html>
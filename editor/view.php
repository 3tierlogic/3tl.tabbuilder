<?php

	// this file is responsible for displaying tabs on facebook

	header('P3P: CP="CAO PSA OUR"');

	// includes
	require_once('config.php');
	require_once('functions.php');

	$signed_request = $_REQUEST["signed_request"];

	$appId = $_GET["id"];
	if ( ! isset( $appId ) ) exit();

	$secret = ars_get_secret_by_app_id( $appId, $app_details );
	if ( empty( $secret ) ) exit();

	include_once('facebook/fb_sdk/sig_control.php');
	$data = parse_signed_request( $signed_request, $secret );


	// Safari looses session in iframe, so this is the work around for that fix
	$u_agent = $_SERVER['HTTP_USER_AGENT'];
	if ( preg_match( '/Safari/i', $u_agent ) ) {
		$status = $data["app_data"];
		// in case of safari we redirect the users to authorization page, when they land back
		// we detect that they are coming after authorization and we take them to frorm page
		if ( $status ) {
			// redirect if safari has lost the session
			include_once('view-form.php');
			exit();
		}
	} // safari fix end


	$page_fan = $data["page"]["liked"];
	$page_admin = $data["page"]["admin"];
	$page_id = $data["page"]["id"];
	$page_data = json_decode( file_get_contents("https://graph.facebook.com/$page_id") );
	$page_link = $page_data->link;


	// store some variables for contest form page
	session_start();
	$_SESSION['appId'] = $appId;
	$_SESSION['appSecret'] = $secret;
	$_SESSION['page_id'] = $page_id;


	// get tab to show
	$query = "SELECT id FROM pages WHERE fan_page_id = '".$page_id."' AND app_id = '".$appId."'
							AND published = '1' LIMIT 1 ";
	$query = mysql_query( $query ) or exit( mysql_error() );
	// if tab is published - show it
	if ( mysql_num_rows( $query ) > 0 ) {
		$tabId = mysql_result( $query, 0 );
		$tab = ars_getPageInfo( $tabId );
	}
	// if not published, check if it was created
	else {
		$query = "SELECT id FROM pages WHERE fan_page_id = '".$page_id."' AND app_id = '".$appId."'
								AND published = '0' LIMIT 1 ";
		$query = mysql_query( $query ) or exit( mysql_error() );
		// if yes - show default page
		if ( mysql_num_rows( $query ) > 0 ) $commingSoon = true;
		// if invalid request - don't show anything
		else $commingSoon = false; //exit();
	}


	// GET USER ID FOR CONTEST

	//try to include the facebook base file
	try{
		include_once "facebook/fb_sdk/facebook.php";
	}
	catch ( Exception $o ) {
		error_log($o);
	}
	$user = null; //facebook user uid


	// Create our Application instance.
	$facebook = new Facebook(array(
		'appId'  => $appId,
		'secret' => $secret,
		'cookie' => true
	));


	// We get user info from API using this code
	$user = $facebook->getUser();
	if ( $user ) {
		try {
			$user_info = $facebook->api('/me');
		}
		catch ( FacebookApiException $e ) {
			print_r( $user_info );
			$user = null;
		}
	}

?><!doctype html>
<html lang="en-US" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
<meta charset="UTF-8" />
<title><?php echo $tab['name']; ?></title>
<link rel="stylesheet" href="css/style.css?v=1.01" />
<script src="js/jquery.1.7.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/fb.js"></script>
<script src="js/both.js"></script>
<script src="https://widgets.twimg.com/j/2/widget.js"></script>
<script src="https://apis.google.com/js/plusone.js"></script>
<script src="https://maps.google.com/maps/api/js?sensor=false"></script>
<style>* { margin: 0; padding: 0; }</style>
<script>
$(window).load( function() {

	// DISPLAY WIDGETS
	displayWidgetsOnload( $('.pageBody') );

});
</script>
</head>



<body class="fb-view">

	<!-- include Javascript SDK -->
	<div id="fb-root"></div>
	<script>jsSDK();</script>

	<script>

		window.fbAsyncInit = function() {
			FB.init({
				appId: '<?php echo $appId; ?>',
				status: true,
				cookie: true,
				xfbml: true,
				oauth: true
			});
		}; // fbAsyncInit()


		// Load the SDK Asynchronously
		(function() {
			var e = document.createElement('script'); e.async = true;
			e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
			document.getElementById('fb-root').appendChild(e);
		}());


		function contestEnterFunc( response ) {
			FB.login( function( response ) {
				if ( response.authResponse ) {
					// we have to pass $signed_request for safari fix
					location.href = 'view-form.php?request_var=<?php echo $signed_request; ?>';
				} else {
					//user cancelled login or did not grant authorization
					alert("Please agree to the Facebook permissions in order to participate in the contest.");
				}
			}, { scope:'user_location, user_birthday, email, publish_actions' } );
		} // contestEnterFunc()

	</script>


	<?php // show content
	// page was not created - show default
	if ( $commingSoon ) {
		echo '<h1 style="text-align:center;">The page is comming soon</h1>';
	}
	// page was created
	else {
		// user wants to see ugc gallery
		if ( strpos( $_SERVER['QUERY_STRING'], 'show=ugc_gallery' ) !== false ) {
			echo $tab['content_contest_ugc_gallery'];
			// get all ugc contest images (they will be moved to the gallery by js)
			echo '<div class="hiddenUgcContestImages">' . prepareUgcGalleryImages( $tab['id'], $user_info['id'] ) . '</div>';
		}
		// user is a fan and contest is set - show contest page
		elseif ( $page_fan && $tab['contest'] == '1' ) {
			echo $tab['content_contest_fans'];
		}
		// user is a fan and contest is not set - show fans page
		elseif ( $page_fan ) {
			echo $tab['content_fans'];
		}
		// user is not a fan - show nonfans page
		else {
			echo $tab['content_nonfans'];
			// we also can show fans content, if nonfans is not set
			// if ( ! strpos( $tab['content_nonfans'], '"block"' ) ) echo $tab['content_fans'];
		}
	} // page was created
	?>


	<?php $_SESSION['appLink'] = $page_link . '?sk=app_' . $appId; ?>


	<div id="includes">
		<div class="appId"><?php echo $appId; ?></div>
		<div class="pageId"><?php echo $page_id; ?></div>
		<div class="signedRequest"><?php echo $signed_request; ?></div>
		<div class="appLink"><?php echo $_SESSION['appLink']; ?></div>
		<div class="appName"><?php echo $tab['name']; ?></div>
		<div class="fbUserId"><?php echo $user_info['id']; ?></div>
		<div class="userIP"><?php echo $_SERVER['REMOTE_ADDR']; ?></div>
	</div><!-- / .includes -->

	<script src="https://connect.facebook.net/en_US/all.js"></script>

</body>
</html>
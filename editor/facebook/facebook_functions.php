<?php


	//facebook application
	//@author: Muhammad Nouman
	require_once('config.php');
	require_once('../config.php');
	require_once('../functions.php');


	$user = null; //facebook user uid

	try {
		include_once "fb_sdk/facebook.php";
	}
	catch(Exception $o){
		error_log($o);
	}

    // Create our Application instance.
    $facebook = new Facebook(array(
      'appId'  => $fbconfig['appid'],
      'secret' => $fbconfig['secret'],
      'cookie' => true
    ));

	//Facebook Authentication part
    $user = $facebook->getUser();

	if ( ! $user ) {
        //Failed to load the user means there is some API problem
		echo "user not loged ing";
		exit();
     }



	// define FB vars
	if ( isset( $_REQUEST['id'] ) )
		$pageId = $_REQUEST['id'];

	if ( isset( $_REQUEST['app_id'] ) )
		$appId = $_REQUEST['app_id'];

	$tabName = $_REQUEST['tab_name'];



	# WHAT TO DO...
	switch ( $_REQUEST['fb_act'] )
	{


		// ADD APPLICATION TO FAN PAGE
		case 'app_install':

			$install = true; // if false - user attempts to reinstall the app

			if ( ! isset( $appId ) ) {
				$appId = ars_get_free_app_id( $pageId, $app_details );
			} else {
				$install = false;
			}

			if ( $user ) {
				try {
					$page_info = $facebook->api( "/$pageId?fields=access_token" );
					if ( ! empty( $page_info['access_token'] ) ) {
						$args = array(
							'access_token' => $page_info['access_token'],
							'app_id'             => $appId
						);
						$post_id = $facebook->api( "/$pageId/tabs", "post", $args );
						if ( $post_id ) {
							if ( $install ) echo $appId;
							else echo 'ok';
						}
						// echo $post_id . "if you see on it means tab/app has been added to the fan page";
					}
				} catch (FacebookApiException $e) {
					$user = null;
				}
			}
			break;


		// UPDATE NAME OF THE TAB/APP ON FAN PAGE
		case 'app_update':
			if ( $user ) {
				try {
					$page_info = $facebook->api( "/$pageId?fields=access_token" );
					if ( ! empty( $page_info['access_token'] ) ) {
						$args = array(
							'access_token'  => $page_info['access_token'],
							'custom_name'	=> $tabName
						);
						$post_id = $facebook->api( "/$pageId/tabs/app_$appId", "post", $args );
						if ( $post_id ) echo 'ok'; // if you see '1' it means location and name updated
					}
				} catch (FacebookApiException $e) {
					echo $e;
					$user = null;
				}
			}
			break;


		// DELETE A FAN PAGE APP/TAB
		case 'app_delete':
			if ( $user ) {
				try {
					$page_info = $facebook->api( "/$pageId?fields=access_token" );
					if ( ! empty($page_info['access_token']) ) {
						$args = array(
							'access_token'  => $page_info['access_token']
						);
						$post_id = $facebook->api( "/$pageId/tabs/app_$appId", "delete", $args );
						if ( $post_id ) echo 'ok'; // if you see '1' it means that the app is deleted
					}
				} catch (FacebookApiException $e) {
					$user = null;
				}
			}
			break;


	} // switch end


	// документ UTF-8

?>
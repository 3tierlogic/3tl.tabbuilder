

	//------------------------- GLOBAL VARS-----------------------------//

	// check file before uploading, must be the same with one in "functions.php"
	var $maxFileSize = 1024 * 1024 * 1;




	//----------- THESE FUNCTIONS ARE USED IN EDITOR AND FACEBOOK -----------//



	// OBJECT, DISPLAYING SOME WIDGETS
	function displayWidget() {


		// PHP WORDWRAP() EMULATION
		this.wordwrap = function( str, width, brk, cut ) {
			width = width || 75;
			brk = brk || '\n';
			cut = cut || false;
			if ( ! str ) { return str; }
			var regex = '.{1,' +width+ '}(\\s|$)' + (cut ? '|.{' +width+ '}|.+$' : '|\\S+?(\\s|$)');
			var newStr = str.match( RegExp(regex, 'g') ).join( brk );
			var removeSpacesBeforeBreaks = new RegExp( ' ' + brk, 'g' );
			return newStr.replace( removeSpacesBeforeBreaks, brk );
		}



		// CUT UGC TITLE AND DESCRIPTION
		this.ugcTitlesCut = function( $str, $chars, $break ) {
			$chars = $chars || 10;
			$break = $break || '<br />';
			if ( $str.length > $chars ) {
				// search spaces among the first "$length" symbols
				var $cut = ( $str.indexOf(' ') != -1 && $str.indexOf(' ') < $chars ) ? false : true;
				var $displayWidget = new displayWidget();
				$str = $displayWidget.wordwrap( $str, $chars, $break, $cut );
				$str = $str.substring( 0, $str.indexOf($break) ) + '...';
			}
			return $str;
		}



		// GET FILE CONTENT
		this.ajaxGetFileContent = function( $path ) {
			$('body').append('<div class="tempDiv" style="display:none;" />');
			var $element = $('.tempDiv:last');
			$element.load( $path, function() {
				$('#includes').append( $element.html() );
				$element.remove();
			});
		} // ajaxGetFileContent()



		// REMOVE CDATA
		this.removeCDATA = function( $string ) {
			while ( $string.indexOf('<![CDATA[') != -1 ) $string = $string.replace( '<![CDATA[', '' );
			while ( $string.indexOf(']]>') != -1 ) $string = $string.replace( ']]>', '' );
			return $string;
		} // removeCDATA()



		// FORM DATE STRING
		this.formDateString = function( $minutes ) {
			var $date = 'about ';
			if ( $minutes < 60 ) {
				if ( $minutes == '1' ) $date += '1 minute';
				else $date += $minutes + ' minutes';
			}
			else if ( $minutes < 1440 ) {
				if ( $minutes >= 60 && $minutes < 120 ) $date += '1 hour';
				else $date += parseInt( $minutes / 60 ) + ' hours';
			}
			else if ( $minutes < 43200 ) {
				if ( $minutes >= 1440 && $minutes < 2880 ) $date += '1 day';
				else $date += parseInt( $minutes / 1440 ) + ' days';
			}
			else {
				if ( $minutes >= 43200 && $minutes < 86400 ) $date += '1 month';
				else $date += parseInt( $minutes / 43200 ) + ' months';
			}
			$date += ' ago';
			return $date;
		} // formDateString()



		// PARSE TWITTER
		this.parseTwitter = function( $body ) {
			var $id = $body.attr('id');
			var $name = $body.attr('data-name');
			var $linkColor = $body.attr('data-link-color');
			var $textColor = $body.attr('data-text-color');
			var $postsNumber = $body.attr('data-posts');
			new TWTR.Widget({
				id: $id,
				version: 2,
				type: 'profile',
				rpp: $postsNumber,
				interval: 30000,
				width: 'auto',
				theme: {
					shell: { color: 'none', background: 'none' },
					tweets: { links: $linkColor, color: $textColor, background: 'none' }
				},
				features: { scrollbar: false, loop: false, live: true, behavior: 'all' }
			}).setUser( $name ).render().start();
			// styling doesn't work in IE, so we need to add styles manually
			if ( $.browser.msie ) {
				var $timer = setInterval( function() {
					if ( $body.find('.twtr-doc').length ) {
						clearInterval( $timer );
						setTimeout( function() {
							$id = '#' + $id;
							var $css = '<style>';
							$css += $id + ' .twtr-bd, ' + $id + ' .twtr-timeline i a, ' + $id + ' .twtr-bd p { color: ' + $textColor + ' !important; }';
							$css += $id + ' .twtr-tweet a { color: ' + $linkColor + ' !important; }';
							$css += '</style>';
							$body.append( $css );
						}, 100 );
					}
				}, 100 );
			} // IE
		} // parseTwitter()



		// PARSE GOOGLE MAPS
		this.parseGoogleMaps = function( $element ) {
			var $id = $element.attr('id');
			var $address = $element.attr('data-address');
			var $zoom = parseInt( $element.attr('data-zoom') );
			$zoom = isNaN( $zoom ) ? 15 : $zoom;

			$.ajax({
				url: 'action.php?act=get_gm_coords&address=' + $address,
				type: 'POST',
				dataType: 'json',
				error: function () { alert('Error'); },
				success: function ( data ) {

					// get coords
					var $location = data.results[0].geometry.location;
					var $lat = $location.lat;
					var $lng = $location.lng;

					// create map
					var $myCoords = new google.maps.LatLng( $lat, $lng );

					var $options = {
						zoom: $zoom,
						center: $myCoords,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					};

					var $map = new google.maps.Map( document.getElementById( $id ), $options );

					// create marker
					var $marker = new google.maps.Marker({
						position: $myCoords,
						map: $map
					});

				} // success end
			}); // ajax end
		} // parseGoogleMaps()



		// PARSE GOOGLE PLUS
		this.parseGooglePlus = function( $element ) {
			// if there's no id - add it
			var $id = $element.is('[id]') ? $element.attr('id') : 'googlePlus-' + new Date().getTime();
			$element.attr({ 'id': $id });
			// create, add and render btn
			var gPlusOne = document.createElement('g:plusone');
			gPlusOne.setAttribute( 'href', $element.attr('data-href') );
			document.getElementById( $id ).appendChild( gPlusOne );
			gapi.plusone.go( $id );
			$element.removeAttr('id');
		}



		// PARSE RSS
		this.parseRSS = function( $element ) {
			var $postsNumber = parseInt( $element.attr('data-posts') );
			var $linkColor = $element.attr('data-link-color');
			var $textColor = $element.attr('data-text-color');
			var $url = 'action.php?act=get_rss&url=' + $element.attr('data-url');

			$.ajax({
				url: $url,
				type: 'POST',
				success: function( data ) {

					// function - find all $search and replace them by $replace
					function replaceElement( $haystack, $search, $replace ) {
						while ( $haystack.indexOf( '<' + $search ) != -1 ) {
							$haystack = $haystack.replace( '<' + $search, '<' + $replace );
						}
						while ( $haystack.indexOf( '</' + $search + '>' ) != -1 ) {
							$haystack = $haystack.replace( '</' + $search + '>', '</' + $replace + '>' );
						}
						return $haystack;
					};

					// function - get element from string data
					function getElement( $haystack, $eName, $eNumber ) {
						var $element;
						var $posSearch = 0;
						$eNumber = typeof( $eNumber ) != 'undefined' ? $eNumber : 0;
						for ( var $i = 0; $i <= $eNumber; $i++ ) {
							var $posStart = $haystack.indexOf( '<' + $eName, $posSearch );
							// for paired tags
							var $posEnd = $haystack.indexOf( '</' + $eName + '>', $posStart ) + ( $eName.length + 3 );
							// for singular tag
							if ( $posEnd == -1 ) $posEnd = $haystack.indexOf( '>', $posStart + $eName.length + 2 ) + 1;
							if ( $i == $eNumber ) {
								$element = $haystack.substring( $posStart, $posEnd );
								break;
							}
							$posSearch = $posEnd++;
						}
						return $element;
					};

					// function - get attribute from element
					function getAttr( $element, $attr ) {
						var $result;
						// check if attr exist
						if ( $element.indexOf( $attr ) == -1 ) $result = false;
						// if exist - cut attr value
						else {
							var $posStart = $element.indexOf( '"', $element.indexOf($attr) ) + 1;
							var $posEnd = $element.indexOf( '"', $posStart );
							$result = $element.substring( $posStart, $posEnd );
						}
						return $result;
					};

					// function - remove parent tags
					function removeParentTags( $element ) {
						return $element.substring( $element.indexOf('>') + 1, $element.lastIndexOf('<') );
					};

					var $feed = data;
					var $displayWidget = new displayWidget();

					$feed = $displayWidget.removeCDATA( $feed );
					$feed = replaceElement( $feed, 'entry', 'item' );
					$feed = replaceElement( $feed, 'url', 'link' );
					$feed = replaceElement( $feed, 'summary', 'description' );
					$feed = replaceElement( $feed, 'updated', 'pubDate' );
					$feed = replaceElement( $feed, 'modified', 'pubDate' );

					var $output = '<ul>';

					for ( var $i = 0; $i < $postsNumber; $i++ ) {
						// get item
						var $item = getElement( $feed, 'item', $i );
						// item title
						var $title = removeParentTags( getElement( $item, 'title' ) );
						// item link
						var $link = getElement( $item, 'link' );
						$link = getAttr( $link, 'href' ) ? getAttr( $link, 'href' ) : removeParentTags( $link );
						// item description
						var $description = removeParentTags( getElement( $item, 'description' ) );
						// item date
						var $date = removeParentTags( getElement( $item, 'pubDate' ) );
						var regExp = /\dT\d/;
						if ( /\dT\d/.test( $date ) ) {
							$date = $date.replace( 'T', ' ' );
							$date = $date.replace( 'Z', '' );
						}
						$date = new Date().getTime() - Date.parse( $date );
						$date = $displayWidget.formDateString( parseInt( $date / 1000 / 60 ) );
						// output
						$output += '<li style="color: ' + $textColor + '">';
						$output += '<div class="title">';
						$output += '<a  href="' + $link + '" style="color:' + $linkColor + '" target="_blank">' + $title + '</a>';
						$output += '</div>';
						$output += '<div class="description">' + $description + '</div>';
						$output += '<div class="date">' + $date + '</div>';
						$output += '</li>';
					} // endfor;

					$output += '</ul>';

					$element.append( $output );

				} // success
			}); // ajax end
		} // parseRSS()



		// PARSE COUNTDOWN
		this.parseCountdown = function( $element ) {
			var $date = $element.attr('data-date');
			var $color = $element.attr('data-color');
			var $size = $element.attr('data-size');

			var $layout = '<span class="countdown_row">';
				$layout += '{d<}<span class="countdown_section">';
					$layout += '<span class="countdown_amount">{dn}</span><br>{dl}</span>{d>}';
				$layout += '{h<}<span class="countdown_section">';
					$layout += '<span class="countdown_amount">{hn}</span><br>{hl}</span>{h>}';
				$layout += '{m<}<span class="countdown_section">';
					$layout += '<span class="countdown_amount">{mn}</span><br>{ml}</span>{m>}';
				$layout += '{s<}<span class="countdown_section">';
					$layout += '<span class="countdown_amount">{sn}</span><br>{sl}</span>{s>}';
			$layout += '</span>';
			$layout += '<style type="text/css">';
			$layout += '.countdown_section, .countdown_amount ';
			$layout += '{ color: ' + $color + '; font-size: ' + $size + 'px; line-height: ' + $size + 'px; }';
			$layout += '</style>';

			if ( $element.is('.hasCountdown') )
				$element.countdown( 'change', {
					until: new Date( $date ), layout: $layout,
					labels: [ 'YY', 'MM', 'WW', 'DD', 'HH', 'MM', 'SS' ],
					labels1: [ 'YY', 'MM', 'WW', 'DD', 'HH', 'MM', 'SS' ]
				});
			else
				$element.countdown({
					until: new Date( $date ), layout: $layout,
					labels: [ 'YY', 'MM', 'WW', 'DD', 'HH', 'MM', 'SS' ],
					labels1: [ 'YY', 'MM', 'WW', 'DD', 'HH', 'MM', 'SS' ]
				});
		} // parseCountdown()



		// FB INVITE
		this.sendInvite = function( $appLink, $title, $message ) {
			FB.ui({
				method: 'apprequests',
				title: $title,
				message: $message,
				data: { "appLink": $appLink }
			});
		} // sendInvite()



		// FB POST TO FEED
		this.postToFeed = function( $link, $name, $descr, $img, $caption ) {
			var obj = {
				method: 'feed',
				link: $link,
				name: $name,
				description: $descr,
				picture: $img,
				caption: $caption
			};
			function callback( response ) {
				//document.getElementById('msg').innerHTML = "Post ID: " + response['post_id'];
			}
			FB.ui( obj, callback );
		} // postToFeed()



		// GET YOUTUBE VIDEO ID
		this.getYoutubeVideoId = function( $url ) {
			var $id;
			if ( $url.indexOf('v=') != -1 ) {
				var $from = $url.indexOf('v=') + 2;
				var $to = $url.indexOf('&') != -1 ? $url.indexOf( '&', $from ) : null;
				$id = ( $to != null ) ? $url.substring( $from, $to ) : $url.substring( $from );
			} else {
				$id = $url.substr( $url.lastIndexOf('/') + 1 );
			}
			return $id;
		}



		// CREATE YOUTUBE
		this.createYoutube = function( $url ) {
			var $displayWidget = new displayWidget();
			var $id = $displayWidget.getYoutubeVideoId( $url );
			$url = 'https://www.youtube.com/v/' + $id;
			$output = '<object class="youtube" data-url="' + $url + '" ';
			$output +=	'classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">';
			$output +=	'<param name="allowfullscreen" value="true" />';
			$output +=	'<param name="allowscriptaccess" value="always" />';
			$output +=	'<param name="quality" value="high" />';
			$output +=	'<param name="wmode" value="opaque" />';
			$output +=	'<param name="movie" value="' + $url + '" />';
			$output += '<embed src="' + $url + '" type="application/x-shockwave-flash" ';
			$output += 'allowfullscreen="true" allowscriptaccess="always" wmode="opaque"></embed>';
			$output +=	'</object>';
			return $output;
		}



		// CREATE VIMEO
		this.createVimeo = function( $url ) {
			var $id = $url.substr( $url.lastIndexOf('/') + 1 );
			var $output = '<object class="vimeo" data-url="' + $url + '" ';
			$output +=	'classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">';
			$output +=	'<param name="allowfullscreen" value="true" />';
			$output +=	'<param name="allowscriptaccess" value="always" />';
			$output +=	'<param name="quality" value="high" />';
			$output +=	'<param name="wmode" value="opaque" />';
			$output +=	'<param name="movie" value="https://vimeo.com/moogaloop.swf?clip_id=' + $id;
			$output += '&server=vimeo.com&show_title=0&show_byline=0&show_portrait=0&color=ffffff';
			$output += '&fullscreen=1&autoplay=0&loop=0" />';
			$output += '<embed src="https://vimeo.com/moogaloop.swf?clip_id=' + $id;
			$output += '&server=vimeo.com&show_title=0&show_byline=0&show_portrait=0&color=ffffff';
			$output += '&fullscreen=1&autoplay=0&loop=0" type="application/x-shockwave-flash" ';
			$output += 'allowfullscreen="true" allowscriptaccess="always" wmode="opaque"></embed>';
			$output +=	'</object>';
			return $output;
		}



		// CREATE COOLSLIDER SLIDES
		this.createCoolSliderSlides = function( $slides, $sWidth, $sHeight ) {
			var $displayWidget = new displayWidget();
			var $output = '';
			$slides.each( function() {
				var $url, $item;
				if ( $(this).find('.youtube').length ) {
					$url = $(this).find('.youtube').attr('data-url');
					$item = $displayWidget.createYoutube( $url );
				} else if ( $(this).find('.vimeo').length ) {
					$url = $(this).find('.vimeo').attr('data-url');
					$item = $displayWidget.createVimeo( $url );
				} else {
					$url = $(this).find('img').attr('src');
					$item = $('<div />').append( $(this).find('img').clone() ).html();
				}
				$output += '<li style="width: ' + $sWidth + 'px; height: ' + $sHeight + 'px;" data-url="' + $url + '">';
				$output += $item + '<span /></li>';
			});
			return $output;
		}


	} // displayWidget()



	// --------------------------------------------------------------------------------------------------- //
	// ALL FUNCTIONS BELOW ARE USED FOR DISPLAYING WIDGETS ON FACEBOOK   //
	// OR IN PREVIEW MODE IN THE EDITOR                                                                                         //
	// --------------------------------------------------------------------------------------------------- //


	// POPUP (where validation errors are shown, used on contest pages)
	function myAlert( $theBody, $headline, $message, $contestUnavailable ) {
		this.show = function( $theBody, $headline, $message, $contestUnavailable ) {

			$headline = ( $headline != '' ) ? $headline : 'Please correct the following:';

			// if popup doesn't exist, create it
			if ( ! $theBody.find('.myAlert').length ) {
				// error popup's html
				var $popupHtml = '<div class="form myAlert">';
					$popupHtml += '<div class="headline"></div>';
					$popupHtml += '<div class="inner">';
						$popupHtml += '<div class="field">';
							$popupHtml += '<span class="message"></span>';
						$popupHtml += '</div>';
					$popupHtml += '</div>';	
					$popupHtml += '<div class="footline">';
						$popupHtml += '<a class="aButton done" href="">Ok</a>';
					$popupHtml += '</div>';
				$popupHtml += '</div>';
				// pagemask's html
				var $pageMask = '<div class="pageMask" />';
				// append
				$('.pageBody').append( $popupHtml ).append( $pageMask );
			}

			// open
			var $popup = $theBody.find('.myAlert');
			$popup.find('.headline').html( $headline ).end().find('.message').html( $message );
			if ( $contestUnavailable ) {
				$popup.find('a.done').addClass('goodbye');
				$theBody.find('.pageMask').show();
			}
			$popup.show().css({ opacity: 0 }).animate({ opacity: 1 }, 200 );


			// close
			$popup.find('a.done').live( 'click', function() {
				if ( $(this).is('.goodbye') ) {
					location.href = 'view-thank.php';
					// if we need to return back to the splash page
					// top.location.href = $('.appLink').html();
				}
				else {
					$popup.animate({ opacity: 0 }, 200, function() {
						$(this).css({ opacity: 1 }).hide();
					});
				}
				return false;
			});

		} // show()
	} // myAlert()



	function displayWidgetsOnload( $theBody ) {

		// DON'T FORGET TO UPDATE THESE PATHS
		// website URL
		var $websiteURL = 'https://platform.3tierlogic.com/tabbuilder1_3/editor/';
		// folder with all uploaded images
		var $uploadedImagesFolder = 'https://platform.3tierlogic.com/tabbuilder1_3/editor/uploads/';


		// init objects
		var $displayWidget = new displayWidget();
		var $myAlert = new myAlert();


		// FB INIT
		FB.init({
			appId: $theBody.find('.appId').html(),
			status: true,
			cookie: true,
			frictionlessRequests: true,
			oauth: true
		});



		// FB PAGE WIDTH AND HEIGHT
		FB.Canvas.scrollTo( 0, 0 );
		FB.Canvas.setSize({
			width: $('.pageBody').width(),
			height: $('.pageBody').height()
		});



		// GOOGLE ANALYTICS
		function googleAnalytics( $UAcode ) {
			var _gaq = _gaq || [];
			_gaq.push([ '_setAccount', "'" + $UAcode + "'" ]);
			_gaq.push(['_trackPageview']);
			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
			//alert( 'Google Analytics executed, your UA code: ' + $UAcode );
		}; $theBody.find('.analytics_code').each( function() { googleAnalytics( $(this).html() ); });



		// CREATE CONTACT FORM
		function createContactForm( $object ) {
			var $sendTo = $object.children('a').attr('href').substr(7);
			var $output = '<div class="CFPopup">';
				$output += '<form method="post" action="">';
					$output += '<div class="form">';
						$output += '<a class="closeWindow" href=""></a>';

						// field name
						if ( $object.find('.name').length ) {
							$output += '<div class="field"><div class="label">Name:</div>';
							$output += '<input type="text" name="name" maxlength="30" value="" /></div>';
						}

						// field phone
						if ( $object.find('.phone').length ) {
							$output += '<div class="field"><div class="label">Phone:</div>';
							$output += '<input type="text" name="phone" maxlength="30" value="" /></div>';
						}

						// field email
						$output += '<div class="field"><div class="label">Email:</div>';
						$output += '<input type="text" name="email" maxlength="30" value="" /></div>';

						// field message
						$output += '<div class="field"><div class="label">Message:</div>';
						$output += '<textarea rows="5" cols="50" name="message" maxlength="1500"></textarea></div>';

						$output += '<input type="hidden" name="sendTo" value="' + $sendTo + '" />';
						$output += '<input type="submit" value="Send" />';

					$output += '</div>';
				$output += '</form>';
				$output += '<div class="bg"></div>';
			$output += '</div>'; // .CFPopup end
			$object.parents('.pageBody').append( $output );
		} // createContactForm() end



		// CONTACT FORM
		// show
		$theBody.find('.contentContactForm').each( function() {
			var $formObject = $(this);
			$formObject.children('a').click( function() {
				createContactForm( $formObject );
				return false;
			});
		});
		// hide
		$theBody.find('a.closeWindow').live( 'click', function() {
			$(this).parents('.CFPopup').remove();
			return false;
		});
		// submit
		$theBody.find('.CFPopup [type="submit"]').live( 'click', function() {
			var $form = $(this).parents('.CFPopup');
			// validation
			var $error = false;
			$form.find('[type="text"], textarea').each( function() {
				if ( $(this).val() == '' ) {
					$error = true;
					$myAlert.show( $('body'), '', 'Fill all fields' );
				}
			});
			// valid
			if ( ! $error ) {
				var $sendTo = $theBody.find('[name="sendTo"]').val();
				var $email = $theBody.find('[name="email"]').val();
				var $message = $theBody.find('[name="message"]').val();
				// data for sending
				var $data = 'sendTo=' + $sendTo + '&email=' + $email + '&message=' + $message;
				if ( $theBody.find('[name="name"]').length ) {
					$data += '&name=' + $theBody.find('[name="name"]').val();
				}
				if ( $theBody.find('[name="phone"]').length ) {
					$data += '&phone=' + $theBody.find('[name="phone"]').val();
				}
				// ajax send
				$.ajax({
					url: 'mail.php',
					type: 'GET',
					data: $data,
					complete: function() { $form.remove(); },
					error: function() { alert('Error'); },
					success: function( data ) {
						FB.Canvas.scrollTo( 0, 0 );
						$myAlert.show( $('body'), 'Thank You', data );
					}
				});
			} // valid
			return false;
		});



		// FB INVITE
		$theBody.find('.contentInvite').click( function() {
			$displayWidget.sendInvite( $('.appLink').html(), $(this).attr('data-title'), $(this).attr('data-message') );
		});


		// RSS
		$theBody.find('.contentRSS').each( function() { $displayWidget.parseRSS( $(this) ); });


		// FB POST TO FEED
		$theBody.find('.contentFbPostFeed').live( 'click', function() {
			var $link = $(this).attr('data-url') != '' ? $(this).attr('data-url') : $theBody.find('.appLink').html();
			var $name = $(this).attr('data-name');
			var $descr = $(this).attr('data-descr');
			var $img = $(this).attr('data-img');
			var $caption = $(this).attr('data-caption');
			$displayWidget.postToFeed( $link, $name, $descr, $img, $caption );
		});


		// TWITTER SHARE BTN
		$theBody.find('.contentTwitterShare').each( function() {
			if ( $(this).attr('href') == '' ) {
				$twitterLink = 'http://twitter.com/share?related=' + $(this).attr('data-account');
				$twitterLink += '&text=' + encodeURIComponent( $(this).attr('data-text') );
				$twitterLink += '&url=' + encodeURIComponent( $('.appLink').html() );
				$(this).attr({ 'href': $twitterLink });
			}
		});


		// TWITTER STREAM
		$theBody.find('.contentTwitter').each( function() {
			var $tempId = 'twitter-' + new Date().getTime();
			$(this).attr( 'id', $tempId );
			$displayWidget.parseTwitter( $(this) );
		});


		// GOOGLE PLUS
		$theBody.find('.googlePlus').each( function() {
			$displayWidget.parseGooglePlus( $(this) );
		});


		// GOOGLE MAPS
		$theBody.find('.contentGoogleMaps').each( function() {
			var $tempId = 'gm-' + new Date().getTime();
			$(this).attr( 'id', $tempId );
			$displayWidget.parseGoogleMaps( $(this) );
		});


		// COUNTDOWN
		$theBody.find('.contentCountdown').each( function() { $displayWidget.parseCountdown( $(this) ); });


		// PINTEREST PIN IT BUTTONS
		setTimeout( function() {
			if ( $('.contentPinIt').length ) {
				$('.contentPinIt').each( function() {
					$(this).removeClass('unparsed').html( $(this).attr('data-btn') );
				});
				$.ajax({ url: 'http://assets.pinterest.com/js/pinit.js', dataType: 'script', cache: true });
			}
		}, 1000 );



		// CONTEST ENTER BUTTON
		$theBody.find('img.contestEnter').click( function() {
			// run once with current status and whenever the status changes
			FB.getLoginStatus( contestEnterFunc );
			FB.Event.subscribe( 'auth.statusChange', contestEnterFunc );
		});



		// CONTEST UGC VIEW BUTTON
		$theBody.find('img.contestUGCView').click( function() {
			location.href = 'view.php?id=' + $('.appId').html() + '&signed_request=' + $('.signedRequest').html() + '&show=ugc_gallery';
		});



		// RETURN HOME BUTTON
		$theBody.find('img.returnHome').click( function() {
			location.href = 'view.php?id=' + $('.appId').html() + '&signed_request=' + $('.signedRequest').html();
		});



		// SUBMIT CONTEST FORM
		$theBody.find('.contestSubmit').live( 'click', function() {
			var $form = $(this).parents('form');

			$form.validate({
				rules: {
					Zip_Code: { zip_validation: true },
					Text_Open_Field_1: { zip_validation: true },
					Text_Open_Field_2: { zip_validation: true },
					Text_Open_Field_3: { zip_validation: true },
					Text_Open_Field_4: { zip_validation: true },
					Text_Open_Field_5: { zip_validation: true },
					Text_Open_Field_6: { zip_validation: true },
					Text_Open_Field_7: { zip_validation: true },
					Text_Open_Field_8: { zip_validation: true },
					Text_Open_Field_9: { zip_validation: true },
					Text_Open_Field_10: { zip_validation: true }
				}
			});

			// validate zip code
			$.validator.addMethod( 'zip_validation', function( value, element ) {
				return ! $(element).is('.zip') || /^[A-Za-z0-9]{5,6}$/.test( value );
			}, 'Please enter a valid zip code.' );

			// the form is not valid
			if ( ! $form.valid() ) {
				// show an error in popup
				var $errorLabels = $('label.error').filter( function() { return $(this).css('display') != 'none' });
				var $error = $errorLabels.siblings(':checkbox, :text, textarea').attr('data-error');
				$myAlert.show( $('body'), '', $error );
				FB.Canvas.scrollTo( 0, 0 );
			}
			// the form is valid
			else {

				// before submit we must check ugc image
				var $ugcImageValid = true;
				if ( $('.contestUGCUploadArea').length && ! $('.userUGCImage').length ) {
					$ugcImageValid = false;
					$error = $('.contestUGCUploadArea').attr('data-error');
					$myAlert.show( $('body'), '', $error );
					FB.Canvas.scrollTo( 0, 0 );
				}

				if ( $ugcImageValid ) {
					// if this is ugc form page
					if ( $('.pageBody.contest-ugc-form').length )
						$form.unbind('submit').removeAttr('target').attr( 'action', $form.attr('data-action') ).submit();
					// if this is any other form
					else $form.submit();
				}
			} // form is valid

			return false;
		}); // submit contest form



		// UPLOAD IMAGE INSTANTLY (used in ugc and images slider)
		function ajaxUploadImage( $fileInput, $uploadArea ) {

			var $form = $fileInput.parents('form');
			var $fileInputParent = $fileInput.parent('div');
			var $fileInputEmpty = '<input type="file" name="' + $fileInput.attr('name') + '" />';
			var $ajaxIframe = '#ajaxResult';
			var $ajaxLoader = '#ajaxLoader';

			// user submits the form
			$form.submit( function() {
				$( $ajaxLoader ).fadeIn();

				var $response = '';

				$( $ajaxIframe ).load( function() {
					$response = $( $ajaxIframe ).contents().find('body').html();
				});

				// periodically check the result in iframe
				var $timer = setInterval( function() {
					if ( $response != '' ) {
						clearInterval( $timer );

						// file is invalid
						if ( $response.indexOf('ERROR') != -1 ) {
							$myAlert.show( $('body'), '', 'Error - ' + $response.substr(6) );
						}

						// file is valid - add new element
						else {
							$uploadArea.html('<span /><img class="userUGCImage" src="' + $uploadedImagesFolder + $response + '" alt="" />');
							$uploadArea.find('img').css({
								'max-width': $uploadArea.width(),
								'max-height': $uploadArea.height()
							});
							// update ugc hidden fields
							$('[name="ugc_photo_name"]').val( $response );
							$('[name="ugc_photo_path"]').val( $uploadedImagesFolder + $response );
						} // file is valid

						$( $ajaxLoader ).fadeOut();

						// success
						if ( $response.length ) {
							$fileInputParent.append( $fileInputEmpty );
							$fileInput.remove();
							$fileInput = $fileInputParent.find('[type="file"]');
						}

					} // got the response
				}, 100 );

			}); // form submit

		} // ajaxUploadImage()



		// UPLOAD UGC CONTEST IMAGE
		$('[name="contestUGCUploadImg"]').live( 'change', function() {
			// validate file size (except IE)
			var $validationErrror = '';
			if ( ! $.browser.msie ) {
				var $fileSize = this.files[0].size;
				if ( $fileSize > $maxFileSize ) {
					$validationErrror = 'Please choose another file, so that its size was less than ' + $maxFileSize / 1024 / 1024 + ' Mb';
					$myAlert.show( $('body'), '', $validationErrror );
				}
			}
			// file is valid
			if ( $validationErrror == '' ) {
				ajaxUploadImage( $(this), $('.contestUGCUploadArea') );
				$(this).parents('form').submit();
				// update ugc hidden field
				var $filename = $(this).val();
				if ( $filename.lastIndexOf('\\') != -1 )
					$filename = $filename.substr( $filename.lastIndexOf('\\') + 1 );
				$('[name="ugc_original_photo_name"]').val( $filename );
			}
		});



		// GET NAME OF A PERSON, WHO UPLOADED A UGC CONTEST IMAGE
		function getUgcUploaderName() {
			var $name = $('[name="First_Name"]').length ? $('[name="First_Name"]').val() : '';
			var $surname = $('[name="Last_Name"]').length ? $('[name="Last_Name"]').val() : '';
			if ( $surname != '' ) $surname = $surname.substr( 0, 1 ) + '.';
			return $name + ' ' + $surname;
		}



		// UGC POPUP
		// add to "ugc-form" page and to "ugc-gallery" page
		if ( $theBody.parents().is('.fb-view, .pageBodyPreview') ) {
			if ( $theBody.is('.contest-ugc-form, .contest-ugc-gallery') ) {
				var $popupUGC = '<div class="ugcPopup">';
					$popupUGC += '<div class="theTitle" />';
					$popupUGC += '<div class="uploadedBy" />';
					$popupUGC += '<div class="theLikeBtn" />';
					$popupUGC += '<img src="" alt="" />';
					$popupUGC += '<div class="theDescription" />';
					$popupUGC += '<a class="close" href="">X</a>';
				$popupUGC += '</div>';
				if ( ! $theBody.find('.ugcPopup').length ) $theBody.append( $popupUGC );
			}
		}



		// show on click, when user uploads his own photo for the contest
		$('.contestUGCPreview').live( 'click', function() {
			var $popup = $('.ugcPopup');
			var $title = $displayWidget.ugcTitlesCut( $('[name="UGC_Title"]').val() );
			var $description = $displayWidget.ugcTitlesCut( $('[name="UGC_Description"]').val(), 40 );
			$popup.find('.theTitle').html( $title );
			$popup.find('.uploadedBy').html( getUgcUploaderName() );
			$popup.find('.theLikeBtn').hide();
			$popup.find('.theDescription').html( $description.replace(/\n\r?/g, '<br />') );
			$popup.find('img').attr({ src: $('.userUGCImage').attr('src') });
			$popup.find('a.close').live( 'click', function() { $popup.hide(); return false; });
			$popup.show();
		});



		// UGC GALLERY
		$.fn.ugcGallery = function() {
			var $ugcGallery = $('.ugcGallery');
			var $searchInput = $('[name="ugcGallerySearch"]');
			var $searchBtn = $ugcGallery.find('.aButton.search');
			var $showAllBtn = $ugcGallery.find('.aButton.showAll');
			var $paginationWrapper = $ugcGallery.find('.pagination');
			var $pagination = $paginationWrapper.find('#ugcPagination');
			var $ugcContestImages = $('.hiddenUgcContestImages');
			var $onOnePage = ( $ugcGallery.attr('data-layout') == '5x2' ) ? 10 : 15;
			var $imageElements;
			var $total;

			// add images to the gallery
			$ugcGallery.find('.in ul').html( $ugcContestImages.html() );
			$ugcContestImages.remove();

			// save images into a var and temporary hide them
			$imageElements = $ugcGallery.find('.in li');

			// pagination function
			function pagination( $keyword ) {
				$keyword = typeof( $keyword ) != 'undefined' ? $keyword : null;
				$imageElements.add( $paginationWrapper ).hide();
				// count elements number
				if ( $keyword == null ) {
					$total = $imageElements.length;
				} else {
					$imageElements.each( function() {
						// search is made by username and image title
						var $title = $(this).find('.text .title').html().toLowerCase();
						var $user = $(this).find('.text .user').html().toLowerCase();
						if ( $title.indexOf( $keyword ) != -1 || $user.indexOf( $keyword ) != -1 ) {
							$(this).addClass('searchResult');
						} else {
							$(this).removeClass('searchResult');
						}
					});
					$total = $imageElements.filter('.searchResult').length;
				}
				// show and paginate images
				if ( $total <= $onOnePage ) {
					( $keyword == null ) ? $imageElements.show() : $imageElements.filter('.searchResult').show();
				} else {
					$paginationWrapper.show();
					// add select's options for each page
					var $options = '';
					var $pages = Math.ceil( $total / $onOnePage );
					for ( $i = 1; $i <= $pages; $i++ ) $options += '<option>' + $i + '</option>';
					$pagination.html( $options );
					// change page function
					$pagination.change( function() {
						$imageElements.hide().each( function() {
							var $min = parseInt( $pagination.val() ) * $onOnePage - $onOnePage;
							var $max = parseInt( $pagination.val() ) * $onOnePage - 1;
							if ( $keyword == null ) {
								if ( $(this).index() >= $min && $(this).index() <= $max )
									$(this).show();
							} else {
								if ( $(this).index('.searchResult') >= $min && $(this).index('.searchResult') <= $max )
									$(this).show();
							}
						});
					});
					// show the first page elements
					$pagination.find('[value="1"]').prop( 'selected', true ).end().trigger('change');
				} // $total > $onOnePage
			} // pagination()

			// search images
			$searchBtn.click( function() {
				var $keyword = $searchInput.val().toLowerCase();
				pagination( $keyword );
			});

			// show all images
			$showAllBtn.click( function() {
				pagination();
			});

			// show pagination if needed
			pagination();

			// show on click, when user looks at the photos of other users
			$ugcGallery.find('.pic img, a.view').live( 'click', function() {
				var $imgWrapper = $(this).parents('li');
				var $popup = $('.ugcPopup');
				$popup.addClass('hasSlider').hide();

				// add slider
				var $sliderHtml = '<div class="in">';
					$sliderHtml += '<span class="arrow left"></span>';
					$sliderHtml += '<span class="arrow right"></span>';
					$sliderHtml += '<div class="frame"><ul /></div>';
				$sliderHtml += '</div>';
				$popup.find('.in, img').remove();
				$popup.find('.theDescription').before( $sliderHtml );

				var $slider = $popup.find('.in');

				// add slides
				$ugcGallery.find('li').each( function() {
					var $element = $(this);
					var $slide = '<li>';
						$slide += $element.find('.pic').html();
						$slide += $('<div />').append( $element.find('.text').clone() ).html();
					$slide += '</li>';
					$slider.find('ul').append( $slide );
				});

				// slider function
				$.fn.ugcGallerySlider = function( options ) {
					var settings = $.extend({
						speed:                    250,
						slideWidth:         360,
						slideMargin:       0,
						currentSlide:      0
					}, options );

					var $body = $(this);
					var $arrowLeft = $body.find('.arrow.left');
					var $arrowRight = $body.find('.arrow.right');
					var $elementsNumber = $body.find('li').length;
					var $displacement = settings.slideWidth + settings.slideMargin;
					var $containerWidth = $displacement * $elementsNumber;
					var $currentSlide = settings.currentSlide;

					$body.mousedown( function() { return false; });

					// set container width and scroll to the required slide
					$body.find('ul').css({ width: $containerWidth, left: -$displacement * $currentSlide });

					// show arrows
					if ( $currentSlide > 0 ) $arrowLeft.show();
					if ( $currentSlide < $elementsNumber - 1 ) $arrowRight.show();

					// display picture's name, description and name of a user
					function updateSlideText() {
						var $theSlide = $popup.find('li').eq( $currentSlide );
						var $infoDiv = $theSlide.find('.text');
						var $imgUrl = $theSlide.find('img').attr('src');
						var $pageUrl = $uploadedImagesFolder + 'index.php?url=' + $imgUrl;
						var $likeBtn = '<fb:like id="unparsedFB" href="' + $pageUrl + '" send="false" ';
						$likeBtn += 'width="77" data-layout="button_count" show-faces="false"></fb:like>';
						$popup.find('.theTitle').html( $infoDiv.find('.title').html() );
						$popup.find('.uploadedBy').html( $infoDiv.find('.user').html() );
						$popup.find('.theDescription').html( $infoDiv.find('.desc').html() );
						$popup.find('.theLikeBtn').html( $likeBtn );
						// display "Like" button
						FB.XFBML.parse( document.getElementById('unparsedFB').parentNode );
						$('#unparsedFB').removeAttr('id');
					}; updateSlideText();

					// slide
					$arrowLeft.add( $arrowRight ).click( function() {
						if ( ! $body.find('ul').is(':animated') ) {
							( $(this).is( $arrowLeft ) ) ? $currentSlide -= 1 : $currentSlide += 1;
							$body.find('ul').animate({ left: -$displacement * $currentSlide }, settings.speed, function() {
								updateSlideText();
							});
							// update arrows
							( $currentSlide == 0 ) ? $arrowLeft.hide() : $arrowLeft.show();
							( $currentSlide == $elementsNumber - 1 ) ? $arrowRight.hide() : $arrowRight.show();
						} // not animated
					});
				} // ugcGallerySlider()

				// run slider
				$popup.ugcGallerySlider({ currentSlide: $imgWrapper.index() });

				// close popup function
				$popup.find('a.close').live( 'click', function() { $popup.hide(); return false; });

				// show popup
				$popup.show();

			}); // onclick - show popup with slider

			// vote for an image
			$ugcGallery.find('a.vote').live( 'click', function() {
				var $voteBtn = $(this);
				var $imgName = $voteBtn.siblings('.pic').find('img').attr('src');
				$imgName = $imgName.substr( $imgName.lastIndexOf('/') + 1 );
				$.ajax({
					url: 'action.php?act=contest_ugc_image_vote',
					type: 'POST',
					data: {
						user_ip:           $('.userIP').html(),
						fb_user_id:    $('.fbUserId').html(),
						img_name:     $imgName
					},
					async: false,
					error: function() { alert('Error'); },
					success: function( data ) {
						$voteBtn.addClass('clicked');
						// notify user about the result
						var $message = ( data.indexOf('ok') != -1 ) ? 'Thank you for your vote!' : data;
						$myAlert.show( $('body'), $('.appName').html(), $message );
					}
				});
				return false;
			});

		}; $theBody.ugcGallery();



		// COOL SLIDER
		$.fn.coolSlider = function( options ) {
			var settings = $.extend({
				speed:                    250,
				slideWidth:         150,
				slideMargin:       15,
				slidesVisible:      1,
				currentSlide:      0
			}, options );

			var $body = $(this);
			var $arrowL = $body.find('.arrow.left');
			var $arrowR = $body.find('.arrow.right');
			var $elementsNumber = $body.find('li').length;
			var $displacement = settings.slideWidth + settings.slideMargin;
			var $containerWidth = $displacement * $elementsNumber;
			var $currentSlide = settings.currentSlide;

			$body.mousedown( function() { return false; });

			// set container width and scroll to the required slide
			$body.find('ul').css({ width: $containerWidth, left: -$displacement * $currentSlide });

			// load media
			$body.find('li:empty').each( function() {
				var $url = $(this).attr('data-url');
				var $item;
				if ( $url.indexOf('youtu') != -1 ) $item = $displayWidget.createYoutube( $url );
				else if ( $url.indexOf('vimeo') != -1 ) $item = $displayWidget.createVimeo( $url );
				else $item = '<img src="' + $url + '" alt="" /><span />';
				$(this).html( $item );
			});

			// show arrows
			if ( $currentSlide > 0 ) $arrowL.show();
			if ( $currentSlide < $elementsNumber - settings.slidesVisible ) $arrowR.show();

			// slide
			$arrowL.add( $arrowR ).click( function() {
				if ( ! $body.find('ul').is(':animated') ) {
					( $(this).is( $arrowL ) ) ? $currentSlide -= 1 : $currentSlide += 1;
					$body.find('ul').animate({ left: -$displacement * $currentSlide }, settings.speed );
					// update arrows
					( $currentSlide == 0 ) ? $arrowL.hide() : $arrowL.show();
					( $currentSlide == $elementsNumber - settings.slidesVisible ) ? $arrowR.hide() : $arrowR.show();
				} // not animated
			});
		};
		// call function
		$theBody.find('.coolSlider').each( function() {
			$(this).coolSlider({
				slideWidth: parseInt( $(this).attr('data-width') ),
				slidesVisible: parseInt( $(this).attr('data-visible') ),
				slideMargin: parseInt( $(this).find('li').css('margin-right') )
			});
		});



		// TRANSFORM STATE AND CARRIER INPUTS INTO SELECTS

		// load contest includes
		$displayWidget.ajaxGetFileContent('includes/us_states.html');
		$displayWidget.ajaxGetFileContent('includes/us_carriers.html');
		$displayWidget.ajaxGetFileContent('includes/ca_provinces.html');
		$displayWidget.ajaxGetFileContent('includes/ca_carriers.html');

		setTimeout( function() {
			var $contest_US_states = $('#us_states').html();
			var $contest_US_carriers = $('#us_carriers').html();
			var $contest_CA_provinces = $('#ca_provinces').html();
			var $contest_CA_carriers = $('#ca_carriers').html();

			// save original inputs
			var $inputState = $('<div />').append( $('[name="State"]').clone() ).html();
			var $inputCarrier = $('<div />').append( $('[name="Cellular_Carrier"]').clone() ).html();

			// function replaces text input by select with the same name
			function replaceStateOrCarrier( $inputName, $options ) {
				var $newSelect = '<select name="' + $inputName + '">' + $options + '</select>';
				$('[name="' + $inputName + '"]').after( $newSelect ).remove();
			}

			// transform after changing the country
			$('[name="Country"]').change( function() {
				// show US states
				if ( $(this).val() == 'US' && $('[name="State"]').length ) {
					replaceStateOrCarrier( 'State', $contest_US_states );
				}
				// show US carriers
				if ( $(this).val() == 'US' && $('[name="Cellular_Carrier"]').length ) {
					replaceStateOrCarrier( 'Cellular_Carrier', $contest_US_carriers );
				}
				// show CA provinces
				if ( $(this).val() == 'CA' && $('[name="State"]').length ) {
					replaceStateOrCarrier( 'State', $contest_CA_provinces );
				}
				// show CA carriers
				if ( $(this).val() == 'CA' && $('[name="Cellular_Carrier"]').length ) {
					replaceStateOrCarrier( 'Cellular_Carrier', $contest_CA_carriers );
				}
				// restore inputs
				if ( $(this).val() != 'US' && $(this).val() != 'CA' ) {
					$('select[name="State"]').after( $inputState ).remove();
					$('select[name="Cellular_Carrier"]').after( $inputCarrier ).remove();
				}
				// add css styles to added selects
				$('select[name="State"], select[name="Cellular_Carrier"]').attr({
					style: $('[name="Country"]').attr('style')
				});
			}); // change
		}, 1000 );


	} // displayWidgetsOnload()


	// документ UTF-8
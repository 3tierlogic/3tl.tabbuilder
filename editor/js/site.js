
	// DON'T FORGET TO UPDATE THESE PATHS

	// website URL
	var $websiteURL = 'https://platform.3tierlogic.com/tabbuilder1_3/editor/';
	// folder with all uploaded images
	var $uploadedImagesFolder = 'https://platform.3tierlogic.com/tabbuilder1_3/editor/uploads/';



	//------------------------- GLOBAL VARS-----------------------------//


	// create a new element (true) or edit the old one (false)
	var $createNewElement = true;
	// the element, currently being edited
	var $focusedElement = null;
	// the page container, currently being edited
	var $focusedContainer = null;
	// defines, what function to run, when an image has been uploaded
	var $uploadedImageRefersTo = null;
	// there're different sripts for cases, when user uploads an image or not after editing
	var $userEditsUploadedImage = false;
	// ajax loading image
	var $ajaxLoader = '#ajaxLoader';
	// ajax request message container
	var $ajaxMessage = '#ajaxMessage';
	// iframe for uploading files
	var $ajaxIframe = '#ajaxResult';
	// page containers
	var $pageBody = 'pageBody';
	var $nonfansClass = 'nonfans';
	var $fansClass = 'fans';
	var $contestFansClass = 'contest-fans';
	var $contestFormClass = 'contest-form';
	var $contestUGCFormClass = 'contest-ugc-form';
	var $contestUGCGalleryClass = 'contest-ugc-gallery';
	var $contestThankClass = 'contest-thank';
	// field outer for contest form
	var $contestFieldClass = 'contestFormField';
	// hidden fields in contest popup
	var $hiddenPopupFields = 'hiddenPopupFields';
	// classes names for added widgets
	var $block = 'block';
	var $borders = 'borders';
	var $overlay = 'overlay';
	var $buttons = 'buttons';
	var $buttonDelete = 'buttonDelete';
	var $buttonEdit = 'buttonEdit';
	// position of a new element
	var $newPosX = $newPosY = null;
	// new textarea, which is used to replace an old one, after closing the popup
	var $textareaForTextWidget = '<textarea cols="40" rows="10" name="text" maxlength="1500"></textarea>';
	// enable changing page height
	var $enableChangingPageHeight = false;
	// contest includes
	var $contestCountries = null;
	// array of numbers, which widget images need to be updated
	var arrImagesToUpdate = [];
	// variables for widget, when more than 1 image can be upload
	var _postFeedImgImage = _postFeedBtnImage = '';
	// check file before uploading, must be the same with ones in "functions.php"
	var $maxFileSize = 1024 * 1024 * 1;



	//------------------- AUXILIARY FUNCTIONS ----------------------//


	// HEX CONVERTER
	function hexConverter( $color ) {
		if ( $color.indexOf('#') == -1 ) {
			var hex = '#';
			$.each( $color.substring(4).split(','), function ( i, str ) {
				var h = ( $.trim(str.replace(')','') ) * 1 ).toString(16);
				hex += (h.length == 1) ? "0" + h : h;
			});
		} else {
			var hex = $color;
		}
		return hex;
	}



	// GENERATE UNIQUE ID
	function generateId() {
		return '' + new Date().getTime();
	}



	// PARSE SERVER RESPONSE AND GET IMAGES NAMES
	function getUploadedImgName( $response, $number ) {
		var $divider = $response.indexOf('|');
		var $img = [
			$response.substr( 0, $divider ),
			$response.substr( $divider + 1 )
		];
		return $img[$number];
	}



	// CALCULATE CONTEST SELECT WIDTH DIFFERENCE
	function contestSelectWidth( $field, $borderInput ) {
		var $padding;
		if ( $field.find('textarea, [type="text"]').length )
			$padding = parseInt( $field.find('textarea, [type="text"]').css('paddingLeft') ) * 2;
		else
			$padding = 12;

		var $borders = $borderInput.val() != '' ? ( parseInt( $borderInput.val() ) * 2 ) : 2;

		return $borders + $padding;
	} // contestSelectWidth()



	// REPLACE SPACES IN FIELD NAMES
	function replaceSpaces( $str ) {
		return $str.replace( / /g, '_' );
	}



	// TOGGLE CONTEST TEXT INPUT VALIDATION
	function toggleTextInputValidation( $action ) {
		var $field = $('[name="contestTextValidation"]').parent('.field');
		if ( $action == 'show' ) $field.show();
		else if ( $action == 'hide' ) $field.hide();
	}



	// RESTORE SELECTS' SELECTED OPTION AFFTER DRAGGING
	function restoreSelectedOption() {
		$('select').each( function() {
			if ( $(this).find('[data-selected]').length )
				$(this).find('[data-selected]').attr( 'selected', true );
		});
	}



	// CONTEST BUTTON WIDGET - CLEAR URL AFTER BUTTON TYPE CHANGING
	$('[name="contestBtnType"]').live( 'change', function() {
		$('[name="contestBtnImageURL"]').val('');
	});



	// CHANGE PAGE HEIGHT
	function pageBodyHeight() {

		if ( $enableChangingPageHeight == false ) {
			$enableChangingPageHeight = true;

			var $input = $('[name="pageHeight"]');
			var $buttonIncrease = $('#popup_pageHeight .plus');
			var $buttonDecrease = $('#popup_pageHeight .minus');
			var $interval;
			var $intervalStep = 3;			// both $intervalStart and $intervalEnd must be divisible by $intervalStep
			var $intervalStart = 150;		// initial delay between value changes
			var $intervalEnd = 15;			// finite delay

			function increaseValue() {
				var $val = parseInt( $input.val() ) + 1;
				$input.val( $val );
				$focusedContainer.height( $val );
			}

			function decreaseValue() {
				var $val = parseInt( $input.val() ) - 1;
				$input.val( $val );
				$focusedContainer.height( $val );
			}

			function mousedownIncrease() {
				$interval = setTimeout( function() {
					increaseValue();
					if ( $newIntervalStart > $intervalEnd ) $newIntervalStart -= $intervalStep;
					mousedownIncrease();
				}, $newIntervalStart );
			}

			function mousedownDecrease() {
				$interval = setTimeout( function() {
					decreaseValue();
					if ( $newIntervalStart > $intervalEnd ) $newIntervalStart -= $intervalStep;
					mousedownDecrease();
				}, $newIntervalStart );
			}

			// user enters height
			$input.keyup( function() { $focusedContainer.height( $(this).val() ); });

			// user clicks increase/decrease buttons
			$buttonIncrease.click( function() { increaseValue(); });
			$buttonDecrease.click( function() { decreaseValue(); });

			// user hold mouse button after clicking on increase/decrease buttons
			$buttonIncrease.add( $buttonDecrease ).mousedown( function() {
				var $newIntervalStart = $intervalStart;
				if ( $(this).is( $buttonIncrease ) )
					mousedownIncrease();
				else
					mousedownDecrease();
				return false;
			}).mouseup( function() {
				clearTimeout( $interval );
				$newIntervalStart = $intervalStart;
			}).mouseleave( function() {
				clearTimeout( $interval );
				$newIntervalStart = $intervalStart;
			});

		} // $enableChangingPageHeight == false
	} // pageBodyHeight() end



	// INCREASE PAGE HEIGHT IF CONTENT OVERFLOWS
	function noOverflowingContent( $element ) {
		var $parent = $element.parents( '.' + $pageBody );
		var $requiredHeight = $element.height() + parseInt( $element.css('top') );
		if ( $requiredHeight > $parent.height() ) {
			if ( confirm('Widget content overflows. Increase the page height?') )
				$parent.height( $requiredHeight );
		}
	}



	// ADDED WIDGETS MUST BE DRAGGABLE AND RESIZABLE
	function widgetDR( $element ) {
		// temporarily change $focusedContainer
		var $currentFocusedContainer = $focusedContainer;
		$focusedContainer = $element.parents( '.' + $pageBody );
		// add auxiliary divs and buttons, if they don't exist
		if ( $element.find( '.' + $borders ).length == 0 ) {
			var $output = '<div class="' + $borders + '"></div>';
			$output += '<div class="' + $overlay + '"></div>';
			$output += '<div class="' + $buttons + '">';
				$output += '<span class="' + $buttonDelete + '">Delete</span>';
				$output += '<span class="' + $buttonEdit + '">Edit</span>';
			$output += '</div>';
			$element.append( $output );
		}
		// if content overflows the page - then increase page hight
		noOverflowingContent( $element );
		// some elements takes more time to load
		setTimeout( function() { noOverflowingContent( $element ); }, 2000 );
		// resizable
		$element.resizable({
			containment: 'parent',
			start: function(event, ui) {
				// find possible elements for resizing
				var $a = ui.helper;
				var $toResize = null;
				if ( $a.find('.contentContactForm').length ) $toResize = $a.find('.contentContactForm, img');
				else if ( $a.find('.contentHotspot').length ) $toResize = $a.find('.contentHotspot');
				else if ( $a.find('.contentGoogleMaps').length ) $toResize = $a.find('.contentGoogleMaps');
				else if ( $a.find('.contestUGCUploadArea').length ) $toResize = $a.find('.contestUGCUploadArea, .contestUGCUploadArea img');
				else if ( $a.find('.contestButton').length )
					$toResize = $a.find('.contestButton').add( $a.find('.contestButton img') );
				else if ( $a.find('img').length ) $toResize = $a.find('img');
				else if ( $a.find('iframe').length ) $toResize = $a.find('iframe');
				else if ( $a.find('object').length ) $toResize = $a.find('object');
				// if element is found, set 100% width and height to it
				if ( $toResize != null ) $toResize.addClass('resized');
			}
		});
		// draggable
		$element.draggable('destroy');
		if ( $focusedContainer.find('.grid').length && $focusedContainer.find('.grid').attr('data-snap') == 'yes' ) {
			var $grid = $focusedContainer.find('.grid').attr('data-size');
			$element.draggable({
				containment: 'parent', handle: '.' + $overlay, stack: '.' + $block, grid: [ $grid, $grid ],
				stop: function() { restoreSelectedOption(); }
			})
			.find( '.' + $overlay ).resizable({ containment: '.' + $pageBody, alsoResize: $element });
		} else {
			$element.draggable({
				containment: 'parent', handle: '.' + $overlay, stack: '.' + $block,
				stop: function() { restoreSelectedOption(); }
			})
			.find( '.' + $overlay ).resizable({ containment: '.' + $pageBody, alsoResize: $element });
		}
		// restore $focusedContainer
		$focusedContainer = $currentFocusedContainer;
		// just added videos must look the same in all browsers
		if ( $element.find('.youtube, .vimeo').length && ! $element.find('.youtube, vimeo').is('resized') ) {
			$element.css({ width: 200, height: 200 }).find('.youtube, .vimeo').addClass('resized');
		}
	} // widgetDR()



	// CALL A POPUP, WHEN AN ICON WAS DROPPED TO THE PAGE
	function pageDroppable() {
		$focusedContainer.droppable({
			drop: function( event, ui ) {
				// get coordinates of the dropped element
				$newPosX = ui.offset.left - $(this).offset().left;
				$newPosY = ui.offset.top - $(this).offset().top;
				// save the class of the dropped icon
				var $iconClass = ui.draggable.attr('class');
				$iconClass = $iconClass.split(' ');
				$iconClass = $iconClass[0];
				// clear all forms
				$('select').each( function() { $(this).find('option:first').attr( 'selected', true ); });
				$('[type="checkbox"]').each( function() { $(this).attr( 'checked', false ); });
				$('[type="text"], textarea').each( function() {
					if ( ! $(this).is('[name="pageHeight"]') ) $(this).val('');
				});
				// contest - hide fields, that must be hidden by default
				$( '.' + $hiddenPopupFields ).hide();
				// enable field type select
				$('#contestTextFieldType, #contestTextareaFieldType, #contestSelectFieldType, #contestRadioFieldType, #contestCheckboxFieldType').attr( 'disabled', false );
				// hide validation options for contest text fields
				toggleTextInputValidation('hide');
				// show fancybox
				$('a[href="#popup_' + $iconClass + '"]').trigger('click');
			}
		});
	}



	// COLOR PICKER FOR POPUPS
	function popupColorPicker( $element, $color ) {
		$element.ColorPicker({
			color: $color,
			onShow: function(colpkr) {
				$(colpkr).fadeIn(400);
				return false;
			},
			onHide: function(colpkr) {
				$(colpkr).fadeOut(400);
				return false;
			},
			onChange: function(hsb, hex, rgb) {
				$element.css({ backgroundColor: '#' + hex });
				if ( $element.is('.bgPicker') ) $focusedContainer.css({ backgroundColor: '#' + hex });
			}
		}); // color picker
		// load new color
		$element.css({ backgroundColor: $color }).ColorPickerSetColor( $color );
	}



	// TEXT WIDGET - FONT STYLES
	function addFontStyles() {
		$('.contentText font').each(function() {
			var $size = $(this).attr('size');
			$(this).removeClass().addClass( 'size' + $size );
		});
	}



	// GET BACKGROUND IMAGE URL
	function getBgImgUrl( $bg, $returnFilename ) {
		var $from = $bg.indexOf('http');
		for ( $i = $bg.length - 1; $i > 0; $i-- ) {
			if ( $bg.charAt( $i ).match(/\w/) ) { var $to = $i + 1; break; }
		}
		$url = $bg.substring( $from, $to );
		// we can return...
		$returnFilename = typeof( $returnFilename ) != 'undefined' ? true : false;
		// ...only filename...
		if ( $returnFilename ) return $url.substr( $url.lastIndexOf('/') + 1 );
		// ...or whole url
		else return $url;
	}



	// UPDATE BACKGROUND
	function updateBG( $bgURL ) {
		// update background repeat
		$focusedContainer.css( 'backgroundRepeat', $('#bgRepeat').val() );
		// update background image?
		$bgURL = typeof( $bgURL ) != 'undefined' ? $bgURL : null;
		if ( $bgURL != null ) {
			if ( $bgURL == '' ) {
				// remove bg image
				$focusedContainer.css( 'backgroundImage', '' );
			} else {
				// if path is relative...
				if ( $bgURL.indexOf('http') == -1 ) $bgURL = $uploadedImagesFolder + $bgURL;
				// update
				$focusedContainer.css( 'backgroundImage', 'url(' + $bgURL + ')' );
			}
		}
	} // updateBG() end



	// CREATE OR UPDATE CONTENT IMAGE
	function createUpdateImage( $imageURL, $imageLinkURL ) {
		// if path is relative, make it absolute
		if ( $imageURL.indexOf('http') == -1 ) $imageURL = $uploadedImagesFolder + $imageURL;
		// update...
		if ( $createNewElement == false ) {
			// ...src
			if ( $imageURL != '' ) {
				$focusedElement.find('.contentImg').attr( 'src', $imageURL );
			}
			// ...link
			// if href is empty, remove the link
			if ( $imageLinkURL == '' && $focusedElement.find('.contentImg').parent('a').length ) {
				$focusedElement.find('.contentImg').unwrap();
			}
			// if href is full
			else if ( $imageLinkURL != '' ) {
				// if link doesn't exist, wrap it arount the image
				if ( $focusedElement.find('.contentImg').parent('a').length == 0 )
					$focusedElement.find('.contentImg').wrap('<a target="_blank" href="" />');
				// define new url
				$focusedElement.find('.contentImg').parent('a').attr( 'href', $imageLinkURL );
			}
			return null;
		}
		// create
		else {
			// create without link
			if ( $imageLinkURL == '' ) {
				$output = '<img class="contentImg" src="' + $imageURL + '" alt="" />';
			}
			// create with link
			else {
				$output = '<a target="_blank" href="' + $imageLinkURL + '">';
				$output += '<img class="contentImg" src="' + $imageURL + '" alt="" /></a>';
			}
			return $output;
		}
	} // createUpdateImage() end



	// CREATE OR UPDATE FB INVITE BUTTON
	function createUpdateInviteButton( $title, $message, $btnURL ) {
		// button image url defined?
		$btnURL = $btnURL != '' ? $btnURL : $websiteURL + 'images/buttonInvite.png';
		// if path is relative, make it absolute
		if ( $btnURL.indexOf('http') == -1 ) $btnURL = $uploadedImagesFolder + $btnURL;
		// create
		if ( $createNewElement ) {
			var $output = '<img class="contentInvite" src="' + $btnURL + '" alt="" ';
			$output += 'data-title="' + $title + '" data-message="' + $message + '" />';
			return $output;
		}
		// update
		else {
			var $btnInvite = $focusedElement.find('.contentInvite');
			$btnInvite.attr({
				'data-title': $title,
				'data-message': $message,
				'src': $btnURL
			});
		}
	}



	// CREATE OR UPDATE FB POST TO FEED
	function createUpdateFbPostFeed( $pLink, $pName, $pDescr, $pCaption ) {

		// check post image
		if ( _postFeedImgImage != '' ) {
			// if path is relative, make it absolute
			if ( _postFeedImgImage.indexOf('http') == -1 )
				_postFeedImgImage = $uploadedImagesFolder + _postFeedImgImage;
		}

		// check button
		if ( _postFeedBtnImage != '' ) {
			// if path is relative, make it absolute
			if ( _postFeedBtnImage.indexOf('http') == -1 )
				_postFeedBtnImage = $uploadedImagesFolder + _postFeedBtnImage;
		} else {
			// if no url defined, use standard button
			_postFeedBtnImage = $websiteURL + 'images/buttonShare.png';
		}

		// create
		if ( $createNewElement ) {
			$output = '<img class="contentFbPostFeed" src="' + _postFeedBtnImage + '" alt="" ';
			$output += 'data-url="' + $pLink + '" data-name="' + $pName + '" data-descr="' + $pDescr + '" ';
			$output += 'data-img="' + _postFeedImgImage + '" data-caption="' + $pCaption + '" />';
			return $output;
		}
		// update
		else {
			var $postFeed = $focusedElement.find('.contentFbPostFeed');
			$postFeed.attr({
				'data-url':            $pLink,
				'data-name':      $pName,
				'data-descr':       $pDescr,
				'data-caption':   $pCaption,
				'data-img':          _postFeedImgImage,
				'src':                      _postFeedBtnImage
			});
		}
	} // createUpdateFbPostFeed()



	// CREATE OR UPDATE CONTACT FORM
	function createUpdateContactForm( $cAddress, $cbURL, $cName, $cPhone ) {
		// button image url defined?
		$cbURL = $cbURL != '' ? $cbURL : $websiteURL + 'images/buttonEmail.png';
		// if path is relative, make it absolute
		if ( $cbURL.indexOf('http') == -1 ) $cbURL = $uploadedImagesFolder + $cbURL;
		// create
		if ( $createNewElement ) {
			var $newContactForm = '<a href="mailto:' + $cAddress + '"><img src="' + $cbURL + '" alt="" /></a>';
			if ( $cName ) $newContactForm += '<div class="name" />';
			if ( $cPhone ) $newContactForm += '<div class="phone" />';
			var $output = '<div class="contentContactForm">' + $newContactForm + '</div>';
			return $output;
		}
		// update
		else {
			var $contactForm = $focusedElement.find('.contentContactForm');
			// address
			if ( $contactForm.find('a').attr('href').substr(7) != $cAddress )
				$contactForm.find('a').attr( 'href', 'mailto:' + $cAddress );
			// button url
			if ( $contactForm.find('img').attr('src') != $cbURL )
				$contactForm.find('img').attr( 'src', $cbURL );
			// input name
			if ( $cName && $contactForm.find('.name').length == 0 )
				$contactForm.append('<div class="name" />');
			else if ( ! $cName && $contactForm.find('.name').length )
				$contactForm.find('.name').remove();
			// input phone
			if ( $cPhone && $contactForm.find('.phone').length == 0 )
				$contactForm.append('<div class="phone" />');
			else if ( ! $cPhone && $contactForm.find('.phone').length )
				$contactForm.find('.phone').remove();
		}
	}



	// CREATE OR UPDATE EMAIL BUTTON
	function createUpdateEmailButton( $eAddress, $ebURL ) {
		// button image url defined?
		$ebURL = $ebURL != '' ? $ebURL : $websiteURL + 'images/buttonEmail.png';
		// if path is relative, make it absolute
		if ( $ebURL.indexOf('http') == -1 ) $ebURL = $uploadedImagesFolder + $ebURL;
		// create
		if ( $createNewElement ) {
			var $output = '<a class="contentEmail" href="mailto:' + $eAddress + '">';
			$output += '<img src="' + $ebURL + '" alt="" /></a>';
			return $output;
		}
		// update
		else {
			var $eButton = $focusedElement.find('.contentEmail');
			// address
			if ( $eButton.attr('href').substr(7) != $eAddress )
				$eButton.attr( 'href', 'mailto:' + $eAddress );
			// button url
			if ( $eButton.find('img').attr('src') != $ebURL )
				$eButton.find('img').attr( 'src', $ebURL );
		}
	}



	// CREATE OR UPDATE TWITTER SHARE BUTTON
	function createUpdateTwitterShareButton( $account, $text, $url, $buttonURL ) {
		// button image url defined?
		$buttonURL = $buttonURL != '' ? $buttonURL : $websiteURL + 'images/buttonTwitterShare.png';
		// if path is relative, make it absolute
		if ( $buttonURL.indexOf('http') == -1 ) $buttonURL = $uploadedImagesFolder + $buttonURL;
		// twitter link
		var $twitterLink = '';
		if ( $url != '' ) {
			$twitterLink = 'http://twitter.com/share?related=' + $account;
			$twitterLink += '&text=' + encodeURIComponent( $text );
			$twitterLink += '&url=' + encodeURIComponent( $url );
		}
		// create
		if ( $createNewElement ) {
			var $output = '<a class="contentTwitterShare" target="_blank" href="' + $twitterLink + '" ';
			$output += 'data-account="' + $account + '" data-text="' + $text + '" data-url="' + $url + '">';
			$output += '<img src="' + $buttonURL + '" alt="" /></a>';
			return $output;
		}
		// update
		else {
			var $button = $focusedElement.find('.contentTwitterShare');
			$button.attr( 'href', $twitterLink );
			$button.attr( 'data-account', $account );
			$button.attr( 'data-text', $text );
			$button.attr( 'data-url', $url );
			$button.find('img').attr( 'src', $buttonURL );
		}
	}



	// CREATE OR UPDATE CONTEST BUTTON
	function createUpdateContestButton( $btnType, $btnURL ) {

		// get image url for a button
		function getBtnUrl( $btnType, $btnURL ) {
			// button image url defined?
			if ( $btnURL == '' ) {
				switch( $btnType ) {
					case 'typeHome':
						$btnURL = $websiteURL + 'images/buttonHome.png';
						break;
					case 'typeContestEnter':
						$btnURL = $websiteURL + 'images/buttonContestEnter.png';
						break;
					case 'typeContestSubmit':
						$btnURL = $websiteURL + 'images/buttonContestSubmit.png';
						break;
					case 'typeUgcView':
						$btnURL = $websiteURL + 'images/buttonContestUGCView.png';
						break;
					case 'typeUgcPreview':
						$btnURL = $websiteURL + 'images/buttonContestUGCPreview.png';
						break;
					case 'typeUgcUpload':
						$btnURL = $websiteURL + 'images/buttonContestUGCUpload.png';
						break;
				}
			}
			// if path is relative, make it absolute
			if ( $btnURL.indexOf('http') == -1 ) $btnURL = $uploadedImagesFolder + $btnURL;
			return $btnURL;
		} // getBtnUrl()

		// create
		if ( $createNewElement ) {
			var $output = $btnClass = $additionalElements = '';
			switch( $btnType ) {
				case 'typeHome': $btnClass = 'returnHome'; break;
				case 'typeContestEnter': $btnClass = 'contestEnter'; break;
				case 'typeContestSubmit': $btnClass = 'contestSubmit'; break;
				case 'typeUgcView': $btnClass = 'contestUGCView'; break;
				case 'typeUgcPreview': $btnClass = 'contestUGCPreview'; break;
				case 'typeUgcUpload':
					$btnClass = 'contestUGCUpload';
					$additionalElements = '<input type="file" name="contestUGCUploadImg" title="" />';
					break;
			}
			$output = '<div class="contestButton">';
			$output += '<img src="' + getBtnUrl( $btnType, $btnURL ) + '" alt="" ';
			$output += 'data-type="' + $btnType + '" class="' + $btnClass + '" />';
			$output += $additionalElements;
			$output += '</div>';
			return $output;
		} // create

		// update
		else {
			var $btnClass;
			switch( $btnType ) {
				case 'typeHome': $btnClass = 'returnHome'; break;
				case 'typeContestEnter': $btnClass = 'contestEnter'; break;
				case 'typeContestSubmit': $btnClass = 'contestSubmit'; break;
				case 'typeUgcView': $btnClass = 'contestUGCView'; break;
				case 'typeUgcPreview': $btnClass = 'contestUGCPreview'; break;
				case 'typeUgcUpload': $btnClass = 'contestUGCUpload'; break;
			}
			var $btn = $focusedElement.find('img');
			$btn.attr({
				'class': $btnClass,
				'data-type': $btnType,
				'src': getBtnUrl( $btnType, $btnURL )
			});
			// add or remove file input for ugc upload button
			if ( $btnType == 'typeUgcUpload' && ! $btn.siblings(':file').length )
				$btn.after('<input type="file" name="contestUGCUploadImg" />');
			else if ( $btnType != 'typeUgcUpload' && $btn.siblings(':file').length )
				$btn.siblings(':file').remove();
		} // update

	} // createUpdateContestButton()



	// CREATE OR UPDATE UGC GALLERY
	function createUpdateUgcGallery( $grid, $bg, $border, $text, $btn, $btnText ) {
		// create
		if ( $createNewElement ) {
			var $output = '<div class="ugcGallery layout' + $grid + '" data-layout="' + $grid + '" ';
			$output += 'style="background: ' + $bg + '; border-color: ' + $border + '; color: ' + $text + ';" ';
			$output += 'data-bg-color="' + $bg + '" data-border-color="' + $border + '" data-text-color="' + $text + '" ';
			$output += 'data-btn-color="' + $btn + '" data-btn-text-color="' + $btnText + '">';
				$output += '<div class="css"><style>';
					$output += '.ugcGallery .pic { background: ' + $border + '; }';
					$output += '.ugcGallery .aButton { background: ' + $btn + '; color: ' + $btnText + ' !important; }';
				$output += '</style></div>';
				$output += '<div class="controlBar">';
					$output += '<input type="text" name="ugcGallerySearch" />';
					$output += '<a class="aButton search">Search</a>';
					$output += '<a class="aButton showAll">Show All</a>';
					$output += '<div class="pagination">';
						$output += '<span>Page</span>';
						$output += '<select id="ugcPagination"><option>1</option></select>';
					$output += '</div>';
				$output += '</div>';
				$output += '<div class="in"><ul>';
					var $previewItemsNum = ( $grid == '5x2' ) ? 10 : 15;
					for ( $i = 0; $i < $previewItemsNum; $i++ ) $output += '<li />';
				$output += '</ul></div>';
			$output += '</div>';
			return $output;
		}
		// update
		else {
			var $ug = $focusedElement.find('.ugcGallery');
			var $newCss = '<style>';
			$newCss += '.ugcGallery .pic { background: ' + $border + '; }';
			$newCss += '.ugcGallery .aButton { background: ' + $btn + '; color: ' + $btnText + ' !important; }';
			$newCss += '</style>';
			$ug.attr({
				'class': 'ugcGallery layout' + $grid,
				'data-layout': $grid,
				'data-bg-color': $bg,
				'data-border-color': $border,
				'data-text-color': $text,
				'data-btn-color': $btn,
				'data-btn-text-color': $btnText
			}).css({
				'background': $bg,
				'border-color': $border,
				'color': $text
			}).find('.css').html( $newCss );
		}
	} // createUpdateUgcGallery()




	//--------------------- SAVE / LOAD PAGES -------------------------//


	// SHOW AJAX REQUEST MESSAGE
	function showAjaxMessage( $message ) {
		$( $ajaxMessage ).fadeIn().html( $message );
		setTimeout(function () { $( $ajaxMessage ).fadeOut(); }, 2000 );
	}



	// GET ID BY NAME
	function ajaxGetIdByName( $name ) {
		var $id = false;
		$.ajax({
			url: 'action.php?act=get_id_by_name&name=' + $name,
			type: 'POST',
			async: false,
			beforeSend: function() { $( $ajaxLoader ).fadeIn(); },
			complete: function() { $( $ajaxLoader ).fadeOut(); },
			success: function( data ) { $id = data; }
		});
		return $id;
	} // ajaxGetIdByName()



	// DELETE FILE
	function ajaxDeleteFile( $fileID ) {
		var $result = false;
		$.ajax({
			url: 'action.php?act=delete_file&id=' + $fileID,
			type: 'POST',
			async: false,
			beforeSend: function () { $( $ajaxLoader ).fadeIn(); },
			complete: function () { $( $ajaxLoader ).fadeOut(); },
			success: function () { $result = true; }
		});
		return $result;
	} // ajaxDeleteFile



	// BLANK PAGE
	function blankPage() {
		$('.pageBodyInner').html('');
		// create new page containers
		$output = '<div class="' + $pageBody + ' ' + $nonfansClass + '" />';
		$output += '<div class="' + $pageBody + ' ' + $fansClass + '" />';
		$output += '<div class="' + $pageBody + ' ' + $contestFansClass + '" />';
		$output += '<div class="' + $pageBody + ' ' + $contestFormClass + '" />';
		$output += '<div class="' + $pageBody + ' ' + $contestUGCFormClass + '" />';
		$output += '<div class="' + $pageBody + ' ' + $contestUGCGalleryClass + '" />';
		$output += '<div class="' + $pageBody + ' ' + $contestThankClass + '" />';
		$('.pageBodyInner').html( $output );
		// set page width and height
		$( '.' + $pageBody ).width(810).height( $('[name="pageHeight"]').val() );
		// select the fan tab as focused and call required functions
		$('.tabSteps li').eq(0).trigger('click');
	}



	// SAVE PAGE
	function ajaxSavePage( id, url, successMessage, errorMessage, preview ) {
		var $content = $('.pageBodyInner').clone();

		// get contest properties
		var $oForm = $content.find( '.' + $pageBody + '.' + $contestFormClass );
		var $oDescr = $oForm.is('[data-contest-descr]') ? $oForm.attr('data-contest-descr') : '';
		var $oStart = $oForm.is('[data-contest-start]') ? $oForm.attr('data-contest-start') : '';
		var $oEnd = $oForm.is('[data-contest-end]') ? $oForm.attr('data-contest-end') : '';
		var $oVotingStart = $oForm.is('[data-contest-voting-start]') ? $oForm.attr('data-contest-voting-start') : '';
		var $oVotingEnd = $oForm.is('[data-contest-voting-end]') ? $oForm.attr('data-contest-voting-end') : '';
		// if voting start-end dates are not set, we use campaign start-end dates
		$oVotingStart = ( $oVotingStart != '' ) ? $oVotingStart : $oStart;
		$oVotingEnd = ( $oVotingEnd != '' ) ? $oVotingEnd : $oEnd;
		var $oVotingTimes = $oForm.is('[data-contest-voting-times]') ? $oForm.attr('data-contest-voting-times') : '-1';
		var $oEntries = $oForm.is('[data-contest-entries]') ? $oForm.attr('data-contest-entries') : '';
		var $oAge = $oForm.is('[data-contest-age]') ? $oForm.attr('data-contest-age') : '';

		// get tabs content
		$content
			.find( '.' + $pageBody ).css({ display: 'block' }).end()
			.find('.ui-resizable-handle').remove().end()
			.find('.ui-droppable').removeClass('ui-droppable').end()
			.find('.ui-draggable').removeClass('ui-draggable').end()
			.find('.ui-resizable').removeClass('ui-resizable').end()
			.find('[class=""]').removeAttr('class').end()
			.find( '.' + $block + ' .' + $borders ).remove().end()
			.find( '.' + $block + ' .' + $overlay ).remove().end()
			.find( '.' + $block + ' .' + $buttons ).remove().end()
			.find('.contentHotspot').addClass('savedHidden').end()
			.find('.contentTwitter').removeClass().addClass('contentTwitter').empty().end()
			.find('.contentRSS').empty().end()
			.find('.contentGoogleMaps').empty().end()
			.find('.googlePlus').empty().end()
			.find('.contentCountdown').removeClass('hasCountdown').empty().end()
			.find('fb\\:like, fb\\:send, fb\\:comments').removeClass().html('').end()
			.find('.grid').addClass('hidden').end()
			.find('.coolSlider li').html('').end()
			.find('.block').each( function() {
				if ( $(this).find('fb\\:like').length || $(this).find('fb\\:send').length || $(this).find('fb\\:comments').length ) $(this).css({ zIndex: parseInt( $(this).css('zIndex') ) + 1000 });
			});

		// prepare data for saving
		function prepareToSave( $class ) {
			var $element = $('<div />').append( $content.find( '.' + $pageBody + '.' + $class ).clone() ).html();
			return escape( $element );
		}

		var $cNonFans = prepareToSave( $nonfansClass );
		var $cFans = prepareToSave( $fansClass );
		var $cContestFans = prepareToSave( $contestFansClass );
		var $cContestForm = prepareToSave( $contestFormClass );
		var $cContestUGCForm = prepareToSave( $contestUGCFormClass );
		var $cContestUGCGallery = prepareToSave( $contestUGCGalleryClass );
		var $cContestThank = prepareToSave( $contestThankClass );
		var $contest = $('.tabSteps ul').is('.noContest') ? '0' : '1';
		var $ugc = $('.tabSteps ul').is('.noUGC') || $contest == '0' ? '0' : '1';

		// save
		function savePage() {
			$.ajax({
				url: url,
				type: 'POST',
				data: {
					content_nonfans:                              $cNonFans,
					content_fans:                                      $cFans,
					content_contest_fans:                     $cContestFans,
					content_contest_form:                   $cContestForm,
					content_contest_ugc_form:         $cContestUGCForm,
					content_contest_ugc_gallery:      $cContestUGCGallery,
					content_contest_thank:                 $cContestThank,
					contest:                                                  $contest,
					ugc:                                                          $ugc,
					contest_descr:                                     $oDescr,
					contest_start:                                      $oStart,
					contest_end:                                        $oEnd,
					contest_voting_start:                       $oVotingStart,
					contest_voting_end:                         $oVotingEnd,
					contest_voting_times:                     $oVotingTimes,
					contest_entries:                                  $oEntries,
					contest_age:                                         $oAge
				},
				beforeSend: function() { $( $ajaxLoader ).fadeIn(); },
				complete: function() { $( $ajaxLoader ).fadeOut(); },
				error: function() { showAjaxMessage ( errorMessage ); },
				success: function() {

					showAjaxMessage( successMessage );

					// update publish/unpublish button
					var $publishBtn = $('a.bigButton.publish');
					if ( successMessage.indexOf('unpublished') != -1 ) {
						$publishBtn.attr({ 'data-published': 0 }).find('i').html('Publish');
					} else if ( successMessage.indexOf('published') != -1 ) {
						$publishBtn.attr({ 'data-published': 1 }).find('i').html('Unpublish');
					}

					// show preview if needed
					if ( preview ) {

						// load preview in iframes
						$('.pageBodyPreview').show();
						var $id = $('a.bigButton.save').attr('href');

						function loadPreview( $class ) {
							$('.pageBodyPreview').append('<div class="preview ' + $class + '" />');
							var $contentToLoad = 'preview.php?content=' + $class + '&id=' + $id + ' .' + $pageBody;
							$('.pageBodyPreview .preview:last').load( $contentToLoad, function() {
								// display widgets
								displayWidgetsOnload( $(this) );
								// after all previews are loaded
								if ( $(this).is( '.' + $contestThankClass ) ) {
									// hide unnecessary elements
									$('.editorButtons').find('a').hide().end().find('a.publish').show();
									$('.editorInstruments').add('.contentRuler').css({ visibility: 'hidden' });
									var $switch = $('.fbArea').find('.contentTypeSwitch.forPreview');
									if ( $('.tabSteps ul').is('.noContest.noUGC') )
										$switch.addClass('noContest noUGC');
									else if ( $('.tabSteps ul').is('.noUGC') )
										$switch.removeClass('noContest').addClass('noUGC');
									else
										$switch.removeClass('noContest noUGC');
									$switch.show().find('a.nonfans').click();
								}
							});
						} // loadPreview()

						loadPreview( $nonfansClass );
						loadPreview( $fansClass );
						loadPreview( $contestFansClass );
						loadPreview( $contestFormClass );
						loadPreview( $contestUGCFormClass );
						loadPreview( $contestUGCGalleryClass );
						loadPreview( $contestThankClass );

						// hide working area
						$('.pageBodyInner').hide();

					} // preview end

				} // success
			});
		} // savePage()


		// firstly save all contest form fields
		if ( ! $('.contestFormField').length ) savePage();
		else {

			// deactivate existing fields in db
			$.ajax({
				url: 'action.php?act=deactivate_contest_fields&id=' + id,
				success: function() {

					var $fieldsNumber = $('.contestFormField').length;

					// iterate over each field
					$('.contestFormField').each( function( index ) {

						var $input;
						var $label = $(this).find('label').html();

						// text input, textarea, checkbox
						if ( $(this).is('.contestTextInput, .contestTextarea, .contestCheckbox') ) {
							$input = $(this).find('input, textarea, checkbox');
							var $required = $input.is('.required') ? '1' : '0';
							var $error = $input.attr('data-error');
							var $configId = $input.attr('data-config-id');
							$configId = ( typeof $configId !== 'undefined' && $configId !== false ) ? $configId : '';
						}
						// select, radio
						else if ( $(this).is('.contestSelect, .contestRadio') ) {
							$input = $(this).find('select, input');
							var $required = '0';
							var $error = '';
							var $configId = $input.attr('data-config-id');
							$configId = ( typeof $configId !== 'undefined' && $configId !== false ) ? $configId : '';
						}

						var $name;
						switch ( $input.attr('name') ) {
							case 'First_Name':           $name = 'firstname'; break;
							case 'Last_Name':            $name = 'lastname'; break;
							case 'Email':                        $name = 'email'; break;
							case 'Cell_Phone':            $name = 'mobilephone'; break;
							case 'Home_Phone':       $name = 'phonenumber'; break;
							case 'Address':                    $name = 'address'; break;
							case 'City':                            $name = 'city'; break;
							case 'Zip_Code':                $name = 'zipcode'; break;
							case 'State':                          $name = 'state'; break;
							case 'Cellular_Carrier':   $name = 'mobilecarrier'; break;
							case 'Gender':                     $name = 'gender'; break;
							case 'Birthday_Year':       $name = 'birthdayyear'; break;
							case 'Birthday_Month':  $name = 'birthdaymonth'; break;
							case 'Birthday_Day':        $name = 'birthdayday'; break;
							case 'Country':                    $name = 'country'; break;
							case 'SMS_opt_in':           $name = 'smsopt'; break;
							case 'Email_opt_in':        $name = 'emailopt'; break;
							case 'Text_Open_Field_1': $name = 'opentext1'; break;
							case 'Text_Open_Field_2': $name = 'opentext2'; break;
							case 'Text_Open_Field_3': $name = 'opentext3'; break;
							case 'Text_Open_Field_4': $name = 'opentext4'; break;
							case 'Text_Open_Field_5': $name = 'opentext5'; break;
							case 'Text_Open_Field_6': $name = 'opentext6'; break;
							case 'Text_Open_Field_7': $name = 'opentext7'; break;
							case 'Text_Open_Field_8': $name = 'opentext8'; break;
							case 'Text_Open_Field_9': $name = 'opentext9'; break;
							case 'Text_Open_Field_10': $name = 'opentext10'; break;
							case 'Textarea_Open_Field_1': $name = 'opentextarea1'; break;
							case 'Textarea_Open_Field_2': $name = 'opentextarea2'; break;
							case 'Textarea_Open_Field_3': $name = 'opentextarea3'; break;
							case 'Textarea_Open_Field_4': $name = 'opentextarea4'; break;
							case 'Textarea_Open_Field_5': $name = 'opentextarea5'; break;
							case 'Textarea_Open_Field_6': $name = 'opentextarea6'; break;
							case 'Textarea_Open_Field_7': $name = 'opentextarea7'; break;
							case 'Textarea_Open_Field_8': $name = 'opentextarea8'; break;
							case 'Textarea_Open_Field_9': $name = 'opentextarea9'; break;
							case 'Textarea_Open_Field_10': $name = 'opentextarea10'; break;
							case 'Select_Open_Field_1': $name = 'opendropdown1'; break;
							case 'Select_Open_Field_2': $name = 'opendropdown2'; break;
							case 'Select_Open_Field_3': $name = 'opendropdown3'; break;
							case 'Select_Open_Field_4': $name = 'opendropdown4'; break;
							case 'Select_Open_Field_5': $name = 'opendropdown5'; break;
							case 'Select_Open_Field_6': $name = 'opendropdown6'; break;
							case 'Select_Open_Field_7': $name = 'opendropdown7'; break;
							case 'Select_Open_Field_8': $name = 'opendropdown8'; break;
							case 'Select_Open_Field_9': $name = 'opendropdown9'; break;
							case 'Select_Open_Field_10': $name = 'opendropdown10'; break;
							case 'Radio_Open_Field_1': $name = 'openradio1'; break;
							case 'Radio_Open_Field_2': $name = 'openradio2'; break;
							case 'Radio_Open_Field_3': $name = 'openradio3'; break;
							case 'Radio_Open_Field_4': $name = 'openradio4'; break;
							case 'Radio_Open_Field_5': $name = 'openradio5'; break;
							case 'Radio_Open_Field_6': $name = 'openradio6'; break;
							case 'Radio_Open_Field_7': $name = 'openradio7'; break;
							case 'Radio_Open_Field_8': $name = 'openradio8'; break;
							case 'Radio_Open_Field_9': $name = 'openradio9'; break;
							case 'Radio_Open_Field_10': $name = 'openradio10'; break;
							case 'Checkbox_Open_Field_1': $name = 'opencheckbox1'; break;
							case 'Checkbox_Open_Field_2': $name = 'opencheckbox2'; break;
							case 'Checkbox_Open_Field_3': $name = 'opencheckbox3'; break;
							case 'Checkbox_Open_Field_4': $name = 'opencheckbox4'; break;
							case 'Checkbox_Open_Field_5': $name = 'opencheckbox5'; break;
							case 'Checkbox_Open_Field_6': $name = 'opencheckbox6'; break;
							case 'Checkbox_Open_Field_7': $name = 'opencheckbox7'; break;
							case 'Checkbox_Open_Field_8': $name = 'opencheckbox8'; break;
							case 'Checkbox_Open_Field_9': $name = 'opencheckbox9'; break;
							case 'Checkbox_Open_Field_10': $name = 'opencheckbox10'; break;
						} // switch

						// ajax
						var $url = 'action.php?act=save_contest_config&id=' + id + '&config_id=' + $configId;
						$url += '&label=' + $label + '&name=' + $name + '&required=' + $required + '&error=' + $error;
						$.ajax({
							url: $url,
							error: function() { alert('error') },
							success: function( data ) {
								// if a field is saved for the first time, add an attr with its id from db
								if ( $configId == '' ) $input.attr( 'data-config-id', data );
								// after the last field is saved, save the whole page
								if ( index == $fieldsNumber - 1 ) savePage();
							}
						});

					}); // iterate over each field

				} // success
			}); // ajax

		} // save all contest form fields

	} // ajaxSavePage()



	// LOAD PAGE
	function ajaxLoadPage( page_id, errorMessage ) {
		errorMessage = typeof( errorMessage ) != 'undefined' ? errorMessage : 'Error when loading the page';
		var $url = 'action.php?act=load_page&id=' + page_id;

		$.ajax({
			url: $url,
			type: 'POST',
			beforeSend: function() { $( $ajaxLoader ).fadeIn(); },
			complete: function() { $( $ajaxLoader ).fadeOut(); },
			error: function() { showAjaxMessage ( errorMessage ); },
			success: function( data ) {

				var $displayWidget = new displayWidget();

				// append the data
				$('.pageBodyInner').html( data );

				// added widgets must be draggable and resizable
				$( '.' + $block ).each(function() { widgetDR( $(this) ); });

				// show grid
				$('.grid').each( function() {
					if ( $(this).attr('data-restore') != 'hidden' ) $(this).removeClass('hidden');
				});

				// select the fan tab as focused and call required functions
				$('.tabSteps li').eq(0).trigger('click');

				// parse FB includes
				if ( typeof(FB) != 'undefined' && FB != null ) FB.XFBML.parse();

				// parse google plus buttons
				if ( $('.googlePlus').length ) gapi.plusone.go();

				// parse twitter streams
				$('.contentTwitter').each( function() { $displayWidget.parseTwitter( $(this) ); });

				// parse and rss streams
				$('.contentRSS').each(function() { $displayWidget.parseRSS( $(this) ); });

				// parse google plus buttons
				$('.googlePlus').each( function() { $displayWidget.parseGooglePlus( $(this) ); });

				// parse google maps
				$('.contentGoogleMaps').each( function() { $displayWidget.parseGoogleMaps( $(this) ); });

				// parse countdown
				$('.contentCountdown').each( function() { $displayWidget.parseCountdown( $(this) ); });

				// pinterest pin it buttons
				setTimeout( function() {
					if ( $('.contentPinIt').length ) {
						$('.contentPinIt').each( function() {
							$(this).removeClass('unparsed').html( $(this).attr('data-btn') );
						});
						$.ajax({ url: 'http://assets.pinterest.com/js/pinit.js', dataType: 'script', cache: true });
					}
				}, 1000 );

				// load cool slider media
				$('.coolSlider li:empty').each( function() {
					var $url = $(this).attr('data-url');
					var $item;
					if ( $url.indexOf('youtu') != -1 ) $item = $displayWidget.createYoutube( $url );
					else if ( $url.indexOf('vimeo') != -1 ) $item = $displayWidget.createVimeo( $url );
					else $item = '<img src="' + $url + '" alt="" /><span />';
					$(this).html( $item );
				});

			} // success
		});
	} // ajaxLoadPage




										//////////////////////////////
										//                                                    //
										//                ONLOAD                 //
										//                                                    //
										//////////////////////////////

$( function() {


	//-------------------------- DASHBOARD -----------------------------//


	// DASHBOARD - BUTTON CREATE NEW TAB
	$('a.createTab').click( function() {
		$('[name="tab_name"]').val('');
		createTabsMenu();
		// make first step to be active
		$('.createNewTab .steps .li').removeClass('nextIsActive active').eq(0).addClass('active');
		// show the 1st form
		$('.createNewTab').find('.step').hide().eq(0).show();
		// hide steps and content in editor
		$('.myTabs, .tabSteps, .editorContent').hide();
		// show "return to dashboard" button
		$(this).siblings('.hidden').show();
		return false;
	});




	// DASHBOARD - CREATE TABS MENU
	function createTabsMenu() {

		var $body = $('.createNewTab');
		var $steps = $body.find('.steps .li');
		var $forms = $body.find('.step');

		$body.add( $forms.eq(0) ).show();

		$steps.mousedown( function() { return false; });

		function changeTabMenuActiveStep( $element ) {
			$steps.removeClass('nextIsActive active');
			$element.addClass('active');
			$element.prev().addClass('nextIsActive');
		}

		// when user click "add another tab", clear the form and show it again
		$('#tab_add_another').one( 'click', function() {
			$('[name="tab_name"]').val('');
			changeTabMenuActiveStep( $steps.eq(0) );
			$forms.eq(2).hide().end().eq(0).show();
			return false;
		});

		// user chose fan page (step 1)
		$('#fan_page_id_submit').one( 'click', function() {
			changeTabMenuActiveStep( $steps.eq(1) );
			$forms.eq(0).hide().end().eq(1).show();
			return false;
		});

		// user entered tab name (step 2)
		$('#tab_name_submit').one( 'click', function() {

			// validate the form
			$(this).parents('form').validate();

			// the form is valid
			if ( $(this).parents('form').valid() ) {

				var $loader = $('.ajaxLoader');

				var $userId = $('[name="user_id"]').val();
				var $pageId = $('[name="fan_page_id"]').val();
				var $tabName = $('[name="tab_name"]').val();
				var $partnerId = $('[name="partner_id"]').val();

				// check user tabs limit
				$.ajax({
					url: 'action.php?act=tabs_limit&user_id=' + $userId + '&fan_page_id=' + $pageId,
					type: 'POST',
					beforeSend: function() { $loader.show(); },
					error: function() { alert('Error') },
					success: function( data ) {

						// user can't create more tabs
						if ( data == 'limit' ) {
							$loader.hide();
							alert('Sorry, you can not create any more tabs for the chosen page');
							$('a.createTab').click();
						}

						// check tab name
						else if ( data == 'ok' ) {
							$.ajax({
								url: 'action.php?act=check_tab_name&fb_user_id=' + $userId + '&name=' + $tabName,
								type: 'POST',
								error: function() { alert('Error'); },
								success: function( data ) {

									if ( data != 'ok' ) {
										$loader.hide();
										alert('You already have a tab with such name');
										$('a.createTab').click();
									}

									// install on FB
									else {
										$.ajax({
											url: 'facebook/facebook_functions.php?fb_act=app_install&id=' + $pageId,
											type: 'POST',
											error: function() { alert('Error') },
											success: function( data ) {

												if ( data == '' ) {
													$loader.hide();
													alert('Session has expired. You have to login again');
													window.location.href = 'index.php';
												}

												// set custom name on FB
												else {
													var $newAppId = data;
													$.ajax({
														url: 'facebook/facebook_functions.php?fb_act=app_update&id=' + $pageId + '&tab_name=' + $tabName + '&app_id=' + $newAppId,
														type: 'POST',
														error: function() { alert('Error') },
														success: function( data ) {

															if ( data != 'ok' ) {
																$loader.hide();
																alert('Session has expired. You have to login again');
																window.location.href = 'index.php';
															}

															// save into DB
															else {
																var $url = 'action.php?act=create_tab&user_id=' + $userId;
																$url += '&page_id=' + $pageId + '&app_id=' + $newAppId + '&name=' + $tabName + '&partner_id=' + $partnerId;
																$.ajax({
																	url: $url,
																	type: 'POST',
																	complete: function() { $loader.hide(); },
																	error: function() { alert('Error') },
																	success: function( data ) {
																		changeTabMenuActiveStep( $steps.eq(2) );
																		$forms.eq(1).hide().end().eq(2).show();
																		$('#tab_edit_in_editor').attr( 'href', 'editor.php?id=' + data );
																	}
																});
															} // save into DB

														}
													});
												} // set custom name on FB

											}
										});
									} // install on FB

								}
							}); // check tab name

						} // user can create more tabs
					}
				}); // check user tabs limit

			} // form is valid
			return false;
		});

	}; if ( $('.createNewTab.show').length ) $('a.createTab').trigger('click');
	// createTabsMenu()




	// DASHBOARD - REINSTALL TAB
	$('.myTabs a.reinstall').click( function() {
		var $btn = $(this);
		var $parentContainer = $(this).parents('.myTabs li');
		var $tabName = $parentContainer.find('.tabName span').html();
		var $pageId = $(this).attr('href');
		var $appId = $(this).attr('data-appid');

		// reinstall on FB
		$.ajax({
			url: 'facebook/facebook_functions.php?fb_act=app_install&id=' + $pageId + '&app_id=' + $appId,
			type: 'POST',
			error: function() { alert('Error'); },
			beforeSend: function() { $( $ajaxLoader ).show(); },
			success: function( data ) {

				if ( data != 'ok' ) {
					$( $ajaxLoader ).hide();
					alert('Session has expired. You have to login again');
					window.location.href = 'index.php';
				}

				// set custom name on FB
				else {
					$.ajax({
						url: 'facebook/facebook_functions.php?fb_act=app_update&id=' + $pageId + '&app_id=' + $appId + '&tab_name=' + $tabName,
						type: 'POST',
						error: function() { alert('Error') },
						complete: function() { $( $ajaxLoader ).hide(); },
						success: function( data ) {
							if ( data != 'ok' ) {
								alert('Session has expired. You have to login again');
								window.location.href = 'index.php';
							} else {
								$btn.remove();
								alert('The tab was successfully reinstalled!');
							}
						}
					});
				} // set custom name on FB

			}
		}); // reinstall on FB

		return false;
	});




	// DASHBOARD - RENAME TAB
	$('.myTabs a.rename').live( 'click', function() {
		var $parentContainer = $(this).parents('.myTabs li');
		var $oldName = $parentContainer.find('.tabName span').html();
		var $prompt = false;

		do {
			var $newName = prompt( 'Rename your Tab', $oldName );
			// cancel renaming
			if ( $newName == null ) break;
			// correct name
			else if ( $newName && $newName != $oldName ) $prompt = true;
			// name is the same or empty
			else alert('Please provide a new name for your tab');
		} while ( ! $prompt )

		if ( $prompt ) {
			var $tabID = $(this).attr('href');
			var $pageId = $(this).attr('data-fbid');
			var $appId = $(this).attr('data-appid');
			var $fbUserId = $(this).attr('data-fbuser');

			// check tab name
			$.ajax({
				url: 'action.php?act=check_tab_name&fb_user_id=' + $fbUserId + '&name=' + $newName,
				type: 'POST',
				beforeSend: function() { $( $ajaxLoader ).show(); },
				error: function() { alert('Error'); },
				success: function( data ) {

					if ( data != 'ok' ) {
						$( $ajaxLoader ).hide();
						alert('You already have a tab with such name');
					}

					// rename on FB
					else {
						$.ajax({
							url: 'facebook/facebook_functions.php?fb_act=app_update&id=' + $pageId + '&app_id=' + $appId + '&tab_name=' + $newName,
							type: 'POST',
							error: function() { alert('Error'); },
							success: function( data ) {

								if ( data != 'ok' ) {
									$( $ajaxLoader ).hide();
									alert('Session has expired. You have to login again');
									window.location.href = 'index.php';
								}

								// rename in DB
								else {
									$.ajax({
										url: 'action.php?act=rename_tab&id=' + $tabID + '&app_id=' + $appId + '&name=' + $newName,
										type: 'POST',
										error: function() { alert('Error'); },
										complete: function() { $( $ajaxLoader ).hide(); },
										success: function( data ) {
											if ( data != 'ok' ) {
												alert('Database error. Please, try later');
											} else {
												$parentContainer.find('.tabName span.name').html( $newName );
											}
										}
									});
								} // rename in DB

							}
						});
					} // rename on FB

				}
			}); // check tab name

		} // successful prompt
		return false;
	});




	// DASHBOARD - DELETE TAB
	$('.myTabs a.delete').live( 'click', function() {
		if ( confirm('Are you sure you want to delete this tab?') ) {
			var $tabID = $(this).attr('href');
			var $fbID = $(this).attr('data-fbid');
			var $appID = $(this).attr('data-appid');

			// uninstall from FB
			$.ajax({
				url: 'facebook/facebook_functions.php?fb_act=app_delete&id=' + $fbID + '&app_id=' + $appID,
				type: 'POST',
				error: function() { alert('Error') },
				beforeSend: function() { $( $ajaxLoader ).show(); },
				success: function( data ) {

					// uninstall from DB
					if ( data == 'ok' ) {
						$.ajax({
							url: 'action.php?act=delete_page&id=' + $tabID + '&app_id=' + $appID,
							type: 'POST',
							error: function() { alert('Error') },
							complete: function() { $( $ajaxLoader ).hide(); },
							success: function() { window.location.href = 'dashboard.php'; }
						});
					}

				}
			});
		} // confirm
		return false;
	});




	//-------------------- COOL SLIDER WIDGET -----------------------//


	// function - add items to slider
	function coolSlider_newItem( $url ) {
		if ( $url.indexOf('http') == -1 ) $url = $uploadedImagesFolder + $url;

		var $displayWidget = new displayWidget();

		// create
		var $item;
		if ( $url.indexOf('youtu') != -1 ) $item = $displayWidget.createYoutube( $url );
		else if ( $url.indexOf('vimeo') != -1 ) $item = $displayWidget.createVimeo( $url );
		else $item = '<img src="' + $url + '" alt="" />';

		// add
		var $container = $('.coolSliderPreview ul');
		$container.append('<li><div class="overlay" /><span /><a />' + $item + '</li>');

		// make sortable
		$container.sortable('destroy').sortable({
			// start-stop callbacks are needed because of a ui-sortable bug
			// it appears, when a user edits slider, changing slides order
			start: function( event, ui ) {
				$('[name^="coolSlider"]').each( function() {
					$(this).attr({ 'data-value': $(this).val() });
				});
			},
			stop: function( event, ui ) {
				$('[name^="coolSlider"]').each( function() {
					$(this).val( $(this).attr('data-value') ).removeAttr('data-value');
				});
			}
		});

		function updateContainerWidth() {
			var $li = $container.find('li');
			// set width
			$container.width( $li.outerWidth(true) * $li.length );
			// set scroll
			if ( $container.width() > $container.parent().width() + parseInt($li.css('marginRight')) )
				$container.parent().css( 'overflow-x', 'scroll' );
			else
				$container.parent().css( 'overflow-x', 'hidden' );
		}; updateContainerWidth();

		// delete
		$('.coolSliderPreview li a').live( 'click', function() {
			$(this).parents('li').remove();
			updateContainerWidth();
			return false;
		});

	} // coolSlider_newItem()



	// button - add image or video from a url
	$('a.coolSliderAddItem').live( 'click', function() {
		var $input = $('[name="coolSliderURL"]');
		var $url = $input.val();
		if ( $url == '' ) alert('Please paste a url to your media file and then click the Add button');
		else {
			$input.val('');
			coolSlider_newItem( $url );
		}
		return false;
	});



	// button - upload image
	$('a.coolSliderUploadImage').live( 'click', function() {
		var $input = $('[name="coolSliderImage"]');
		if ( $input.val() == '' ) alert('Please select a file and then click the Upload button');
		else {
			// validate file size (except IE)
			var $validationErrror = '';
			if ( ! $.browser.msie ) {
				var $fileSize;
				$input.each( function() { $fileSize = this.files[0].size; });
				if ( $fileSize > $maxFileSize ) {
					$validationErrror = 'Maximum image size is ' + $maxFileSize / 1024 / 1024 + ' Mb';
					alert( $validationErrror );
				}
			}
			// file is valid
			if ( $validationErrror == '' ) {
				$uploadedImageRefersTo = 'uploadImageToCoolSlider';
				$(this).parents('form').submit();
			}
		}
		return false;
	});



	// create or update cool slider
	function createUpdateCoolSlider() {
		var $displayWidget = new displayWidget();
		var $sBg = hexConverter( $('.coolSliderBgColor').css('background-color') );
		var $sBorder = hexConverter( $('.coolSliderBorderColor').css('background-color') );
		var $sWidth = parseInt( $('[name="coolSliderWidth"]').val() );
		var $sHeight = parseInt( $('[name="coolSliderHeight"]').val() );
		var $sVisible = parseInt( $('[name="coolSliderVisible"]').val() );
		var $slides = $('.coolSliderPreview li');
		var $frameWidth = $sVisible * ( $sWidth + 15 ) - 15;
		var $ulWidth = $slides.length * ( $sWidth + 15 );
		// create
		if ( $createNewElement ) {
			var $output = '<div class="coolSlider" data-bg="' + $sBg + '" data-border="' + $sBorder + '" ';
			$output += 'data-width="' + $sWidth + '" data-height="' + $sHeight + '" data-visible="' + $sVisible + '" ';
			$output += 'style="width: ' + $frameWidth + 'px; background: ' + $sBg + '; border-color: ' + $sBorder + ';">';
				$output += '<span class="arrow left" /><span class="arrow right" />';
				$output += '<div class="frame" style="width: ' + $frameWidth + 'px;">';
					$output += '<ul style="width: ' + $ulWidth + 'px;">';
						$output += $displayWidget.createCoolSliderSlides( $slides, $sWidth, $sHeight );
					$output += '</ul>';
				$output += '</div>';
			$output += '</div>';
			return $output;
		}
		// update
		else {
			var $coolSlider = $focusedElement.find('.coolSlider');
			$coolSlider.attr({
				'data-bg': $sBg,
				'data-border': $sBorder,
				'data-width': $sWidth,
				'data-height': $sHeight,
				'data-visible': $sVisible
			}).css({
				'width': $frameWidth,
				'background': $sBg,
				'border-color': $sBorder
			})
			.find('.frame').css({ 'width': $frameWidth })
			.find('ul').css({ 'width': $ulWidth })
			.find('li').remove();
			$coolSlider.find('ul').append( $displayWidget.createCoolSliderSlides( $slides, $sWidth, $sHeight ) );
		}
	} // createUpdateCoolSlider()




	//----------------------------- OTHER ---------------------------------//


	// TWITTER SHARE AND FB SHARE WIDGETS - SWITCH URL
	$('[name="twitterShareURLSwitch"], [name="postFeedLinkSwitch"]').change( function() {
		var $input;
		if ( $(this).is('[name="twitterShareURLSwitch"]') ) $input = $('[name="twitterShareURL"]');
		else if ( $(this).is('[name="postFeedLinkSwitch"]') ) $input = $('[name="postFeedLink"]');
		if ( $(this).val() == '1' )
			$input.prop( 'disabled', false ).focus();
		else
			$input.prop( 'disabled', true ).val('');
	});



	// CONTEST PROPERTIES WIDGET - SWITCH VOTING
	$('[name="contestVotingTimes"]').change( function() {
		var $input = $('[name="contestVotingPerDay"]');
		if ( $(this).val() == '1' )
			$input.prop( 'disabled', false );
		else
			$input.prop( 'disabled', true ).val('');
	});



	// LOAD CONTEST INCLUDES
	if ( $('#includes').length ) {
		var $displayWidget = new displayWidget();
		$displayWidget.ajaxGetFileContent('includes/countries.html');
		setTimeout( function() {
			$contestCountries = $('#countries');
		}, 1000 );
	}



	// CONTEST FORM DESIGN - ADD FONT SIZE OPTIONS AND BORDER SIZE OPTIONS
	$('#contestLabelFontSize, #contestInputFontSize').each( function() {
		for ( $i = 8; $i <= 20; $i++ )
			$(this).append('<option value="' + $i + 'px">' + $i + 'px</option>');
	});
	$('#contestInputBorderSize').each( function() {
		for ( $i = 0; $i <= 7; $i++ )
			$(this).append('<option value="' + $i + 'px">' + $i + 'px</option>');
	});



	// CONTEST FORM DESIGN - ADD FONT SIZE OPTIONS AND BORDER SIZE OPTIONS
	$('#contestLabelAlign, #contestInputAlign').each( function() {
		$(this).append('<option value="left">Left</option>')
						.append('<option value="right">Right</option>')
						.append('<option value="center">Center</option>')
						.append('<option value="justify">Justify</option>');
	});



	// CONTENT TYPE SWITCH IN PREVIEW AREA
	$('.fbArea .contentTypeSwitch.forPreview a').mousedown( function() { return false; }).click( function() {
		$('.pageBodyPreview .preview.' + $(this).attr('class') ).show().siblings().hide();
		return false;
	});



	// ENABLE/DISABLE CONTEST PAGES
	$('.editorInstruments a.contest').click( function() {
		if ( $(this).is('.disable') ) {
			$(this).removeClass('disable');
			$('.tabSteps ul').addClass('noContest');
		} else {
			$(this).addClass('disable');
			$('.tabSteps ul').removeClass('noContest');
		}
		$('.tabSteps li').eq(0).trigger('click');
		// show/hide contest buttons
		$('.contestButtons').toggleClass('hidden');
		// if contest was enabled, show properties popup
		if ( ! $('.contestButtons').is('.hidden') )
			$('a[href="#popup_contest_properties"]').trigger('click');
		return false;
	});



	// ENABLE/DISABLE CONTEST UGC
	$('.editorInstruments a.ugc').click( function() {
		if ( $(this).is('.disable') ) {
			$(this).removeClass('disable');
			$('.tabSteps ul').addClass('noUGC');
		} else {
			$(this).addClass('disable');
			$('.tabSteps ul').removeClass('noUGC');
		}
		$('.tabSteps li').eq(0).trigger('click');
		return false;
	});



	// CONTENT TYPE SWITCH - NEXT BUTTON
	$('.editorButtons a[data-step="next"]').click( function() {
		var $step = $('.tabSteps li');
		var $total = $step.length;
		var $index = $step.filter('.active').index();
		if ( $index < $total ) {
			// no contest
			if ( $('.tabSteps ul').is('.noContest.noUGC') ) {
				if ( ! $('.tabSteps li.active').is('.a2') )
					$step.eq( $index + 1 ).trigger('click');
				else
					$step.filter('.a8').trigger('click');
			}
			// contest without ugc
			else if ( $('.tabSteps ul').is('.noUGC') ) {
				if ( ! $('.tabSteps li.active').is('.a4') )
					$step.eq( $index + 1 ).trigger('click');
				else
					$step.filter('.a7').trigger('click');
			}
			// contest with ugc
			else {
				$step.eq( $index + 1 ).trigger('click');
			}
		} // $index < $total
		return false;
	});



	// CONTENT TYPE SWITCH (CONTENT + PREVIEW)
	$.fn.tabSteps = function() {

		var $body = $(this);
		var $container = $body.find('ul');
		var $step = $container.children('li');

		$step.mousedown( function() { return false; }).click( function() {

			var $class = $(this).attr('data-switch');

			// change active step
			$step.removeClass('nextIsActive active');
			$(this).addClass('active');

			// no contest
			if ( $('.tabSteps ul').is('.noContest.noUGC') ) {
				if ( $container.find('li.active').is('.a8') )
					$container.find('li.a2').addClass('nextIsActive');
				else
					$(this).prev().addClass('nextIsActive');
			}
			// contest without ugc
			else if ( $('.tabSteps ul').is('.noUGC') ) {
				if ( $container.find('li.active').is('.a7') )
					$container.find('li.a4').addClass('nextIsActive');
				else
					$(this).prev().addClass('nextIsActive');
			}
			// contest with ugc
			else {
				$(this).prev().addClass('nextIsActive');
			}

			// toggle contest widgets group
			var $mask = '.mask';
			var $contestFilterBtn = $('.filterWidgetsBy span:contains("Contest Features")');
			var $contestWidgets = $('.widgetsSlider li[data-cat="Contest Features"]');
			var $cTextInput = $('.contest_text_input');
			var $cTextarea = $('.contest_textarea');
			var $cButton = $('.contest_button');
			var $ugcUploadArea = $('.ugc_upload_area');
			var $ugcGallery = $('.ugc_gallery');

			if ( $class == 'contest-fans' ) {
				$contestFilterBtn.show();
				// hide all form widgets
				$contestWidgets.find( $mask ).show();
				// show button widget
				$cButton.find( $mask ).hide();
			}
			else if ( $class == 'contest-form' ) {
				$contestFilterBtn.show();
				// show all form widgets
				$contestWidgets.find( $mask ).hide();
				// hide ugc form widgets
				$ugcUploadArea.add( $ugcGallery ).find( $mask ).show();
			}
			else if ( $class == 'contest-ugc-form' ) {
				$contestFilterBtn.show();
				// hide all form widgets
				$contestWidgets.find( $mask ).show();
				// show button and ugc form widgets
				$cButton.add( $ugcUploadArea ).add( $cTextInput ).add( $cTextarea ).find( $mask ).hide();
			}
			else if ( $class == 'contest-ugc-gallery' ) {
				$contestFilterBtn.show();
				// hide all form widgets
				$contestWidgets.find( $mask ).show();
				$ugcGallery.find( $mask ).hide();
				// show button and ugc gallery widgets
				$cButton.add( $ugcGallery ).find( $mask ).hide();
			}
			else {
				$contestFilterBtn.hide();
				$contestWidgets.find( $mask ).show();
				if ( $contestFilterBtn.is('.active') ) $('.filterWidgetsBy span:first').trigger('click');
			}

			// if current step is not the last one
			if ( $class != 'previewAndPublish' ) {
				// show necessary elements ( other part is in ajaxSavePage() )
				$('.editorButtons').find('a').show().end().find('a.publish').hide();
				$('.editorInstruments').css({ visibility: 'visible' });
				$('.pageBodyPreview').empty().hide();
				$('.pageBodyInner').show();
				$('.fbArea').find('.contentTypeSwitch.forPreview').hide();
			}

			// last step is preview - so save the tab
			if ( $class == 'previewAndPublish' ) {
				var $id = $('a.bigButton.save').attr('href');
				var $url = 'action.php?act=save_page&id=' + $id;
				var $successMessage = 'The tab has been saved!';
				var $errorMessage = 'Error when saving the tab.';
				ajaxSavePage( $id, $url, $successMessage, $errorMessage, true );
			}

			// show required content type
			$focusedContainer = $( '.' + $pageBody + '.' + $class );
			$focusedContainer.show().siblings( '.' + $pageBody ).hide();

			// update height input
			$('[name="pageHeight"]').val( $focusedContainer.height() );

			// enable height change
			pageBodyHeight();

			// call a popup, when an icon has been dropped to the page
			pageDroppable();

			// background colorpicker initialization
			popupColorPicker( $('.bgPicker'), hexConverter( $focusedContainer.css('background-color') ) );

			$focusedContainer.find( '.contentHotspot').removeClass('savedHidden');
		});

	}; $('.tabSteps').tabSteps();



	// RESTORING ORIGINAL COLOR IN COLOR PICKERS
	$('.uniPicker').each( function() {
		var $color = hexConverter( $(this).css('background-color') );
		if ( $(this).attr('data-color') == undefined ) $(this).attr( 'data-color', $color );
	});



	// FANCYBOX
	$('a[href^="#popup_"]').fancybox({
		padding:                              0,
		centerOnScroll:                true,
		overlayOpacity:                0.5,
		overlayColor:                    '#000',
		titleShow:                           false,
		hideOnOverlayClick:      false,
		onComplete: function() {

			// show cleditor
			if ( $(this).attr('href') == '#popup_iconText' ) {
				setTimeout(function() {
					$('[name="text"]').cleditor({
						width: 602,
						controls:
							"source | undo redo | removeformat | bold italic underline strikethrough subscript superscript | " +
							"numbering bullets | link unlink | font size | color highlight | alignleft center alignright justify | " +
							"indent outdent"
					});
				}, 50 );
			} // show cleditor end

			// init popup color pickers
			if ( $('#fancybox-wrap .uniPicker').length ) {
				$('#fancybox-wrap .uniPicker').not('.bgPicker').each( function() {
					// get current color
					var $color = hexConverter( $(this).css('background-color') );
					// if create new element, show default colors, else show current color
					if ( $createNewElement ) popupColorPicker( $(this), $(this).attr('data-color') );
					else popupColorPicker( $(this), $color );
				});
			} // init popup color pickers end

			// init date picker
			if ( $('#fancybox-wrap .datePicker').length ) {
				$('#fancybox-wrap .datePicker').each( function() {
					$(this).datepicker({ dateFormat: 'yy-mm-dd', minDate: new Date() });
				});
			} // init date picker end

			// init datetime picker
			if ( $('#fancybox-wrap .dateTimePicker').length ) {
				$('#fancybox-wrap .dateTimePicker').each( function() {
					$(this).datetimepicker({ minDate: 0 });
				});
			} // init datetime picker end

			// generate contest selects with available input field types
			generateSelectsWithContestInputFields();

			// contest button widget
			if ( $('#fancybox-wrap [name="contestBtnType"]').length ) {
				function contestBtnOption( $type ) {
					var $option = $('[name="contestBtnType"] [value="' + $type + '"]');

					// by default an option is visible
					$option.show();

					// if a button is already added to the active canvas, the option is disabled
					if ( $focusedContainer.find('img[data-type="' + $type + '"]').length ) {
						$option.hide();
					}

					// "home" button
					if ( $type == 'typeHome' ) {
						// is not allowed only on splash page
						if ( $focusedContainer.is( '.' + $contestFansClass ) )
							$option.hide();
					}

					// contest "enter" button
					else if ( $type == 'typeContestEnter' ) {
						// allowed only on splash and ugc-gallery pages
						if ( ! $focusedContainer.is( '.' + $contestFansClass + ', .' + $contestUGCGalleryClass ) )
							$option.hide();
					}

					// contest "submit" button
					else if ( $type == 'typeContestSubmit' ) {
						// allowed only on contest-form and ugc-form pages
						if ( ! $focusedContainer.is( '.' + $contestFormClass + ', .' + $contestUGCFormClass ) )
							$option.hide();
					}

					// contest ugc "view entries" button
					else if ( $type == 'typeUgcView' ) {
						// is not allowed only on ugc-gallery page or if UGC contest is turned off
						if ( $focusedContainer.is( '.' + $contestUGCGalleryClass ) || $('.tabSteps > ul').is('.noUGC') )
							$option.hide();
					}

					// contest ugc "preview entry" and "upload" buttons
					else if ( $type == 'typeUgcPreview' || $type == 'typeUgcUpload' ) {
						// is allowed only on ugc-form page
						if ( ! $focusedContainer.is( '.' + $contestUGCFormClass ) )
							$option.hide();
					}

				} // contestBtnOption()
				contestBtnOption('typeHome');
				contestBtnOption('typeContestEnter');
				contestBtnOption('typeContestSubmit');
				contestBtnOption('typeUgcView');
				contestBtnOption('typeUgcPreview');
				contestBtnOption('typeUgcUpload');
			} // contest button widget

		},
		onClosed: function() {

			// remove scroll and delete slides from cool slider preview
			$('.coolSliderPreview').css( 'overflow-x', 'hidden' ).find('ul').empty();

			// hide cleditor
			if ( $('.cleditorMain').length ) $('.cleditorMain').replaceWith( $textareaForTextWidget );

			// clear popup (contest "select" and "radio" widgets)
			$('.contestSelectOptionsList, [name="contestSelectDefault"] option')
				.add( $('.contestRadioOptionsList, [name="contestRadioDefault"] option') ).remove();

		}
	});



	// PRESSING ENTER BUTTON SUBMITS FANCYBOX (EXCEPT HTML WIDGET)
	$('body').keypress( function( event ) {
		if ( event.keyCode == 13 ) {
			if ( $('#fancybox-wrap textarea:focus').length ) {
				// do nothing
			} else $('#fancybox-wrap .aButton.done').trigger('click');
		}
	});



	// USER CLOSES A POPUP WITHOUT CHANGES
	function popupClosedWithoutChanges() {
		$createNewElement = true;
		$focusedElement = null;
	}



	// FANCYBOX - BUTTON CANCEL
	$('.hiddenForms a.cancel').click(function () {
		popupClosedWithoutChanges();
		$.fancybox.close();
		return false;
	});



	// FANCYBOX - BUTTON ESC, CLOSE AND OUTSIDE CLICK
	$(document).keyup( function(e) {
		if ( e.keyCode == 27 ) popupClosedWithoutChanges();
	})
	.find('a#fancybox-close').live( 'click', function() {
		popupClosedWithoutChanges();
	}).end()
	.find('#fancybox-overlay').live( 'click', function() {
		popupClosedWithoutChanges();
	});
	$('#fancybox-wrap').live( 'click', function( event ) {
		event.stopPropagation();
	});



	// FUNCTION - AJAX FILE UPLOADER
	// this function creates new elements, but only in case, when user uploads a file
	$.fn.fileUploader = function( $inputName ) {

		var $fileInput = $(this);
		var $label = $fileInput.siblings('span');
		var $fileInputEmpty = '<input type="file" class="uploadFile" name="' + $inputName + '" />';
		var $form = $fileInput.parents('form');

		// user submits the form
		$form.submit( function() {
			$( $ajaxLoader ).fadeIn();

			var $response = '';

			$( $ajaxIframe ).load( function () {
				$response = $( $ajaxIframe ).contents().find('body').html();
			});

			// periodically check the result in iframe
			var $timer = setInterval( function() {

				if ( $response != '' ) {
					clearInterval( $timer );

					// file is invalid
					if ( $response.indexOf('ERROR') != -1 ) {
						showAjaxMessage( 'Error - ' + $response.substr(6) );
					}

					// file is valid
					else {

						// variable for new element
						var $output = null;

						//change background image
						if ( $uploadedImageRefersTo == 'changeBG' ) {
							updateBG( $response );
						}

						// add image to cool slider
						else if ( $uploadedImageRefersTo == 'uploadImageToCoolSlider' ) {
							coolSlider_newItem( $response );
						}

						// add content image
						else if ( $uploadedImageRefersTo == 'addImage' ) {
							$output = createUpdateImage( $response, $('[name="imageLinkURL"]').val() );
						}

						// add fb invite button
						else if ( $uploadedImageRefersTo == 'addInviteButton' ) {
							var $title = $('[name="inviteTitle"]').val();
							var $message = $('[name="inviteMessage"]').val();
							$output = createUpdateInviteButton( $title, $message, $response );
						}

						// add fb post feed
						else if ( $uploadedImageRefersTo == 'addFbPostFeed' ) {
							// update global variable(s)
							if ( $.inArray( 0, arrImagesToUpdate ) != -1 )
								_postFeedImgImage = getUploadedImgName( $response, 0 );
							if ( $.inArray( 1, arrImagesToUpdate ) != -1 )
								_postFeedBtnImage = getUploadedImgName( $response, 1 );
							var $pLink = $('[name="postFeedLink"]').val();
							var $pName = $('[name="postFeedName"]').val();
							var $pDescr = $('[name="postFeedDescr"]').val();
							var $pCaption = $('[name="postFeedCaption"]').val();
							$output = createUpdateFbPostFeed( $pLink, $pName, $pDescr, $pCaption );
						}

						// add contact form
						else if ( $uploadedImageRefersTo == 'addContactForm' ) {
							var $cAddress = $('[name="contactAddress"]').val();
							var $cName = $('#contactName').is(':checked') ? true : false;
							var $cPhone = $('#contactPhone').is(':checked') ? true : false;
							$output = createUpdateContactForm( $cAddress, $response, $cName, $cPhone );
						}

						// add email button
						else if ( $uploadedImageRefersTo == 'addEmailButton' ) {
							var $eAddress = $('[name="emailAddress"]').val();
							$output = createUpdateEmailButton( $eAddress, $response );
						}

						// add twitter share button
						else if ( $uploadedImageRefersTo == 'addTwitterShareButton' ) {
							var $account = $('[name="twitterShareAccount"]').val();
							var $text = $('[name="twitterShareText"]').val();
							var $url = $('[name="twitterShareURL"]').val();
							$output = createUpdateTwitterShareButton( $account, $text, $url, $response );
						}

						// add contest button
						else if ( $uploadedImageRefersTo == 'addContestButton' ) {
							var $btnType = $('[name="contestBtnType"]').val();
							$output = createUpdateContestButton( $btnType, $response );
						}

						// add new element
						if ( $output != null ) addNewElement( $output );

						// added widgets must be draggable and resizable
						$( '.' + $block ).each( function() { widgetDR( $(this) ); });

					} // file is valid

					// update global var
					if ( $uploadedImageRefersTo != 'uploadImageToCoolSlider' ) {
						$createNewElement = true;
						$userEditsUploadedImage = false;
						$uploadedImageRefersTo = null;
						$focusedElement = null;
					}

					$( $ajaxLoader ).fadeOut();

					// success
					if ( $response.length ) {
						$label.after( $fileInputEmpty );
						$fileInput.remove();
						$fileInput = $label.siblings('[type="file"]');
					}

				} // got the response
			}, 100 );

		}); // form submit
	};
	// run this function for each .uploadFile
	$('input.uploadFile, input[type="file"]').each( function() {
		var $inputName = $(this).attr('name');
		$(this).fileUploader( $inputName );
	});




	//--------------------- CONTEST FUNCTIONS ----------------------//


	// CONTEST - SELECT AND RADIO - ADD OPTION
	$('a.addContestSelectOption, a.addContestRadioOption').live( 'click', function() {
		var $inputText;
		var $inputValue;
		var $selectForDefaultValue;
		var $optionsClass;
		var $delClass;

		if ( $(this).is('a.addContestSelectOption') ) {
			$inputText = $('[name="contestSelectOptionText"]');
			$inputValue = $('[name="contestSelectOptionValue"]');
			$selectForDefaultValue = $('[name="contestSelectDefault"]');
			$optionsClass = 'contestSelectOptionsList';
			$delClass = 'deleteContestSelectOption';
		} else {
			$inputText = $('[name="contestRadioOptionText"]');
			$inputValue = $('[name="contestRadioOptionValue"]');
			$selectForDefaultValue = $('[name="contestRadioDefault"]');
			$optionsClass = 'contestRadioOptionsList';
			$delClass = 'deleteContestRadioOption';
		}

		// validate
		var $error = false;
		$inputText.add( $inputValue ).each( function() {
			var $element = $(this);
			var $mustEnter = $element.is( $inputText ) ? 'text' : 'a value';
			if ( $element.val() == '' ) {
				$element.val( 'Enter ' + $mustEnter );
				setTimeout( function() { $element.val(''); }, 1000 );
				$error = true;
			}
		});

		// valid
		if ( ! $error ) {
			// add list outer, if it does not exist
			if ( ! $inputText.prev().is( '.' + $optionsClass ) )
				$inputText.before('<ul class="' + $optionsClass + '" />');
			// add option
			var $text = $inputText.val();
			var $value = $inputValue.val();
			$inputText.add( $inputValue ).val('');
			$selectForDefaultValue.append('<option value="' + $value + '">' + $text + '</option>');
			var $str = '<li>' + $text + ' <a class="' + $delClass + '" href="'+$value+'" data-text="'+$text+'">x</a></li>';
			$( '.' + $optionsClass ).append( $str );
		} // valid

		return false;
	});


	// CONTEST - SELECT AND RADIO - DELETE OPTION
	$('a.deleteContestSelectOption, a.deleteContestRadioOption').live( 'click', function() {
		var $selectForDefaultValue;
		var $optionsClass;

		if ( $(this).is('a.deleteContestSelectOption') ) {
			$selectForDefaultValue = $('[name="contestSelectDefault"]');
			$optionsClass = 'contestSelectOptionsList';
		} else {
			$selectForDefaultValue = $('[name="contestRadioDefault"]');
			$optionsClass = 'contestRadioOptionsList';
		}

		$selectForDefaultValue.find('option[value="' + $(this).attr('href') + '"]').remove();
		if ( $(this).parent('li').siblings().length )
			$(this).parent('li').remove();
		else
			$(this).parents( '.' + $optionsClass ).remove();

		return false;
	});



	// CONTEST - FIELD TYPE DROPDOWN - CHANGE
	$('#contestTextFieldType, #contestTextareaFieldType, #contestSelectFieldType, #contestRadioFieldType, #contestCheckboxFieldType').change( function() {
		// show fields
		$(this).parents('form').find( '.' + $hiddenPopupFields ).show();
		// get values
		var $val = $(this).val();
		var $text = $(this).find(':selected').html();
		// autofill name and label
		if ( $(this).is('#contestTextFieldType') ) {
			$('[name="contestTextName"]').val( $val );
			$('[name="contestTextLabel"]').val( $text );
		}
		else if ( $(this).is('#contestTextareaFieldType') ) {
			$('[name="contestTextareaName"]').val( $val );
			$('[name="contestTextareaLabel"]').val( $text );
		}
		else if ( $(this).is('#contestSelectFieldType') ) {
			$('[name="contestSelectName"]').val( $val );
			$('[name="contestSelectLabel"]').val( $text );
		}
		else if ( $(this).is('#contestRadioFieldType') ) {
			$('[name="contestRadioName"]').val( $val );
			$('[name="contestRadioLabel"]').val( $text );
		}
		else if ( $(this).is('#contestCheckboxFieldType') ) {
			$('[name="contestCheckboxName"]').val( $val );
			$('[name="contestCheckboxLabel"]').val( $text );
		}


		// show validation options for contest text fields
		if ( $(this).is('#contestTextFieldType') && $(this).find('option:last').is(':selected') )
			toggleTextInputValidation('show');
		else
			toggleTextInputValidation('hide');

		// autofilling validation for predefined text input fields
		if ( $(this).is('#contestTextFieldType') ) {
			var $select = $('[name="contestTextValidation"]');
			var $option;
			if ( $(this).val() == 'Email' )
				$option = $select.find('[value="email"]');
			else if ( $(this).val() == 'Zip_Code' )
				$option = $select.find('[value="zip"]');
			else if ( $(this).val() == 'Cell_Phone' || $(this).val() == 'Home_Phone' )
				$option = $select.find('[value="digits"]');
			else
				$option = $select.find('[value=""]');
			$option.attr( 'selected', true );
		} // autofilling validation


		// select - auto generating options
		if ( $(this).is('#contestSelectFieldType') ) {
			var $inputText = $('[name="contestSelectOptionText"]');
			var $inputValue = $('[name="contestSelectOptionValue"]');
			var $button = $('a.addContestSelectOption');
			var $options;

			// remove created options list, if exists
			$('.contestSelectOptionsList').remove();
			$('[name="contestSelectDefault"]').empty();

			// gender
			if ( $(this).val() == 'Gender' ) {
				$options = [ [ 'Male', 'male' ], [ 'Female', 'female' ] ];
				for ( $i = 0; $i < $options.length; $i++ ) {
					$inputText.val( $options[$i][0] );
					$inputValue.val( $options[$i][1] );
					$button.trigger('click');
				}
			}

			// birthday - year
			else if ( $(this).val() == 'Birthday_Year' ) {
				for ( $i = 2012; $i >= 1900; $i-- ) {
					$inputText.val( $i );
					$inputValue.val( $i );
					$button.trigger('click');
				}
			}

			// birthday - month
			else if ( $(this).val() == 'Birthday_Month' ) {
				$options = [ [ 'January', '01' ], [ 'February', '02' ], [ 'March', '03' ], [ 'April', '04' ], [ 'May', '05' ],
										[ 'June', '06' ], [ 'July', '07' ], [ 'August', '08' ], [ 'September', '09' ], [ 'October', '10' ],
										[ 'November', '11' ], [ 'December', '12' ] ];
				for ( $i = 0; $i < $options.length; $i++ ) {
					$inputText.val( $options[$i][0] );
					$inputValue.val( $options[$i][1] );
					$button.trigger('click');
				}
			}

			// birthday - day
			else if ( $(this).val() == 'Birthday_Day' ) {
				for ( $i = 1; $i <= 31; $i++ ) {
					if ( $i < 10 ) $i = '0' + $i;
					$inputText.val( $i );
					$inputValue.val( $i );
					$button.trigger('click');
				}
			}

			// country
			else if ( $(this).val() == 'Country' ) {
				$contestCountries.find('option').each( function() {
					$inputText.val( $(this).html() );
					$inputValue.val( $(this).val() );
					$button.trigger('click');
				});
			}

		} // select - auto generating options
	});



	// CONTEST - GENERATE SELECT WITH INPUT TYPES
	function generateSelectsWithContestInputFields() {
		var $body;
		var $select = $('#contestTextFieldType, #contestTextareaFieldType, #contestSelectFieldType, #contestRadioFieldType, #contestCheckboxFieldType');
		$select.empty();
		// arrays with options
		var $arrText = [ '', 'First Name', 'Last Name', 'Email', 'Cell Phone', 'Home Phone',
										'Address', 'City', 'Zip Code', 'State', 'Cellular Carrier', 'UGC Title' ];
		var $arrTextarea = [ '', 'UGC Description' ];
		var $arrSelect = [ '', 'Gender', 'Birthday Year', 'Birthday Month', 'Birthday Day', 'Country' ];
		var $arrRadio = [ '' ];
		var $arrCheckbox = [ '', 'SMS opt in', 'Email opt in' ];
		// generate options for each select
		$select.each( function( $index, $element ) {
			var $arr;
			var $openPrefix;
			// update variables...
			if ( $(this).is('#contestTextFieldType') ) {
				$body = $('.contestTextInput');
				$arr = $arrText;
				$openPrefix = 'Text';
			}
			else if ( $(this).is('#contestTextareaFieldType') ) {
				$body = $('.contestTextarea');
				$arr = $arrTextarea;
				$openPrefix = 'Textarea';
			}
			else if ( $(this).is('#contestSelectFieldType') ) {
				$body = $('.contestSelect');
				$arr = $arrSelect;
				$openPrefix = 'Select';
			}
			else if ( $(this).is('#contestRadioFieldType') ) {
				$body = $('.contestRadio');
				$arr = $arrRadio;
				$openPrefix = 'Radio';
			}
			else if ( $(this).is('#contestCheckboxFieldType') ) {
				$body = $('.contestCheckbox');
				$arr = $arrCheckbox;
				$openPrefix = 'Checkbox';
			}
			// add predefined fields
			for ( $i = 0; $i < $arr.length; $i++ ) {
				if ( ! $body.find('[name="' + replaceSpaces( $arr[$i] ) + '"]').length )
					$select.eq($index).append('<option value="' + replaceSpaces( $arr[$i] ) + '">' + $arr[$i] + '</option>');
			}
			// add open field
			var $openFieldsTotal = 0;
			$body.each( function() {
				var $name = $(this).find('input, textarea, select').attr('name');
				if ( $name.indexOf('Open_Field') != -1 )
					$openFieldsTotal++;
			});
			if ( $openFieldsTotal < 10 ) {
				// find available name
				var $openName;
				for ( $i = 1; $i <= 10; $i++ ) {
					$openName = $openPrefix + '_Open_Field_' + $i;
					if ( ! $body.find('[name="' + $openName + '"]').length ) break;
				}
				$select.eq( $index ).append('<option value="' + $openName + '">Open Field</option>');
			}
		}); // each
	} // generateSelectsWithContestInputFields()




	//------------------- WIDGETS ICONS SLIDER ---------------------//

	$.fn.widgetsSlider = function( options ) {

		var $body = $(this);
		var $arrowLeft;
		var $arrowRight;
		var $slides = $body.find('ul');
		var $elementsNumber = $slides.length;
		var $speed = 0;
		var $nowSlide = 0;

		$body.prepend('<span class="arrow left" /><span class="arrow right" />');
		$arrowLeft = $body.find('span.left');
		$arrowRight = $body.find('span.right');

		$arrowLeft.hide();

		// prevent undesired selection
		$body.mousedown( function () { return false; });

		// show the first slide
		$slides.hide().eq( $nowSlide ).show();

		// click on arrow
		$arrowLeft.add( $arrowRight ).click(function () {
			if ( ! $slides.is(':animated') ) {
				var $oldSlide = $nowSlide;
				if ( $(this).is( $arrowLeft ) ) {
					$nowSlide--;
					if ( $nowSlide < 0 ) $nowSlide = 0;
				} else {
					$nowSlide++;
					if ( $nowSlide > $elementsNumber -1 ) $nowSlide = $elementsNumber -1;
				}
				// show/hide arrows
				if ( $nowSlide == 0 ) {
					$arrowLeft.hide();
					$arrowRight.show();
				} else if ( $nowSlide == $elementsNumber -1 ) {
					$arrowLeft.show();
					$arrowRight.hide();
				} else {
					$arrowLeft.show();
					$arrowRight.show();
				}
				// animation
				if ( $oldSlide != $nowSlide ) {
					$slides.eq( $oldSlide ).fadeOut( $speed, function() {
						$slides.eq( $nowSlide ).fadeIn( $speed );
					});
				} // animation
			} // not animated
			return false;
		});

	} // widgetsSlider()




	//---------------------- WIDGETS FILTER ---------------------------//

	$.fn.widgetsFilter = function() {

		var $divFilter = $('.filterWidgetsBy');
		var $inRow = 15;
		var $body = $(this);
		var $container = $body.find('.frame');
		var $elements = $container.find('li');
		var $arrayCat = [];

		// get categories
		$elements.each( function() {
			var $category = $(this).attr('data-cat');
			if ( $.inArray( $category, $arrayCat ) == -1 ) $arrayCat.push( $category );
		});

		// create "view all" category
		$divFilter.append('<span class="active">All Widgets</span>');

		for ( $i = 0; $i < $arrayCat.length; $i++ ) {

			// create groups
			$divFilter.append('<span>' + $arrayCat[$i] + '</span>');

			// create content wrapper for each group
			$body.append('<div class="frame"><ul /></div>');

			// add widgets into each group
			$elements.each( function() {
				var $currentCat = $arrayCat[$i];
				if ( $currentCat.indexOf( $(this).attr('data-cat') ) != -1 )
					$body.find('.frame:last ul').append( $(this).clone() );
			});

		} // endfor

		// create slides and slider, if needed
		$body.find('.frame').each( function() {
			var $a = $(this).find('li').clone();
			if ( $a.length > $inRow ) {
				$(this).empty();
				$(this).append( $a );
				do $( $a.slice( 0, $inRow ) ).wrapAll('<ul />');
				while ( ( $a = $a.slice($inRow) ).length > 0 );
				$(this).widgetsSlider();
			}
		});

		// switch widgets filter
		$divFilter.find('span').live( 'click', function() {
			$(this).addClass('active').siblings().removeClass('active');
			$body.find('.frame').hide().eq( $(this).index() - 1 ).show();
			return false;
		});

		// add default settings...

		// show the first group
		$container.filter(':first').show().find('ul:first').show();

		// hide contest group
		$divFilter.find('span:contains("Contest")').hide();

		// hide contest widgets
		$body.find('li').each( function() {
			if ( $(this).attr('data-cat') == 'Contest Features' )
				$(this).append('<i class="mask" />');
		});

	}; $('.widgetsSlider').widgetsFilter();





	//------ WIDGET AND TEMPLATE ICONS WITH TOOLTIP ------//

	$('.widgetsSlider a').click(function () { return false; }).find('span').each(function( e ) {

		var $icon = $(this);
		var $tooltipOffsetLeft = 2;
		var $tooltipOffsetTop = 62;

		// widget icons must be draggable
		$icon.filter('span').mouseenter(function() {
			$(this).draggable({ helper: 'clone', opacity: 0.5 });
		});

		// get the tooltip title and remove it
		var $title = $icon.parent().attr('title');
		$icon.parent().removeAttr('title');

		if ( typeof $title !== 'undefined' && $title !== false ) {
			// if there must be a title...
			if ( $title.indexOf(' - ') != -1 ) { $title = $title.replace( ' - ', '<br />' ); }
			// generate and append the tooltip
			$output = '<em class="tooltip">';
			$output += $title;
			$output += '<em class="top"></em>';
			$output += '<em class="bottom"></em>';
			$output += '</em>';
			// if img - append tootlip to parent link
			$icon.is('img') ? $icon.parent().append( $output ) : $icon.append( $output );
		}

		// tooltip show/hide
		$icon
			.mouseenter( function() {
				var $tooltip = $icon.is('img') ? $(this).parent().find('.tooltip') : $(this).find('.tooltip');
				// don't show tooltips, when dragging any widget icon
				if ( ! $('.ui-draggable-dragging').length ) {
					if ( $(window).scrollTop() > $highestTooltipPosition )
						$('.tooltip').addClass('reversed');
					else
						$('.tooltip').removeClass('reversed');
					// show tooltip
					$tooltip.show().css({ left: -$tooltipOffsetLeft });
					// normal position - above the widget
					if ( ! $tooltip.is('.reversed') ) {
						$tooltip.css({ top: 'auto', bottom: $tooltipOffsetTop });
					}
					// reversed position - below the widget
					else {
						$tooltip.css({ top: $tooltipOffsetTop, bottom: 'auto' });
					}
				}
			})
			.mouseleave(function () { $('.tooltip').hide(); })
			.mousedown(function () { $('.tooltip').hide(); });

	}); // draggable icons



	// define tooltip position
	function getTooltipPosition() {
		var $highestTooltip;
		var $highestTooltipIcon;
		var $highestTooltipHeight = 0;
		$('.widgetsSlider .tooltip').each( function() {
			if ( $(this).height() > $highestTooltipHeight ) {
				$highestTooltip = $(this);
				$highestTooltipIcon = $(this).parent('span');
				$highestTooltipHeight = $(this).height();
			}
		});
		var $tPosTop = $highestTooltipIcon.offset().top - $highestTooltip.innerHeight() - 32;
		return $tPosTop;
	}; var $highestTooltipPosition = getTooltipPosition();




	//-------- ADDING, EDITING, REMOVING ELEMENTS ----------//


	// FUNCTION ADD NEW ELEMENT
	function addNewElement( $element ) {
		// if we edit an element - we have to make sure X and Y positions are set
		if ( $focusedElement != null ) {
			$newPosX = parseInt( $focusedElement.css('left') );
			$newPosY = parseInt( $focusedElement.css('top') );
		}
		var $output = '<div class="' + $block + '" style="left: ' + $newPosX + 'px; top: ' + $newPosY + 'px;">';
			$output += $element;
			$output += '<div class="' + $borders + '"></div>';
			$output += '<div class="' + $overlay + '"></div>';
			$output += '<div class="' + $buttons + '">';
				$output += '<span class="' + $buttonDelete + '">Delete</span>';
				$output += '<span class="' + $buttonEdit + '">Edit</span>';
			$output += '</div>';
		$output += '</div>';
		$focusedContainer.append( $output );
		$newPosX = $newPosY = null;
	}



	// BUTTON DELETE
	$( '.' + $block ).find( '.' + $buttonDelete ).live( 'click', function () {
		if ( confirm('Do you really want to delete the widget?') ) $(this).parents( '.' + $block ).remove();
	});



	// BUTTON EDIT
	$( '.' + $block ).find( '.' + $buttonEdit ).live( 'click', function () {

		// show hidden fields
		$( '.' + $hiddenPopupFields ).show();

		// update the global vars
		$createNewElement = false;
		$focusedElement = $(this).parents( '.' + $block );

		// updating the appropriate form and then show fancybox to edit the elements

		// disable field type select
		$('#contestTextFieldType, #contestTextareaFieldType, #contestSelectFieldType, #contestRadioFieldType, #contestCheckboxFieldType').attr( 'disabled', true );

		// hide validation dropdown
		toggleTextInputValidation('hide');
		// but show it, if user edits input text "open" field
		if ( $focusedElement.find(':text').length ) {
			if ( $focusedElement.find(':text').attr('name').indexOf('Open_Field') != -1 )
				toggleTextInputValidation('show');
		}

		// google plus
		if ( $focusedElement.find('.googlePlus').length ) {
			var $url = $focusedElement.find('.googlePlus').attr('data-href');
			$('[name="google1URL"]').val( $url );
			// show form
			$('a[href="#popup_iconGoogle1"]').trigger('click');
		} // google plus end

		// iframe
		else if ( $focusedElement.find('.iframe').length ) {
			// update iframe url
			var $url = $focusedElement.find('iframe').attr('src');
			$('[name="iFrameURL"]').val( $url );
			// update scrolling
			var $scroll = $focusedElement.find('iframe').attr('scrolling');
			$scroll = $scroll == 'yes' ? true : false;
			$('#iFrameScrolling').prop( 'checked', $scroll );
			// show form
			$('a[href="#popup_iconIframe"]').trigger('click');
		} // iframe end

		// cool slider
		else if ( $focusedElement.find('.coolSlider').length ) {
			var $coolSlider = $focusedElement.find('.coolSlider');
			$('.coolSliderBgColor').css({ backgroundColor: $coolSlider.attr('data-bg') });
			$('.coolSliderBorderColor').css({ backgroundColor: $coolSlider.attr('data-border') });
			$('[name="coolSliderWidth"]').val( $coolSlider.attr('data-width') );
			$('[name="coolSliderHeight"]').val( $coolSlider.attr('data-height') );
			$('[name="coolSliderVisible"]').val( $coolSlider.attr('data-visible') );
			$coolSlider.find('li').each( function() {
				var $url = $(this).attr('data-url');
				coolSlider_newItem( $url );
			});
			// show form
			$('a[href="#popup_iconCoolSlider"]').trigger('click');
		} // cool slider end

		// youtube
		else if ( $focusedElement.find('.youtube').length ) {
			// update youtube url
			var $url = $focusedElement.find('embed').attr('src');
			$url = 'https://www.youtube.com/watch?v=' + $url.substr( $url.lastIndexOf('/') + 1 );
			$('[name="youtubeURL"]').val( $url );
			// show form
			$('a[href="#popup_iconYoutube"]').trigger('click');
		} // youtube end

		// vimeo
		else if ( $focusedElement.find('.vimeo').length ) {
			// update vimeo id
			var $url = $focusedElement.find('embed').attr('src');
			var $posStart = $url.indexOf('clip_id=') + 8;
			var $posEnd = $url.indexOf( '&', $posStart );
			var $id = $url.substring( $posStart, $posEnd );
			$('[name="vimeoURL"]').val( 'https://vimeo.com/' + $id );
			// show form
			$('a[href="#popup_iconVimeo"]').trigger('click');
		} // vimeo end

		// image
		else if ( $focusedElement.find('.contentImg').length ) {
			var $myImg = $focusedElement.find('.contentImg');
			// update imageURL
			$('[name="imageURL"]').val( $myImg.attr('src') );
			// update hyperlink field, if required
			if ( $myImg.parent('a').length )
				$('[name="imageLinkURL"]').val( $myImg.parent('a').attr('href') );
			else
				$('[name="imageLinkURL"]').val('');
			// if edited image is uploaded file, prepare to delete it
			if ( $myImg.attr('src').indexOf( $uploadedImagesFolder ) != -1 ) {
				var $fileName = $myImg.attr('src').substr( $uploadedImagesFolder.length );
				var $fileID = ajaxGetIdByName( $fileName );
				$('[name="imageToUpdate"]').val( $fileID );
			} else {
				$('[name="imageToUpdate"]').val('');
			}
			// show form
			$('a[href="#popup_iconImage"]').trigger('click');
		} // image end

		// hotspot
		else if ( $focusedElement.find('.contentHotspot').length ) {
			$('[name="hotspotURL"]').val( $focusedElement.find('.contentHotspot').parent('a').attr('href') );
			// show form
			$('a[href="#popup_iconHotspot"]').trigger('click');
		} // hotspot end

		// html
		else if ( $focusedElement.find('.contentHtml').length ) {
			$('[name="html"]').val( $focusedElement.find('.contentHtml').html() );
			// show form
			$('a[href="#popup_iconHtml"]').trigger('click');
		} // html end

		// text
		else if ( $focusedElement.find('.contentText').length ) {
			var $text = $focusedElement.find('.contentText').html();
			$('[name="text"]').val( $text );
			// show form
			$('a[href="#popup_iconText"]').trigger('click');
		} // text end

		// FB like
		else if ( $focusedElement.find('fb\\:like').length ) {
			var $url = $focusedElement.find('fb\\:like').attr('data-url');
			$('[name="likeURL"]').val( $url );
			// show form
			$('a[href="#popup_iconLike"]').trigger('click');
		} // FB like end

		// FB send
		else if ( $focusedElement.find('fb\\:send').length ) {
			var $url = $focusedElement.find('fb\\:send').attr('href');
			$('[name="sendURL"]').val( $url );
			// show form
			$('a[href="#popup_iconFacebookSend"]').trigger('click');
		} // FB send end

		// FB comments
		else if ( $focusedElement.find('fb\\:comments').length ) {
			var $url = $focusedElement.find('fb\\:comments').attr('href');
			var $num = $focusedElement.find('fb\\:comments').attr('num_posts');
			$('[name="commentsURL"]').val( $url );
			$('[name="commentsNumber"] option[value="' + $num + '"]').prop( 'selected', true );
			// show form
			$('a[href="#popup_iconComment"]').trigger('click');
		} // FB comments end

		// FB invite button
		else if ( $focusedElement.find('.contentInvite').length ) {
			var $button = $focusedElement.find('.contentInvite');
			$('[name="inviteTitle"]').val( $button.attr('data-title') );
			$('[name="inviteMessage"]').val( $button.attr('data-message') );
			$('[name="inviteBtnURL"]').val( $button.attr('src') );
			// if edited invite button is uploaded file, prepare to delete it
			if ( $button.attr('src').indexOf( $uploadedImagesFolder ) != -1 ) {
				var $fileName = $button.attr('src').substr( $uploadedImagesFolder.length );
				var $fileID = ajaxGetIdByName( $fileName );
				$('[name="inviteToUpdate"]').val( $fileID );
			} else {
				$('[name="inviteToUpdate"]').val('');
			}
			// show form
			$('a[href="#popup_iconFacebookInvite"]').trigger('click');
		} // FB invite button end

		// FB share (post to feed)
		else if ( $focusedElement.find('.contentFbPostFeed').length ) {
			var $postFeed = $focusedElement.find('.contentFbPostFeed');
			var $pLink = $postFeed.attr('data-url');
			var $pName = $postFeed.attr('data-name');
			var $pDescr = $postFeed.attr('data-descr');
			var $pCaption = $postFeed.attr('data-caption');
			_postFeedImgImage = $postFeed.attr('data-img');
			_postFeedBtnImage = $postFeed.attr('src');
			$('[name="postFeedLink"]').val( $pLink );
			$('[name="postFeedName"]').val( $pName );
			$('[name="postFeedDescr"]').val( $pDescr );
			$('[name="postFeedCaption"]').val( $pCaption );
			$('[name="postFeedImgURL"]').val( _postFeedImgImage );
			$('[name="postFeedBtnURL"]').val( _postFeedBtnImage );
			// switch
			var $pLinkSwitch = $('[name="postFeedLinkSwitch"]');
			if ( $pLink == '' )
				$pLinkSwitch.filter( function() { return $(this).val() == 0 }).prop( 'checked', true ).change();
			else
				$pLinkSwitch.filter( function() { return $(this).val() == 1 }).prop( 'checked', true ).change();
			// if edited post image is an uploaded file, prepare to delete it
			if ( _postFeedImgImage.indexOf( $uploadedImagesFolder ) != -1 ) {
				var $fileName = _postFeedImgImage.substr( $uploadedImagesFolder.length );
				var $fileID = ajaxGetIdByName( $fileName );
				$('[name="postFeedImgToUpdate"]').val( $fileID );
			} else {
				$('[name="postFeedImgToUpdate"]').val('');
			}
			// if edited button is an uploaded file, prepare to delete it
			if ( _postFeedBtnImage.indexOf( $uploadedImagesFolder ) != -1 ) {
				var $fileName = _postFeedBtnImage.substr( $uploadedImagesFolder.length );
				var $fileID = ajaxGetIdByName( $fileName );
				$('[name="postFeedBtnToUpdate"]').val( $fileID );
			} else {
				$('[name="postFeedBtnToUpdate"]').val('');
			}
			// show form
			$('a[href="#popup_iconFacebookPostToFeed"]').trigger('click');
		} // FB share (post to feed) end

		// twitter (stream)
		else if ( $focusedElement.find('.contentTwitter').length ) {
			var $twitter = $focusedElement.find('.contentTwitter');
			$('[name="twitterName"]').val( $twitter.attr('data-name') );
			$('[name="tweetsNumber"] option[value="' + $twitter.attr('data-posts') + '"]').prop( 'selected', true );
			$('.twitter.link').css({ backgroundColor: $twitter.attr('data-link-color') });
			$('.twitter.text').css({ backgroundColor: $twitter.attr('data-text-color') });
			// show form
			$('a[href="#popup_iconTwitter"]').trigger('click');
		} // twitter stream end

		// twitter share button
		else if ( $focusedElement.find('.contentTwitterShare').length ) {
			var $button = $focusedElement.find('.contentTwitterShare');
			$('[name="twitterShareURL"]').val( $button.attr('data-url') );
			$('[name="twitterShareText"]').val( $button.attr('data-text') );
			$('[name="twitterShareAccount"]').val( $button.attr('data-account') );
			$('[name="twitterShareButtonURL"]').val( $button.find('img').attr('src') );
			// switch
			var $urlSwitch = $('[name="twitterShareURLSwitch"]');
			if ( $button.attr('data-url') == '' )
				$urlSwitch.filter( function() { return $(this).val() == 0 }).prop( 'checked', true ).change();
			else
				$urlSwitch.filter( function() { return $(this).val() == 1 }).prop( 'checked', true ).change();
			// if edited twitter share button is uploaded file, prepare to delete it
			if ( $button.find('img').attr('src').indexOf( $uploadedImagesFolder ) != -1 ) {
				var $fileName = $button.find('img').attr('src').substr( $uploadedImagesFolder.length );
				var $fileID = ajaxGetIdByName( $fileName );
				$('[name="twitterShareToUpdate"]').val( $fileID );
			} else {
				$('[name="twitterShareToUpdate"]').val('');
			}
			// show form
			$('a[href="#popup_iconTwitterShare"]').trigger('click');
		} // twitter share button end

		// pinterest pin it button
		else if ( $focusedElement.find('.contentPinIt').length ) {
			var $button = $focusedElement.find('.contentPinIt');
			$('[name="pinItPageURL"]').val( $button.attr('data-page') );
			$('[name="pinItImageURL"]').val( $button.attr('data-img') );
			$('[name="pinItDescr"]').val( $button.attr('data-descr') );
			$('[name="pinItCount"] option[value="' + $button.attr('data-count') + '"]').prop( 'selected', true );
			// show form
			$('a[href="#popup_iconPinterestPinIt"]').trigger('click');
		} // pinterest pin it button end

		// pinterest follow button
		else if ( $focusedElement.find('.contentPinFollow').length ) {
			var $button = $focusedElement.find('.contentPinFollow');
			$('[name="pinFollowUser"]').val( $button.attr('data-user') );
			$('[name="pinFollowBtnType"] [value="' + $button.attr('data-btn') + '"]').prop( 'selected', true );
			// show form
			$('a[href="#popup_iconPinterestFollow"]').trigger('click');
		} // pinterest follow button end

		// rss
		else if ( $focusedElement.find('.contentRSS').length ) {
			var $rss = $focusedElement.find('.contentRSS');
			$('[name="rssURL"]').val( $rss.attr('data-url') );
			$('[name="rssNumber"] option[value="' + $rss.attr('data-posts') + '"]').prop( 'selected', true );
			$('.rss.link').css({ backgroundColor: $rss.attr('data-link-color') });
			$('.rss.text').css({ backgroundColor: $rss.attr('data-text-color') });
			// show form
			$('a[href="#popup_iconRSS"]').trigger('click');
		} // rss end

		// google maps
		else if ( $focusedElement.find('.contentGoogleMaps').length ) {
			var $gm = $focusedElement.find('.contentGoogleMaps');
			$('[name="gmAddress"]').val( decodeURIComponent( $gm.attr('data-address') ) );
			$('[name="gmZoom"]').val( $gm.attr('data-zoom') );
			// show form
			$('a[href="#popup_iconGoogleMaps"]').trigger('click');
		} // google maps end

		// countdown
		else if ( $focusedElement.find('.contentCountdown').length ) {
			var $countdown = $focusedElement.find('.contentCountdown');
			$('[name="countdownDate"]').val( $countdown.attr('data-date') );
			$('[name="countdownSize"]').val( $countdown.attr('data-size') );
			$('.countdown.text').css({ backgroundColor: $countdown.attr('data-color') });
			// show form
			$('a[href="#popup_iconCountdown"]').trigger('click');
		} // countdown end

		// contact form
		else if ( $focusedElement.find('.contentContactForm').length ) {
			var $contactForm = $focusedElement.find('.contentContactForm');
			$('[name="contactAddress"]').val( $contactForm.find('a').attr('href').substr(7) );
			$('[name="contactButtonURL"]').val( $contactForm.find('img').attr('src') );
			var $fieldName = $contactForm.find('.name').length ? true : false;
			var $fieldPhone = $contactForm.find('.phone').length ? true : false;
			$('[name="contactName"]').prop( 'checked', $fieldName );
			$('[name="contactPhone"]').prop( 'checked', $fieldPhone );
			// if edited contact button is uploaded file, prepare to delete it
			if ( $contactForm.find('img').attr('src').indexOf( $uploadedImagesFolder ) != -1 ) {
				var $fileName = $contactForm.find('img').attr('src').substr( $uploadedImagesFolder.length );
				var $fileID = ajaxGetIdByName( $fileName );
				$('[name="contactButtonToUpdate"]').val( $fileID );
			} else {
				$('[name="contactButtonToUpdate"]').val('');
			}
			// show form
			$('a[href="#popup_iconContact"]').trigger('click');
		} // contact form end

		// email button
		else if ( $focusedElement.find('.contentEmail').length ) {
			var $eButton = $focusedElement.find('.contentEmail');
			$('[name="emailAddress"]').val( $eButton.attr('href').substr(7) );
			$('[name="emailButtonURL"]').val( $eButton.find('img').attr('src') );
			// if edited email button is uploaded file, prepare to delete it
			if ( $eButton.find('img').attr('src').indexOf( $uploadedImagesFolder ) != -1 ) {
				var $fileName = $eButton.find('img').attr('src').substr( $uploadedImagesFolder.length );
				var $fileID = ajaxGetIdByName( $fileName );
				$('[name="emailToUpdate"]').val( $fileID );
			} else {
				$('[name="emailToUpdate"]').val('');
			}
			// show form
			$('a[href="#popup_iconEmail"]').trigger('click');
		} // email button end

		// flickr
		else if ( $focusedElement.find('.contentFlickr').length ) {
			var $flickrId = $focusedElement.find('.contentFlickr').attr('data-id');
			$('[name="flickrId"]').val( $flickrId );
			// show form
			$('a[href="#popup_iconFlickr"]').trigger('click');
		} // flickr end

		// contest - text input
		else if ( $focusedElement.find('.contestTextInput').length ) {
			var $input = $focusedElement.find('.contestTextInput input');
			// label, name and error
			$('[name="contestTextLabel"]').val( $input.siblings('label').html() );
			$('[name="contestTextName"]').val( $input.attr('name') );
			$('[name="contestTextError"]').val( $input.attr('data-error') );
			// required
			var $required = $input.is('.required') ? true : false;
			$('[name="contestTextRequired"]').prop( 'checked', $required );
			// validation
			$('[name="contestTextValidation"] [value="' + $input.attr('data-validation') + '"]').attr( 'selected', true );
			// show form
			$('a[href="#popup_iconContestText"]').trigger('click');
		} // contest - text input end

		// contest - textarea
		else if ( $focusedElement.find('.contestTextarea').length ) {
			var $textarea = $focusedElement.find('.contestTextarea textarea');
			$('[name="contestTextareaLabel"]').val( $textarea.siblings('label').html() );
			$('[name="contestTextareaName"]').val( $textarea.attr('name') );
			$('[name="contestTextareaError"]').val( $textarea.attr('data-error') );
			var $required = $textarea.is('.required') ? true : false;
			$('[name="contestTextareaRequired"]').prop( 'checked', $required );
			// show form
			$('a[href="#popup_iconContestTextarea"]').trigger('click');
		} // contest - textarea end

		// contest - select
		else if ( $focusedElement.find('.contestSelect').length ) {
			var $select = $focusedElement.find('.contestSelect select');
			var $optionsList = $select.find('option');
			// label and name
			$('[name="contestSelectLabel"]').val( $select.siblings('label').html() );
			$('[name="contestSelectName"]').val( $select.attr('name') );
			// default value
			var $options;
			for ( $i = 0; $i < $optionsList.length; $i++ ) {
				var $text = $optionsList.eq( $i ).html();
				var $value = $optionsList.eq( $i ).val();
				if ( $optionsList.eq( $i ).is('[data-selected]') )
					$options += '<option value="' + $value + '" selected="selected">' + $text + '</option>';
				else
					$options += '<option value="' + $value + '">' + $text + '</option>';
			}
			$('[name="contestSelectDefault"]').append( $options );
			// add options list
			$('[name="contestSelectOptionText"]').before('<ul class="contestSelectOptionsList" />');
			var $options = '';
			for ( $i = 0; $i < $optionsList.length; $i++ ) {
				var $text = $optionsList.eq( $i ).html();
				var $value = $optionsList.eq( $i ).val();
				$options += '<li>' + $text + ' <a class="deleteContestSelectOption" href="' + $value + '" ';
				$options += 'data-text="' + $text + '">x</a></li>';
			}
			$('.contestSelectOptionsList').append( $options );
			// show form
			$('a[href="#popup_iconContestSelect"]').trigger('click');
		} // contest - select end

		// contest - radio
		else if ( $focusedElement.find('.contestRadio').length ) {
			var $radioGroup = $focusedElement.find('.contestRadio .radioGroup');
			var $optionsList = $radioGroup.find('input');
			// label and name
			$('[name="contestRadioLabel"]').val( $radioGroup.siblings('label').html() );
			$('[name="contestRadioName"]').val( $optionsList.eq(0).attr('name') );
			// default value
			var $options;
			for ( $i = 0; $i < $optionsList.length; $i++ ) {
				var $text = $optionsList.eq( $i ).next('.radioValue').html();
				var $value = $optionsList.eq( $i ).val();
				if ( $optionsList.eq( $i ).is('[checked]') )
					$options += '<option value="' + $value + '" selected="selected">' + $text + '</option>';
				else
					$options += '<option value="' + $value + '">' + $text + '</option>';
			}
			$('[name="contestRadioDefault"]').append( $options );
			// add options list
			$('[name="contestRadioOptionText"]').before('<ul class="contestRadioOptionsList" />');
			var $options = '';
			for ( $i = 0; $i < $optionsList.length; $i++ ) {
				var $text = $optionsList.eq( $i ).next('.radioValue').html();
				var $value = $optionsList.eq( $i ).val();
				$options += '<li>' + $text + ' <a class="deleteContestRadioOption" href="' + $value + '" ';
				$options += 'data-text="' + $text + '">x</a></li>';
			}
			$('.contestRadioOptionsList').append( $options );
			// show form
			$('a[href="#popup_iconContestRadio"]').trigger('click');
		} // contest - radio end

		// contest - checkbox
		else if ( $focusedElement.find('.contestCheckbox').length ) {
			var $checkbox = $focusedElement.find('.contestCheckbox input');
			$('[name="contestCheckboxLabel"]').val( $checkbox.siblings('label').html() );
			$('[name="contestCheckboxName"]').val( $checkbox.attr('name') );
			$('[name="contestCheckboxError"]').val( $checkbox.attr('data-error') );
			$('[name="contestCheckboxValue"]').val( $checkbox.attr('value') );
			var $required = $checkbox.is('.required') ? true : false;
			$('[name="contestCheckboxRequired"]').prop( 'checked', $required );
			// show form
			$('a[href="#popup_iconContestCheckbox"]').trigger('click');
		} // contest - checkbox end

		// contest - button
		else if ( $focusedElement.find('.contestButton').length ) {
			var $btn = $focusedElement.find('.contestButton img');
			$('[name="contestBtnType"] [value="' + $btn.attr('data-type') + '"]').attr( 'selected', true );
			$('[name="contestBtnImageURL"]').val( $btn.attr('src') );
			// if edited button is uploaded file, prepare to delete it
			if ( $btn.attr('src').indexOf( $uploadedImagesFolder ) != -1 ) {
				var $fileName = $btn.attr('src').substr( $uploadedImagesFolder.length );
				var $fileID = ajaxGetIdByName( $fileName );
				$('[name="contestBtnImageToUpdate"]').val( $fileID );
			} else {
				$('[name="contestBtnImageToUpdate"]').val('');
			}
			// show form
			$('a[href="#popup_iconContestBtn"]').trigger('click');
		} // contest - button end

		// contest - ugc gallery
		else if ( $focusedElement.find('.ugcGallery').length ) {
			var $ug = $focusedElement.find('.ugcGallery');
			$('[name="ugcGalleryLayout"] option[value="' + $ug.attr('data-layout') + '"]').prop( 'selected', true );
			$('.ugcGalleryBgColor').css({ backgroundColor: $ug.attr('data-bg-color') });
			$('.ugcGalleryBorderColor').css({ backgroundColor: $ug.attr('data-border-color') });
			$('.ugcGalleryTextColor').css({ backgroundColor: $ug.attr('data-text-color') });
			$('.ugcGalleryBtnColor').css({ backgroundColor: $ug.attr('data-btn-color') });
			$('.ugcGalleryBtnTextColor').css({ backgroundColor: $ug.attr('data-btn-text-color') });
			// show form
			$('a[href="#popup_iconContestUGCGallery"]').trigger('click');
		} // contest - ugc gallery end

		// contest - ugc upload area
		else if ( $focusedElement.find('.contestUGCUploadArea').length ) {
			var $UGCArea = $focusedElement.find('.contestUGCUploadArea');
			$('.ugcUploadAreaBgColor').css({ backgroundColor: $UGCArea.attr('data-bg') });
			$('[name="ugcUploadAreaError"]').val( $UGCArea.attr('data-error') );
			// show form
			$('a[href="#popup_iconContestUGCUploadArea"]').trigger('click');
		} // contest - ugc upload area end

	}); // button edit



	// CLICK TO EDIT CONTEST PROPERTIES
	$('a[href="#popup_contest_properties"]').click( function() {
		var $contest = $( '.' + $pageBody + '.' + $contestFormClass );
		$('[name="contestDescr"]').val( $contest.attr('data-contest-descr') );
		$('[name="contestStart"]').val( $contest.attr('data-contest-start') );
		$('[name="contestEnd"]').val( $contest.attr('data-contest-end') );
		$('[name="contestVotingStart"]').val( $contest.attr('data-contest-voting-start') );
		$('[name="contestVotingEnd"]').val( $contest.attr('data-contest-voting-end') );
		// voting times
		var $votingTimes = parseInt( $contest.attr('data-contest-voting-times') );
		if ( isNaN($votingTimes) ) $votingTimes = '';
		if ( $votingTimes <= 0 ) {
			$('[name="contestVotingTimes"][value="' + $votingTimes + '"]').attr( 'checked', true );
		} else {
			$('[name="contestVotingTimes"][value="1"]').attr( 'checked', true );
			$('[name="contestVotingTimes"]').trigger('change');
			$('[name="contestVotingPerDay"]').val( $votingTimes );
		}
		// entries number
		$('[name="contestEntries"]').val( $contest.attr('data-contest-entries') );
		// age
		var $age = $contest.attr('data-contest-age') == '1' ? true : false;
		$('[name="contestAge"]').prop( 'checked', $age );
	});



	// CLICK TO EDIT CONTEST FORM DESIGN
	$('a[href="#popup_contest_form_design"]').click( function() {
		var $field = $('.contestFormField');
		// width
		$('#contestLabelWidth').val( $field.find('label').width() );
		if ( $field.find('textarea, [type="text"], .radioGroup').length )
			$('#contestInputWidth').val( $field.find('textarea, [type="text"], .radioGroup').width() );
		else
			$('#contestInputWidth').val( $field.find('label').width() );
		// font size
		$('#contestLabelFontSize').val( $field.find('label').css('fontSize') );
		$('#contestInputFontSize').val( $field.find('select, textarea, [type="text"], .radioGroup').css('fontSize') );
		// text align
		$('#contestLabelAlign').val( $field.find('label').css('textAlign') );
		$('#contestInputAlign').val( $field.find('select, textarea, [type="text"]').css('textAlign') );
		// border size
		$('#contestInputBorderSize').val( $field.find('select, textarea, [type="text"]').css('border-left-width') );
		// font weight
		var $labelBold = $field.find('label').css('fontWeight') == 700 ? true : false;
		var $inputBold = $field.find('select, textarea, [type="text"]').css('fontWeight') == 700 ? true : false;
		$('#contestLabelBold').prop( 'checked', $labelBold );
		$('#contestInputBold').prop( 'checked', $inputBold );
		// font style
		var $labelItalic = $field.find('label').css('fontStyle') == 'italic' ? true : false;
		var $inputItalic = $field.find('select, textarea, [type="text"]').css('fontStyle') == 'italic' ? true : false;
		$('#contestLabelItalic').prop( 'checked', $labelItalic );
		$('#contestInputItalic').prop( 'checked', $inputItalic );
		// color
		$('#contestLabelColor').css({
			backgroundColor: hexConverter( $field.find('label').css('color') )
		});
		$('#contestInputColor').css({
			backgroundColor: hexConverter( $field.find('select, textarea, [type="text"], .radioGroup').css('color') )
		});
		$('#contestBackground').css({
			backgroundColor: hexConverter( $field.find('select, textarea, [type="text"]').css('backgroundColor') )
		});
		$('#contestBorderColor').css({
			backgroundColor: hexConverter( $field.find('select, textarea, [type="text"]').css('border-left-color') )
		});
	});



	// CLICK TO EDIT BACKGROUND
	$('a[href="#popup_backgroundSettings"]').click( function() {
		// update url
		var $bgURL = $focusedContainer.css('backgroundImage');
		if ( $bgURL != 'none' )
			$('[name="bgURL"]').val( getBgImgUrl($bgURL) );
		else
			$('[name="bgURL"]').val('');
		// find bg repeat
		var $bgRepeat = $focusedContainer.css('backgroundRepeat');
		$('#bgRepeat option[value="' + $bgRepeat + '"]').prop( 'selected', true );
		// if current bg is uploaded file, prepare to delete it
		var $bgImage = $focusedContainer.css('backgroundImage');
		if ( $bgImage.indexOf( $uploadedImagesFolder ) != -1 ) {
			var $fileName = getBgImgUrl( $bgImage, true );
			var $fileID = ajaxGetIdByName( $fileName );
			$('[name="bgToUpdate"]').val( $fileID );
		} else {
			$('[name="bgToUpdate"]').val('');
		}
	});



	// CLICK TO EDIT GOOGLE ANALYTICS
	$('a[href="#popup_analytics"]').click( function() {
		if ( $focusedContainer.find('.analytics_code').length )
			$('[name="googleUAcode"]').val( $focusedContainer.find('.analytics_code').html() );
		else
			$('[name="googleUAcode"]').val('');
	});



	// CLICK TO EDIT GRID
	$('a[href="#popup_gridSettings"]').click( function() {
		// set grid size
		if ( $focusedContainer.find('.grid').length ) {
			$('[name="gridSize"]').val( $focusedContainer.find('.grid').attr('data-size') );
		}
		// set grid visibility
		if ( $focusedContainer.find('.grid').length ) {
			var $visibility = $focusedContainer.find('.grid').is('.hidden') ? false : true;
			$('#gridVisible').prop( 'checked', $visibility );
		} else {
			$('#gridVisible').prop( 'checked', false );
		}
		// set grid snap
		if ( $focusedContainer.find('.grid').length ) {
			var $snap = $focusedContainer.find('.grid').attr('data-snap') == 'yes' ? true : false;
			$('#gridSnap').prop( 'checked', $snap );
		} else {
			$('#gridSnap').prop( 'checked', false );
		}
	});



	// RULER
	function createRuler() {
		var $stepLength = 50;

		// function splits long numbers by <br />
		function vertNumber( $number ) {
			var $newNumber = '';
			$number = $number.toString();
			for ( $a = 0; $a < $number.length; $a++ )
				$newNumber += $number.charAt( $a ) + '<br />';
			return $newNumber;
		}

		$('.pageBodyOuter').prepend('<div class="contentRuler horz" /><div class="contentRuler vert" />');

		// add cells to horizontal ruler
		for ( $i = 0; $i < 17 * $stepLength; $i += $stepLength )
			$('.contentRuler.horz').append('<div class="cell" style="left: ' + $i + 'px;">' + $i + '</div>');

		// add cells to vertical ruler
		for ( $i = 0; $i < 200 * $stepLength; $i += $stepLength )
			$('.contentRuler.vert').append('<div class="cell" style="top: ' + $i + 'px;">' + vertNumber($i) + '</div>');
	}; createRuler();

	// ruler show/hide
	$('.editorInstruments a.ruler').click( function() {
		var $element = $('.pageBodyOuter').find('.contentRuler');
		if ( $element.css('visibility') == 'visible' )
			$element.css({ visibility: 'hidden' });
		else
			$element.css({ visibility: 'visible' });
		return false;
	});



	// GLOBAL FUNCTION ADDING AND EDITING ELEMENTS
	function addingEditingElements() {

		// popup window submit
		$('.hiddenForms a.aButton.done').click( function() {

			// validate the form
			$(this).parents('form').validate({
				rules: {
					imageURL: { required: function() { return $('[name="contentImage"]').val() == ''; } },
					gridSize: { required: '[name="gridVisible"]:checked', number: true, min: 0, max: 810 },
					contestEntries: { required: true, number: true, min: 1 },
					contestTextError: { required: '[name="contestTextRequired"]:checked' },
					contestTextareaError: { required: '[name="contestTextareaRequired"]:checked' },
					contestCheckboxError: { required: '[name="contestCheckboxRequired"]:checked' },
					// file inputs
					bgImage: { file_size_validation: true },
					contentImage: { file_size_validation: true },
					inviteBtnImage: { file_size_validation: true },
					postFeedImgImage: { file_size_validation: true },
					postFeedBtnImage: { file_size_validation: true },
					twitterShareButtonImage: { file_size_validation: true },
					contactButtonImage: { file_size_validation: true },
					emailButtonImage: { file_size_validation: true },
					contestBtnImage: { file_size_validation: true },
					// ugc contest dates
					contestStart: { contest_start_date_validation: true },
					contestEnd: { contest_end_date_validation: true },
					contestVotingStart: { contest_voting_start_date_validation: true },
					contestVotingEnd: { contest_voting_end_date_validation: true }
				}
			});

			// validate contest start-end dates
			function contest_date_validation() {
				var $startDate = new Date( Date.parse($('[name="contestStart"]').val()) ).getTime();
				var $endDate = new Date( Date.parse($('[name="contestEnd"]').val()) ).getTime();
				return $startDate < $endDate;
			}
			$.validator.addMethod( 'contest_start_date_validation',
				contest_date_validation, 'Start date must be less than end date' );
			$.validator.addMethod( 'contest_end_date_validation',
				contest_date_validation, 'End date must be greater than start date' );

			// validate contest voting start-end dates
			function contest_voting_date_validation() {
				$result = false;
				var $vStartDate = $('[name="contestVotingStart"]').val();
				var $vEndDate = $('[name="contestVotingEnd"]').val();
				if ( $vStartDate == '' && $vEndDate == '' ) {
					$result = true;
				} else if ( $vStartDate != '' && $vEndDate == '' ) {
					$result = false;
				} else if ( $vStartDate == '' && $vEndDate != '' ) {
					$result = false;
				} else {
					$vStartDate = new Date( Date.parse($vStartDate) ).getTime();
					$vEndDate = new Date( Date.parse($vEndDate) ).getTime();
					if ( $vStartDate < $vEndDate ) $result = true;
				}
				return $result;
			}
			$.validator.addMethod( 'contest_voting_start_date_validation',
				contest_voting_date_validation, 'Voting start date must be less than voting end date' );
			$.validator.addMethod( 'contest_voting_end_date_validation',
				contest_voting_date_validation, 'Voting end date must be greater than voting start date' );

			// validate file size (except IE)
			$.validator.addMethod( 'file_size_validation', function( value, element ) {
				if ( $.browser.msie ) return true;
				else return value == '' || element.files[0].size <= $maxFileSize;
			}, 'Maximum image size is ' + $maxFileSize / 1024 / 1024 + ' Mb' );

			// validate file extension
			if ( $(this).parents('.form').find('.uploadFile').length ) {
				$(this).parents('.form').find('.uploadFile').each( function() {
					$(this).rules( 'add', {
						accept: 'png|jpe?g|gif',
						messages: {
							accept: 'Image must be JPG, GIF or PNG'
						}
					});
				});
			}

			// additional validation for ugc select and radio elements
			var $ugcSelectAndRadioOptionsValid = true;
			if ( $('#fancybox-wrap').find('.contestSelectOptionsList, .contestRadioOptionsList').length ) {
				if ( $('.contestSelectOptionsList, .contestRadioOptionsList').find('li').length == 1 ) {
					$ugcSelectAndRadioOptionsValid = false;
					var $inputs = $('[name="contestSelectOptionText"], [name="contestRadioOptionText"]');
					$inputs.val('Please add at least 2 options');
					setTimeout( function() { $inputs.val(''); }, 1000 );
				}
			}


			// the form is valid
			if ( $(this).parents('form').valid() && $ugcSelectAndRadioOptionsValid ) {

				var $displayWidget = new displayWidget();


				//--- OPEN WINDOW IS UNRELATED TO ANY WIDGET ---//
				if ( $(this).parents().is('.unrelatedToWidgets') ) {


					// change background
					if ( $(this).parents().is('#popup_backgroundSettings') ) {
						var $bgURL = $('[name="bgURL"]').val();
						// bg from uploaded image
						if ( $('input[name="bgImage"]').val() != '' ) {
							$uploadedImageRefersTo = 'changeBG';
							$(this).parents('form').submit();
						}
						// bg from url
						else updateBG( $bgURL );
					} // change background end


					// google analytics
					else if ( $(this).parents().is('#popup_analytics') ) {
						// get ua-code
						var $UAcode = $('[name="googleUAcode"]').val();
						// create a container for UA-code, if it doesn't exist
						if ( $focusedContainer.find('.analytics_code').length == 0 )
							$focusedContainer.prepend('<div class="analytics_code" />');
						// save the code
						$focusedContainer.find('.analytics_code').html( $UAcode );
						showAjaxMessage('The UA code has been added!');
					} // google analytics end


					// grid settings
					else if ( $(this).parents().is('#popup_gridSettings') ) {
						var $gridSize = parseInt( $('[name="gridSize"]').val() );
						$gridSize = isNaN( $gridSize ) ? 0 : $gridSize;
						var $gridVisible = $('#gridVisible').is(':checked') ? '' : 'hidden';
						var $gridSnap = $('#gridSnap').is(':checked') ? 'yes' : 'no';
						// create grid, if it doesn't exist
						if ( ! $focusedContainer.find('.grid').length ) {
							$focusedContainer.prepend('<div class="grid ' + $gridVisible + '" data-restore="' + $gridVisible + '" data-size="' + $gridSize + '" data-snap="' + $gridSnap + '" />');
						}
						// store grid container
						var $grid = $focusedContainer.find('.grid');
						// update grid
						$grid.removeClass('hidden').addClass( $gridVisible )
							.attr({ 'data-restore': $gridVisible, 'data-size': $gridSize, 'data-snap': $gridSnap })
							.css({ 'background-image': 'url(' + $websiteURL + 'images/grid/grid-' + $gridSize + '.png)' });
					} // grid settings end


					// contest properties
					else if ( $(this).parents().is('#popup_contest_properties') ) {
						var $cDescr = $('[name="contestDescr"]').val();
						var $cStart = $('[name="contestStart"]').val();
						var $cEnd = $('[name="contestEnd"]').val();
						var $cVotingStart = $('[name="contestVotingStart"]').val();
						var $cVotingEnd = $('[name="contestVotingEnd"]').val();
						var $cVotingTimes = $('[name="contestVotingTimes"]:checked').val();
						$cVotingTimes = ( $cVotingTimes != '1' ) ? $cVotingTimes : $('[name="contestVotingPerDay"]').val();
						var $cEntries = $('[name="contestEntries"]').val();
						var $cAge = $('[name="contestAge"]').is(':checked') ? '1' : '0';
						// add/update properties (they are saved as attributes of the contest form page and in DB)
						$( '.' + $pageBody + '.' + $contestFormClass ).attr({
							'data-contest-descr': $cDescr,
							'data-contest-start': $cStart,
							'data-contest-end': $cEnd,
							'data-contest-voting-start': $cVotingStart,
							'data-contest-voting-end': $cVotingEnd,
							'data-contest-voting-times': $cVotingTimes,
							'data-contest-entries': $cEntries,
							'data-contest-age': $cAge
						});
						showAjaxMessage('Contest properties have been set/updated!');
					} // contest properties end


					// contest form design
					else if ( $(this).parents().is('#popup_contest_form_design') ) {
						var $field = $('.contestFormField');

						// update data-color for all color pickers
						$('#popup_contest_form_design .uniPicker').each( function() {
							$(this).attr( 'data-color', hexConverter( $(this).css('background-color') ) )
						});

						// width and font (weight, style, size, color, align)
						var $labelBold = $('#contestLabelBold').is(':checked') ? 'bold' : '';
						var $labelItalic = $('#contestLabelItalic').is(':checked') ? 'italic' : '';
						$field.find('label').css({
							width:                $('#contestLabelWidth').val(),
							color:                 $('#contestLabelColor').attr('data-color'),
							textAlign:         $('#contestLabelAlign').val(),
							fontSize:           $('#contestLabelFontSize').val(),
							fontWeight:    $labelBold,
							fontStyle:         $labelItalic
						});

						var $inputBold = $('#contestInputBold').is(':checked') ? 'bold' : '';
						var $inputItalic = $('#contestInputItalic').is(':checked') ? 'italic' : '';
						$field.find('select, textarea, [type="text"], .radioGroup').css({
							width:                $('#contestInputWidth').val(),
							color:                 $('#contestInputColor').attr('data-color'),
							textAlign:         $('#contestInputAlign').val(),
							fontSize:           $('#contestInputFontSize').val(),
							fontWeight:    $inputBold,
							fontStyle:         $inputItalic
						});

						// inputs: border size, background color and border color
						$field.find('select, textarea, [type="text"]').css({
							borderWidth:            $('#contestInputBorderSize').val(),
							borderColor:              $('#contestBorderColor').attr('data-color'),
							backgroundColor:   $('#contestBackground').attr('data-color')
						});

						// line-height for all fields
						var $borderSize = parseInt( $('#contestInputBorderSize').val() );
						var $fontSize = parseInt( $('#contestInputFontSize').val() );
						var $lineHeight = ( $borderSize * 2 ) + $fontSize + 6 + 4;
						$field.css({ lineHeight: $lineHeight + 'px' });

						// text input height
						var $textInputHeight = parseInt( $lineHeight - 6 - ( $borderSize * 2 ) );
						$field.find('[type="text"]').css({
							height:           $textInputHeight + 'px',
							lineHeight:  $textInputHeight + 'px'
						});

						// select's width
						var $dif = contestSelectWidth( $field, $('#contestInputBorderSize') );
						$field.find('select').css({
							width: parseInt( $('#contestInputWidth').val() ) + $dif
						});

						// select's height
						$field.find('select').css({
							height:           $lineHeight + 'px',
							lineHeight:  $lineHeight + 'px'
						});

						// radio and checkbox margin-top
						var $marginTop = Math.ceil( ( $lineHeight - 13 ) / 2 );
						$field.find('[type="checkbox"], [type="radio"]').css({ marginTop: $marginTop + 'px' });

					} // contest form design end


				} //--- OPEN WINDOW IS UNRELATED TO ANY WIDGET END ---//



				//--- OPEN WINDOW IS RELATED TO A WIDGET ---//
				else {


					//--- CREATE NEW ELEMENT ---//
					if ( $createNewElement ) {

						// variable for new element
						var $output = null;

						// create Google Plus
						if ( $(this).parents().is('#popup_iconGoogle1') ) {
							var $url = $('[name="google1URL"]').val();
							$output = '<div class="googlePlus" id="unparsedGooglePlus" data-href="' + $url + '" />';
						} // create Google Plus end

						// create iframe
						else if ( $(this).parents().is('#popup_iconIframe') ) {
							var $url = $('[name="iFrameURL"]').val();
							var $scroll = $('#iFrameScrolling').is(':checked') ? 'yes' : 'no';
							$output = '<iframe class="iframe" src="' + $url + '" scrolling="' + $scroll + '" ';
							$output += 'frameborder="0"></iframe>';
						} // create iframe end

						// create cool slider
						else if ( $(this).parents().is('#popup_iconCoolSlider') ) {
							$output = createUpdateCoolSlider();
						} // create cool slider end

						// create youtube
						else if ( $(this).parents().is('#popup_iconYoutube') ) {
							var $url = $('[name="youtubeURL"]').val();
							$output = $displayWidget.createYoutube( $url );
						} // create youtube end

						// create vimeo
						else if ( $(this).parents().is('#popup_iconVimeo') ) {
							var $url = $('[name="vimeoURL"]').val();
							$output = $displayWidget.createVimeo( $url );
						} // create vimeo end

						// create image
						else if ( $(this).parents().is('#popup_iconImage') ) {
							var $imageURL = $('[name="imageURL"]').val();
							var $imageLinkURL = $('[name="imageLinkURL"]').val();
							// if user uploads an image
							if ( $('[name="contentImage"]').val() != '' ) {
								$uploadedImageRefersTo = 'addImage';
								$(this).parents('form').submit();
							}
							else $output = createUpdateImage( $imageURL, $imageLinkURL );
						} // create image end

						// create hotspot
						else if ( $(this).parents().is('#popup_iconHotspot') ) {
							var $url = $('[name="hotspotURL"]').val();
							$output = '<a target="_blank" href="' + $url + '"><img class="contentHotspot" ';
							$output += 'src="' + $websiteURL + 'images/transparentPixel.png" /></a>';
						} // create hotspot end

						// create html
						else if ( $(this).parents().is('#popup_iconHtml') ) {
							var $html = $('[name="html"]').val();
							$output = '<div class="contentHtml">' + $html + '</div>';
						} // create html end

						// create text
						else if ( $(this).parents().is('#popup_iconText') ) {
							var $text = $('.cleditorMain iframe').contents().find('body').html();
							$output = '<div class="contentText">' + $text + '</div>';
						} // create text end

						// create FB like button
						else if ( $(this).parents().is('#popup_iconLike') ) {
							var $url = $('[name="likeURL"]').val();
							var $pageUrl = $uploadedImagesFolder + 'index.php?url=' + $url;
							$output = '<fb:like id="unparsedFB" data-url="' + $url + '" href="' + $pageUrl + '" send="false" ';
							$output += 'width="77" data-layout="button_count" show-faces="false"></fb:like>';
						} // create FB like button end

						// create FB send button
						else if ( $(this).parents().is('#popup_iconFacebookSend') ) {
							var $url = $('[name="sendURL"]').val();
							$output = '<fb:send id="unparsedFB" href="' + $url + '"></fb:send>';
						} // create FB send button end

						// create FB comments
						else if ( $(this).parents().is('#popup_iconComment') ) {
							var $url = $('[name="commentsURL"]').val();
							var $num = $('[name="commentsNumber"]').val();
							$output = '<fb:comments id="unparsedFB" href="' + $url + '" num_posts="' + $num + '" ';
							$output += 'width="470"></fb:comments>';
						} // create FB comments end

						// create FB invite button
						else if ( $(this).parents().is('#popup_iconFacebookInvite') ) {
							var $title = $('[name="inviteTitle"]').val();
							var $message = $('[name="inviteMessage"]').val();
							var $buttonURL = $('[name="inviteBtnURL"]').val();
							// if user uploads an image
							if ( $('[name="inviteBtnImage"]').val() != '' ) {
								$uploadedImageRefersTo = 'addInviteButton';
								$(this).parents('form').submit();
							}
							else $output = createUpdateInviteButton( $title, $message, $buttonURL );
						} // create FB invite button end

						// create FB share (post to feed)
						else if ( $(this).parents().is('#popup_iconFacebookPostToFeed') ) {
							var $pLink = '';
							if ( $('[name="postFeedLinkSwitch"]:checked').val() == 1 )
								$pLink = $('[name="postFeedLink"]').val();
							var $pName = $('[name="postFeedName"]').val();
							var $pDescr = $('[name="postFeedDescr"]').val();
							var $pCaption = $('[name="postFeedCaption"]').val();
							_postFeedImgImage = $('[name="postFeedImgURL"]').val();
							_postFeedBtnImage = $('[name="postFeedBtnURL"]').val();
							// if user uploads one or more images
							if ( $('[name="postFeedImgImage"]').val() != '' || $('[name="postFeedBtnImage"]').val() != '' ) {
								$uploadedImageRefersTo = 'addFbPostFeed';
								// which images need to be updated
								arrImagesToUpdate = [];
								if ( $('[name="postFeedImgImage"]').val() != '' ) arrImagesToUpdate.push( 0 );
								if ( $('[name="postFeedBtnImage"]').val() != '' ) arrImagesToUpdate.push( 1 );
								$(this).parents('form').submit();
							}
							else $output = createUpdateFbPostFeed( $pLink, $pName, $pDescr, $pCaption );
						} // create share (post to feed) end

						// create twitter (stream)
						else if ( $(this).parents().is('#popup_iconTwitter') ) {
							var $name = $('[name="twitterName"]').val();
							var $postsNumber = $('[name="tweetsNumber"]').val();
							var $linkColor = hexConverter( $('.twitter.link').css('background-color') );
							var $textColor = hexConverter( $('.twitter.text').css('background-color') );
							$output = '<div class="contentTwitter" id="twitter-' + generateId() + '" ';
							$output += 'data-name="' + $name + '" data-posts="' + $postsNumber + '" ';
							$output += 'data-link-color="' + $linkColor + '" data-text-color="' + $textColor + '" />';
						} // create twitter steam end

						// create twitter share button
						else if ( $(this).parents().is('#popup_iconTwitterShare') ) {
							var $account = $('[name="twitterShareAccount"]').val();
							var $text = $('[name="twitterShareText"]').val();
							var $url = '';
							if ( $('[name="twitterShareURLSwitch"]:checked').val() == 1 )
								$url = $('[name="twitterShareURL"]').val();
							var $buttonURL = $('[name="twitterShareButtonURL"]').val();
							// if user uploads an image
							if ( $('[name="twitterShareButtonImage"]').val() != '' ) {
								$uploadedImageRefersTo = 'addTwitterShareButton';
								$(this).parents('form').submit();
							}
							else $output = createUpdateTwitterShareButton( $account, $text, $url, $buttonURL );
						} // create twitter share button end

						// create pinterest pin it button
						else if ( $(this).parents().is('#popup_iconPinterestPinIt') ) {
							var $pinItPage = $('[name="pinItPageURL"]').val();
							var $pinItImage = $('[name="pinItImageURL"]').val();
							var $pinItDescr = $('[name="pinItDescr"]').val();
							var $pinItCount = $('[name="pinItCount"]').val();
							var $btn = "<a class='pin-it-button' count-layout='" + $pinItCount + "' ";
							$btn += "href='http://pinterest.com/pin/create/button/";
							$btn += "?url=" + encodeURIComponent( $pinItPage );
							$btn += "&media=" + encodeURIComponent( $pinItImage );
							$btn += "&description=" + encodeURIComponent( $pinItDescr ) + "'>";
							$btn += "<img title='Pin It' src='//assets.pinterest.com/images/PinExt.png' /></a>";
							$output = '<div class="contentPinIt unparsed" data-page="' + $pinItPage + '" ';
							$output += 'data-img="' + $pinItImage + '" data-descr="' + $pinItDescr + '" ';
							$output += 'data-count="' + $pinItCount + '" data-btn="' + $btn + '">' + $btn + '</div>';
						} // create pinterest pin it button end

						// create pinterest follow button
						else if ( $(this).parents().is('#popup_iconPinterestFollow') ) {
							var $user = $('[name="pinFollowUser"]').val();
							var $type = $('[name="pinFollowBtnType"]').val();
							$output = '<a class="contentPinFollow" data-user="' + $user + '" data-btn="' + $type + '" ';
							$output += 'href="http://pinterest.com/' + $user + '/" target="_blank">';
							$output += '<img src="http://passets-lt.pinterest.com/images/about/buttons/';
							$output += $type + '-button.png" alt="Follow Me on Pinterest" /></a>';
						} // create pinterest follow button end

						// create rss
						else if ( $(this).parents().is('#popup_iconRSS') ) {
							var $url = $('[name="rssURL"]').val();
							var $postsNumber = $('[name="rssNumber"]').val();
							var $linkColor = hexConverter( $('.rss.link').css('background-color') );
							var $textColor = hexConverter( $('.rss.text').css('background-color') );
							$output = '<div class="contentRSS" ';
							$output += 'data-url="' + $url + '" data-posts="' + $postsNumber + '" ';
							$output += 'data-link-color="' + $linkColor + '" data-text-color="' + $textColor + '" />';
						} // create rss end

						// create google maps
						else if ( $(this).parents().is('#popup_iconGoogleMaps') ) {
							var $address = encodeURIComponent( $('[name="gmAddress"]').val() );
							var $zoom = $('[name="gmZoom"]').val();
							$output = '<div class="contentGoogleMaps" id="gm-' + generateId() + '" ';
							$output += 'data-address="' + $address + '" data-zoom="' + $zoom + '" />';
						} // create google maps end

						// create countdown
						else if ( $(this).parents().is('#popup_iconCountdown') ) {
							var $date = $('[name="countdownDate"]').val();
							var $size = $('[name="countdownSize"]').val();
							var $color = hexConverter( $('.countdown.text').css('background-color') );
							$output = '<div class="contentCountdown" data-date="' + $date + '" ';
							$output += 'data-color="' + $color + '" data-size="' + $size + '" />';
						} // create countdown end

						// create contact form
						else if ( $(this).parents().is('#popup_iconContact') ) {
							var $cAddress = $('[name="contactAddress"]').val();
							var $cButtonURL = $('[name="contactButtonURL"]').val();
							var $cName = $('#contactName').is(':checked') ? true : false;
							var $cPhone = $('#contactPhone').is(':checked') ? true : false;
							// if user uploads an image
							if ( $('[name="contactButtonImage"]').val() != '' ) {
								$uploadedImageRefersTo = 'addContactForm';
								$(this).parents('form').submit();
							}
							else $output = createUpdateContactForm( $cAddress, $cButtonURL, $cName, $cPhone );
						} // create contact form end

						// create email button
						else if ( $(this).parents().is('#popup_iconEmail') ) {
							var $eAddress = $('[name="emailAddress"]').val();
							var $eButtonURL = $('[name="emailButtonURL"]').val();
							// if user uploads an image
							if ( $('[name="emailButtonImage"]').val() != '' ) {
								$uploadedImageRefersTo = 'addEmailButton';
								$(this).parents('form').submit();
							}
							else $output = createUpdateEmailButton( $eAddress, $eButtonURL );
						} // create email button end

						// create flickr
						else if ( $(this).parents().is('#popup_iconFlickr') ) {
							var $flickrId = $('[name="flickrId"]').val();
							$output = '<iframe src="http://www.flickr.com/slideShow/index.gne?user_id=' + $flickrId + '" ';
							$output += 'class="contentFlickr" data-id="' + $flickrId + '" frameBorder="0" scrolling="no">';
							$output += '</iframe>';
						} // create flickr end

						// create contest - text input
						else if ( $(this).parents().is('#popup_iconContestText') ) {
							var $label = $('[name="contestTextLabel"]').val();
							var $name = $('[name="contestTextName"]').val();
							var $required = $('[name="contestTextRequired"]').is(':checked') ? 'required' : '';
							var $validation = $('[name="contestTextValidation"]').val();
							var $error = $('[name="contestTextError"]').val();
							$output = '<div class="' + $contestFieldClass + ' contestTextInput">';
							$output += '<label>' + $label + '</label>';
							$output += '<input type="text" name="' + $name + '" class="' + $required + ' ' + $validation + '" ';
							$output += 'data-validation="' + $validation + '" data-error="' + $error + '" />';
							$output += '</div>';
						} // create contest - text input end

						// create contest - textarea
						else if ( $(this).parents().is('#popup_iconContestTextarea') ) {
							var $label = $('[name="contestTextareaLabel"]').val();
							var $name = $('[name="contestTextareaName"]').val();
							var $required = $('[name="contestTextareaRequired"]').is(':checked') ? 'required' : '';
							var $error = $('[name="contestTextareaError"]').val();
							$output = '<div class="' + $contestFieldClass + ' contestTextarea">';
							$output += '<label>' + $label + '</label>';
							$output += '<textarea name="' + $name + '" class="' + $required + '" data-error="' + $error + '" />';
							$output += '</div>';
						} // create contest - textarea end

						// create contest - select
						else if ( $(this).parents().is('#popup_iconContestSelect') ) {
							var $label = $('[name="contestSelectLabel"]').val();
							var $name = $('[name="contestSelectName"]').val();
							var $default = $('[name="contestSelectDefault"]').val();
							var $optionsList = $('.contestSelectOptionsList li');
							var $options = '';
							for ( $i = 0; $i < $optionsList.length; $i++ ) {
								var $text = $optionsList.eq( $i ).find('a').attr('data-text');
								var $value = $optionsList.eq( $i ).find('a').attr('href');
								if ( $value == $default ) {
									$options += '<option value="' + $value + '" selected="selected" data-selected="1">';
									$options += $text + '</option>';
								} else {
									$options += '<option value="' + $value + '">' + $text + '</option>';
								}
							}
							$output = '<div class="' + $contestFieldClass + ' contestSelect">';
							$output += '<label>' + $label + '</label>';
							$output += '<select name="' + $name + '">' + $options + '</select>';
							$output += '</div>';
						} // create contest - select end

						// create contest - radio
						else if ( $(this).parents().is('#popup_iconContestRadio') ) {
							var $label = $('[name="contestRadioLabel"]').val();
							var $name = $('[name="contestRadioName"]').val();
							var $default = $('[name="contestRadioDefault"]').val();
							var $optionsList = $('.contestRadioOptionsList li');
							var $radio = '';
							for ( $i = 0; $i < $optionsList.length; $i++ ) {
								var $text = $optionsList.eq( $i ).find('a').attr('data-text');
								var $value = $optionsList.eq( $i ).find('a').attr('href');
								if ( $value == $default ) {
									$radio += '<input type="radio" name="' + $name + '" ';
									$radio += 'value="' + $value + '" checked="checked" />';
								} else {
									$radio += '<input type="radio" name="' + $name + '" value="' + $value + '" />';
								}
								$radio += '<span class="radioValue">' + $text + '</span><br />';
							}
							$output = '<div class="' + $contestFieldClass + ' contestRadio">';
							$output += '<label>' + $label + '</label>';
							$output += '<span class="radioGroup">' + $radio + '</span>';
							$output += '</div>';
						} // create contest - radio end

						// create contest - checkbox
						else if ( $(this).parents().is('#popup_iconContestCheckbox') ) {
							var $label = $('[name="contestCheckboxLabel"]').val();
							var $name = $('[name="contestCheckboxName"]').val();
							var $value = $('[name="contestCheckboxValue"]').val();
							var $required = $('[name="contestCheckboxRequired"]').is(':checked') ? 'required' : '';
							var $error = $('[name="contestCheckboxError"]').val();
							$output = '<div class="' + $contestFieldClass + ' contestCheckbox">';
							$output += '<input type="checkbox" name="' + $name + '" class="' + $required + '" ';
							$output += 'data-error="' + $error + '" value="' + $value + '" />';
							$output += '<label>' + $label + '</label>';
							$output += '</div>';
						} // create contest - checkbox end

						// create contest - button
						else if ( $(this).parents().is('#popup_iconContestBtn') ) {
							var $btnType = $('[name="contestBtnType"]').val();
							var $btnImageURL = $('[name="contestBtnImageURL"]').val();
							// if user uploads an image
							if ( $('[name="contestBtnImage"]').val() != '' ) {
								$uploadedImageRefersTo = 'addContestButton';
								$(this).parents('form').submit();
							}
							else $output = createUpdateContestButton( $btnType, $btnImageURL );
						} // create contest - button end

						// create contest - ugc gallery
						else if ( $(this).parents().is('#popup_iconContestUGCGallery') ) {
							var $gGrid = $('[name="ugcGalleryLayout"]').val();
							var $gBg = hexConverter( $('.ugcGalleryBgColor').css('background-color') );
							var $gBorder = hexConverter( $('.ugcGalleryBorderColor').css('background-color') );
							var $gText = hexConverter( $('.ugcGalleryTextColor').css('background-color') );
							var $gBtn = hexConverter( $('.ugcGalleryBtnColor').css('background-color') );
							var $gBtnText = hexConverter( $('.ugcGalleryBtnTextColor').css('background-color') );
							$output = createUpdateUgcGallery( $gGrid, $gBg, $gBorder, $gText, $gBtn, $gBtnText );
						} // create contest - ugc gallery end

						// create contest - ugc upload area
						else if ( $(this).parents().is('#popup_iconContestUGCUploadArea') ) {
							var $areaBgColor = hexConverter( $('.ugcUploadAreaBgColor').css('background-color') );
							var $error = $('[name="ugcUploadAreaError"]').val();
							$output = '<div class="contestUGCUploadArea" style="background: ' + $areaBgColor + ';" ';
							$output += 'data-bg="' + $areaBgColor + '" data-error="' + $error + '">';
							$output += '<img src="' + $websiteURL + 'images/transparentPixelReal.png" /></div>';
						} // create contest - ugc upload area end

						// add new element
						if ( $output != null ) addNewElement( $output );

					} //--- CREATE NEW ELEMENT END ---//



					//--- EDIT OLD ELEMENT ---//
					else {

						// edit Google Plus
						if ( $(this).parents().is('#popup_iconGoogle1') ) {
							var $googlePlus = $focusedElement.find('.googlePlus');
							var $url = $('[name="google1URL"]').val();
							if ( $googlePlus.attr('data-href') != $url ) {
								$output = '<div class="googlePlus" id="unparsedGooglePlus" data-href="' + $url + '" />';
								$googlePlus.after( $output ).remove();
							}
						} // edit Google Plus end

						// edit iframe
						else if ( $(this).parents().is('#popup_iconIframe') ) {
							var $iframe = $focusedElement.find('iframe');
							var $url = $('[name="iFrameURL"]').val();
							var $scroll = $('#iFrameScrolling').is(':checked') ? 'yes' : 'no';
							// update the iframe
							if ( $iframe.attr('src') != $url || $iframe.attr('scrolling') != $scroll )
								$iframe.attr({ 'src': $url, 'scrolling': $scroll });
						} // edit iframe end

						// edit cool slider
						else if ( $(this).parents().is('#popup_iconCoolSlider') ) {
							createUpdateCoolSlider();
						} // edit cool slider end

						// edit youtube
						else if ( $(this).parents().is('#popup_iconYoutube') ) {
							var $youtube = $focusedElement.find('.youtube');
							var $url = $('[name="youtubeURL"]').val();
							var $newId = $displayWidget.getYoutubeVideoId( $url );
							var $oldId = $youtube.find('embed').attr('src');
							$oldId = $oldId.substr( $oldId.lastIndexOf('/') + 1 );
							// update youtube
							if ( $oldId != $newId ) {
								$output = $displayWidget.createYoutube( $url );
								addNewElement( $output );
								$focusedElement.remove();
							}
						} // edit youtube end

						// edit vimeo
						else if ( $(this).parents().is('#popup_iconVimeo') ) {
							var $vimeo = $focusedElement.find('.vimeo');
							var $url = $('[name="vimeoURL"]').val();
							if ( $url != $vimeo.attr('data-url') ) {
								$output = $displayWidget.createVimeo( $url );
								addNewElement( $output );
								$focusedElement.remove();
							}
						} // edit vimeo end

						// edit image
						else if ( $(this).parents().is('#popup_iconImage') ) {
							var $imageURL = $('[name="imageURL"]').val();
							var $imageLinkURL = $('[name="imageLinkURL"]').val();
							// if user uploads an image
							if ( $('input[name="contentImage"]').val() != '' ) {
								$userEditsUploadedImage = true;
								$uploadedImageRefersTo = 'addImage';
								$(this).parents('form').submit();
							}
							else createUpdateImage( $imageURL, $imageLinkURL );
						} // edit image end

						// edit hotspot
						else if ( $(this).parents().is('#popup_iconHotspot') ) {
							var $hotspot = $focusedElement.find('.contentHotspot').parent('a');
							var $url = $('[name="hotspotURL"]').val();
							if ( $hotspot.attr('href') != $url ) $hotspot.attr( 'href', $url );
						} // edit hotspot end

						// edit html
						else if ( $(this).parents().is('#popup_iconHtml') ) {
							var $htmlContainer = $focusedElement.find('.contentHtml');
							var $html = $('[name="html"]').val();
							if ( $htmlContainer.html() != $html ) $htmlContainer.html( $html );
						} // edit html end

						// edit text
						else if ( $(this).parents().is('#popup_iconText') ) {
							var $textBlock = $focusedElement.find('.contentText');
							var $text = $('.cleditorMain iframe').contents().find('body').html();
							if ( $textBlock.html() != $text ) $textBlock.html( $text );
						} // edit text end

						// edit FB like
						else if ( $(this).parents().is('#popup_iconLike') ) {
							var $fbLike = $focusedElement.find('fb\\:like');
							var $url = $('[name="likeURL"]').val();
							var $pageUrl = $uploadedImagesFolder + 'index.php?url=' + $url;
							if ( $fbLike.attr('data-url') != $url ) {
								$output = '<fb:like id="unparsedFB" data-url="' + $url + '" href="' + $pageUrl + '" send="false" ';
								$output += 'width="77" data-layout="button_count" show-faces="false"></fb:like>';
								$fbLike.after( $output );
								$fbLike.remove();
							}
						} // edit FB like end

						// edit FB send
						else if ( $(this).parents().is('#popup_iconFacebookSend') ) {
							var $fbSend = $focusedElement.find('fb\\:send');
							var $url = $('[name="sendURL"]').val();
							if ( $fbSend.attr('href') != $url ) {
								$output = '<fb:send id="unparsedFB" href="' + $url + '"></fb:send>';
								$fbSend.after( $output );
								$fbSend.remove();
							}
						} // edit FB send end

						// edit FB comments
						else if ( $(this).parents().is('#popup_iconComment') ) {
							var $fbComments = $focusedElement.find('fb\\:comments');
							var $url = $('[name="commentsURL"]').val();
							var $num = $('[name="commentsNumber"]').val();
							if ( $fbComments.attr('href') != $url || $fbComments.attr('num_posts') != $num ) {
								$output = '<fb:comments id="unparsedFB" href="' + $url + '" num_posts="' + $num + '" ';
								$output += 'width="470"></fb:comments>';
								$fbComments.after( $output );
								$fbComments.remove();
							}
						} // edit FB comments end

						// edit FB invite button
						else if ( $(this).parents().is('#popup_iconFacebookInvite') ) {
							var $title = $('[name="inviteTitle"]').val();
							var $message = $('[name="inviteMessage"]').val();
							var $buttonURL = $('[name="inviteBtnURL"]').val();
							// if user uploads an image
							if ( $('[name="inviteBtnImage"]').val() != '' ) {
								$userEditsUploadedImage = true;
								$uploadedImageRefersTo = 'addInviteButton';
								$(this).parents('form').submit();
							}
							else createUpdateInviteButton( $title, $message, $buttonURL );
						} // edit FB invite button end

						// edit FB share (post to feed)
						else if ( $(this).parents().is('#popup_iconFacebookPostToFeed') ) {
							var $pLink = '';
							if ( $('[name="postFeedLinkSwitch"]:checked').val() == 1 )
								$pLink = $('[name="postFeedLink"]').val();
							var $pName = $('[name="postFeedName"]').val();
							var $pDescr = $('[name="postFeedDescr"]').val();
							var $pCaption = $('[name="postFeedCaption"]').val();
							_postFeedImgImage = $('[name="postFeedImgURL"]').val();
							_postFeedBtnImage = $('[name="postFeedBtnURL"]').val();
							// if user uploads one or more images
							if ( $('[name="postFeedImgImage"]').val() != '' || $('[name="postFeedBtnImage"]').val() != '' ) {
								$userEditsUploadedImage = true;
								$uploadedImageRefersTo = 'addFbPostFeed';
								// which images need to be updated
								arrImagesToUpdate = [];
								if ( $('[name="postFeedImgImage"]').val() != '' ) arrImagesToUpdate.push( 0 );
								if ( $('[name="postFeedBtnImage"]').val() != '' ) arrImagesToUpdate.push( 1 );
								$(this).parents('form').submit();
							}
							else createUpdateFbPostFeed( $pLink, $pName, $pDescr, $pCaption );
						} // edit FB share (post to feed) end

						// edit twitter (stream)
						else if ( $(this).parents().is('#popup_iconTwitter') ) {
							var $twitter = $focusedElement.find('.contentTwitter');
							var $name = $('[name="twitterName"]').val();
							var $postsNumber = $('[name="tweetsNumber"]').val();
							var $linkColor = hexConverter( $('.twitter.link').css('background-color') );
							var $textColor = hexConverter( $('.twitter.text').css('background-color') );
							var $reparse = false;
							if ( $twitter.attr('data-name') != $name ) {
								$twitter.attr( 'data-name', $name );
								$reparse = true;
							}
							if ( $twitter.attr('data-posts') != $postsNumber ) {
								$twitter.attr( 'data-posts', $postsNumber );
								$reparse = true;
							}
							if ( $twitter.attr('data-link-color') != $linkColor ) {
								$twitter.attr( 'data-link-color', $linkColor );
								$reparse = true;
							}
							if ( $twitter.attr('data-text-color') != $textColor ) {
								$twitter.attr( 'data-text-color', $textColor );
								$reparse = true;
							}
							// remove old twitter stream, so that script could reparse
							if ( $reparse ) $twitter.html('');
						} // edit twitter end

						// edit twitter share button
						else if ( $(this).parents().is('#popup_iconTwitterShare') ) {
							var $account = $('[name="twitterShareAccount"]').val();
							var $text = $('[name="twitterShareText"]').val();
							var $url = '';
							if ( $('[name="twitterShareURLSwitch"]:checked').val() == 1 )
								$url = $('[name="twitterShareURL"]').val();
							var $buttonURL = $('[name="twitterShareButtonURL"]').val();
							// if user uploads an image
							if ( $('[name="twitterShareButtonImage"]').val() != '' ) {
								$userEditsUploadedImage = true;
								$uploadedImageRefersTo = 'addTwitterShareButton';
								$(this).parents('form').submit();
							}
							else createUpdateTwitterShareButton( $account, $text, $url, $buttonURL );
						} // edit twitter share button end

						// edit pinterest pin it button
						else if ( $(this).parents().is('#popup_iconPinterestPinIt') ) {
							var $button = $focusedElement.find('.contentPinIt');
							var $pinItPage = $('[name="pinItPageURL"]').val();
							var $pinItImage = $('[name="pinItImageURL"]').val();
							var $pinItDescr = $('[name="pinItDescr"]').val();
							var $pinItCount = $('[name="pinItCount"]').val();
							var $btn = "<a class='pin-it-button' count-layout='" + $pinItCount + "' ";
							$btn += "href='http://pinterest.com/pin/create/button/";
							$btn += "?url=" + encodeURIComponent( $pinItPage );
							$btn += "&media=" + encodeURIComponent( $pinItImage );
							$btn += "&description=" + encodeURIComponent( $pinItDescr ) + "'>";
							$btn += "<img title='Pin It' src='//assets.pinterest.com/images/PinExt.png' /></a>";
							$button.attr({
								'data-page': $pinItPage,
								'data-img': $pinItImage,
								'data-descr': $pinItDescr,
								'data-count': $pinItCount,
								'data-btn': $btn
							}).addClass('unparsed').html( $btn );
						} // edit pinterest pin it button end

						// edit pinterest follow button
						else if ( $(this).parents().is('#popup_iconPinterestFollow') ) {
							var $button = $focusedElement.find('.contentPinFollow');
							var $user = $('[name="pinFollowUser"]').val();
							var $type = $('[name="pinFollowBtnType"]').val();
							$button.attr({
								'data-user': $user,
								'data-btn': $type,
								'href': 'http://pinterest.com/' + $user + '/'
							}).find('img').attr({
								src: 'http://passets-lt.pinterest.com/images/about/buttons/' + $type + '-button.png'
							});
						} // edit pinterest follow button end

						// edit rss
						else if ( $(this).parents().is('#popup_iconRSS') ) {
							var $rss = $focusedElement.find('.contentRSS');
							var $url = $('[name="rssURL"]').val();
							var $postsNumber = $('[name="rssNumber"]').val();
							var $linkColor = hexConverter( $('.rss.link').css('background-color') );
							var $textColor = hexConverter( $('.rss.text').css('background-color') );
							var $reparse = false;
							if ( $rss.attr('data-url') != $url ) {
								$rss.attr( 'data-url', $url );
								$reparse = true;
							}
							if ( $rss.attr('data-posts') != $postsNumber ) {
								$rss.attr( 'data-posts', $postsNumber );
								$reparse = true;
							}
							if ( $rss.attr('data-link-color') != $linkColor ) {
								$rss.attr( 'data-link-color', $linkColor );
								$reparse = true;
							}
							if ( $rss.attr('data-text-color') != $textColor ) {
								$rss.attr( 'data-text-color', $textColor );
								$reparse = true;
							}
							// remove old rss stream, so that script could reparse
							if ( $reparse ) $rss.html('');
						} // edit rss end

						// edit google maps
						else if ( $(this).parents().is('#popup_iconGoogleMaps') ) {
							var $gm = $focusedElement.find('.contentGoogleMaps');
							var $address = encodeURIComponent( $('[name="gmAddress"]').val() );
							var $zoom = $('[name="gmZoom"]').val();
							var $reparse = false;
							if ( $gm.attr('data-address') != $address ) {
								$gm.attr( 'data-address', encodeURIComponent( $address ) );
								$reparse = true;
							}
							if ( $gm.attr('data-zoom') != $zoom ) {
								$gm.attr( 'data-zoom', $zoom );
								$reparse = true;
							}
							// remove old google maps, so that script could reparse
							if ( $reparse ) $gm.html('');
						} // edit google maps end

						// edit countdown
						else if ( $(this).parents().is('#popup_iconCountdown') ) {
							var $countdown = $focusedElement.find('.contentCountdown');
							var $date = $('[name="countdownDate"]').val();
							var $size = $('[name="countdownSize"]').val();
							var $color = hexConverter( $('.countdown.text').css('background-color') );
							var $reparse = false;
							if ( $countdown.attr('data-date') != $date ) {
								$countdown.attr( 'data-date', $date );
								$reparse = true;
							}
							if ( $countdown.attr('data-size') != $size ) {
								$countdown.attr( 'data-size', $size );
								$reparse = true;
							}
							if ( $countdown.attr('data-color') != $color ) {
								$countdown.attr( 'data-color', $color );
								$reparse = true;
							}
							// remove old countdown, so that script could reparse
							if ( $reparse ) $countdown.html('');
						} // edit countdown end

						// edit contact form
						else if ( $(this).parents().is('#popup_iconContact') ) {
							var $cAddress = $('[name="contactAddress"]').val();
							var $cButtonURL = $('[name="contactButtonURL"]').val();
							var $cName = $('#contactName').is(':checked') ? true : false;
							var $cPhone = $('#contactPhone').is(':checked') ? true : false;
							// if user uploads an image
							if ( $('[name="contactButtonImage"]').val() != '' ) {
								$userEditsUploadedImage = true;
								$uploadedImageRefersTo = 'addContactForm';
								$(this).parents('form').submit();
							}
							else createUpdateContactForm( $cAddress, $cButtonURL, $cName, $cPhone );
						} // edit contact form end

						// edit email button
						else if ( $(this).parents().is('#popup_iconEmail') ) {
							var $eAddress = $('[name="emailAddress"]').val();
							var $eButtonURL = $('[name="emailButtonURL"]').val();
							// if user uploads an image
							if ( $('[name="emailButtonImage"]').val() != '' ) {
								$userEditsUploadedImage = true;
								$uploadedImageRefersTo = 'addEmailButton';
								$(this).parents('form').submit();
							}
							else createUpdateEmailButton( $eAddress, $eButtonURL );
						} // edit email button end

						// edit flickr
						else if ( $(this).parents().is('#popup_iconFlickr') ) {
							var $flickr = $focusedElement.find('.contentFlickr');
							var $flickrId = $('[name="flickrId"]').val();
							if ( $flickr.attr('data-id') != $flickrId ) {
								$flickr.attr({
									src: 'http://www.flickr.com/slideShow/index.gne?user_id=' + $flickrId,
									'data-id': $flickrId
								});
							}
						} // edit flickr end

						// edit contest - text input
						else if ( $(this).parents().is('#popup_iconContestText') ) {
							var $input = $focusedElement.find('.contestTextInput input');
							var $label = $('[name="contestTextLabel"]').val();
							var $name = $('[name="contestTextName"]').val();
							var $required = $('[name="contestTextRequired"]').is(':checked') ? 'required' : '';
							var $validation = $('[name="contestTextValidation"]').val();
							var $error = $('[name="contestTextError"]').val();
							$input.removeClass().addClass( $required + ' ' + $validation )
								.attr({ 'name': $name, 'data-error': $error, 'data-validation': $validation })
								.siblings('label').html( $label );
						} // edit contest - text input end

						// edit contest - textarea
						else if ( $(this).parents().is('#popup_iconContestTextarea') ) {
							var $textarea = $focusedElement.find('.contestTextarea textarea');
							var $label = $('[name="contestTextareaLabel"]').val();
							var $name = $('[name="contestTextareaName"]').val();
							var $required = $('[name="contestTextareaRequired"]').is(':checked') ? 'required' : '';
							var $error = $('[name="contestTextareaError"]').val();
							$textarea.removeClass().addClass( $required )
								.attr({ 'name': $name, 'data-error': $error })
								.siblings('label').html( $label );
						} // edit contest - textarea end

						// edit contest - select
						else if ( $(this).parents().is('#popup_iconContestSelect') ) {
							var $select = $focusedElement.find('.contestSelect select');
							var $label = $('[name="contestSelectLabel"]').val();
							var $name = $('[name="contestSelectName"]').val();
							var $default = $('[name="contestSelectDefault"]').val();
							var $optionsList = $('.contestSelectOptionsList li');
							var $options = '';
							for ( $i = 0; $i < $optionsList.length; $i++ ) {
								var $text = $optionsList.eq( $i ).find('a').attr('data-text');
								var $value = $optionsList.eq( $i ).find('a').attr('href');
								if ( $value == $default ) {
									$options += '<option value="' + $value + '" selected="selected" data-selected="1">';
									$options += $text + '</option>';
								} else {
									$options += '<option value="' + $value + '">' + $text + '</option>';
								}
							}
							$select.attr( 'name', $name ).siblings('label').html( $label );
							$select.empty().append( $options );
						} // edit contest - select end

						// edit contest - radio
						else if ( $(this).parents().is('#popup_iconContestRadio') ) {
							var $radioGroup = $focusedElement.find('.contestRadio .radioGroup');
							var $label = $('[name="contestRadioLabel"]').val();
							var $name = $('[name="contestRadioName"]').val();
							var $default = $('[name="contestRadioDefault"]').val();
							var $optionsList = $('.contestRadioOptionsList li');
							var $radio = '';
							for ( $i = 0; $i < $optionsList.length; $i++ ) {
								var $text = $optionsList.eq( $i ).find('a').attr('data-text');
								var $value = $optionsList.eq( $i ).find('a').attr('href');
								if ( $value == $default ) {
									$radio += '<input type="radio" name="' + $name + '" ';
									$radio += 'value="' + $value + '" checked="checked" />';
								} else {
									$radio += '<input type="radio" name="' + $name + '" value="' + $value + '" />';
								}
								$radio += '<span class="radioValue">' + $text + '</span><br />';
							}
							$radioGroup.siblings('label').html( $label );
							$radioGroup.empty().append( $radio );
						} // edit contest - radio end

						// edit contest - checkbox
						else if ( $(this).parents().is('#popup_iconContestCheckbox') ) {
							var $checkbox = $focusedElement.find('.contestCheckbox input');
							var $label = $('[name="contestCheckboxLabel"]').val();
							var $name = $('[name="contestCheckboxName"]').val();
							var $value = $('[name="contestCheckboxValue"]').val();
							var $required = $('[name="contestCheckboxRequired"]').is(':checked') ? 'required' : '';
							var $error = $('[name="contestCheckboxError"]').val();
							$checkbox.removeClass().addClass( $required )
								.attr({ 'name': $name, 'data-error': $error, 'value': $value })
								.siblings('label').html( $label );
						} // edit contest - checkbox end

						// edit contest - button
						else if ( $(this).parents().is('#popup_iconContestBtn') ) {
							var $btnType = $('[name="contestBtnType"]').val();
							var $btnImageURL = $('[name="contestBtnImageURL"]').val();
							// if user uploads an image
							if ( $('[name="contestBtnImage"]').val() != '' ) {
								$userEditsUploadedImage = true;
								$uploadedImageRefersTo = 'addContestButton';
								$(this).parents('form').submit();
							}
							else createUpdateContestButton( $btnType, $btnImageURL );
						} // edit contest - button end

						// edit contest - ugc gallery
						else if ( $(this).parents().is('#popup_iconContestUGCGallery') ) {
							var $gGrid = $('[name="ugcGalleryLayout"]').val();
							var $gBg = hexConverter( $('.ugcGalleryBgColor').css('background-color') );
							var $gBorder = hexConverter( $('.ugcGalleryBorderColor').css('background-color') );
							var $gText = hexConverter( $('.ugcGalleryTextColor').css('background-color') );
							var $gBtn = hexConverter( $('.ugcGalleryBtnColor').css('background-color') );
							var $gBtnText = hexConverter( $('.ugcGalleryBtnTextColor').css('background-color') );
							createUpdateUgcGallery( $gGrid, $gBg, $gBorder, $gText, $gBtn, $gBtnText );
						} // edit contest - ugc gallery end

						// edit contest - ugc upload area
						else if ( $(this).parents().is('#popup_iconContestUGCUploadArea') ) {
							var $UGCArea = $focusedElement.find('.contestUGCUploadArea');
							var $areaBgColor = hexConverter( $('.ugcUploadAreaBgColor').css('background-color') );
							var $error = $('[name="ugcUploadAreaError"]').val();
							$UGCArea.css({ 'background-color': $areaBgColor }).attr({
								'data-bg': $areaBgColor,
								'data-error': $error
							});
						} // edit contest - ugc upload area end

						// update the global vars
						if ( $userEditsUploadedImage == false ) {
							$createNewElement = true;
							$focusedElement = null;
						}

					} //--- EDIT OLD ELEMENT END ---//

					// add proper font size and line-height in the text widget elements
					addFontStyles();

				} //--- OPEN WINDOW IS RELATED TO A WIDGET END ---//


				// widgets must be draggable and resizable
				$( '.' + $block ).each(function () { widgetDR( $(this) ); });

				// live preview of all FB includes
				if ( $('#unparsedFB').length ) {
					FB.XFBML.parse( document.getElementById('unparsedFB').parentNode );
					$('#unparsedFB').removeAttr('id');
				}

				// live preview of twitter stream
				$('.contentTwitter').each( function() {
					if ( ! $(this).children().length ) $displayWidget.parseTwitter( $(this) );
				});

				// live preview of pinterest pin it button
				setTimeout( function() {
					if ( $('.contentPinIt.unparsed').length ) {
						$('.contentPinIt').each( function() {
							$(this).removeClass('unparsed').html( $(this).attr('data-btn') );
						});
						$.ajax({ url: 'http://assets.pinterest.com/js/pinit.js', dataType: 'script', cache: true });
					}
				}, 1000 );

				// live preview of rss
				$('.contentRSS').each( function() {
					if ( ! $(this).children().length ) $displayWidget.parseRSS( $(this) );
				});

				// live preview of google plus buttons
				$('.googlePlus').each( function() {
					if ( ! $(this).children().length ) $displayWidget.parseGooglePlus( $(this) );
				});

				// live preview of google maps
				$('.contentGoogleMaps').each( function() {
					if ( ! $(this).children().length ) $displayWidget.parseGoogleMaps( $(this) );
				});

				// live preview of countdown
				$('.contentCountdown').each( function() {
					if ( ! $(this).children().length ) $displayWidget.parseCountdown( $(this) );
				});

				// close fancybox
				$.fancybox.close();

			} // the form is valid
			return false;
		}); // popup window submit

	}; addingEditingElements();




	//----------------------- FUNCTIONS CALLS  ------------------------//


	// EDITOR - VISIBLE WHEN SCROLLING
	$.fn.visibleWhenScrolling = function() {
		var $element = $(this);
		var $elementPosition = $element.offset().top;
		$(window).scroll( function() {
			// was the position changed?
			if ( $elementPosition != $element.offset().top && ! $element.is('.fixed') )
				$elementPosition = $element.offset().top;
			// adding .fixed class means that the element has fixed postion
			if ( $element.is('.editorScrollingWidgets') ) {
				// add or remove class .fixed
				if ( $(window).scrollTop() > $elementPosition ) {
					$element.addClass('fixed');
				}
				else if ( $(window).scrollTop() < $elementPosition ) {
					$element.removeClass('fixed');
				}
			}
			else {
				// add or remove class .fixed
				if ( $(window).scrollTop() + 122 > $elementPosition )
					$element.addClass('fixed');
				else if ( $(window).scrollTop() + 122 < $elementPosition )
					$element.removeClass('fixed');
			}
		}); // widow scroll
	}
	$('.editorScrollingWidgets, .editorInstruments, .contentRuler.horz').each( function() {
		$(this).visibleWhenScrolling();
	});



	// SAVE PAGE
	$('a.bigButton.save').click( function() {
		var $id = $(this).attr('href');
		var $url = 'action.php?act=save_page&id=' + $id;
		var $successMessage = 'The tab has been saved!';
		var $errorMessage = 'Error when saving the tab.';
		ajaxSavePage( $id, $url, $successMessage, $errorMessage );
		return false;
	});



	// PUBLISH PAGE
	$('a.bigButton.publish').live( 'click', function() {
		var $id = $(this).attr('href');
		var $url = $successMessage = $errorMessage = '';
		if ( $(this).attr('data-published') == '0' ) {
			$url = 'action.php?act=publish_page&id=' + $id;
			$successMessage = 'The tab has been published!';
			$errorMessage = 'Error when publishing the tab.';
		} else {
			$url = 'action.php?act=unpublish_page&id=' + $id;
			$successMessage = 'The tab has been unpublished!';
			$errorMessage = 'Error when unpublishing the tab.';
		}
		ajaxSavePage( $id, $url, $successMessage, $errorMessage );
		return false;
	});



	// CLEAR CONTENT
	$('.editorInstruments a.clear').click( function() {
		if ( confirm('Are you sure you want to delete the current contents of your App?') ) blankPage();
		return false;
	});


}); // документ UTF-8
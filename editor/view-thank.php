<?php

	// includes
	require_once('config.php');
	require_once('functions.php');

	session_start();

	$appId = $_SESSION['appId'];
	$secret = $_SESSION['appSecret'];
	$page_id = $_SESSION['page_id'];

	if ( ! isset( $appId ) || ! isset( $secret ) || ! isset( $page_id ) ) exit();

	// get tab to show
	$query = "SELECT id FROM pages WHERE fan_page_id = '".$page_id."' AND app_id = '".$appId."'
							AND published = '1' LIMIT 1 ";
	$query = mysql_query( $query ) or exit( mysql_error() );

	// if tab is published - show it
	if ( mysql_num_rows( $query ) > 0 ) {
		$tabId = mysql_result( $query, 0 );
		$tab = ars_getPageInfo( $tabId );
	}

	// when user enters a contest, but has no more entries, he is redirected to "thank you" page
	// in order not to create an empty record in DB, we clear 'campaign_id' field
	// so if it is empty, we don't need to save any data
	if ( $_REQUEST['campaign_id'] != '' ) {

		// DECLARE VARIABLES
		$firstName = '';
		$lastName = '';
		$city = '';
		$state = '';
		$email = '';
		$address = '';
		$zipCode = '';
		$cellPhone = '';
		$homePhone = '';
		$cellularCarrier = '';
		$gender = '';
		$country = '';
		$birthYear = '';
		$birthMonth = '';
		$birthDay = '';
		$birthDate = '';
		$optInSms = 'no';
		$optInEmail = 'no';
		$openText1 = $openText2 = $openText3 = $openText4 = $openText5 = '';
		$openText6 = $openText7 = $openText8 = $openText9 = $openText10 = '';
		$openTextarea1 = $openTextarea2 = $openTextarea3 = $openTextarea4 = $openTextarea5 = '';
		$openTextarea6 = $openTextarea7 = $openTextarea8 = $openTextarea9 = $openTextarea10 = '';
		$openSelect1 = $openSelect2 = $openSelect3 = $openSelect4 = $openSelect5 = '';
		$openSelect6 = $openSelect7 = $openSelect8 = $openSelect9 = $openSelect10 = '';
		$openCheckbox1 = $openCheckbox2 = $openCheckbox3 = $openCheckbox4 = $openCheckbox5 = '';
		$openCheckbox6 = $openCheckbox7 = $openCheckbox8 = $openCheckbox9 = $openCheckbox10 = '';
		$openRadio1 = $openRadio2 = $openRadio3 = $openRadio4 = $openRadio5 = '';
		$openRadio6 = $openRadio7 = $openRadio8 = $openRadio9 = $openRadio10 = '';


		// GET SENT VALUES
		foreach ( $_POST as $key => $value ) {
			$value = escapeStr( $value );

			switch ( $key ) {
				case 'First_Name': $firstName = $value; break;
				case 'Last_Name': $lastName = $value; break;
				case 'City': $city = $value; break;
				case 'State': $state = $value; break;
				case 'Email': $email = $value; break;
				case 'Address': $address = $value; break;
				case 'Zip_Code': $zipCode = $value; break;
				case 'Cell_Phone': $cellPhone = $value; break;
				case 'Home_Phone': $homePhone = $value; break;
				case 'Cellular_Carrier': $cellularCarrier = $value; break;
				case 'Gender': $gender = $value; break;
				case 'Country': $country = $value; break;
				case 'Birthday_Year': $birthYear = $value; break;
				case 'Birthday_Month': $birthMonth = $value; break;
				case 'Birthday_Day': $birthDay = $value; break;

				case 'SMS_opt_in': $optInSms = 'yes'; break;
				case 'Email_opt_in': $optInEmail = 'yes'; break;

				case 'Text_Open_Field_1': $openText1 = $value; break;
				case 'Text_Open_Field_2': $openText2 = $value; break;
				case 'Text_Open_Field_3': $openText3 = $value; break;
				case 'Text_Open_Field_4': $openText4 = $value; break;
				case 'Text_Open_Field_5': $openText5 = $value; break;
				case 'Text_Open_Field_6': $openText6 = $value; break;
				case 'Text_Open_Field_7': $openText7 = $value; break;
				case 'Text_Open_Field_8': $openText8 = $value; break;
				case 'Text_Open_Field_9': $openText9 = $value; break;
				case 'Text_Open_Field_10': $openText10 = $value; break;

				case 'Textarea_Open_Field_1': $openTextarea1 = $value; break;
				case 'Textarea_Open_Field_2': $openTextarea2 = $value; break;
				case 'Textarea_Open_Field_3': $openTextarea3 = $value; break;
				case 'Textarea_Open_Field_4': $openTextarea4 = $value; break;
				case 'Textarea_Open_Field_5': $openTextarea5 = $value; break;
				case 'Textarea_Open_Field_6': $openTextarea6 = $value; break;
				case 'Textarea_Open_Field_7': $openTextarea7 = $value; break;
				case 'Textarea_Open_Field_8': $openTextarea8 = $value; break;
				case 'Textarea_Open_Field_9': $openTextarea9 = $value; break;
				case 'Textarea_Open_Field_10': $openTextarea10 = $value; break;

				case 'Select_Open_Field_1': $openSelect1 = $value; break;
				case 'Select_Open_Field_2': $openSelect2 = $value; break;
				case 'Select_Open_Field_3': $openSelect3 = $value; break;
				case 'Select_Open_Field_4': $openSelect4 = $value; break;
				case 'Select_Open_Field_5': $openSelect5 = $value; break;
				case 'Select_Open_Field_6': $openSelect6 = $value; break;
				case 'Select_Open_Field_7': $openSelect7 = $value; break;
				case 'Select_Open_Field_8': $openSelect8 = $value; break;
				case 'Select_Open_Field_9': $openSelect9 = $value; break;
				case 'Select_Open_Field_10': $openSelect10 = $value; break;

				case 'Checkbox_Open_Field_1': $openCheckbox1 = $value; break;
				case 'Checkbox_Open_Field_2': $openCheckbox2 = $value; break;
				case 'Checkbox_Open_Field_3': $openCheckbox3 = $value; break;
				case 'Checkbox_Open_Field_4': $openCheckbox4 = $value; break;
				case 'Checkbox_Open_Field_5': $openCheckbox5 = $value; break;
				case 'Checkbox_Open_Field_6': $openCheckbox6 = $value; break;
				case 'Checkbox_Open_Field_7': $openCheckbox7 = $value; break;
				case 'Checkbox_Open_Field_8': $openCheckbox8 = $value; break;
				case 'Checkbox_Open_Field_9': $openCheckbox9 = $value; break;
				case 'Checkbox_Open_Field_10': $openCheckbox10 = $value; break;

				case 'Radio_Open_Field_1': $openRadio1 = $value; break;
				case 'Radio_Open_Field_2': $openRadio2 = $value; break;
				case 'Radio_Open_Field_3': $openRadio3 = $value; break;
				case 'Radio_Open_Field_4': $openRadio4 = $value; break;
				case 'Radio_Open_Field_5': $openRadio5 = $value; break;
				case 'Radio_Open_Field_6': $openRadio6 = $value; break;
				case 'Radio_Open_Field_7': $openRadio7 = $value; break;
				case 'Radio_Open_Field_8': $openRadio8 = $value; break;
				case 'Radio_Open_Field_9': $openRadio9 = $value; break;
				case 'Radio_Open_Field_10': $openRadio10 = $value; break;
			} // switch
		} // foreach

		if ( ! empty( $birthYear ) && ! empty( $birthMonth ) && ! empty( $birthDay ) )
			$birthDate = $birthYear . '-' . $birthMonth . '-' . $birthDay;


		// STORE FORM DATA
		mysql_query("INSERT INTO sp_campaign_result VALUES ( 0,
									'".$_REQUEST['campaign_id']."',
									'".$_REQUEST['user_fb_id']."',
									'".$_SERVER['REMOTE_ADDR']."',
									'',
									'".$firstName."',
									'".$lastName."',
									'".$gender."',
									'".$birthYear."',
									'".$birthMonth."',
									'".$birthDay."',
									'".$birthDate."',
									'".$cellPhone."',
									'".$homePhone."',
									'".$cellularCarrier."',
									'".$email."',
									'".$address."',
									'".$city."',
									'".$state."',
									'".$country."',
									'".$zipCode."',
									'".$optInSms."',
									'".$optInEmail."',

									'".$openText1."', '".$openText2."', '".$openText3."', '".$openText4."', '".$openText5."',
									'".$openText6."', '".$openText7."', '".$openText8."', '".$openText9."', '".$openText10."',

									'".$openTextarea1."', '".$openTextarea2."', '".$openTextarea3."', '".$openTextarea4."',
									'".$openTextarea5."', '".$openTextarea6."', '".$openTextarea7."', '".$openTextarea8."',
									'".$openTextarea9."', '".$openTextarea10."',

									'".$openSelect1."', '".$openSelect2."', '".$openSelect3."', '".$openSelect4."', '".$openSelect5."',
									'".$openSelect6."', '".$openSelect7."', '".$openSelect8."', '".$openSelect9."', '".$openSelect10."',

									'".$openCheckbox1."', '".$openCheckbox2."', '".$openCheckbox3."', '".$openCheckbox4."',
									'".$openCheckbox5."', '".$openCheckbox6."', '".$openCheckbox7."', '".$openCheckbox8."',
									'".$openCheckbox9."', '".$openCheckbox10."',

									'".$openRadio1."', '".$openRadio2."', '".$openRadio3."', '".$openRadio4."', '".$openRadio5."',
									'".$openRadio6."', '".$openRadio7."', '".$openRadio8."', '".$openRadio9."', '".$openRadio10."',

									'', '', '', '', '', '', '', '', '', '',
									NOW()
									);") or exit( mysql_error() );


		// STORE UGC DATA
		if ( $_REQUEST['save-ugc'] == '1' ) {

			// get the last record form sp_campaign_result
			$query = "SELECT * FROM sp_campaign_result ORDER BY cpresult_sid DESC LIMIT 1";
			$query = mysql_query( $query ) or exit( mysql_error() );
			$lastRecord = mysql_fetch_assoc( $query );

			list( $photoWidth, $photoHeight ) = getimagesize( $_REQUEST['ugc_photo_path'] );

			mysql_query("INSERT INTO sp_campaign_ugc VALUES ( 0,
										'".$_REQUEST['ugc_photo_name']."',
										'".$_REQUEST['ugc_original_photo_name']."',
										'".$_REQUEST['campaign_id']."',
										NOW(), '', '',
										'".$_REQUEST['user_fb_id']."',
										'".$lastRecord['cpresult_sid']."',
										'".escapeStr( $_REQUEST['UGC_Title'] )."',
										'".escapeStr( $_REQUEST['UGC_Description'] )."',
										'',
										'photo',
										'en',
										'', '',
										'".$photoWidth."',
										'".$photoHeight."'
										);") or exit( mysql_error() );

			// save uploaded ugc photo
			mysql_query("UPDATE pages SET saved = '1'
										WHERE type = 'ugc' AND name = '".$_REQUEST['ugc_photo_name']."'
										") or exit( mysql_error() );
		} // store ugc data

	} // no need to save any data

?><!doctype html>
<html lang="en-US" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
<meta charset="UTF-8" />
<title><?php echo $tab['name']; ?></title>
<link rel="stylesheet" href="css/style.css?v=1.01" />
<script src="js/jquery.1.7.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/fb.js"></script>
<script src="js/both.js"></script>
<script src="https://widgets.twimg.com/j/2/widget.js"></script>
<script src="https://apis.google.com/js/plusone.js"></script>
<script src="https://maps.google.com/maps/api/js?sensor=false"></script>
<style>* { margin: 0; padding: 0; }</style>
<script>
$(window).load( function() {

	// DISPLAY WIDGETS
	displayWidgetsOnload( $('.pageBody') );

});
</script>
</head>



<body class="fb-view">

	<!-- include Javascript SDK -->
	<div id="fb-root"></div>
	<script>jsSDK();</script>

	<div id="includes">
		<div class="appId"><?php echo $appId; ?></div>
		<div class="pageId"><?php echo $page_id; ?></div>
		<div class="signedRequest"><?php echo $signed_request; ?></div>
		<div class="appLink"><?php echo $_SESSION['appLink']; ?></div>
	</div><!-- / .includes -->

	<?php echo $tab['content_contest_thank']; ?>
	
	<script>

		window.fbAsyncInit = function() {
			FB.init({
				appId  : $appId,
				status : true,
				cookie : true,
				xfbml  : true
			});
		};

		(function() {
			var e = document.createElement('script');
			e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?php echo $appId; ?>';
			e.async = true;
			document.getElementById('fb-root').appendChild(e);
		}());

	</script>
</body>
</html>
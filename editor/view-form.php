<?php

	// this file is responsible for displaying "contest form" on facebook

	//facebook application
	//@author: Muhammad Nouman

	header('P3P: CP="CAO PSA OUR"');  // this is IE session issue fix

	// includes
	require_once('config.php');
	require_once('functions.php');

	session_start();

	$appId = $_SESSION['appId'];
	$secret = $_SESSION['appSecret'];
	$page_id = $_SESSION['page_id'];
	$app_link = $_SESSION['appLink'];

	if ( ! isset( $appId ) || ! isset( $secret ) || ! isset( $page_id ) ) exit();


	// get values from the sting and then search the db to get correct app secret
	$fbconfig['appid' ] = $appId;
	$fbconfig['api'] = $appId;
	$fbconfig['secret'] = $secret;


	//try to include the facebook base file
	try{
		include_once "facebook/fb_sdk/facebook.php";
	}
	catch ( Exception $o ) {
		error_log($o);
	}
	$user = null; //facebook user uid


	// Create our Application instance.
	$facebook = new Facebook(array(
		'appId'  => $fbconfig['appid'],
		'secret' => $fbconfig['secret'],
		'cookie' => true
	));


	// If the user is from safari and session is not valid, this the session bug, fixed here
	if ( ! count($_COOKIE) > 0 && strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') && empty($data) )
	{
		$signed_var = $_GET["request_var"];
		$secret = $fbconfig['secret'];  //get app secret from db
		include_once('facebook/fb_sdk/sig_control.php');
		$data=parse_signed_request($signed_var, $secret);
		$page_id=$data["page"]["id"];
		$page_data = json_decode(file_get_contents("https://graph.facebook.com/$page_id"));
		$page_link=$page_data->link;
		echo $page_id;
		echo $page_link;
		$fbconfig['pageUrl']    =   $page_data->link;
		$fbconfig['appPageUrl'] =   "{$fbconfig['pageUrl']}?sk=app_{$fbconfig['appid' ]}&app_data=true";
		$loginUrl   = $facebook->getLoginUrl(
				array(
					'scope'         => 'user_location,user_birthday,email,publish_actions',
					'redirect_uri'  => $fbconfig['appPageUrl'],
				)
		); 
		// if the user session is not valid on safari then we will redirect to get the authorization
		echo "<script type='text/javascript'>top.location.href = '$loginUrl';</script>";
	}


	// We get user info from API using this code
	$user = $facebook->getUser();
	if ( $user ) {
		try {
			$user_info = $facebook->api('/me');
		}
		catch ( FacebookApiException $e ) {
			//print_r( $user_info );
			$user = null;
		}
	}


	// get tab to show
	$query = "SELECT id FROM pages WHERE fan_page_id = '" . $page_id . "' AND app_id = '" . $appId . "'
							AND published = '1' LIMIT 1 ";
	$query = mysql_query( $query ) or exit( mysql_error() );
	if ( mysql_num_rows( $query ) > 0 ) {
		$tabId = mysql_result( $query, 0 );
		$tab = ars_getPageInfo( $tabId );
	}


	// get contest properties
	$query = "SELECT * FROM sp_campaign_info WHERE campaign_sid = '".$tab['id']."' LIMIT 1 ";
	$query = mysql_query( $query ) or exit( mysql_error() );
	if ( mysql_num_rows( $query ) > 0 )
		$contestProperties = mysql_fetch_assoc( $query );


	// FUNCTION CHECKS, IF A USER CAN ENTER A CONTEST
	function contestAvailable( $user_info, $contestProperties ) {
		$allow_child = $allow_start = $allow_end = $allow_entries = false;

		// get user birthday
		function getMinDate( $birthday ) {
			$year = (int) substr( $birthday, 6 ) + 18;
			$month = substr( $birthday, 0, 2 );
			$day = substr( $birthday, 3, 2 );
			return strtotime( $year . '-' . $month . '-' . $day );
		}

		// check age (over 18 years old)
		if ( $contestProperties['campaign_is_allow_child'] == '0' )      // children are allowed
			$allow_child = true;
		elseif ( strlen( $user_info['birthday'] ) < 10 )                                   // birthday wasn't set
			$allow_child = true;
		elseif ( getMinDate( $user_info['birthday'] ) < time() )                // user is over 18
			$allow_child = true;
		else
			$allow_child = false;

		// check contest start date
		if ( strtotime( $contestProperties['campaign_start_time'] ) < time() )
			$allow_start = true;

		// check contest end date
		if ( strtotime( $contestProperties['campaign_end_time'] ) > time() )
			$allow_end = true;

		// check entries number
		$query = "SELECT * FROM sp_campaign_result WHERE cpresult_user_id = '".$user_info['id']."'
								AND campaign_sid = '".$contestProperties['campaign_sid']."' ";
		$query = mysql_query( $query ) or exit( mysql_error() );
		if ( mysql_num_rows( $query ) < $contestProperties['campaign_is_check_reentry'] )
			$allow_entries = true;

		// return result
		if ( ! $allow_entries ) $error = 'We have already received your entry.<br /> Thanks for participating!';
		elseif ( ! $allow_start ) $error = 'Sorry, the contest hasn\'t started yet.';
		elseif ( ! $allow_end ) $error = 'Sorry, the contest has already ended.';
		elseif ( ! $allow_child ) $error = 'Sorry, you must be over 18 years old to participate in the contest.';
		else $error = '';

		return $error;
	} // contestAvailable()


?><!doctype html>
<html lang="en-US" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
<meta charset="UTF-8" />
<title><?php echo $tab['name']; ?></title>
<link rel="stylesheet" href="css/style.css?v=1.01" />
<script src="js/jquery.1.7.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/fb.js"></script>
<script src="js/both.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="https://widgets.twimg.com/j/2/widget.js"></script>
<script src="https://apis.google.com/js/plusone.js"></script>
<script src="https://maps.google.com/maps/api/js?sensor=false"></script>
<style>* { margin: 0; padding: 0; }</style>
<script>
$(window).load( function() {

	// DISPLAY WIDGETS
	displayWidgetsOnload( $('.pageBody') );


	// FORM AUTOFILLING
	var $birthDate = $('#user_fb_birthday').html();
	if ( $birthDate != '' ) {
		var $birthMonth = $birthDate.substr( 0, 2 );
		var $birthDay = $birthDate.substr( 3, 2 );
		var $birthYear = $birthDate.substr( 6 );

		if ( $('[name="Birthday_Month"]').length )
			$('[name="Birthday_Month"]').val( $birthMonth );

		if ( $('[name="Birthday_Day"]').length )
			$('[name="Birthday_Day"]').val( $birthDay );

		if ( $('[name="Birthday_Year"]').length )
			$('[name="Birthday_Year"]').val( $birthYear );
	} // birthdate

	if ( $('[name="Gender"]').length )
		$('[name="Gender"]').val( $('#user_fb_gender').html() );

	if ( $('[name="First_Name"]').length )
		$('[name="First_Name"]').val( $('#user_fb_first_name').html() );

	if ( $('[name="Last_Name"]').length )
		$('[name="Last_Name"]').val( $('#user_fb_last_name').html() );

	if ( $('[name="City"]').length )
		$('[name="City"]').val( $('#user_fb_location').html() );

	if ( $('[name="Email"]').length )
		$('[name="Email"]').val( $('#user_fb_email').html() );


	// CONTEST IS NOT AVAILABLE
	if ( $('#contest_available').html() != '' ) {
		var $myAlert = new myAlert();
		$myAlert.show( $('body'), $('.appName').html(), $('#contest_available').html(), true );
	}


}); // onload
</script>

</head>



<body class="fb-view">

	<!-- include Javascript SDK -->
	<div id="fb-root"></div>
	<script>jsSDK();</script>


	<div id="includes">
		<div class="appId"><?php echo $appId; ?></div>
		<div class="pageId"><?php echo $page_id; ?></div>
		<div class="signedRequest"><?php echo $signed_request; ?></div>
		<div class="appLink"><?php echo $_SESSION['appLink']; ?></div>
		<div class="appName"><?php echo $tab['name']; ?></div>
		<div id="contest_available"><?php echo contestAvailable( $user_info, $contestProperties ); ?></div>
		<div id="user_fb_first_name"><?php echo $user_info["first_name"]; ?></div>
		<div id="user_fb_last_name"><?php echo $user_info["last_name"]; ?></div>
		<div id="user_fb_birthday"><?php echo $user_info["birthday"]; ?></div>
		<div id="user_fb_location"><?php echo $user_info["location"]["name"]; ?></div>
		<div id="user_fb_gender"><?php echo $user_info["gender"]; ?></div>
		<div id="user_fb_email"><?php echo $user_info["email"]; ?></div>
	</div><!-- / .includes -->


	<form method="post"
		action="<?php if ( $tab['ugc'] == '1' ) echo 'view-ugc-form.php'; else echo 'view-thank.php'; ?>">
		<?php echo $tab['content_contest_form']; ?>
		<input type="hidden" name="user_fb_id" value="<?php echo $user_info["id"]; ?>" />
		<input type="hidden" name="campaign_id" value="<?php echo $tab['id']; ?>" />
	</form>


	<script>

		window.fbAsyncInit = function() {
			FB.init({
				appId  : $appId,
				status : true,
				cookie : true,
				xfbml  : true
			});
		};

		(function() {
			var e = document.createElement('script');
			e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?php echo $appId; ?>';
			e.async = true;
			document.getElementById('fb-root').appendChild(e);
		}());

	</script>

</body>
</html>
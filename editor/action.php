<?php

	// includes
	require_once('config.php');
	require_once('functions.php');


	# WHAT TO DO...
	switch ( $_REQUEST['act'] )
	{


		############## CHECK USER TABS LIMIT #############

		case 'tabs_limit':
			$fbUserId = $_REQUEST['user_id'];
			$fbPageId = $_REQUEST['fan_page_id'];
			// get limit number
			$query = "SELECT tabs_limit FROM users WHERE fb_id = '".$fbUserId."' LIMIT 1 ";
			$query = mysql_query( $query ) or exit( mysql_error() );
			$limit = mysql_result( $query, 0 );
			// get created tabs number
			$query = "SELECT COUNT(*) FROM pages WHERE type = 'tab' AND fan_page_id = '".$fbPageId."' ";
			$query = mysql_query( $query ) or exit( mysql_error() );
			$tabsNumber = mysql_result( $query, 0 );
			// show result
			if ( $limit > $tabsNumber ) echo 'ok'; else echo 'limit';
			break;




		############### CHECK TAB NAME ################

		case 'check_tab_name':
			// check if a user already has a tab with a provided name
			$query = "SELECT name FROM pages WHERE user_id = '" . $_REQUEST['fb_user_id'] . "'
								AND BINARY name = '" . escapeStr( $_REQUEST['name'] ) . "' ";
			$query = mysql_query( $query ) or exit( mysql_error() );
			if ( mysql_num_rows( $query ) == 0 ) echo 'ok';
			break;




		################# CREATE NEW TAB ################

		case 'create_tab':
			// before creating a new tab, we have to check, if such tab doesn't exist
			$query = "SELECT * FROM pages WHERE user_id = '".$_REQUEST['user_id']."'
								AND fan_page_id = '".$_REQUEST['page_id']."'
								AND app_id = '".$_REQUEST['app_id']."' LIMIT 1 ";
			$query = mysql_query( $query ) or exit( mysql_error() );
			// no similar records found
			if ( ! mysql_num_rows( $query ) ) {
				$name = escapeStr( $_REQUEST['name'] );
				// add new tab into "pages" table
				mysql_query("INSERT INTO pages VALUES ( 0,
											'".$_REQUEST['user_id']."',
											'".$_REQUEST['page_id']."',
											'".$_REQUEST['app_id']."',
											'0', 'tab', '".$name."', '', '', '', '', '', '', '', '0', '0', '0', '0'
											);") or exit( mysql_error() );
				// get the added tab id
				$newTab = ars_getPageInfo();
				// create new record with campaign properties in "sp_campaign_info" table
				mysql_query("INSERT INTO sp_campaign_info VALUES (
											'".$newTab['id']."',
											'".$_REQUEST['partner_id']."',
											'', '', '', '', '', '', '', '', '',
											'', '', '', '', '', '', '', '', '', '',
											'', '', '', '', '', '', '', '', '', '',
											'', '', '', '-1',
											'".$name."'
											);") or exit( mysql_error() );
				// show the added tab id
				echo $newTab['id'];
			}
			break;




		################# RENAME TAB ##################

		case 'rename_tab':
			// change name
			mysql_query("UPDATE pages SET
										name = '". escapeStr( $_REQUEST['name'] ) ."'
										WHERE id = '".$_REQUEST['id']."' AND app_id = '".$_REQUEST['app_id']."'
										") or exit( mysql_error() );
			echo 'ok';
			break;




		################## DELETE PAGE ##################

		case 'delete_page':
			// delete page children
			ars_delete( $_REQUEST['id'], "parent_id" );
			// delete page itself
			ars_delete( $_REQUEST['id'] );
			// redirection
			header ( 'Location: dashboard.php' );
			break;




		################ AJAX PAGE LOAD #################

		case 'load_page':
			// get content
			$query = "SELECT * FROM pages WHERE id='".$_REQUEST['id']."' LIMIT 1 ";
			$query = mysql_query( $query ) or exit( mysql_error() );
			$content = mysql_fetch_assoc( $query );
			// show the whole content
			echo $content['content_nonfans'];
			echo $content['content_fans'];
			echo $content['content_contest_fans'];
			echo $content['content_contest_form'];
			echo $content['content_contest_ugc_form'];
			echo $content['content_contest_ugc_gallery'];
			echo $content['content_contest_thank'];
			// delete all unused files
			ars_deleteUnusedFiles( $_REQUEST['id'] );
			break;




		################# AJAX PAGE SAVE #################

		case 'save_page':
			$cNonFans = ars_urldecode( $_REQUEST['content_nonfans'] );
			$cFans = ars_urldecode( $_REQUEST['content_fans'] );
			$cContestFans = ars_urldecode( $_REQUEST['content_contest_fans'] );
			$cContestForm = ars_urldecode( $_REQUEST['content_contest_form'] );
			$cContestUGCForm = ars_urldecode( $_REQUEST['content_contest_ugc_form'] );
			$cContestUGCGallery = ars_urldecode( $_REQUEST['content_contest_ugc_gallery'] );
			$cContestThank = ars_urldecode( $_REQUEST['content_contest_thank'] );
			mysql_query("UPDATE pages SET
										content_nonfans = '".$cNonFans."',
										content_fans = '".$cFans."',
										content_contest_fans = '".$cContestFans."',
										content_contest_form = '".$cContestForm."',
										content_contest_ugc_form = '".$cContestUGCForm."',
										content_contest_ugc_gallery = '".$cContestUGCGallery."',
										content_contest_thank = '".$cContestThank."',
										contest = '". $_REQUEST['contest'] ."',
										ugc = '". $_REQUEST['ugc'] ."',
										saved = '1'
										WHERE id = '".$_REQUEST['id']."'
										") or exit( mysql_error() );
			// delete all unused files
			ars_deleteUnusedFiles( $_REQUEST['id'] );
			// update contest properties
			mysql_query("UPDATE sp_campaign_info SET
										campaign_description = '". escapeStr( $_REQUEST['contest_descr'] ) ."',
										campaign_start_time = '". $_REQUEST['contest_start'] ."',
										campaign_end_time = '". $_REQUEST['contest_end'] ."',
										campaign_voting_start_time = '". $_REQUEST['contest_voting_start'] ."',
										campaign_voting_end_time = '". $_REQUEST['contest_voting_end'] ."',
										campaign_vote_frequency = '". $_REQUEST['contest_voting_times'] ."',
										campaign_is_check_reentry = '". $_REQUEST['contest_entries'] ."',
										campaign_is_allow_child = '". $_REQUEST['contest_age'] ."',
										campaign_update_time = NOW()
										WHERE campaign_sid = '".$_REQUEST['id']."'
										") or exit( mysql_error() );
			// if contest is enabled and contest create time is not set yet - save current datetime
			if ( $_REQUEST['contest'] == '1' ) {
				$query = "SELECT campaign_create_time FROM sp_campaign_info
									WHERE campaign_sid = '".$_REQUEST['id']."' LIMIT 1 ";
				$query = mysql_query( $query ) or exit( mysql_error() );
				$createTime = mysql_result( $query, 0 );
				if ( $createTime == '0000-00-00 00:00:00' ) {
					mysql_query("UPDATE sp_campaign_info SET
												campaign_create_time = NOW()
												WHERE campaign_sid = '".$_REQUEST['id']."'
												") or exit( mysql_error() );
				}
			}
			break;




		########## AJAX DEACTIVATE CONTEST FIELDS ###########

		// it is needed to check if a contest form field still exist;
		// fields will be activated later by "save_contest_config" case
		case 'deactivate_contest_fields':
			mysql_query("UPDATE sp_campaign_config SET cpfldcfg_is_valid = '0'
										WHERE campaign_sid = '".$_REQUEST['id']."'
										") or exit( mysql_error() );
			break;




		############# AJAX SAVE CONTEST CONFIG #############

		case 'save_contest_config':

			// field is stored for the first tme
			if ( $_REQUEST['config_id'] == '' ) {
				mysql_query("INSERT INTO sp_campaign_config VALUES ( 0,
											'',
											'".$_REQUEST['id']."',
											'".$_REQUEST['label']."',
											'".$_REQUEST['name']."',
											'',
											'".$_REQUEST['required']."',
											'', '', '', '', '',
											'".$_REQUEST['error']."',
											'', '', '', '', '', '', '', '1', NOW(), NOW()
											);") or exit( mysql_error() );
				// show added field id
				$query = "SELECT * FROM sp_campaign_config ORDER BY cpfldcfg_sid DESC LIMIT 1";
				$query = mysql_query( $query ) or exit( mysql_error() );
				echo mysql_result( $query, 0 );
			}
			// field has been already stored
			else {
				mysql_query("UPDATE sp_campaign_config SET
											cpfldcfg_label = '".$_REQUEST['label']."',
											cpfldcfg_var_name = '".$_REQUEST['name']."',
											cpfldcfg_is_mandatory = '".$_REQUEST['required']."',
											cpfldcfg_invalid_message = '".$_REQUEST['error']."',
											cpfldcfg_is_valid = '1',
											cpfldcfg_update_time = NOW()
											WHERE cpfldcfg_sid = '".$_REQUEST['config_id']."'
											") or exit( mysql_error() );
			}
			break;




		################ AJAX PAGE PUBLISH ###############

		case 'publish_page':
			$query = mysql_query("UPDATE pages SET published = '1' WHERE id = '".$_REQUEST['id']."' ")
								or exit( mysql_error() );
			break;




		############### AJAX PAGE UNPUBLISH ##############

		case 'unpublish_page':
			$query = mysql_query("UPDATE pages SET published = '0' WHERE id = '".$_REQUEST['id']."' ")
								or exit( mysql_error() );
			break;




		################ AJAX DELETE FILE ################

		case 'delete_file':
			ars_delete( $_REQUEST['id'] );
			break;




		############### AJAX GET ID BY NAME ###############

		case 'get_id_by_name':
			$query = "SELECT id FROM pages WHERE name='".$_REQUEST['name']."' ";
			$query = mysql_query( $query ) or exit( mysql_error() );
			echo mysql_result( $query, 0 );
			break;




		########### AJAX SAVE BACKGROUND IMAGE ###########

		case 'save_bg':
			if ( $_REQUEST['bgToUpdate'] )
				echo ars_updateFile( 'bgImage', $_REQUEST['bgToUpdate'] );
			else
				echo ars_uploadFile( 'bgImage', $_REQUEST['pageID'], 'bg' );
			break;




		############# AJAX ADD CONTENT IMAGE #############

		case 'content_image':
			if ( $_REQUEST['imageToUpdate'] )
				echo ars_updateFile( 'contentImage', $_REQUEST['imageToUpdate'] );
			else
				echo ars_uploadFile( 'contentImage', $_REQUEST['pageID'], 'attachment' );
			break;




		############ AJAX ADD CONTACT FORM #############

		case 'content_contact_form':
			if ( $_REQUEST['contactButtonToUpdate'] )
				echo ars_updateFile( 'contactButtonImage', $_REQUEST['contactButtonToUpdate'] );
			else
				echo ars_uploadFile( 'contactButtonImage', $_REQUEST['pageID'], 'attachment' );
			break;




		############ AJAX ADD EMAIL BUTTON #############

		case 'content_email':
			if ( $_REQUEST['emailToUpdate'] )
				echo ars_updateFile( 'emailButtonImage', $_REQUEST['emailToUpdate'] );
			else
				echo ars_uploadFile( 'emailButtonImage', $_REQUEST['pageID'], 'attachment' );
			break;




		############ AJAX ADD TWITTER SHARE ############

		case 'content_twitter_share':
			if ( $_REQUEST['twitterShareToUpdate'] )
				echo ars_updateFile( 'twitterShareButtonImage', $_REQUEST['twitterShareToUpdate'] );
			else
				echo ars_uploadFile( 'twitterShareButtonImage', $_REQUEST['pageID'], 'attachment' );
			break;




		########### AJAX ADD CONTEST BUTTON #############

		case 'contest_button':
			if ( $_REQUEST['contestBtnImageToUpdate'] )
				echo ars_updateFile( 'contestBtnImage', $_REQUEST['contestBtnImageToUpdate'] );
			else
				echo ars_uploadFile( 'contestBtnImage', $_REQUEST['pageID'], 'attachment' );
			break;




		########## AJAX ADD UGC CONTEST IMAGE ###########

		case 'contest_ugc_image':
			// one ugc entry can have only one photo form one user,
			// so if there's a photo with the id, we must update it
			$query = "SELECT * FROM pages WHERE type = 'ugc' AND saved = '0'
								AND parent_id = '".$_REQUEST['pageID']."' LIMIT 1";
			$query = mysql_query( $query ) or exit( mysql_error() );
			if ( mysql_num_rows( $query ) > 0 ) {
				$uploadedPhoto = mysql_fetch_assoc( $query );
				echo ars_updateFile( 'contestUGCUploadImg', $uploadedPhoto['id'] );
			} else {
				echo ars_uploadFile( 'contestUGCUploadImg', $_REQUEST['pageID'], 'ugc' );
			}
			break;




		######## AJAX VOTE FOR UGC CONTEST IMAGE #########

		case 'contest_ugc_image_vote':
			// get ugc_sid and ugc_campaign_sid from sp_campaign_ugc table
			$ugc_img_sid = getImgUgcSidAndCampaignSid( $_REQUEST['img_name'], 'img' );
			$ugc_campaign_sid = getImgUgcSidAndCampaignSid( $_REQUEST['img_name'], 'campaign' );
			// check voting ability
			$vAbility = checkVotingAbility( $ugc_campaign_sid, $_REQUEST['fb_user_id'], $ugc_img_sid );
			if ( $vAbility !== true ) {
				echo $vAbility;
			}
			else {
				mysql_query("INSERT INTO sp_campaign_ugc_vote VALUES ( 0,
											'".$ugc_img_sid."',
											'".$_REQUEST['fb_user_id']."',
											NOW(),
											'".$ugc_campaign_sid."',
											'".$_REQUEST['user_ip']."',
											''
											);") or exit( mysql_error() );
				echo 'ok';
			}
			break;




		########## AJAX ADD IMAGE TO COOL SLIDER #########

		case 'content_coolslider_image':
			echo ars_uploadFile( 'coolSliderImage', $_REQUEST['pageID'], 'attachment' );
			break;




		############ AJAX ADD FB POST TO FEED ############

		case 'content_fb_post_feed':
			// post image
			if ( $_REQUEST['postFeedImgToUpdate'] )
				echo ars_updateFile( 'postFeedImgImage', $_REQUEST['postFeedImgToUpdate'] );
			else
				echo ars_uploadFile( 'postFeedImgImage', $_REQUEST['pageID'], 'attachment' );
			// divider for parsing
			echo '|';
			// custom button
			if ( $_REQUEST['postFeedBtnToUpdate'] )
				echo ars_updateFile( 'postFeedBtnImage', $_REQUEST['postFeedBtnToUpdate'] );
			else
				echo ars_uploadFile( 'postFeedBtnImage', $_REQUEST['pageID'], 'attachment' );
			break;




		############## AJAX ADD FB INVITE ###############

		case 'content_invite':
			if ( $_REQUEST['inviteToUpdate'] )
				echo ars_updateFile( 'inviteBtnImage', $_REQUEST['inviteToUpdate'] );
			else
				echo ars_uploadFile( 'inviteBtnImage', $_REQUEST['pageID'], 'attachment' );
			break;




		################# GET RSS FEED #################

		case 'get_rss':
			echo file_get_contents( $_REQUEST['url'] );
			break;




		############ GET GOOGLE MAPS COORDS ############

		case 'get_gm_coords':
			$url = 'https://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=';
			$url .= $_REQUEST['address'];
			$url = str_replace( ' ', '+', $url );
			echo file_get_contents( $url );
			break;


	} // switch


	// документ UTF-8
?>
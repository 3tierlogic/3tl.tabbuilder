<!doctype html>

<html lang="en-US">
<head>
<meta charset="UTF-8" />
<title>Help</title>
<link href="../style.css" rel="stylesheet" type="text/css" />
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if IE 6]>
<script src="../js/belatedPNG.js"></script>
<script>
	DD_belatedPNG.fix('*');
</script>
<![endif]-->
</head>

<body class="normalpage">
<section id="page">
<div id="bodywrap">
<section id="top">
<nav>
<h1 id="sitename">
  <a href="#">Help</a></h1>
  
  <ul id="sitenav">
  	<li class="current"><a href="../editor/dashboard.php">Back to Tab Editor</a></li>
  </ul>
  
</nav>
<header id="normalheader"></header>
</section>
<section id="contentwrap">
	<div id="contents" class="normalcontents"><section id="normalpage">
		<section id="left">
			<h2>Help</h2>
			<article>
			<ul>
            	<li>Welcome Package - <a href="welcome_package.zip" target="_blank">Download</a></li>
                <li>Sweepstakes Creative Requirements - <a href="sweepstakes_creative_requirements.pdf" target="_blank">Download</a></li>
                <li>Sweepstakes Business Requirements - <a href="sweepstakes_business_requirements.xls" target="_blank">Download</a></li>
                <li>Photo Contest Creative Requirement - <a href="photo_contest_creative_requirements.pdf" target="_blank">Download</a></li>
                <li>Photo Contest Business Requirement - <a href="photo_contest_business_requirements.xlsx" target="_blank">Download</a></li>
            </ul>
			</article>
		</section>
		<section id="sidebar">
        <h2>Twitter</h2>
<article class="testimonials">

<script src="http://widgets.twimg.com/j/2/widget.js"></script>
<script>
new TWTR.Widget({
  version: 2,
  type: 'search',
  search: 'from:3tierlogicrobc OR from:3tierlogicmarie OR from:3tierlogiccarlo OR from:3TierLogicABat OR from:3tierlogicDOM OR from:3TierLogicBlack OR from:3tierlogicJILL OR from:3tierlogicPaul OR from:3tierlogicsandi',
  interval: 6000,
  title: '3 Tier Logic Twitter Feeds',
  subject: 'LIVE',
  width: 280,
  height: 300,
  theme: {
    shell: {
       background: '#333333',
       color: '#ffffff'
    },
    tweets: {
      background: '#000000',
      color: '#ffffff',
      links: '#4aed05'
    }
  },
  features: {
    scrollbar: false,
    loop: true,
    live: true,
    hashtags: true,
    timestamp: false,
    avatars: true,
    toptweets: true,
    behavior: 'default'
  }
}).render().start();
</script>
</article>
        
        
        </section>
		<div class="clear"></div>
	</div>
</section>
</div>

<footer id="pagefooter">
<div id="bottom">
<div class="block1">
<h2>Meet The Team</h2>

<div class="teamimg">

<? include("../editor/team.php")?>

</div>
</div>
<div class="block2">
<h2>Latest Project</h2>
<div class="projectthmb"><? include("../editor/latestproject.php")?></div>

</div>
<div class="block3">

<h2>About</h2>
<p>3 Tier Logic is a full service digital marketing and technology services company that enables organizations to grow their customer and prospect communities through anywhere-anytime communications. </p>

</div>
<div class="clear"></div>
</div>
<div id="credits">
<p>
<span class="copyright">
&copy; 2012 | 3 Tier Logic Inc |  All Rights Reserved </span>

</div>
</footer>

</section>
</body>
</html>

<?php
require_once("db.php");
$con = mysql_connect($dbhost, $dbuser, $dbpasswd);		// connect to database
if (!$con) {											// error checking and handling
    die('Could not connect: ' . mysql_error());
}
mysql_select_db($dbname);
session_start();

/*
echo "cid: ".$_POST['campaignID'];
echo "cname: ".$_POST['campaignName'];
echo "<br/>";
echo "start date: ".$_POST['sdate'];
echo "end date: ".$_POST['edate'];
*/

// $_POST['campaignID'] = 'CAMP0000000174';
// $_POST['campaignName'] = 'aaa';
 
$_POST['campaignID'] = $REQUEST['cID'];
$_POST['campaignName'] = $REQUEST['cName'];

$sqlEndDate = $_POST['edate'];

$x_axis = "";
$y_axis = "";
$displaygender_div = 0;
$displayemail_div=0;
$displayagerange_div=0;
$displayzipcode_div=0;
$displaycarrier_div = 0;

$countDyn = 0;
$displaydyn_div[$countDyn]=0;
$dynstr_data[$countDyn] = "";
$labels[$countDyn] = ""; 


$y2_axis = ""; // for distinct entries.

// add where condition to support date range.
function addCon($original_sql, $s, $e){

	// detect where;
	$pos = strpos($original_sql, 'where', 1); 
	if($pos == false){ // try capital again.
		$pos = strpos($original_sql, 'WHERE', 1);
	}
	
	/*
	print "+++ <br/>";
	print "sql0: ".$original_sql."<br/>";
	print "pos: ".$pos."<br/>";
	*/
	
	$sub1 = substr($original_sql,0,$pos+1+4); // "select...where"
	//print "sub1: ".$sub1."<br/>";
	$sub2 = " cpresult_create_time between '".$s."' and '".$e."' and ";
	//print "sub2: ".$sub2."<br/>";
	$sub3 = substr($original_sql, $pos+1+4+1); // " ..."
	//print "sub3: ".$sub3."<br/>";
	
	$original_sql = "";
	$original_sql = $original_sql.$sub1;
	$original_sql = $original_sql.$sub2;
	$original_sql = $original_sql.$sub3;
	
	//print "sql: ".$original_sql."<br/>";
	//print "+++ <br/>";

	return $original_sql;
}

// check if is OpenCheckBox or ?opendropdown?
function arai($rawName, $rawMan){

	// checkbox.
	$checkbox = "OpenCheckBox";		
	$pos = strpos($rawName, $checkbox); 
	if($pos !== false){ // triple = is a must!	
		// check if is mandatory: have read the rules...
		if($rawMan == 1){ // is a mandatory checkbox, so skip it.
			return 0;
		}
		return 1; // is OpenCheckBox.
	}
	
	// dropdown.
	$dropdown = "OpenDropDown";
	$pos = strpos($rawName, $dropdown);
	if($pos !== false){ // triple = is a must!
		return 2; // is dropdown.
	}
	
	return 0; // is nothing.
}

function getCACarrierName($carrier){
						
	switch($carrier){
		case "302651":{
			return "BELL";
		}
		case "302720":{
			return "ROGERS";
		}
		case "302655":{
			return "MTS";
		}
		case "302701":{
			return "NB TEL";
		}
		case "302657":{
			return "QUEBECTEL";
		}
		case "302654":{
			return "SASK TEL";
		}
		case "302653":{
			return "TELUS";
		}
		case "302001":{
			return "VIRGIN";
		}
		case "302370":{
			return "FIDO";
		}			
		default:{
			return $carrier;
		}
	}
}

function getUSCarrierName($carrier){ 
			
	switch($carrier){
		case "310980":{
			return "AT&T";
		}
		case "310190":{
			return "ALASKA WIRELESS";
		}
		case "310500":{
			return "ALLTEL";
		}
		case "310420":{
			return "CINCINNATI BELL";
		}
		case "310180":{
			return "CINGULAR";
		}
		case "310016":{
			return "CRICKET";
		}
		case "310560":{
			return "DOBSON";
		}
		case "316010":{
			return "NEXTEL";
		}
		case "316110":{
			return "SPRINT";
		}	
		case "310660":{
			return "TMOBILE USA";
		}
		case "310012":{
			return "VERIZON";
		}			
		default:{
			return $carrier;
		}
	}
}


// to control if to show all dates or the specified period.
$default_date = 1; // defaults to show all dates.

if(($_POST['sdate'] == "") || ($sqlEndDate == "") ){ // show all(default). 
	$sql = "SELECT date_format(cpresult_create_time,'%Y-%m-%d'), count(*) as num FROM facebook.sp_campaign_result where campaign_sid='".$_POST['campaignID']."' group by date_format(cpresult_create_time,'%Y-%m-%d') order by date_format(cpresult_create_time,'%Y-%m-%d') ASC";
	$sql2 = "select dates, count(*) as num from ( SELECT date_format(cpresult_create_time,'%Y-%m-%d') as dates, cpresult_user_id as uid FROM facebook.sp_campaign_result f where f.campaign_sid='".$_POST['campaignID']."' group by date_format(f.cpresult_create_time,'%Y-%m-%d'), f.cpresult_user_id) s group by s.dates order by s.dates asc"; 
	$default_date = 1;
}
else{ //specified data
	$sql = "SELECT date_format(cpresult_create_time,'%Y-%m-%d'), count(*) as num FROM facebook.sp_campaign_result where campaign_sid='".$_POST['campaignID']."' and cpresult_create_time between '".$_POST['sdate']."' and '".$sqlEndDate."' group by date_format(cpresult_create_time,'%Y-%m-%d') order by date_format(cpresult_create_time,'%Y-%m-%d') ASC";
	$sql2 = "select dates, count(*) as num from ( SELECT date_format(cpresult_create_time,'%Y-%m-%d') as dates, cpresult_user_id as uid FROM facebook.sp_campaign_result f where f.campaign_sid='".$_POST['campaignID']."' and cpresult_create_time between '".$_POST['sdate']."' and '".$sqlEndDate."' group by date_format(f.cpresult_create_time,'%Y-%m-%d'), f.cpresult_user_id) s group by s.dates order by s.dates asc"; 
	$default_date = 0;
}
	//echo "sql = ".$sql."<br/>";
	//echo "sql2 = ".$sql2."<br/>";
$res = mysql_query($sql);
$res2 = mysql_query($sql2);

if (mysql_num_rows($res) > 0) {
	//echo "total rows: ".mysql_num_rows($res)."<br>";
	
	$index = 0;
	$total_entry = 0;
	$total_distinct_entry = 0;
	// get Y.
	while (list($currdate, $totalentries) = mysql_fetch_array($res)) {
		//echo "date:".$currdate."- entries: ".$totalentries."<br>";
		$newcurrdate = date('m/d', strtotime($currdate));
		$x_axis = $x_axis . "'".$newcurrdate."',";
		$y_axis = $y_axis . $totalentries.",";
		
		// create array to store sql and sql2 results.
		$array_sql[$index][0] = $newcurrdate;
		$array_sql[$index][1] = $totalentries;
		$total_entry += $totalentries;
		$index++;
	}
	$array_sql[$index][0] = "Total";
	$array_sql[$index][1] = $total_entry;
	
	
	$index = 0;
	// get y2.
	while ( list($d, $dis) = mysql_fetch_array($res2)){
		$y2_axis = $y2_axis . $dis.",";
		$array_sql[$index][2] = $dis;
		$total_distinct_entry += $dis;
		$index++;
	}
	$array_sql[$index][2] = $total_distinct_entry;

	$x_axis = substr($x_axis, 0, -1);
	$y_axis = substr($y_axis, 0, -1);
$y2_axis = substr($y2_axis, 0, -1);
}


//***** Pie charts
// $sql = "SELECT cpfldcfg_label, cpfldcfg_var_name FROM sp_campaign_config where campaign_sid='".$_POST['campaignID']."' and cpfldcfg_is_valid=1";
$sql = "SELECT cpfldcfg_label, cpfldcfg_var_name, cpfldcfg_country_sid, cpfldcfg_is_mandatory FROM sp_campaign_config where campaign_sid='".$_POST['campaignID']."' and cpfldcfg_is_valid=1";

$res = mysql_query($sql);
if (mysql_num_rows($res) > 0) {
	// while (list($field_label, $field_varname, $field_mandatory) = mysql_fetch_array($res)) {
	while (list($field_label, $field_varname, $field_country, $field_mandatory) = mysql_fetch_array($res)) {
		switch ($field_varname) {
    			case "GenderSid":
					$genderstr_data="";
       				echo "Gender chart<br>";					
					$sql_gender = "SELECT cpresult_gender_sid, count(*) as num FROM sp_campaign_result where campaign_sid='".$_POST['campaignID']."' group by cpresult_gender_sid ";
					if(!$default_date){ // all dates.
						$sql_gender = addCon($sql_gender, $_POST['sdate'], $sqlEndDate);
					}
					
					$res_gender = mysql_query($sql_gender); 
					if (mysql_num_rows($res_gender) > 0) {
						while (list($gender, $totalgender) = mysql_fetch_array($res_gender)) {
							if ($gender==0) {
								$currgender="Female";								
							} else {
								$currgender="Male";
							}
							//echo "Gender:".$currgender."- Numbers: ".$totalgender."<br>";
							$genderstr_data = $genderstr_data."['".$currgender."', ".$totalgender."],";
						}						
						
						$genderstr_data = substr($genderstr_data, 0, -1);
						echo $genderstr_data;
						$displaygender_div=1;
					}
        			break;
    			case "Birthday":
					
					$b_t = 0;
				
        			echo "Birthday chart<br>";
					$currentyear = date("Y");
					$agerangestr_data = "";;
					//echo "Current Year: ".$currentyear."<br>";
					//** Check under 18
					$yearlimit = $currentyear - 18;
					//echo "Current Year to check: ".$yearlimit."<br>";
					$sql_bday = "SELECT cpresult_birth_year FROM sp_campaign_result where campaign_sid='".$_POST['campaignID']."' AND cpresult_birth_year > ".$yearlimit;
					
					if(!$default_date){ 
						$sql_bday = addCon($sql_bday, $_POST['sdate'], $sqlEndDate);
					}
					
					//echo $sql_bday."<br>";
					$res_bday = mysql_query($sql_bday); 
					echo "Under 18: ".mysql_num_rows($res_bday)."<br>";
					$agerangestr_data = $agerangestr_data."['Under 18', ".mysql_num_rows($res_bday)."],";
					$b0_17 = mysql_num_rows($res_bday);
					$b_t +=  mysql_num_rows($res_bday);
					
					//** Check 18 - 24
					$yearlimit_high = $currentyear - 18;
					$yearlimit_low = $currentyear - 24;
					$sql_bday = "SELECT cpresult_birth_year FROM sp_campaign_result where campaign_sid='".$_POST['campaignID']."' AND cpresult_birth_year >= ".$yearlimit_low." AND cpresult_birth_year <= ".$yearlimit_high;
					
					if(!$default_date){ 
						$sql_bday = addCon($sql_bday, $_POST['sdate'], $sqlEndDate);
					}
					
					//echo $sql_bday."<br>";
					$res_bday = mysql_query($sql_bday); 
					echo "18 - 24: ".mysql_num_rows($res_bday)."<br>";
					$agerangestr_data = $agerangestr_data."['18 - 24', ".mysql_num_rows($res_bday)."],";
					$b18_24 = mysql_num_rows($res_bday);
					$b_t +=  mysql_num_rows($res_bday);
					
					//** Check 25 - 34
					$yearlimit_high = $currentyear - 25;
					$yearlimit_low = $currentyear - 34;
					$sql_bday = "SELECT cpresult_birth_year FROM sp_campaign_result where campaign_sid='".$_POST['campaignID']."' AND cpresult_birth_year >= ".$yearlimit_low." AND cpresult_birth_year <= ".$yearlimit_high;
					
					if(!$default_date){ 
						$sql_bday = addCon($sql_bday, $_POST['sdate'], $sqlEndDate);
					}
					
					//echo $sql_bday."<br>";
					$res_bday = mysql_query($sql_bday); 
					echo "25 - 34: ".mysql_num_rows($res_bday)."<br>";
					$agerangestr_data = $agerangestr_data."['25 - 34', ".mysql_num_rows($res_bday)."],";
					$b25_34 = mysql_num_rows($res_bday);
					$b_t +=  mysql_num_rows($res_bday);
					
					//** Check 35 - 44
					$yearlimit_high = $currentyear - 35;
					$yearlimit_low = $currentyear - 44;
					$sql_bday = "SELECT cpresult_birth_year FROM sp_campaign_result where campaign_sid='".$_POST['campaignID']."' AND cpresult_birth_year >= ".$yearlimit_low." AND cpresult_birth_year <= ".$yearlimit_high;
					
					if(!$default_date){ 
						$sql_bday = addCon($sql_bday, $_POST['sdate'], $sqlEndDate);
					}
					
					//echo $sql_bday."<br>";
					$res_bday = mysql_query($sql_bday); 
					echo "35 - 44: ".mysql_num_rows($res_bday)."<br>";
					$agerangestr_data = $agerangestr_data."['35 - 44', ".mysql_num_rows($res_bday)."],";
					$b35_44 = mysql_num_rows($res_bday);
					$b_t +=  mysql_num_rows($res_bday);
					
					//** Check 45 - 54
					$yearlimit_high = $currentyear - 45;
					$yearlimit_low = $currentyear - 54;
					$sql_bday = "SELECT cpresult_birth_year FROM sp_campaign_result where campaign_sid='".$_POST['campaignID']."' AND cpresult_birth_year >= ".$yearlimit_low." AND cpresult_birth_year <= ".$yearlimit_high;
					
					if(!$default_date){ 
						$sql_bday = addCon($sql_bday, $_POST['sdate'], $sqlEndDate);
					}
					
					//echo $sql_bday."<br>";
					$res_bday = mysql_query($sql_bday); 
					echo "45 - 54: ".mysql_num_rows($res_bday)."<br>";
					$agerangestr_data = $agerangestr_data."['45 - 54', ".mysql_num_rows($res_bday)."],";
					$b45_54 = mysql_num_rows($res_bday);
					$b_t +=  mysql_num_rows($res_bday);
					
					//** Check 55 - 64
					$yearlimit_high = $currentyear - 55;
					$yearlimit_low = $currentyear - 64;
					$sql_bday = "SELECT cpresult_birth_year FROM sp_campaign_result where campaign_sid='".$_POST['campaignID']."' AND cpresult_birth_year >= ".$yearlimit_low." AND cpresult_birth_year <= ".$yearlimit_high;
					
					if(!$default_date){ 
						$sql_bday = addCon($sql_bday, $_POST['sdate'], $sqlEndDate);
					}
					
					//echo $sql_bday."<br>";
					$res_bday = mysql_query($sql_bday); 
					echo "55 - 64: ".mysql_num_rows($res_bday)."<br>";
					$agerangestr_data = $agerangestr_data."['55 - 64', ".mysql_num_rows($res_bday)."],";
					$b55_64 = mysql_num_rows($res_bday);
					$b_t +=  mysql_num_rows($res_bday);
					
					//** Check 65+
					$yearlimit = $currentyear - 65;
					$sql_bday = "SELECT cpresult_birth_year FROM sp_campaign_result where campaign_sid='".$_POST['campaignID']."' AND cpresult_birth_year <= ".$yearlimit ." AND cpresult_birth_year > 0";
					//echo $sql_bday."<br>";
					$res_bday = mysql_query($sql_bday); 
					
					if(!$default_date){ 
						$sql_bday = addCon($sql_bday, $_POST['sdate'], $sqlEndDate);
					}
					
					echo "65+: ".mysql_num_rows($res_bday)."<br>";
					$agerangestr_data = $agerangestr_data."['65+', ".mysql_num_rows($res_bday)."],";
					$b65_99 = mysql_num_rows($res_bday);
					$b_t +=  mysql_num_rows($res_bday);
					
					//** Check year=0
					$sql_bday = "SELECT cpresult_birth_year FROM sp_campaign_result where campaign_sid='".$_POST['campaignID']."' AND cpresult_birth_year = 0";
					
					if(!$default_date){ 
						$sql_bday = addCon($sql_bday, $_POST['sdate'], $sqlEndDate);
					}
					
					//echo $sql_bday."<br>";
					$res_bday = mysql_query($sql_bday); 
					echo "No year entered ".mysql_num_rows($res_bday)."<br>";
					$agerangestr_data = $agerangestr_data."['No year entered', ".mysql_num_rows($res_bday)."]";
					$b0_0 = mysql_num_rows($res_bday);
					$b_t +=  mysql_num_rows($res_bday);
					
					echo $agerangestr_data;
					$displayagerange_div = 1;
					
					break;
				case "EmailOpt":
        			//echo "Email Opt-in chart<br>";
					$sql_email = "SELECT cpresult_email_opt, count(*) as num FROM sp_campaign_result where campaign_sid='".$_POST['campaignID']."' group by cpresult_email_opt ";
					if(!$default_date){ 
						$sql_email = addCon($sql_email, $_POST['sdate'], $sqlEndDate);
					}
					$res_email = mysql_query($sql_email); 
					if (mysql_num_rows($res_email) > 0) {
						while (list($receiveemail, $totalreceiveemail) = mysql_fetch_array($res_email)) {
							if ($receiveemail=="on") {
								$currreceiveemail="Yes";
							} else {
								$currreceiveemail="No";
							}
							//echo "Receive Email:".$currreceiveemail."- Numbers: ".$totalreceiveemail."<br>";
							$receivestr_data = $receivestr_data."['".$currreceiveemail."', ".$totalreceiveemail."],";
						}
						$receivestr_data  = substr($receivestr_data , 0, -1);
						echo $receivestr_data ;
						$displayemail_div=1;
					}
        			break;
				case "ZipCode":
					//echo "Zipcode chart <br/>";
					//$sql_zipcode = "SELECT UCASE(SUBSTR(cpresult_zip_code, 1,3)) as location, count(*) as num FROM sp_campaign_result where campaign_sid ='".$_POST['campaignID']."' group by SUBSTR(cpresult_zip_code, 1,3)";
					$sql_zipcode = "SELECT UCASE(SUBSTR(cpresult_zip_code, 1,3)) as location, count(*) as num FROM sp_campaign_result where campaign_sid ='".$_POST['campaignID']."' group by SUBSTR(cpresult_zip_code, 1,3) order by num desc";
					
					if(!$default_date){ 
						$sql_zipcode = addCon($sql_zipcode, $_POST['sdate'], $sqlEndDate);
					}
					//echo "sql zipcode = ".$sql_zipcode."<br/>";
					
					$res_zipcode = mysql_query($sql_zipcode);
					
					if(mysql_num_rows($res_zipcode) > 0){
						$count10 = 1; // initial var to count top 10.
						$sumOthers = 0; // num of the rest.
						while(list($digit3, $num) = mysql_fetch_array($res_zipcode)){
							if($count10 <= 10){ // still in top 10.
								$zipcodestr_data = $zipcodestr_data."['".$digit3."', ".$num."],";
								$count10++;
							}
							else{ // sum up offsides.
								$sumOthers += $num;
							}
						}
						if($count10 > 10){
							$zipcodestr_data = $zipcodestr_data."['The rest', ".$sumOthers."],"; // add the rest to string.
						}
						$zipcodestr_data = substr($zipcodestr_data, 0, -1);
						//echo $zipcodestr_data."<br />"; 
						$displayzipcode_div=1;
					}
					
					break;
				case "Carrier":
					//echo "carrier chart <br/>";
					if($field_country == 'CA'){
						//echo "CA <br/>";						
						
						$sql_carrier = "SELECT cpresult_carrier as carrier, count(*) as num FROM sp_campaign_result where campaign_sid ='".$_POST['campaignID']."' and cpresult_carrier != '' group by cpresult_carrier";
						//echo "Carrier SQL: ".$sql_carrier."<br/>";
						
						if(!$default_date){ 
							$sql_carrier = addCon($sql_carrier, $_POST['sdate'], $sqlEndDate);
						}						
						
						$res_carrier = mysql_query($sql_carrier);						
						if(mysql_num_rows($res_carrier) > 0){
							while(list($car, $num) = mysql_fetch_array($res_carrier)){
								//echo "car: ".$car." NUM: ".$num."<br/>";
								$car = getCACarrierName($car); 
								$carrierstr_data = $carrierstr_data."['".$car."', ".$num."],";
							}
							$carrierstr_data = substr($carrierstr_data, 0, -1);
							//echo $carrierstr_data."<br />"; 
							$displaycarrier_div=1;
						}
						
					}
					else{					
						
						//echo "US <br/>";						
						
						$sql_carrier = "SELECT cpresult_carrier as carrier, count(*) as num FROM sp_campaign_result where campaign_sid ='".$_POST['campaignID']."' and cpresult_carrier != '' group by cpresult_carrier";
						//echo "Carrier SQL: ".$sql_carrier."<br/>";
						
						if(!$default_date){ 
							$sql_carrier = addCon($sql_carrier, $_POST['sdate'], $sqlEndDate);
						}						
						
						$res_carrier = mysql_query($sql_carrier);						
						if(mysql_num_rows($res_carrier) > 0){
							while(list($car, $num) = mysql_fetch_array($res_carrier)){
								//echo "car: ".$car." NUM: ".$num."<br/>";
								$car = getUSCarrierName($car); 
								$carrierstr_data = $carrierstr_data."['".$car."', ".$num."],";
							}
							$carrierstr_data = substr($carrierstr_data, 0, -1);
							//echo $carrierstr_data."<br />"; 
							$displaycarrier_div=1;
						}
						
					}
					break;
					
					default:{
						echo $field_varname." ".$field_mandatory."<br/>";
						$isWhat = arai($field_varname, $field_mandatory);
						if( $isWhat == 1){
							echo $field_label." is OpenCheckBox <br/>";
							echo "Dyn chart <br/>";
							
							$sql_dyn = "SELECT cpresult_".$field_varname.", count(*) FROM sp_campaign_result s where campaign_sid ='".$_POST['campaignID']."' group by cpresult_".$field_varname;
							echo "Dyn SQL: ".$sql_dyn."<br/>";
						
							if(!$default_date){ 
								$sql_dyn = addCon($sql_dyn, $_POST['sdate'], $sqlEndDate);
							}						
							
							$res_dyn = mysql_query($sql_dyn);						
							if(mysql_num_rows($res_dyn) > 0){
								while(list($dyn, $num) = mysql_fetch_array($res_dyn)){
									echo "dyn: ".$dyn." NUM: ".$num."<br/>";
									if($dyn == 1){
										$dyn = 'Yes';
									}
									else{
										$dyn = 'No';
									}
									$dynstr_data[$countDyn] = $dynstr_data[$countDyn]."['".$dyn."', ".$num."],";
								}
								$dynstr_data[$countDyn] = substr($dynstr_data[$countDyn], 0, -1);
								echo $dynstr_data[$countDyn]."<br />"; 
								$displaydyn_div[$countDyn]=1; // zaozaoer
								//$countDyn++;
							}
							$labels[$countDyn] = $field_label;
							echo "labels: ".$labels[$countDyn]."<br/>";
							$countDyn++;
							echo "countDyn = ".$countDyn."<br/>";
						}
						else if( $isWhat == 2){							
							echo $field_label." is dropdown <br/>";
							//echo "Dyn chart <br/>";
							$sql_dyn = "SELECT cpresult_".$field_varname.", count(*) FROM sp_campaign_result s where campaign_sid ='".$_POST['campaignID']."' group by cpresult_".$field_varname;
							echo "Dyn SQL: ".$sql_dyn."<br/>";
							if(!$default_date){ 
								$sql_dyn = addCon($sql_dyn, $_POST['sdate'], $sqlEndDate);
							}
							$res_dyn = mysql_query($sql_dyn);
							if(mysql_num_rows($res_dyn) > 0){
								while(list($dyn, $num) = mysql_fetch_array($res_dyn)){
									echo "dyn: ".$dyn." NUM: ".$num."<br/>";									
									$dynstr_data[$countDyn] = $dynstr_data[$countDyn]."['".$dyn."', ".$num."],";
								}
								$dynstr_data[$countDyn] = substr($dynstr_data[$countDyn], 0, -1);
								echo $dynstr_data[$countDyn]."<br />"; 
								$displaydyn_div[$countDyn]=1;
							}
							$labels[$countDyn] = $field_label;
							echo "labels: ".$labels[$countDyn]."<br/>";
							$countDyn++;
							echo "countDyn = ".$countDyn."<br/>";
						}
						else{ // 0
							// do nothing.
							//echo $field_label." is nothing. <br/>";
						}
					}
		}
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>3 Tier Logic Inc - Tracker V1.0</title>
<link rel="stylesheet" href="css/style.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/default.css" media="screen" charset="utf-8" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>
 <!-- Charts JS library: START -->
<script type="text/javascript" src="js/highcharts.js"></script>
<script type="text/javascript" src="js/themes/gray.js"></script>
<script type="text/javascript" src="js/modules/exporting.js"></script>
<!-- Charts JS library: END -->
  
 <script type="text/javascript">
 
function viewEnties(td){
	var tord = td.substring(0,1);
	//alert(tord);

	var r = "r"+td.substring(1);
	var date = document.getElementById(r).innerHTML;
	//alert(date);

	var campid = "<?php echo $_POST['campaignID']; ?>";
	//alert(campid);

	var detailPage = "detail.php";
	
	var sdate = <?php echo '\''.$_POST['sdate'].'\''; ?>;
	var edate = <?php echo '\''.$sqlEndDate.'\''; ?>;

	//var url = detailPage + "?campid=" + campid + "&date=" + date + "&tord=" + tord;
	var url = detailPage + "?campid=" + campid + "&date=" + date + "&tord=" + tord + "&sdate=" + sdate + "&edate=" + edate + "";
	//alert(url);
	 
	window.open(url);   
}
 
 
		var chart;
		var chart_gender;
$(document).ready(function() {

	$('tr').hover(
        function () {
            $(this).addClass('hover');
        },
        function () {
            $(this).removeClass('hover');
        }
    );
    // We don't want the hover effect on our table header, so we unbind the event for our table row containing the th elements
    // $("tr.table-header").unbind('mouseenter mouseleave');

    chart = new Highcharts.Chart({
      chart: {
         renderTo: 'container',
         defaultSeriesType: 'line',
         marginRight: 130,
         marginBottom: 25,
		 zoomType: 'x'
      },
      title: {
         text: '<?=$_POST['campaignName']?>',
         x: -20 //center
      },
      subtitle: {
         text: 'Prepared for: <?=$_POST['companyName']?>',
         x: -20
      },
      xAxis: {
         //categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		 categories: [<?=$x_axis?>],
		
		 
      },
      yAxis: {
         title: {
            text: 'Total Entries'
         },
         plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
         }]
      },
      tooltip: {
         formatter: function() {
                   return '<b>'+ this.series.name +'</b><br/>'+
               this.x +': '+ this.y +' entries';
         }
      },
      legend: {
         layout: 'vertical',
         align: 'right',
         verticalAlign: 'top',
         x: -10,
         y: 100,
         borderWidth: 0
      },
      series: [
      {
         name: 'Total Entries',
         data: [<?=$y_axis?>]
       },
      {
	name: 'Distinct Entries',
        data: [<?=$y2_axis?>]
      }]
   });
  
   var genderPl = document.getElementById('container2');
   if (genderPl != null){
		chart_gender = new Highcharts.Chart({
      				chart: {
         				renderTo: 'container2',
         				plotBackgroundColor: null,
         				plotBorderWidth: null,
         				plotShadow: false
     				 },
      				title: {
         				text: 'Gender breakdown'
      				},
      				tooltip: {
         				formatter: function() {
           				 	return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
							//return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
        				 }
      				},
      				plotOptions: {
         				pie: {
           				 allowPointSelect: true,
           				 cursor: 'pointer',
           				 dataLabels: {
              				enabled: true,
              				color: Highcharts.theme.textColor || '#000000',
               				connectorColor: Highcharts.theme.textColor || '#000000',
               				formatter: function() {
                  				// return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
								return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
               				}
            			}
         			  }
      				},
       				series: [{
         				type: 'pie',
         				name: 'Gender breakdown',
         				data: [
            				<?php echo $genderstr_data;?>
         				]
      				}]
   				});
	}		
	
			var emailPl = document.getElementById('container3');
			if (emailPl != null){
				 chart_email = new Highcharts.Chart({
      				chart: {
         				renderTo: 'container3',
         				plotBackgroundColor: null,
         				plotBorderWidth: null,
         				plotShadow: false
     				 },
      				title: {
         				text: 'Email optin breakdown'
      				},
      				tooltip: {
         				formatter: function() {
           				 	return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
							//return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
        				 }
      				},
      				plotOptions: {
         				pie: {
           				 allowPointSelect: true,
           				 cursor: 'pointer',
           				 dataLabels: {
              				enabled: true,
              				color: Highcharts.theme.textColor || '#000000',
               				connectorColor: Highcharts.theme.textColor || '#000000',
               				formatter: function() {
                  				//return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
								return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
               				}
            			}
         			  }
      				},
       				series: [{
         				type: 'pie',
         				name: 'Email optin breakdown',
         				data: [
            				<?php echo $receivestr_data;?>
         				]
      				}]
   				});
			}
		
		
		var agePl = document.getElementById('container4');
		if (agePl != null){
				  chart_agerange = new Highcharts.Chart({
      				chart: {
         				renderTo: 'container4',
         				plotBackgroundColor: null,
         				plotBorderWidth: null,
         				plotShadow: false
     				 },
      				title: {
         				text: 'Age breakdown'
      				},
      				tooltip: {
         				formatter: function() {
           				 	return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
							//return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
        				 }
      				},
      				plotOptions: {
         				pie: {
           				 allowPointSelect: true,
           				 cursor: 'pointer',
           				 dataLabels: {
              				enabled: true,
              				color: Highcharts.theme.textColor || '#000000',
               				connectorColor: Highcharts.theme.textColor || '#000000',
               				formatter: function() {
                  				//return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
								return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
               				}
            			}
         			  }
      				},
       				series: [{
         				type: 'pie',
         				name: 'Age breakdown',
         				data: [
            				<?php echo $agerangestr_data;?>
         				]
      				}]
   				});
		}	

			var agePl = document.getElementById('container5');
			if (agePl != null){		
				chart_zipcode = new Highcharts.Chart({
      				chart: {
         				renderTo: 'container5',
         				plotBackgroundColor: null,
         				plotBorderWidth: null,
         				plotShadow: false
     				 },
      				title: {
         				text: 'zipcode breakdown'
      				},
      				tooltip: {
         				formatter: function() {
           				 	return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
							//return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
        				 }
      				},
      				plotOptions: {
         				pie: {
           				 allowPointSelect: true,
           				 cursor: 'pointer',
           				 dataLabels: {
              				enabled: true,
              				color: Highcharts.theme.textColor || '#000000',
               				connectorColor: Highcharts.theme.textColor || '#000000',
               				formatter: function() {
                  				//return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
								return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
               				}
            			}
         			  }
      				},
       				series: [{
         				type: 'pie',
         				name: 'zipcode breakdown',
         				data: [
            				<?php echo $zipcodestr_data;?> 
         				]
      				}]
   				});
			}
  			
			var carrierPl = document.getElementById('container6');
			if (carrierPl != null){		
				chart_carrier = new Highcharts.Chart({
      				chart: {
         				renderTo: 'container6',
         				plotBackgroundColor: null,
         				plotBorderWidth: null,
         				plotShadow: false
     				 },
      				title: {
         				text: 'carrier breakdown'
      				},
      				tooltip: {
         				formatter: function() {
           				 	return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
							//return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
        				 }
      				},
      				plotOptions: {
         				pie: {
           				 allowPointSelect: true,
           				 cursor: 'pointer',
           				 dataLabels: {
              				enabled: true,
              				color: Highcharts.theme.textColor || '#000000',
               				connectorColor: Highcharts.theme.textColor || '#000000',
               				formatter: function() {
                  				//return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
								return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
               				}
            			}
         			  }
      				},
       				series: [{
         				type: 'pie',
         				name: 'carrier breakdown',
         				data: [
            				<?php echo $carrierstr_data;?>
         				]
      				}]
   				});
			}


 //alert("4");  

			// Dynamic charts places.
			var containers = <?php echo $countDyn;?>;
			//alert("containers = " + containers);
			
			// simon way.
			var label_array = new Array();
			var str_array = new Array();
			<?php			
			for($s = 0; $s < $countDyn; $s++){
				echo "label_array[".$s."] = '".$labels[$s]."';\n";
				//echo "str_array[".$s."] = \'".$dynstr_data[$s]."\';\n"; // wrong.
				
				// str_array[0] = "['0', 1699],['1', 1735]";
				// echo 'str_array[\''.$s.'\'] = "'.$dynstr_data[$s].'"; '; // cannot use this syntax, because the chart won't accept.
				echo 'str_array[\''.$s.'\'] = '.$dynstr_data[$s].'; ';
			}
			?>


			for(var i=0; i<containers; i++){ // 0~7, 1~8...
				var containerName = "container"+(i+7);
				//alert("containerName = " + containerName);
				
				//var label = '<?=$labels[0]?>';
				var label = label_array[i];
				//alert("label = " + label);
				
				//var strData = "['0', 1699],['1', 1735]";//str_array[i];	
				//alert("str = " + strData);
				
				var dynPl = document.getElementById(containerName);
				if (dynPl != null){	
					//alert("Dyn yes2.");
					
					chart_Dyn = new Highcharts.Chart({
						chart: {
							renderTo: containerName,
							plotBackgroundColor: null,
							plotBorderWidth: null,
							plotShadow: false
						 },
						title: {
							text: label//'DYN breakdown'
						},
						tooltip: {
							formatter: function() {
								return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
								//return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
							 }
						},
						plotOptions: {
							pie: {
							 allowPointSelect: true,
							 cursor: 'pointer',
							 dataLabels: {
								enabled: true,
								color: Highcharts.theme.textColor || '#000000',
								connectorColor: Highcharts.theme.textColor || '#000000',
								formatter: function() {
									//return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
									return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
								}
							}
						  }
						},
						series: [{
							type: 'pie',
							name: label,//'DYN breakdown',
							data: [
								 str_array[i] //['0', 1699],['1', 1735] // <?php echo $dynstr_data[0];?> // CHANGE TO [i]		
							]
						}]
					});
				}
				
			}

//alert("5");   
  
});
</script>
        
 <style>
 
 table#user-list tr.hover {
	color: #fff;
	background-color: #4c95e6;
}
 
		#slidingDiv {
   		 	display: none;
			height:1300px;
			width:600px;
			background-color:#CCC;
			padding:20px;
			margin-top:10px;
			border-bottom:5px solid #3399FF;
			font-family:Arial, Helvetica, sans-serif;
			font-style:normal;
			color:black; 
			float: left;
			position:absolute;
			left:500px;
			top:40px;
			
		}
		
		#slidingInsightsDiv {
   		 	display: none;
			height:1300px;
			width:1000px;
			background-color:#CCC;
			padding:20px;
			margin-top:10px;
			border-bottom:5px solid #3399FF;
			font-family:Arial, Helvetica, sans-serif;
			font-style:normal;
			color:black; 
			float: left;
			position:absolute;
			left:500px;
			top:40px;
			
		}
		
		#slidingSearchDiv {
   		 	display: none;
			height:1300px;
			width:600px;
			background-color:#CCC;
			padding:20px;
			margin-top:10px;
			border-bottom:5px solid #3399FF;
			font-family:Arial, Helvetica, sans-serif;
			font-style:normal;
			color:black; 
			float: left;
			position:absolute;
			left:320px;
			top:40px;
			
		}
		
		#container {
			float: left;
    		width: 33.3%;
			/* Min-height: */
			min-height: 400px;
			height: auto !important; 
			height: 400px;
		}
		
		#container2 {
			float: left;
    		width: 33.3%;
			/* Min-height: */
			min-height: 400px;
			height: auto !important; 
			height: 400px;
		}
		
		#container3 {
			float: left;
    		width: 33.3%;
			/* Min-height: */
			min-height: 400px;
			height: auto !important; 
			height: 400px;
		}
		
		#container4 {
			float: left;
    		width: 33.3%;
			/* Min-height: */
			min-height: 400px;
			height: auto !important; 
			height: 400px;
		}
		
		#container5 {
			float: left;
    		width: 33.3%;
			/* Min-height: */
			min-height: 400px;
			height: auto !important; 
			height: 400px;
		}
		
		#container6 {
			float: left;
    		width: 33.3%;
			/* Min-height: */
			min-height: 400px;
			height: auto !important; 
			height: 400px;
		}
	
<?php
$i = 0;
$dynContainer = 7;
for($i=0; $i < $countDyn; $i++){
	if ($displaydyn_div[$i] == 1){		
		echo "#container".$dynContainer."{
			float: left;
    		width: 33.3%;			
			min-height: 400px;
			height: auto !important; 
			height: 400px;
		}";
		$dynContainer++;
	}
}
?>
	
</style>  

<style type="text/css">
      @import "DTmedia/css/demo_page.css";
      @import "DTmedia/css/demo_table.css";
    </style>
    <script type="text/javascript" language="javascript" src="DTmedia/js/jquery.js"></script>
    <script class="jsbin" src="http://datatables.net/download/build/jquery.dataTables.nightly.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('#example').dataTable();
});
</script>
     

</head>
<body>

<p>Data collected on this campaign:</p>
<?
$sql = "SELECT cpfldcfg_label, cpfldcfg_var_name, cpfldcfg_is_mandatory FROM sp_campaign_config where campaign_sid='".$_POST['campaignID']."' and cpfldcfg_is_valid=1";
$res = mysql_query($sql);
if (mysql_num_rows($res) > 0) {
	while (list($field_label, $field_varname, $field_mandatory) = mysql_fetch_array($res)) {
		if ($field_mandatory == 1) {
			$mandatory_str = " is mandatory";
		} else {
			$mandatory_str = " is not mandatory";
		}
		echo $field_label." (".$mandatory_str.")<br>";
	}
}
?>

<?php
echo "<div id='tableContainer'>";
echo "<h1>".$_POST['campaignName']."</h1>";
echo "<h2>cid: ".$_POST['campaignID']."</h2>";



// run the sql again to get data...
//$res = mysql_query($sql);
//if (mysql_num_rows($res) > 0) {
if($index > 0){
//echo "<h4>total days: ".mysql_num_rows($res)."</h4>";
echo "<h4>total days: ".$index."</h4>";
echo "<table cellpadding='0' cellspacing='0' border='2' class='display' id='user-list'>"; //id='example'>";
echo "<thead><tr>
      <th>Date</th><th>Total Entries</th><th>Distinct Entries</th>
      </tr>
      </thead>";
echo "<tbody>";

  
  $i = 0;
   //while (list($currdate, $totalentries) = mysql_fetch_array($res)) {
   while($i <= $index){
     //echo "date:".$currdate."- entries: ".$totalentries."<br>";
     
     if($i % 2 != 0){
       echo "<tr class='odd gradeX'>";
     }
     else{
       echo "<tr class='odd gradeA'>";
     }
     //echo "<td class='center'>".$currdate."</td>";
	 echo "<td class='center'> <a id='r".$i."'>".$array_sql[$i][0]."</a></td>";
//echo "<td class='center'> <a id='t".$i."' href="javascript: viewEnties('t1')" >".$array_sql[$i][1]."</a></td>";???
echo '<td class="center"> <a id="t'.$i.'" href="javascript: viewEnties(\'t'.$i.'\')" >'.$array_sql[$i][1].'</a></td>';
     //$totalentries+=10; // dummy data;
//echo "<td class='center'> <a id='d".$i."' href='www.google.com'>".$array_sql[$i][2]."</a></td>";???
echo '<td class="center"> <a id="d'.$i.'" href="javascript: viewEnties(\'d'.$i.'\')" >'.$array_sql[$i][2].'</a></td>';
     echo "</tr>";
	 $i++;
   }

echo "</tbody>";
echo "</table>";
}       

echo "</div>";
?>

<div id="container" style="width: 900px; height: 400px; margin: 0 auto"></div>


<? if ($displaygender_div == 1) { ?>
	<div id="container2" style="width: 900px; height: 400px; margin: 0 auto"></div>
<? } ?>

<? if ($displayemail_div == 1) { ?>
	<div id="container3" style="width: 900px; height: 400px; margin: 0 auto"></div>
<? } ?>

<? if ($displayagerange_div == 1) { ?>
	<div id="container4" style="width: 900px; height: 400px; margin: 0 auto"></div>
<? } ?>

<? if ($displayzipcode_div == 1) { ?>
	<div id="container5" style="width: 900px; height: 400px; margin: 0 auto"></div>
<? } ?>

<? if ($displaycarrier_div == 1) { ?>
	<div id="container6" style="width: 900px; height: 400px; margin: 0 auto"></div>
<? } ?>

<?php
$i = 0;
$dynContainer = 7;
for($i=0; $i < $countDyn; $i++){
//echo "i = ".$i."<br/>";
//echo "dynContainer = ".$dynContainer."<br/>";

	if ($displaydyn_div[$i] == 1){
		echo "<div id='container".$dynContainer."' style='width: 900px; height: 400px; margin: 0 auto'></div>";
		$dynContainer++;
	}
}
?>

</body>
</html>

      
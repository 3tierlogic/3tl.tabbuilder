<?php
require_once("db.php");
$con = mysql_connect($dbhost, $dbuser, $dbpasswd);		// connect to database
if (!$con) {											// error checking and handling
    die('Could not connect: ' . mysql_error());
}
mysql_select_db($dbname);


// add where condition to support date range.
function addCon($original_sql, $s, $e){

	// detect where;
	$pos = strpos($original_sql, 'where', 1); 
	if($pos == false){ // try capital again.
		$pos = strpos($original_sql, 'WHERE', 1);
	}
	
	$sub1 = substr($original_sql,0,$pos+1+4); // "select...where"
	//print "sub1: ".$sub1."<br/>";
	$sub2 = " cpresult_create_time between '".$s."' and '".$e."' and ";
	//print "sub2: ".$sub2."<br/>";
	$sub3 = substr($original_sql, $pos+1+4+1); // " ..."
	//print "sub3: ".$sub3."<br/>";
	
	$original_sql = "";
	$original_sql = $original_sql.$sub1;
	$original_sql = $original_sql.$sub2;
	$original_sql = $original_sql.$sub3;

	return $original_sql;
}

function nichi($raw){
	// 11/16 to 2011-11-16
	$done = str_replace('/',"-",$raw);
	//echo "done = ".$done."<br/>";
	$done = "2011-".$done;
	//echo "done = ".$done."<br/>";

	return $done;
}

function codeToState($currentStateArray, $currentCode) {
	$codeFound = false;
	$counter = 0;
	$totalStates = count($currentStateArray);
	$thisCode = "Not specified";
	
	while (($codeFound == false) && ($counter <= $totalStates)) {
		if ($currentStateArray[$counter][0] == $currentCode) {
			$codeFound = true;
			$thisCode = $currentStateArray[$counter][3];
		} 
		$counter++;
	}
	return $thisCode;
	
}

//***** Constants declaration
$google_maps_key='AIzaSyCX9oTRIVEhJGpDQGgQBNVJ_rg-ljKp6EQ'; //*** Google Map API key

$arrayStates_us[] = array( '001', 'EN','US', 'Alabama');
$arrayStates_us[] = array( '002', 'EN','US', 'Alaska');
$arrayStates_us[] = array( '003', 'EN','US', 'Arizona');
$arrayStates_us[] = array( '004', 'EN','US', 'Arkansas');
$arrayStates_us[] = array( '005', 'EN','US', 'California');
$arrayStates_us[] = array( '006', 'EN','US', 'Colorado');
$arrayStates_us[] = array( '007', 'EN','US', 'Connecticut');
$arrayStates_us[] = array( '008', 'EN','US', 'Delaware');
$arrayStates_us[] = array( '009', 'EN','US', 'District of Columbia');
$arrayStates_us[] = array( '010', 'EN','US', 'Florida');
$arrayStates_us[] = array( '011', 'EN','US', 'Georgia');
$arrayStates_us[] = array( '012', 'EN','US', 'Hawaii');
$arrayStates_us[] = array( '013', 'EN','US', 'Idaho');
$arrayStates_us[] = array( '014', 'EN','US', 'Illinois');
$arrayStates_us[] = array( '015', 'EN','US', 'Indiana');
$arrayStates_us[] = array( '016', 'EN','US', 'Iowa');
$arrayStates_us[] = array( '017', 'EN','US', 'Kansas');
$arrayStates_us[] = array( '018', 'EN','US', 'Kentucky');
$arrayStates_us[] = array( '019', 'EN','US', 'Louisiana');
$arrayStates_us[] = array( '020', 'EN','US', 'Maine');
$arrayStates_us[] = array( '021', 'EN','US', 'Maryland');
$arrayStates_us[] = array( '022', 'EN','US', 'Massachusetts');
$arrayStates_us[] = array( '023', 'EN','US', 'Michigan');
$arrayStates_us[] = array( '024', 'EN','US', 'Minnesota');
$arrayStates_us[] = array( '025', 'EN','US', 'Mississippi');
$arrayStates_us[] = array( '026', 'EN','US', 'Missouri');
$arrayStates_us[] = array( '027', 'EN','US', 'Montana');
$arrayStates_us[] = array( '028', 'EN','US', 'Nebraska');
$arrayStates_us[] = array( '029', 'EN','US', 'Nevada');
$arrayStates_us[] = array( '030', 'EN','US', 'New Hampshire');
$arrayStates_us[] = array( '031', 'EN','US', 'New Jersey');
$arrayStates_us[] = array( '032', 'EN','US', 'New Mexico');
$arrayStates_us[] = array( '033', 'EN','US', 'New York');
$arrayStates_us[] = array( '034', 'EN','US', 'North Carolina');
$arrayStates_us[] = array( '035', 'EN','US', 'North Dakota');
$arrayStates_us[] = array( '036', 'EN','US', 'Ohio');
$arrayStates_us[] = array( '037', 'EN','US', 'Oklahoma');
$arrayStates_us[] = array( '038', 'EN','US', 'Oregon');
$arrayStates_us[] = array( '039', 'EN','US', 'Pennsylvania');
$arrayStates_us[] = array( '040', 'EN','US', 'Rhode Island');
$arrayStates_us[] = array( '041', 'EN','US', 'South Carolina');
$arrayStates_us[] = array( '042', 'EN','US', 'South Dakota');
$arrayStates_us[] = array( '043', 'EN','US', 'Tennessee');
$arrayStates_us[] = array( '044', 'EN','US', 'Texas');
$arrayStates_us[] = array( '045', 'EN','US', 'Utah');
$arrayStates_us[] = array( '046', 'EN','US', 'Vermont');
$arrayStates_us[] = array( '047', 'EN','US', 'Virginia');
$arrayStates_us[] = array( '048', 'EN','US', 'Washington');
$arrayStates_us[] = array( '049', 'EN','US', 'West Virginia');
$arrayStates_us[] = array( '050', 'EN','US', 'Wisconsin');
$arrayStates_us[] = array( '051', 'EN','US', 'Wyoming');

$arrayStates_ca[] = array( '001', 'EN', 'CA', 'Alberta');
$arrayStates_ca[] = array( '002', 'EN', 'CA', 'British Columbia');
$arrayStates_ca[] = array( '003', 'EN', 'CA', 'Manitoba');
$arrayStates_ca[] = array( '004', 'EN', 'CA', 'New Brunswick');
$arrayStates_ca[] = array( '005', 'EN', 'CA', 'Newfoundland and Labrador');
$arrayStates_ca[] = array( '006', 'EN', 'CA', 'Nova Scotia');
$arrayStates_ca[] = array( '007', 'EN', 'CA', 'Northwest Territories');
$arrayStates_ca[] = array( '008', 'EN', 'CA', 'Nunavut');
$arrayStates_ca[] = array( '009', 'EN', 'CA', 'Ontario');
$arrayStates_ca[] = array( '010', 'EN', 'CA', 'Prince Edward Island');
$arrayStates_ca[] = array( '011', 'EN', 'CA', 'Quebec');
$arrayStates_ca[] = array( '012', 'EN', 'CA', 'Saskatchewan');
$arrayStates_ca[] = array( '013', 'EN', 'CA', 'Yukon');



// start of code.
$campaignID = $_REQUEST['cid'];
$sDate = $_REQUEST['sdate'];
$eDate = $_REQUEST['edate'];
$chartType = $_REQUEST['ctype'];
$countryCode = $_REQUEST['c'];

if (strtoupper($countryCode) == "CA") {
	$arrayStates = $arrayStates_ca;
	$countryLabel = "Canada";
} else {
	$arrayStates = $arrayStates_us;
	$countryLabel = "United States";
}

/*echo "<pre>";
print_r($arrayStates);
echo "</pre>";*/

switch ($chartType) {
	case "state":
		$statestr_data="";
       	//echo "Gender chart<br>";					
		$sql_states = "SELECT cpresult_states, count(*) as num FROM sp_campaign_result where campaign_sid='".$campaignID."' group by cpresult_states order by num desc";
		$sql_states = addCon($sql_states, $sDate, $eDate); //*** Add the start and end dates to the SQL
		echo $sql_states;
		$res_states = mysql_query($sql_states, $con); 
		if (mysql_num_rows($res_states) > 0) {
			$statesArray = array();
			$statesTotalArray = array();
			$counter = 0;
			while (list($states, $totalstates) = mysql_fetch_array($res_states)) {
				//echo $states.":".$totalstates."<br>";
				$currstates = codeToState($arrayStates, $states);								
				$statesstr_data = $statesstr_data."['".$currstates."', ".$totalstates."],";
				$statesArray[$counter] = $currstates;
				$statesTotalArray[$counter] = $totalstates;
				
				//*** Generate the map data to use with Google Map - START
				$url = "http://maps.google.com/maps/geo?q=".$currstates.", ".$countryLabel."&output=xml&key=".$google_maps_key;
				//echo $url;
				$xml = simplexml_load_file($url);
				$status = $xml->Response->Status->code;
				if ($status='200') { //address geocoded correct, show results
					$placemarkcounter = 0;
					foreach ($xml->Response->Placemark as $node) { // loop through the responses
						if ($placemarkcounter == 0) {
							$address = $node->address;
							$quality = $node->AddressDetails['Accuracy'];
							$coordinates = $node->Point->coordinates;
							$coordinatesArray = explode(",", $coordinates );
							$mapData = $mapData."['".$address."',".$coordinatesArray[1].",".$coordinatesArray[0].",".$totalstates."],";
							$placemarkcounter++;
						} else {
							break;// quit. we don't need more than one placemark.
						}
					}
					if ($counter == 0) {
						$firstZipCoord1 = $coordinatesArray[1];	
						$firstZipCoord2 = $coordinatesArray[0];	
					}
				} else {
					$address = "Cannot find Province/State";
				} //*** Generate the map data to use with Google Map - END
					
					
				$counter++;
			}
		}						
		$statesstr_data = substr($statesstr_data, 0, -1);
		//echo $mapData;
	break;
}



?>

<html>

<head>
<link href="style.css" rel="stylesheet" type="text/css" />
<style>
	div.panel{
		height:auto;
		display:none;
	}

	table#user-list tr.hover {
		color: #fff;
		background-color: #4c95e6;
	}	
	#containergraph {
		float: left
    	width: 33.3%;
		/* Min-height: */
		min-height: 400px;
		height: auto !important; 
		height: 400px;
	}
</style>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>
 <!-- Charts JS library: START -->
<script type="text/javascript" src="js/highcharts.js"></script>
<script type="text/javascript" src="js/themes/gray.js"></script>
<script type="text/javascript" src="js/modules/exporting.js"></script>
<!-- Charts JS library: END -->
 
 <style type="text/css">
      @import "DTmedia/css/demo_page.css";
      @import "DTmedia/css/demo_table.css";
</style>
<script class="jsbin" src="http://datatables.net/download/build/jquery.dataTables.js"></script>

<!-- ***** Google Maps:Start ***** -->
<!--<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCX9oTRIVEhJGpDQGgQBNVJ_rg-ljKp6EQ&sensor=false"></script>-->
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>

<script type="text/javascript"> 
function initialize() {
	// create the map
    var locations = [
      //['Bondi Beach', -33.890542, 151.274856, 4],
//      ['Coogee Beach', -33.923036, 151.259052, 5],
//      ['Cronulla Beach', -34.028249, 151.157507, 3],
//      ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
//      ['Maroubra Beach', -33.950198, 151.259302, 1]
		<?=$mapData?>
	];

    var map = new google.maps.Map(document.getElementById('map_canvas'), {
      zoom: 3,
      center: new google.maps.LatLng(<?=$firstZipCoord1?>,<?=$firstZipCoord2?>),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();
	var marker, i;

    for (i = 0; i < locations.length; i++) {  
			marker = new google.maps.Marker({
        	position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        	map: map
    	 });
	  
     	 google.maps.event.addListener(marker, 'click', (function(marker, i) {
        	return function() {
          		infowindow.setContent("<b>"+locations[i][0]+"</b> ("+locations[i][3]+")");
          		infowindow.open(map, marker);
        	}
      	})(marker, i));
    }
}

</script> 

    
<!-- ***** Google Maps:End ***** -->
    
<script>
var chart;
var field="<?=$chartType?>";
$(document).ready(function() {
	$('#example').dataTable({
        "bJQueryUI": true,
        <!--"sPaginationType": "full_numbers",-->
		"bPaginate": false,
		"bFilter": false,
		"bLengthChange": false,
		"bScrollInfinite": true,
        "bScrollCollapse": true,
        "sScrollY": "<?php echo $tableHeight ?>px"
    });
	
	
	switch(field){
		case "States":{	
  			chart = new Highcharts.Chart({
      				chart: {
         				renderTo: 'container',
         				plotBackgroundColor: null,
         				plotBorderWidth: null,
         				plotShadow: false
     				 },
      				title: {
         				text: 'Province/State breakdown'
      				},
      				tooltip: {
         				formatter: function() {
           				 	return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
							//return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
        				 }
      				},
      				plotOptions: {
         				pie: {
           				 allowPointSelect: true,
           				 cursor: 'pointer',
           				 dataLabels: {
              				enabled: true,
              				color: Highcharts.theme.textColor || '#000000',
               				connectorColor: Highcharts.theme.textColor || '#000000',
               				formatter: function() {
                  				//return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
								return '<b>'+ this.point.name +'</b>: '+ Highcharts.numberFormat(this.percentage, 2) +' %';
								
               				}
            			}
         			  }
      				},
       				series: [{
         				type: 'pie',
         				name: 'Province/State breakdown',
         				data: [
            				<?php echo $statesstr_data;?> 
         				]
      				}]
   			});
			break;
		}
		
		default: {
			chart = new Highcharts.Chart({
      				chart: {
        				renderTo: 'container',
         				plotBackgroundColor: null,
         				plotBorderWidth: null,
         				plotShadow: false
      				},
      				title: {
         				text: 'Browser market shares at a specific website, 2010'
      				},
      				tooltip: {
         				formatter: function() {
            				return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
         				}
      				},
      				plotOptions: {
         				pie: {
            				allowPointSelect: true,
            				cursor: 'pointer',
           					dataLabels: {
               					enabled: true,
               					color: Highcharts.theme.textColor || '#000000',
               					connectorColor: Highcharts.theme.textColor || '#000000',
               					formatter: function() {
                  					return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
               					}
            				}
         				}
      				},
       				series: [{
         				type: 'pie',
         				name: 'Browser share',
         				data: [
            				['Firefox',   45.0],
            				['IE',       26.8],
            					{
               						name: 'Chrome',    
               						y: 12.8,
               						sliced: true,
               						selected: true
            					},
            				['Safari',    8.5],
            				['Opera',     6.2],
            				['Others',   0.7]
         				]
      				}]
  			 });	
		}
	} //*** Switch
}); //**** function
   


</script>



</head>
<body onLoad="initialize()">
<div id="container" style="width: 900px; height: 400px; margin: auto;"></div>
<div id="map_canvas" style="width: 900px; height: 400px; margin: auto; border-width: 2px; border-style: ridge; border-color: #666"></div>
<div id="tableContainer" style="width: 900px; height: 400px; margin: auto;" >
<br><br>
<?php

											
							
							echo "<h2>Campaign summary from ".$sDate." to ".$eDate."</h2>";
							echo "<table cellpadding='0' cellspacing='0' border='2' class='display' id='example'>"; //id='user-list'>"; 
							echo "<thead><tr><th>Item</th><th>State/Province</th><th>Total Entries</th></tr></thead>";
							echo "<tbody>";
							
							$counter = 0;
							$t = 0;
						   	foreach ($statesArray as $stateslabel) {
																	if($counter % 2 != 0){
							  			echo "<tr class='odd gradeX'>";
								 	}else{
							   			echo "<tr class='odd gradeA'>";
							 		 }
								 	echo "<td class='center'>".($counter+1)."</td>";
									echo "<td class='center'>".$stateslabel."</td>";
								 	echo "<td class='center'>".$statesTotalArray[$counter]."</a></td>";
								 	echo "</tr>";
									$t = $t + $statesTotalArray[$counter];
								
  								$counter++;
							}
							echo "<tr class='odd gradeA'>";
							echo "<td class='center'>".($counter+1)."</td>";
							echo "<td class='center'><b>Total entries</b></td>";
							echo "<td class='center'><b>".$t."</b></td>";
							echo "</tr>";
							echo "</tbody>";
							echo "</table>";
					?>
</div>
</body>

</html>
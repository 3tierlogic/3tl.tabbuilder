<?php
/*$companyID = $_REQUEST['coID'];
$activityID = $_REQUEST['aID']; // get
$localdb =  $_REQUEST['ldb'];
$campaignID = $activityID;*/

require_once("timlib.php");
require_once("badwords.php");

// Connect to database
require_once("db.php");
$con = mysql_connect($dbhost, $dbuser, $dbpasswd);		// connect to database
if (!$con) {											// error checking and handling
    die('Could not connect: ' . mysql_error());
}
mysql_select_db($dbname);

$con_3tl = mysql_connect($dbhost_3tl, $dbuser_3tl, $dbpasswd_3tl);		// connect to database
if (!$con_3tl) {											// error checking and handling
    die('Could not connect to 3TL: ' . mysql_error());
}
mysql_select_db($dbname_3tl);



session_start();
if ( ! isset ( $_SESSION['companyid'] ) ) {
	header ( 'Location: http://platform.3tierlogic.com/tabbuilder/index1_3.php' );
	exit();
} else {
	if (isset($_REQUEST['customerID'])) {    ///**** This value is set when logged in as a superuser and selecting a company from the drop down to view
		$companyID = $_REQUEST['customerID'];
	} else {
		$companyID = $_SESSION['companyid'];
	}
}
$campaignID = $activityID = $_REQUEST['cID']; 

// run a sql to get camp info for later html use. 
$sql_companyInfo = "SELECT cp_companyName FROM sp_company_info WHERE cp_companyID='".$companyID."'";
$res_companyInfo = mysql_query($sql_companyInfo, $con);
if (mysql_num_rows($res_companyInfo) == 1) {
	list($companyName) = mysql_fetch_array($res_companyInfo);	
}

$sql_campInfo = "SELECT campaign_sid as cid, campaign_name as cname, campaign_description as cdesc, campaign_start_time as sdate, campaign_end_time as edate, campaign_is_check_reentry as creentry FROM sp_campaign_info s where campaign_sid ='".$campaignID."'";
$res0 = mysql_query($sql_campInfo, $con);
if (mysql_num_rows($res0) == 1) {
	list($camp_id, $camp_name, $camp_desc, $camp_sdate, $camp_edate, $camp_reentry) = mysql_fetch_array($res0);	
	$campaignName = $camp_name;
	$campaignDesc = $camp_desc;
	if (strlen($campaignDesc) <= 0) {
		$campaignDesc ="*** No description available ***";
	} 
	$campaignSdate = $camp_sdate;
	$campaignEdate = $camp_edate;
	//$campaignReentry = $camp_reentry;
	if ($camp_reentry == 1) {
		$campaignReentryPic = "images/singleentry.png";
	} else {
		$campaignReentryPic = "images/multipleentries.png";
	}	
}
else{
	echo "Err: could not pull out campaign info for cid: ".$campaignID."<br/>";
}

//*** Get the current URL of this script to add to the links
$domain = $_SERVER['HTTP_HOST'];  
$path = $_SERVER['SCRIPT_NAME'];  
$queryString = $_SERVER['QUERY_STRING'];
$currenturl = "http://" . $domain . $path;

//*****
/*$companyID = $_REQUEST["coID"];
$activityID = $_REQUEST["aID"];*/


if (!isset($_GET['d'])) {
	if (isset($_GET['sdate'])) {
		$startDate = $_GET['sdate'];
	} else {
		$startDate = "";
	}
	if (isset($_GET['edate'])) {
		$endDate = $_GET['edate'];
	} else {
		$endDate = "";
	}
} else {
	$endDate = date("Y-m-d"); 
	$startDate = date("Y-m-d", mktime(0, 0, 0, date("m")  , date("d") - $_GET['d'], date("Y")));
}

if (isset($_GET['s'])) {
	$state = $_GET['s'];
} else {
	$state = "0";  //**** By default - New entries
}


$imageBaseURL = "http://platform.3tierlogic.com/tabbuilder1_3/editor/uploads/";
$imageBaseURL_thumb =  "http://platform.3tierlogic.com/tabbuilder1_3/editor/uploads/";
/*$menuPath =   "http://www2.3tierlogic.com/stlouisrams/brandyourpassion/submission/admin/tabmenu.php?s=$state&u=$currenturl";*/

if($startDate == "" and $endDate=="") {
	$sql = "SELECT ugc_sid, ugc_photo_name, ugc_upload_time, ugc_user_id, ugc_video_id, ugc_type, ugc_title, ugc_description, ugc_admin_comments FROM sp_campaign_ugc WHERE ugc_approve_state = '".$state."' AND ugc_campaign_sid='".$activityID."'";
	$newentry_link = $_SERVER['PHP_SELF']."?s=0&aID=".$activityID;
	$approvedentry_link = $_SERVER['PHP_SELF']."?s=1&cID=".$activityID;
	$rejectedentry_link = $_SERVER['PHP_SELF']."?s=2&cID=".$activityID;
	$reviewlaterentry_link = $_SERVER['PHP_SELF']."?s=3&cID=".$activityID;
} else {
	$newentry_link = $_SERVER['PHP_SELF']."?s=0&sdate=".$startDate."&edate=".$endDate."&cID=".$activityID;
	$approvedentry_link = $_SERVER['PHP_SELF']."?s=1&sdate=".$startDate."&edate=".$endDate."&cID=".$activityID."&coID=".$companyID;
	$rejectedentry_link = $_SERVER['PHP_SELF']."?s=2&sdate=".$startDate."&edate=".$endDate."&cID=".$activityID."&coID=".$companyID;
	$reviewlaterentry_link = $_SERVER['PHP_SELF']."?s=3&sdate=".$startDate."&edate=".$endDate."&cID=".$activityID."&coID=".$companyID;
	//*** Format date with time for accuracy
	$startDate=$startDate." 00:00:00";
	$endDate=$endDate." 23:59:59:";
	$sql = "SELECT ugc_sid, ugc_photo_name, ugc_upload_time, ugc_user_id, ugc_video_id, ugc_type, ugc_title, ugc_description, ugc_admin_comments FROM sp_campaign_ugc WHERE ugc_approve_state = '".$state."' and ugc_upload_time >= '".$startDate."' AND ugc_upload_time <= '".$endDate."' AND ugc_campaign_sid='".$activityID."'";
}


$res = mysql_query($sql, $con);

//*** Check if we have pictures to display
if (mysql_num_rows($res) > 0)  {
	$emptyAlbum=1;
} else {
	$emptyAlbum=0;
}

switch ($state) {
	case 0:
		$sectionName = "New Entries";
		break;
	case 1:
		$sectionName = "Approved Entries";
		break;
	case 2:
	 	$sectionName = "Rejected Entries";
		break;
	case 3: 
		$sectionName = "Entries for Review";
		break;
	case 5: 
		$sectionName = "Quarantined Entries";
		break;
	
}

function isBadWord($text, $badwords) {
 
 //First we list the bad words in array
/* $badwords = array(
 'fuck',
 'suck',
 'com',
 'net',
 'org',
 'info'
 );*/
 
 //Then we perform the bad word check
 foreach($badwords as $badwords) {
    if(stristr($text,$badwords)) {
    	return true;
    }
 }
 return false;
}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr"><head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>3 Tier Logic - Tracker V1.0</title>
 <link href="style.css" rel="stylesheet" type="text/css" />
 <!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if IE 6]>
<script src="js/belatedPNG.js"></script>
<script>
	DD_belatedPNG.fix('*');
</script>
<![endif]-->
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>-->
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js"></script>-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>
 <!-- Charts JS library: START -->
<script type="text/javascript" src="js/highcharts.js"></script>
<script type="text/javascript" src="js/themes/gray.js"></script>
<script type="text/javascript" src="js/modules/exporting.js"></script>
<!-- Charts JS library: END -->

<!-- date Picker start -->
<!--<style type="text/css">@import "css/redmond.datepick.css";</style>
<script src="js/CalendarControl/CalendarControl.js" language="javascript"></script>
<script type="text/javascript" src="js/jquery.datepick.js"></script>-->
<!-- date picker end-->

<!-- documentation found at http://bxslider.com/options -->
<!--<script src="http://bxslider.com/sites/default/files/jquery.bxSlider.min.js" type="text/javascript"></script>-->
<script src="js/jquery.bxSlider.min.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#slider1').bxSlider({
    		auto: true,
    		pager: false,
			controls: false,
			pause: 5000
  		});
  });
  

</script>


<script type="text/javascript">
var starttime = new Date();
var timelastactivity = new Date();
var refreshTimerId = null;
var refreshTimerRunning = false;
var logoutTimerId = null;
var logoutTimerRunning = false;


$(function() {
	$('#popupDatepicker_start').datepicker({dateFormat: 'yy-mm-dd'});
	$('#popupDatepicker_end').datepicker({dateFormat: 'yy-mm-dd'});
});




function showDate(state) {
	//alert(state);

	var sdate = "";
	var edate = "";
	
	switch(state) {
		case 0:
			sdate = document.getElementById('popupDatepicker_start').value;
			edate = document.getElementById('popupDatepicker_end').value;
			break;
		case 1:	
			sdate = '2000-01-01';
			edate = '2100-12-31';
			break;
		case 2:
			sdate = '<?php echo $recentStartDate ?>';
			edate = '<?php echo $recentEndDate ?>';
			break;
		default:
			sdate = '2000-01-01';
			edate = '2100-12-31';
			
	}
	
	//alert('The date chosen is ' + sdate + " " + edate);


	// get every necessary param.
	var coid = "<?php echo $companyID; ?>";
	var cid = "<?php echo $activityID; ?>";
	//var localdb = "<?php echo $localdb; ?>";
	
	// build url.
	var url = '';
	url += 'ugcentries.php';
	//var params = '?aID='+cid+'&sdate='+sdate+'&edate='+edate+'&coID='+coid+'&ldb='+localdb;
	var params = '?aID='+cid+'&sdate='+sdate+'&edate='+edate+'&coID='+coid;
	url += params; 
	//alert(url);
	// go to self with new params.
	window.location.href = url; 

}

//**** Update entry function ::: START
function updateentry(cid) {	
	var selectedoption = "0";
	var notifyuser = "0";
	var notifylanguage = document.getElementById('lang_'+cid).value;
	var selectedoption = document.querySelector('.status'+cid+':checked').value;
	 
	//alert(cid+":lan "+document.getElementById('lang_'+cid).value);
	//var radiooptions = document.getElementsByName('status'+cid);
//	for(var rad in radiooptions) {
//    	if (radiooptions[rad].checked) {
//      		var selectedoption =  radiooptions[rad].value;
//  		}
//	}

	if (document.getElementById('notify'+cid).checked) {
		notifyuser = "1";
	} 
	
	var data = "c="+document.getElementById('admincomment'+cid).value;
	data = data + "&s="+selectedoption;
	data = data + "&id=" + cid;
	data = data + "&coid=" + <?=$companyID?>;
	data = data + "&aid=" + <?=$activityID?>;
	data = data + "&n=" + notifyuser;
	data = data + "&cid=" + document.getElementById('cid_'+cid).value;
	//data = data + "&ldb=" + <?=$localdb?>;
	data = data + "&l=" + notifylanguage;
	//alert(data);
	
	
	//start the ajax
	$.ajax({
		//this is the php file that processes the data and send mail
		url: "updateentry.php",	
		//GET method is used
		type: "POST",
		//pass the data			
		data: data,		
		//Do not cache the page
		cache: false,
		//success
		success: function (returnval) {				
				alert("Entry updated");
				//alert(returnval);
		}		
	});
	//cancel the submit button default behaviours
	return false;
}
//**** Update entry function ::: END

//*** Timer function
function dateDiff(date1, date2) {
    return date1.getTime() - date2.getTime();
}

function checkSessionExpiry() {
	var timenow=new Date();
	var timebetween = dateDiff(timenow, timelastactivity)/60000;
	var data = "";
	data = data+"&cid="+"<?=$companyID?>";
	data = data+"&sid="+"<?=session_id()?>";
	data = data+"&uid="+"<?=$_SESSION['userID']?>";
	data = data+"&action=timeout"; 
	
	//Logout after 15 minutes
	if (timebetween > 15) {
		//start the ajax
		$.ajax({
			//this is the php file that processes the data and send mail
			url: "updatetimer.php",	
			//POST method is used
			type: "POST",
			//pass the data			
			data: data,		
			//Do not cache the page
			cache: false,
			//success
			success: function (returnval) {				
				//alert(returnval);
			}		
		});
		alert("Your session has timed out, please login again to continue.");
		window.location.href = 'loginreview.php';
	}
}

function updateTime() {
	var timenow=new Date();
	var data = "d="+timenow;
	data = data+"&cid="+"<?=$companyID?>";
	data = data+"&sid="+"<?=session_id()?>";
	data = data+"&uid="+"<?=$_SESSION['userID']?>";
	data = data+"&action=update";
	
	
	//start the ajax
	$.ajax({
		//this is the php file that processes the data and send mail
		url: "updatetimer.php",	
		//POST method is used
		type: "POST",
		//pass the data			
		data: data,		
		//Do not cache the page
		cache: false,
		//success
		success: function (returnval) {				
				//alert(returnval);
		}		
	});
}

function startTimer() {
	//alert("Timer starts now..."+starttime);
	// Check every 10 minutes (600,000 milliseconds)
	if (refreshTimerRunning) {
		clearInterval(refreshTimerId);	
	}
	refreshTimerRunning = true;
	refreshTimerId = setInterval(function() {
		updateTime();
   	}, 60000);
	// Check every 15 minutes (900,000 milliseconds) to see whether session has expired
	if (logoutTimerRunning) {
		clearInterval(logoutTimerId);
	}
	logoutTimerRunning = true;
	var logoutTimerId = setInterval(function() {
		checkSessionExpiry();
   	}, 120000);
}

function reset_interval() {
//resets the timer. The timer is reset on each of the below events:<br />
// 1. mousemove   2. mouseclick   3. key press 4. scroliing<br />
//first step: clear the existing timer<br />
	timelastactivity = new Date();
}

function Toggle(id) {
	if (document.getElementById(id).style.display == "none" || document.getElementById(id).style.display == "") {
		document.getElementById(id).style.display = "block";
	} else if (document.getElementById(id).style.display == "block") {
		document.getElementById(id).style.display = "none";
	} else {
		document.getElementById(id).style.display = "none";
	}
}
</script>



<script type="text/javascript">
	$(document).ready(function(){
		
		
		<!-- ***** SCROLL TOP SCRIPT ::: START -->
		$(function () {
			var scroll_timer;
			var displayed = false;
			var $message = $('#message a');
			var $window = $(window);
			var top = $(document.body).children(0).position().top;
			$window.scroll(function () {
				window.clearTimeout(scroll_timer);
				scroll_timer = window.setTimeout(function () {
					if($window.scrollTop() <= top)
					{
						displayed = false;
						$message.fadeOut(500);
					}
					else if(displayed == false)
					{
						displayed = true;
						$message.stop(true, true).show().click(function () { $message.fadeOut(500); });
					}
				}, 100);
			});
		});
		<!-- ***** SCROLL TOP SCRIPT ::: END -->
		
		<!-- ***** FANCYBOX SCRIPT ::: START -->
		
			/*
				Simple image gallery. Uses default settings
			*/
			$('.fancybox').fancybox();

			/*
				Different effects
			*/
			// Change title type, overlay opening speed and opacity
			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedIn : 500,
						opacity : 0.95
					}
				}
			});

			// Disable opening and closing animations, change title type
			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});

			// Set custom style, close if clicked, change title type and overlay color
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background-color' : '#eee'	
						}
					}
				}
			});

			// Remove padding, set opening and closing animations, close if clicked and disable overlay
			$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});

			/*
				Button helper. Disable animations, hide close button, change title type and content
			*/

			$('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});


			/*
				Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
			*/
			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,
				arrows    : false,
				nextClick : true,

				helpers : { 
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});


			/*
				Open manually
			*/
			$("#fancybox-manual-a").click(function() {
				$.fancybox.open('1_b.jpg');
			});

			$("#fancybox-manual-b").click(function() {
				$.fancybox.open({
					href : 'iframe.html',
					type : 'iframe',
					padding : 5
				});
			});

			$("#fancybox-manual-c").click(function() {
				$.fancybox.open([
					{
						href : '1_b.jpg',
						title : 'My title'
					}, {
						href : '2_b.jpg',
						title : '2nd title'
					}, {
						href : '3_b.jpg'
					}
				], {
					helpers : {
						thumbs : {
							width: 75,
							height: 50	
						}
					}	
				});
			});
			<!-- ***** FANCYBOX SCRIPT ::: START -->
			
			
			
	});
</script>

<!-- ***** GALLERIFIC STUFF ::: START -->
<!--<link rel="stylesheet" href="css/basic.css" type="text/css" />-->
<link rel="stylesheet" href="css/galleriffic-5.css" type="text/css" />
<!--<link rel="stylesheet" href="css/buttons.css" type="text/css" />-->
<!-- <link rel="stylesheet" href="css/white.css" type="text/css" /> -->
<!--<link rel="stylesheet" href="css/black.css" type="text/css" />-->
<!--/*<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>*/-->
<!--<script type="text/javascript" src="js/jquery-1.3.2.js"></script>-->
<script type="text/javascript" src="js/jquery.history.js"></script>
<script type="text/javascript" src="js/jquery.galleriffic.js"></script>
<script type="text/javascript" src="js/jquery.opacityrollover.js"></script>
<!-- We only want the thunbnails to display when javascript is disabled -->
<script type="text/javascript">
	document.write('<style>.noscript { display: none; }</style>');
</script>
<!-- ***** GALLERIFIC STUFF ::: END -->

<!-- ***** FACNY BOX STUFF ::: START -->
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="source/jquery.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="source/jquery.fancybox.css" media="screen" />

<!-- Add Button helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-buttons.css?v=2.0.4" />
<script type="text/javascript" src="source/helpers/jquery.fancybox-buttons.js?v=2.0.4"></script>

<!-- Add Thumbnail helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-thumbs.css?v=2.0.4" />
<script type="text/javascript" src="source/helpers/jquery.fancybox-thumbs.js?v=2.0.4"></script>

<style type="text/css">
	.fancybox-custom .fancybox-outer {
		box-shadow: 0 0 50px #222;
	}
</style>
<!-- ***** FACNY BOX STUFF ::: END -->




<style>
#emailtext { width:600px; padding:1px; display:none; }

div.panel{
	height:auto;
	display:none;
}

table#user-list tr.hover {
	color: #fff;
	background-color: #4c95e6;
}	

#containergraph {
	float: left
    width: 33.3%;
	/* Min-height: */
	min-height: 400px;
	height: auto !important; 
	height: 400px;
}

#message a {
	/* display: block before hiding */
	display: block;
	display: none;
 
	/* link is above all other elements */
	z-index: 999; 
 
	/* link doesn't hide text behind it */
	opacity: .8;
 
	/* link stays at same place on page */
	position: fixed;
 
	/* link goes at the bottom of the page */
	top: 100%;
	margin-top: -80px; /* = height + preferred bottom margin */
 
	/* link is centered */
	left: 50%;
	margin-left: -160px; /* = half of width */
 
	/* round the corners (to your preference) */
	-moz-border-radius: 24px;
	-webkit-border-radius: 24px;
 
	/* make it big and easy to see (size, style to preferences) */
	width: 300px;
	line-height: 48px;
	height: 48px;
	padding: 10px;
	background-color: #000;
	font-size: 24px;
	text-align: center;
	color: #fff;
}

#flashContent { width:100%; height:100%; }
</style>

</head>


<body class="normalpage" id="top">
<!--<body class="normalpage" id="top" onload="startTimer()" onmousemove="reset_interval()" onclick="reset_interval()" onkeypress="reset_interval()" onscroll="reset_interval()">-->
<!--<body class="normalpage" id="top" onmousemove="reset_interval()" onclick="reset_interval()" onkeypress="reset_interval()" onscroll="reset_interval()">-->
	<section id="page">
		<div id="bodywrap">
			<section id="top">
				<nav>
					<h1 id="sitename">
  					<a href="#">3 Tier Logic Inc.</a></h1>
  					<ul id="sitenav">
  						<li class="current"><a href="#">UCG Entries Summary</a></li>
  						<li><a href="main.php">Report Dashboard</a></li>
  						<li><a href="../editor/dashboard.php">Back to Tab Editor</a></li>
  					</ul>
  				</nav>
				<header id="normalheader"></header>
			</section>
            
			<section id="contentwrap">
				<div id="contents">
					<div id="topcolumns">
                    	<!-- ***** Column 1 ::: START -->
						<div class="col">
							<h2>Campaign Info</h2>
                            <p>
							<b>Company Name: </b> <?=$companyName?><br> 
							<b>Campaign Name: </b> <?=$camp_name?><br>   
							<b>Start date: </b> <?=$campaignSdate?><br> 
							<b>End date: </b><?=$campaignEdate?><br> 
							<b>Entry requirement: </b><img src="<?=$campaignReentryPic?>" /><br> 
							<b>Rules and regulations: </b><a href="<?=$campaignRulesLink?>" target="_blank">Click here</a><br>
                            <p class="flip">
                            	<b>Description: (Click to view/hide)</b></p><div class="panel"><?=$campaignDesc?></div><br>
							</p>
						</div>
                        <!-- ***** Column 2 ::: START -->
						<div class="col">							
							<h2>Filter by date</h2>
							<table cellpadding="0" cellspacing="0">
                            	<tr>
                      				<td><b>Start Date:</b>&nbsp;</td> 
                                    <td><input name='startdate'  type='text' id='popupDatepicker_start'></input></td>
								</tr>
                                <tr>
									<td><b>End Date:</b>&nbsp;</td>
                                    <td><input name='enddate' type='text' id='popupDatepicker_end'></input></td>
								</tr>
                                <tr>
                                	<td colspan="2" align="right"><br /><input type='button' value='submit' onclick='showDate(0)'></input></td>
                                </tr>
							</table>
						</div>
                        <!-- ***** Column 3 ::: START -->
						<div class="col">							
							<h2>Quick Links</h2>
							<p>
                            <ul>
                            	<li><img src="images/rightarrow.png" border="0" width="10" height="10" />&nbsp;&nbsp;<a href="<?=$newentry_link?>" target="_top">View New Entries</a></li><br/>
								<li><img src="images/rightarrow.png" border="0" width="10" height="10" />&nbsp;&nbsp;<a href="<?=$approvedentry_link?>" target="_top">View Approved Entries</a></li><br/>
                                <li><img src="images/rightarrow.png" border="0" width="10" height="10" />&nbsp;&nbsp;<a href="<?=$rejectedentry_link?>" target="_top">View Rejected Entries</a></li><br/>
                                <li><img src="images/rightarrow.png" border="0" width="10" height="10" />&nbsp;&nbsp;<a href="<?=$reviewlaterentry_link?>" target="_top">View Entries for Review</a></li><br/>
                           	</ul>
							</p>
						</div>	
						<div class="clear"></div>
                        <!-- ****** Display Charts here **** -->
                    	<!--<div id="containergraph" style="width: 900px; height: 400px; margin: auto;"></div><br /><br />-->
                        <h1><?=$sectionName?></h1>
                        <!-- End Gallery Html Containers ::: START -->
                        <div class="navigation-container">
							<div id="thumbs" class="navigation">
								<a class="pageLink prev" style="visibility: hidden;" href="#" title="Previous Page"></a>
								
                                <?
							if ($emptyAlbum !=0 ){
								$imageCount=0;
								echo "<ul class='thumbs noscript'>";
					
								while(list($pid,$photoname, $uploadtime, $cid, $videoid, $entrytype, $entrytitle, $entrycomments, $adminnotes, $language) = mysql_fetch_array($res)) {
									$imageCount++;
									$submitid="submit".$imageCount;
									$responsearea="responseMsg".$imageCount;
									$imageName="photo".$imageCount;
									$formName="contact".$imageCount;
									//$entrycomments = "nothing bad here";
									$containbadwords = isBadWord($entrycomments, $badwordsarray);
									if ($containbadwords) {
										$flagstatustext = "<font color='#FF0000'>(Warning: Some inappropriate language detected)</font><br><br>";
									} else {
										$flagstatustext = "";
									}
									//$photoname=$photoname;
									if (!isset($language) || $language=="") $language="en";
									
									if (!isset($entrytype) || $entrytype=="") $entrytype="photo";
									
									if (strtolower($entrytype)=="photo") {
										//$thumbName=$photoname;
										$imageAddress= $imageBaseURL_thumb.$photoname;
										$fullImageAddress = $imageBaseURL.$photoname;
									} else if (strtolower($entrytype)=="youtube") {
										$imageAddress= "http://img.youtube.com/vi/".$videoid."/2.jpg";
										$fullImageAddress = $imageAddress;
									} else if (strtolower($entrytype)=="record") {
										if ($campaignUGCType == "photovideo") {
											$imageAddress= $imageBaseURL_thumb.$photoname;
										} else {
											$imageAddress= "images/webcam.jpg";
										}
										$fullImageAddress = $imageAddress;
									} else { //*** Assume YouTube for now to keep compatability with older campaigns
										$imageAddress= "http://img.youtube.com/vi/".$videoid."/2.jpg";
										$fullImageAddress = $imageAddress;
									}
									$fullImageAddress = str_replace(" ", "%20", $fullImageAddress); // issue with images with spaces
									
									echo "<li>";
									echo "<a class=\"thumb\"  href=\"".$fullImageAddress."\" title=\"".$displayname."\">";
									echo "<img src=\"".$imageAddress."\" width=\"75\" width=\"75\" alt=\"".$displayname."\" />";
									echo "</a>";
									
								
									echo "<div style=\"background-color: #ccc;\" class=\"caption\">";
									echo "<div class=\"image-title\">".$displayName."</div>";
									echo "<div class=\"image-desc\">".$photoName."</div>";

							?>
												
                                                <div style="word-wrap:break-word;"> <h1><?=$entrytitle?></h1><br /><br /></div>
                                                <div style="position:absolute; top:90px; left:10px; width:640px; height:500px; background-color:#999">
                                                
                                                <? 	if ($campaignUGCType == "") {
														if (strtolower($entrytype)=="photo") { 
															//$size = getimagesize($fullImageAddress);
															list($originalWidth, $originalHeight) = getimagesize($fullImageAddress);
															//echo $originalWidth.":".$originalHeight.":";
															$ratio = $originalWidth / $originalHeight;
															//echo $ratio."<br>";
															$targetWidth = $targetHeight = min(390, max($originalWidth, $originalHeight));

															if ($ratio < 1) {
   																$targetWidth = $targetHeight * $ratio;
															} else {
    															$targetHeight = $targetWidth / $ratio;
															}
															//echo $targetWidth.":".$targetHeight;
												?>
                                                			<img src="<?=$fullImageAddress?>" border=0 width="<?=$targetWidth?>" height="<?=$targetHeight?>" /><br /><br />
                                                    
                                                <? 		} else { ?>
															<iframe width="640" height="390" src="https://www.youtube.com/embed/<?=$videoid?>" frameborder="0" allowfullscreen></iframe>
                                                <? 		} 
													} else {
														//echo "Campaign type:".$campaignUGCType."<br>";
														//**** Images are always included by default in this type of campaign
														list($originalWidth, $originalHeight) = getimagesize($fullImageAddress);
														$ratio = $originalWidth / $originalHeight;
														$targetWidth = $targetHeight = min(500, max($originalWidth, $originalHeight));
														if ($ratio < 1) {
   															$targetWidth = $targetHeight * $ratio;
														} else {
    														$targetHeight = $targetWidth / $ratio;
														}
												?>
                                                		<img src="<?=$fullImageAddress?>" border=0 width="<?=$targetWidth?>" height="<?=$targetHeight?>" /><br /><br />
                                                <?
														if (strtolower($entrytype)=="youtube") { 
														
												?>
                                                			<iframe width="640" height="390" src="https://www.youtube.com/embed/<?=$videoid?>" frameborder="0" allowfullscreen></iframe>
                                                <?		} else if (strtolower($entrytype)=="record") {
															//echo "Recorded video";
												?>
                                                			<div id="flashContent">
																<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="475" height="265" id="videopreviewplayer" align="middle">
																<param name="movie" value="videopreviewplayer.swf" />
                                                                <param name="FlashVars" value="s=<?=$videoid?>&c=bestbuy">
																<param name="quality" value="high" />
																<param name="bgcolor" value="#ffffff" />
																<param name="play" value="true" />
																<param name="loop" value="true" />
																<param name="wmode" value="transparent" />
																<param name="scale" value="showall" />
																<param name="menu" value="true" />
																<param name="devicefont" value="false" />
																<param name="salign" value="" />
																<param name="allowScriptAccess" value="sameDomain" />
																<!--[if !IE]>-->
																<object type="application/x-shockwave-flash" data="videopreviewplayer.swf" width="475" height="265">
																	<param name="movie" value="videopreviewplayer.swf" />
                                                                    <param name="FlashVars" value="s=<?=$videoid?>&c=bestbuy">
																	<param name="quality" value="high" />
																	<param name="bgcolor" value="#ffffff" />
																	<param name="play" value="true" />
																	<param name="loop" value="true" />
																	<param name="wmode" value="transparent" />
																	<param name="scale" value="showall" />
																	<param name="menu" value="true" />
																	<param name="devicefont" value="false" />
																	<param name="salign" value="" />
																	<param name="allowScriptAccess" value="sameDomain" />
															<!--<![endif]-->
																	<a href="http://www.adobe.com/go/getflash">
																		<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
																	</a>
															<!--[if !IE]>-->
															</object>
															<!--<![endif]-->
															</object>
														</div>
                                                <?
														}
												
														
													}
												?> 	
												</div>
                                                
                                                 <div style="position:absolute; top:90px; left:660px; width:280px; height:400px; background-color:#999">
													<?
														$sql_info = "SELECT cpresult_first_name, cpresult_last_name, cpresult_birthday, cpresult_city, cpresult_states, cpresult_email FROM sp_campaign_result WHERE cpresult_sid = '".$cid."' AND campaign_sid='".$activityID."'";
														//echo $sql_info;
														$res_info = mysql_query($sql_info, $con);
														if (mysql_num_rows($res_info) > 0) {
															list($first, $last, $agerange, $city, $prov, $email) = mysql_fetch_array($res_info);
															if ($agerange == "0000-00-00 00:00:00") $agerange = "N/A";
														} else {
															$first = $last = $agerange = $prov = $city = $email = "N/A";
														}
													?>	
                                                    <h1>Name:</h1>
                                                	<div style="word-wrap:break-word;"> <?=$first." ".$last?><br /><br /></div>
                                                	<h1>Age range:</h1>
                                                	<?=$agerange?><br /><br />
                                                	<h1>From:</h1>
                                                	<?=$city." ".$prov?><br /><br />
                                                	<h1>Email:</h1>
                                                	<?=$email?><br /><br />
                                                    <h1>Description:</h1>
                                                    <?=$flagstatustext?>
                                                	<div style="word-wrap:break-word;"> <?=$entrycomments?><br /><br /></div>
												</div>
                                                
                                                <div style="position:absolute; top:600px; left:10px; width:930px; height:300px; background-color:#999">
                                                	<font color="#000000">Date Submitted: <?=$uploadtime?></font><br><br />
                                                    <h1>Notes:</h1>
                                                  	<textarea rows="5" cols="80" name="admincomment<?=$pid?>" id="admincomment<?=$pid?>" style="width: 640px; height: 50px; border: 3px solid #cccccc; padding: 5px; font-family: Tahoma, sans-serif; font-size:12px" /><?=$adminnotes?></textarea><br /><br />
                                                    <h1>Current Status:</h1>
                                                   
                                                    <input type="radio" name="status<?=$pid?>" id="status<?=$pid?>" class="status<?=$pid?>" value="1" /> Approve<br />
                                                    <input type="radio" name="status<?=$pid?>" id="status<?=$pid?>" class="status<?=$pid?>" value="2" /> Reject<br />
                                                    <input type="radio" name="status<?=$pid?>" id="status<?=$pid?>" class="status<?=$pid?>" value="3" /> Review later<br /><br />
                                                    
                                                    <?
														if ($campaignUGCNotify == "1") {
													?>
                                                    	<h1>Notify user:</h1>
                                      
                                                   		<input type="checkbox" name="notify<?=$pid?>" id="notify<?=$pid?>" value="Y"  onclick="Toggle('emailtext')" /> Check to notify user (only approved entries will receive an email notification)<br /><br />
                                                    	<div id="emailtext">
                                                    		<input type="radio" name="emailtemplate_<?=$pid?>" id="emailtemplate_<?=$pid?>" value="0" checked="true" />Default template<br /><br /><br />
														</div>
                                                    <?
														}
													?>
                                                    <input type="hidden" name="lang_<?=$pid?>" id="lang_<?=$pid?>" value="<?=$language?>"  />
                                                    <input type="hidden" name="notify<?=$pid?>" id="notify<?=$pid?>" value="Y"  />
                                                    <input type="hidden" name="cid_<?=$pid?>" id="cid_<?=$pid?>" value="<?=$cid?>" />
                                                    <button onclick="updateentry(<?=$pid?>)" >Save</button>
                                                </div>
                           	<?
										echo "</div>";
									echo "</li>";

								}
								echo "</ul>";
							} else {
								echo "<h2>No entries found...</h2>";
							}		
							?>
							

                                
								<a class="pageLink next" style="visibility: hidden;" href="#" title="Next Page"></a>
							</div>
						</div>
    
                        <!--<div class="content">
							<div class="slideshow-container">-->
								<!--//<div id="controls" class="controls"></div>-->
								<!--<div id="loading" class="loader"></div>
								<div id="slideshow" class="slideshow"></div>  //<!--*** This is where the big image goes *** -->
							<!--</div>

							<div id="caption" class="caption-container">
								<div class="photo-index"></div>
							</div>
						</div>-->
                        
                    	<div class="content">
							<div id="caption" class="caption-container"></div>
						</div>

						<!-- End Gallery Html Containers ::: END-->

					

						
						<div class="clear"></div>
					</div>
				</div>
			</section>
		</div>

				<footer id="pagefooter">
					<div id="bottom">
						<div class="block1">
							<h2>Meet The Team</h2>
							<div class="teamimg">
								<? include("team.php")?>
							</div>
						</div>
						<div class="block2">
							<h2>Featured Projects</h2>
							<div class="projectthmb"> 
                            	<!--<a href="portfolio"><img src="images/latstproject.jpg" width="240" height="150" alt="project"></a>-->
                                <?
                                	$sql = "SELECT ID, post_date, post_content, post_title FROM 3tierweb.tier_posts WHERE post_type='post' and post_parent='0' and post_status='publish' order by post_date desc Limit 4";
									$res = mysql_query($sql, $con_3tl);
									if (mysql_num_rows($res) > 0)  {
								?>
                                			
                                        
                                        	<ul id="slider1">
                                            	<?
												$picon = "";
												while(list($pid, $pdate, $pcontent, $ptitle) = mysql_fetch_array($res))  {
													$sql_icon = "SELECT guid FROM 3tierweb.tier_posts where post_type='attachment' and (post_mime_type='image/jpeg' or post_mime_type='image/png')  and post_parent='".$pid."' order by post_date desc";
													$res_icon = mysql_query($sql_icon, $con_3tl);
													if (mysql_num_rows($res_icon) > 0)  {
														list($picon) = mysql_fetch_array($res_icon);	
													}
													echo "<li><a href='http://www5.3tierlogic.com/?p=".$pid."' target='_blank'><img src='".$picon."' width='240' height='150' /></a><p style='font:Verdana, Geneva, sans-serif; color:#FFF; font-size:10px '>".$ptitle."</p></li>";
													//echo "<li><a href='http://www5.3tierlogic.com/?p=".$pid."' target='_blank'>".$ptitle."</a></li>";
												}
												?>
                                               
                                            </ul>
                                        
                                        
                                <?
									} else {
								?>
                                		<a href="portfolio"><img src="images/latstproject.jpg" width="240" height="150" alt="project"></a>
                                <?
									}
								?>
                            </div>
						</div>
						<div class="block3">
							<h2>About</h2>
							<p>3 Tier Logic is a full service digital marketing and technology services company that enables organizations to grow their customer and prospect communities through anywhere-anytime communications. </p>

						</div>
						<div class="clear"></div>
				</div>
				<div id="credits">
					<p>
					<span class="copyright">
					&copy; 2012 | 3 Tier Logic Inc |  All Rights Reserved </span></p>
				</div>
			</footer>


</section>



 <script type="text/javascript">

			jQuery(document).ready(function($) {

				// We only want these styles applied when javascript is enabled

				$('div.content').css('display', 'block');



				// Initially set opacity on thumbs and add

				// additional styling for hover effect on thumbs

				var onMouseOutOpacity = 0.67;

				$('#thumbs ul.thumbs li, div.navigation a.pageLink').opacityrollover({

					mouseOutOpacity:   onMouseOutOpacity,

					mouseOverOpacity:  1.0,

					fadeSpeed:         'fast',

					exemptionSelector: '.selected'

				});

				

				// Initialize Advanced Galleriffic Gallery

				var gallery = $('#thumbs').galleriffic({

					delay:                     2500,

					numThumbs:                 10,

					preloadAhead:              10,

					enableTopPager:            false,

					enableBottomPager:         false,

					imageContainerSel:         '#slideshow',

					controlsContainerSel:      '#controls',

					captionContainerSel:       '#caption',

					loadingContainerSel:       '#loading',

					renderSSControls:          true,

					renderNavControls:         true,

					playLinkText:              'Play Slideshow',

					pauseLinkText:             'Pause Slideshow',

					prevLinkText:              '&lsaquo; Previous Photo',

					nextLinkText:              'Next Photo &rsaquo;',

					nextPageLinkText:          'Next &rsaquo;',

					prevPageLinkText:          '&lsaquo; Prev',

					enableHistory:             true,

					autoStart:                 false,

					syncTransitions:           true,

					defaultTransitionDuration: 900,

					onSlideChange:             function(prevIndex, nextIndex) {

						// 'this' refers to the gallery, which is an extension of $('#thumbs')

						this.find('ul.thumbs').children()

							.eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()

							.eq(nextIndex).fadeTo('fast', 1.0);



						// Update the photo index display

						this.$captionContainer.find('div.photo-index')

							.html('Photo '+ (nextIndex+1) +' of '+ this.data.length);

					},

					onPageTransitionOut:       function(callback) {

						this.fadeTo('fast', 0.0, callback);

					},

					onPageTransitionIn:        function() {

						var prevPageLink = this.find('a.prev').css('visibility', 'hidden');

						var nextPageLink = this.find('a.next').css('visibility', 'hidden');

						

						// Show appropriate next / prev page links

						if (this.displayedPage > 0)

							prevPageLink.css('visibility', 'visible');



						var lastPage = this.getNumPages() - 1;

						if (this.displayedPage < lastPage)

							nextPageLink.css('visibility', 'visible');



						this.fadeTo('fast', 1.0);

					}

				});



				/**************** Event handlers for custom next / prev page links **********************/



				gallery.find('a.prev').click(function(e) {

					gallery.previousPage();

					e.preventDefault();

				});



				gallery.find('a.next').click(function(e) {

					gallery.nextPage();

					e.preventDefault();

				});



				/****************************************************************************************/



				/**** Functions to support integration of galleriffic with the jquery.history plugin ****/



				// PageLoad function

				// This function is called when:

				// 1. after calling $.historyInit();

				// 2. after calling $.historyLoad();

				// 3. after pushing "Go Back" button of a browser

				function pageload(hash) {

					// alert("pageload: " + hash);

					// hash doesn't contain the first # character.

					if(hash) {

						$.galleriffic.gotoImage(hash);

					} else {

						gallery.gotoIndex(0);

					}

				}



				// Initialize history plugin.

				// The callback is called at once by present location.hash. 

				$.historyInit(pageload, "advanced.html");



				// set onlick event for buttons using the jQuery 1.3 live method

				$("a[rel='history']").live('click', function(e) {

					if (e.button != 0) return true;



					var hash = this.href;

					hash = hash.replace(/^.*#/, '');



					// moves to a new page. 

					// pageload is called at once. 

					// hash don't contain "#", "?"

					$.historyLoad(hash);



					return false;

				});



				/****************************************************************************************/

			});

		</script>
        
        <div id="message"><a href="#top">Scroll to top</a></div>
</body>
</html>

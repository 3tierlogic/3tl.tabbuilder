<?php


// Connect to database
require_once("db.php");
$con = mysql_connect($dbhost, $dbuser, $dbpasswd);		// connect to database
if (!$con) {											// error checking and handling
    die('Could not connect: ' . mysql_error());
}
mysql_select_db($dbname);

/* Load and clear sessions */
session_start();
/*session_destroy();*/
$currentid = $_SESSION['id'];
$_SESSION['companyID'] = $_POST['customerID'];
$sweepstake_baseURL = "http://sweepstake.3tierlogic.com/";

//echo "ID: ".$currentid;
if (!isset($currentid)) {
	echo "<script language = 'javascript'> alert('Your session has expire. Please login again');</script>" ;
	echo "<script language = 'javascript'> window.location.href = 'index.php'; </script>" ;
}


?>
<!doctype html>

<html lang="en-US">
<head>
<meta charset="UTF-8" />
<title>3 Tier Logic - Sweepstakes Tracker V1.0</title>
 <link href="style.css" rel="stylesheet" type="text/css" />
 <!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if IE 6]>
<script src="js/belatedPNG.js"></script>
<script>
	DD_belatedPNG.fix('*');
</script>
<![endif]-->
<link rel="stylesheet" href="colorbox.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="colorbox/jquery.colorbox.js"></script>
<!--<script src="js/jquery-1.4.min.js" type="text/javascript" charset="utf-8"></script>-->
<script src="js/loopedslider.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" charset="utf-8">
	$(function(){
		$('#slider').loopedSlider({
			autoStart: 6000,
			restart: 5000
		});
		
	});
</script>

<script language = "javascript">

	function myformsubmit(ind) {
   		document.myform.submit();
	}
</script>

<script>
	$(document).ready(function(){
		$(".picspopup").colorbox({
			onOpen:function(){  },
			onLoad:function(){  },
			onComplete:function(){  },
			onCleanup:function(){  },
			onClosed:function(){  }
		});
	});
</script>

</head>

<body>
<section id="page">
<div id="bodywrap">
<section id="top">
<nav>
<h1 id="sitename">
  <a href="#">3 Tier Logic Inc.</a></h1>
  
  <ul id="sitenav">
  	<li class="current"><a href="main.php">Home</a></li>
  	<li><a href="services">Summary</a></li>
    <li><a href="about">Entries</a></li>
    <li><a href="blog">Demographics</a></li>
    <li><a href="contact">Help</a></li>
  
  </ul>
  
</nav>
<header id="homeheader">


<div id="slider">	
	<div class="container">
	<ul class="slides">
			<li>
			  <div class="thumbholder"><img src="images/slideimg2.gif" width="413" height="220" alt="First Image" /></div><div class="txtholder">
            <h2>Modern, Accessible and beautiful Websites and web applications</h2>
            <p>Lorema psum dolor sit amet, 
consectetur adipiscing elit. 
Integer egestas purus bibendum 
neque aliquam ut posuere elit semper. Fusce sagittis pharetra eros, sit amet consequat sem mollis vitae. </p>
			</div></li>
			<li><div class="thumbholder"><img src="images/slideimg1.gif" width="413" height="220" alt="First Image" /></div><div class="txtholder">
            <h2>Modern, Accessible and beautiful Websites and web applications</h2>
            <p>Lorema psum dolor sit amet, 
consectetur adipiscing elit. 
Integer egestas purus bibendum 
neque aliquam ut posuere elit semper. Fusce sagittis pharetra eros, sit amet consequat sem mollis vitae. </p>
			</div></li>
			<li>
			  <div class="thumbholder"><img src="images/slideimg2.gif" width="413" height="220" alt="First Image" /></div><div class="txtholder">
            <h2>Modern, Accessible and beautiful Websites and web applications</h2>
            <p>Lorema psum dolor sit amet, 
consectetur adipiscing elit. 
Integer egestas purus bibendum 
neque aliquam ut posuere elit semper. Fusce sagittis pharetra eros, sit amet consequat sem mollis vitae. </p>
			</div></li>
			<li><div class="thumbholder"><img src="images/slideimg1.gif" width="413" height="220" alt="First Image" /></div><div class="txtholder">
            <h2>Modern, Accessible and beautiful Websites and web applications</h2>
            <p>Lorema psum dolor sit amet, 
consectetur adipiscing elit. 
Integer egestas purus bibendum 
neque aliquam ut posuere elit semper. Fusce sagittis pharetra eros, sit amet consequat sem mollis vitae. </p>
			</div></li>
	
			
		</ul>
	
	<ul class="pagination">
		<li><a href="#">1</a></li>
		<li><a href="#">2</a></li>
		<li><a href="#">3</a></li>
		<li><a href="#">4</a></li>

	</ul>	
    </div>
</div>


</header>
</section>
<section id="contentwrap">
<div id="contents">

<section id="normalpage">

<section id="left">

<h2>Report</h2>
<article>
		<?
			//**** Display the list of companies - Available only for super user account
       		$sql = "SELECT partner_sid, partner_company_name FROM facebook.sp_partner_info order by partner_company_name";
      		$res = mysql_query($sql);
		?>
      		<form name="myform" method="POST" action="<?=$_SERVER['PHP_SELF']?>">
				<table width="100%">
					<tr><td>
						<b>Company:</b> <select name = "customerID" onChange = "myformsubmit(1);"> 
                                    <option value = ""> select </option>                                   
                                    <?
                                      while(list($cid,$cname) = mysql_fetch_array($res))  {
									     if($_SESSION['companyID'] == $cid) {
										  	echo "<option value = '$cid' selected> $cname </option>";
										 } else {
									      	echo "<option value = '$cid'> $cname </option>";
									  	 }								  
									  }
									
									?>
                                   </select>
                                  
					</td></tr>				
				</table>
         	</form>
         <?
		 	//echo "companyID: ".$_SESSION['companyID'];
			//**** Get the company Name for the selected companyID
			if ($_SESSION['companyID'] != "") {
				$sql_company = "SELECT partner_company_name FROM facebook.sp_partner_info WHERE partner_sid = '".$_SESSION['companyID']."'";
       			$res_company = mysql_query($sql_company);
				if (mysql_num_rows($res_company) > 0)  {
					list($currentCompanyName) = mysql_fetch_array($res_company);
				} else {
					$currentCompanyName = "Missing company name";
				}
		 ?>
      			<table cellspacing="0" cellpadding="0">
       				<tr>
                		<td align="left">
        					<?
								//***** Select all the campaigns associated with the company
	    						$sql = "select campaign_sid, campaign_name, campaign_is_check_reentry, campaign_like_page, campaign_enter_page, campaign_bg_picture, campaign_thanks_page, campaign_profile_photo FROM facebook.sp_campaign_info WHERE partner_sid = '".$_SESSION['companyID']."'";
	    						$res = mysql_query($sql);
	    						if (mysql_num_rows($res) > 0)  {
									echo "<b>Select the campaign report you want to view:</b><br><br>";
		 				    		while(list($campaignID,$campaignName, $singleentry, $likepage, $enterpage, $bgpage, $thankspage, $profilephoto) = mysql_fetch_array($res)) {
										if ($singleentry == 1) {
											$multipleentry = "One entry per person allowed";
										} else {
											$multipleentry = "Multiple entries per person allowed";
										}
							?>
                    					<!--<a href="#" onClick="renderChart(); return false;"><?=$campaignName?> Report (<?=$multipleentry?>)</a><br />-->
                                		<a href="#" onClick="openView('getprizereport.php','<?=$campaignID?>', '<?=$campaignName?>', '<?=$currentCompanyName?>'); return false;"><?=$campaignName?> Report (<?=$multipleentry?>)</a><br /><br>
                    		<?
										$enterpage = substr($enterpage, 3);
										$likepage = substr($likepage, 3);
										$thankspage = substr($thankspage, 3);
										
										echo "<a class='picspopup' href='".$sweepstake_baseURL.$likepage."'><img src='".$sweepstake_baseURL.$likepage."' border='0' width='148' height='220'></a>&nbsp;&nbsp;"."<a class='picspopup' href='".$sweepstake_baseURL.$enterpage."'><img src='".$sweepstake_baseURL.$enterpage."' border='0' width='148' height='220'></a>&nbsp;&nbsp;"."<a class='picspopup' href='".$sweepstake_baseURL.$thankspage."'><img src='".$sweepstake_baseURL.$thankspage."' border='0' width='148' height='220'></a>&nbsp;&nbsp;<a class='picspopup' href='".$sweepstake_baseURL.$bgpage."'><img src='".$sweepstake_baseURL.$bgpage."' border='0' width='148' height='220'></a><br><br>";
			 							//echo "<a href='#' onClick='"."('renderChart'); return false;'".">$campaignName Report</a> ($multipleentry)<br>";
									}  //** WHILE
		  						} else {
		 			 				echo "No reports!";
						 		}
		 					?>
		 				</td>
          			</tr>     
	  			</table>
		 
		<?
	    	}
		?>
        
        
</article>


</section>
<section id="sidebar">
<h2>Twitter</h2>
<article class="testimonials">

<script src="http://widgets.twimg.com/j/2/widget.js"></script>
<script>
new TWTR.Widget({
  version: 2,
  type: 'search',
  search: 'from:3tierlogicrobc OR from:3tierlogicmarie OR from:3tierlogiccarlo OR from:3TierLogicABat OR from:3tierlogicDOM OR from:3TierLogicBlack OR from:3tierlogicJILL OR from:3tierlogicPaul OR from:3tierlogicsandi',
  interval: 6000,
  title: '3 Tier Logic Twitter Feeds',
  subject: 'LIVE',
  width: 280,
  height: 300,
  theme: {
    shell: {
       background: '#333333',
       color: '#ffffff'
    },
    tweets: {
      background: '#000000',
      color: '#ffffff',
      links: '#4aed05'
    }
  },
  features: {
    scrollbar: false,
    loop: true,
    live: true,
    hashtags: true,
    timestamp: false,
    avatars: true,
    toptweets: true,
    behavior: 'default'
  }
}).render().start();
</script>

</article>
</section>

<div class="clear"></div>
</section>
</div>
</section>
</div>
<footer id="pagefooter">
<div id="bottom">
<div class="block1">
<h2>Meet The Team</h2>

<div class="teamimg">


<div class="imgthmb">
  <img src="images/team5.jpg" width="65" height="65" alt="team"></div>
  <div class="imgthmb">
  <img src="images/team1.jpg" width="65" height="65" alt="team"></div>
  <div class="imgthmb">
  <img src="images/team2.jpg" width="65" height="65" alt="team"></div>
  <div class="imgthmb">
  <img src="images/team4.jpg" width="65" height="65" alt="team"></div>
  <div class="imgthmb">
  <img src="images/team2.jpg" width="65" height="65" alt="team"></div>
  <div class="imgthmb">
  <img src="images/team1.jpg" width="65" height="65" alt="team"></div>
</div>
</div>
<div class="block2">
<h2>Latest Project</h2>
<div class="projectthmb"> <a href="portfolio"><img src="images/latstproject.jpg" width="240" height="150" alt="project"></a></div>

</div>
<div class="block3">

<h2>More About</h2>
<p>PROFESSIONAL PORTFOLIO is a Template suitable for Businesses, Web Design Firms or any I.T related company that require a place to showcase their works. 
This template is released under Creative Commons Attribution License. </p>

</div>
<div class="clear"></div>
</div>
<div id="credits">
<p>
<span class="copyright">
&copy; 2010 | Your Company Name |  All Rights Reserved | Professional Portfolio is a CSS Template relealsed by CSSHeaven</span>
<span id="designcredit">
<!--Creative Common Non-Commercial, Attribution License
Designed by : Roshan Ravi
Author URI : cssheaven.org
Do Not Remove this Credits and Link back to CSSHeaven from the template-->
<a href="http://cssheaven.org" title="Free CSS Website Template by CSSHeaven">Website Template</a>
<!--Design Credits--> by CSSHeaven.org </span>
</p>
</div>
</footer>

</section>
</body>
</html>

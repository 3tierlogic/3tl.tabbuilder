<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>3 Tier Logic Inc | Please login | Tracker v1.0</title>
<meta name="robots" content="noindex">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8">
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>  -->
<link href="css/mainlogin.css" media="screen,projection" rel="stylesheet" type="text/css" />

<style>
a:link, a:visited, a:hover, a:active {
	color: #0269ab;
}
</style>
<script>
	$(document).ready(function(){
	  	$("#username").select();
	});
</script>
<!--[if IE]>
<script>
	$(document).ready(function(){
	  	$("#username").focus();
	});
</script>
<![endif]-->
<script type="text/javascript">

//main function

function validate_form(thisform) {

	with (thisform) {

		if (validate_username(uName,"Enter your username") == false)

			{uName.focus();return false}

		if (validate_password(uPassword,"Enter your password") == false)

			{uPassword.focus();return false}

	}

}



//check username

function validate_username(field,alerttxt) {

	with (field) {

		if (value == "") 

			{alert(alerttxt);return false}

		else {return true}

	}

}



//check password

function validate_password(field,alerttxt) {

	with (field) {

		if (value == "") 

			{alert(alerttxt);return false}

		else {return true}

	}

}

</script>

 
</head>

<body>
	
<div id="additional"></div>
<div id="formWrapper">
	<div id="formCasing">
		<h1>Campaign Report: Login</h1>
		<div id="loginForm">
			<form id="myform" name="myform" method="post" action="logincheck.php" onSubmit="return validate_form(this);">
  			<dl>
				<dt><label for="username">Username</label></dt>

				<dd><input type="text" name="uName" id="username" value="" class="input" tabindex="1" /></dd>
				<dt><label for="password">Password</label></dt>
				<dd><input type="password" name="uPassword" id="password" value="" tabindex="2" class="input" />
				<dd><input type="image" src="images/login.gif" width="93" tabindex="4" height="40" id="btnLogin" name="btnLogin"></dd>
			</dl>
  			</form>
		</div>
    </div>
	<div id="formFooter"></div>
</div>



</body>
</html>




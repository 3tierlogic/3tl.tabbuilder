<?php


// add where condition to support date range.
function addCon($original_sql, $s, $e){

	// detect where;
	$pos = strpos($original_sql, 'where', 1); 
	if($pos == false){ // try capital again.
		$pos = strpos($original_sql, 'WHERE', 1);
	}
	
	/*
	print "+++ <br/>";
	print "sql0: ".$original_sql."<br/>";
	print "pos: ".$pos."<br/>";
	*/
	
	$sub1 = substr($original_sql,0,$pos+1+4); // "select...where"
	//print "sub1: ".$sub1."<br/>";
	$sub2 = " cpresult_create_time between '".$s."' and '".$e."' and ";
	//print "sub2: ".$sub2."<br/>";
	$sub3 = substr($original_sql, $pos+1+4+1); // " ..."
	//print "sub3: ".$sub3."<br/>";
	
	$original_sql = "";
	$original_sql = $original_sql.$sub1;
	$original_sql = $original_sql.$sub2;
	$original_sql = $original_sql.$sub3;
	
	//print "sql: ".$original_sql."<br/>";
	//print "+++ <br/>";

	return $original_sql;
}

// check if is OpenCheckBox or ?opendropdown?
function arai($rawName, $rawMan){

	// checkbox.
	$checkbox = "OpenCheckBox";		
	$pos = strpos($rawName, $checkbox); 
	if($pos !== false){ // triple = is a must!	
		// check if is mandatory: have read the rules...
		if($rawMan == 1){ // is a mandatory checkbox, so skip it.
			return 0;
		}
		return 1; // is OpenCheckBox.
	}
	
	// dropdown.
	$dropdown = "OpenDropDown";
	$pos = strpos($rawName, $dropdown);
	if($pos !== false){ // triple = is a must!
		return 2; // is dropdown.
	}
	
	return 0; // is nothing.
}


function getCACarrierName($carrier){
						
	switch($carrier){
		case "302651":{
			return "BELL";
		}
		case "302720":{
			return "ROGERS";
		}
		case "302655":{
			return "MTS";
		}
		case "302701":{
			return "NB TEL";
		}
		case "302657":{
			return "QUEBECTEL";
		}
		case "302654":{
			return "SASK TEL";
		}
		case "302653":{
			return "TELUS";
		}
		case "302001":{
			return "VIRGIN";
		}
		case "302370":{
			return "FIDO";
		}			
		default:{
			return $carrier;
		}
	}
}

function getUSCarrierName($carrier){ 
			
	switch($carrier){
		case "310980":{
			return "AT&T";
		}
		case "310190":{
			return "ALASKA WIRELESS";
		}
		case "310500":{
			return "ALLTEL";
		}
		case "310420":{
			return "CINCINNATI BELL";
		}
		case "310180":{
			return "CINGULAR";
		}
		case "310016":{
			return "CRICKET";
		}
		case "310560":{
			return "DOBSON";
		}
		case "316010":{
			return "NEXTEL";
		}
		case "316110":{
			return "SPRINT";
		}	
		case "310660":{
			return "TMOBILE USA";
		}
		case "310012":{
			return "VERIZON";
		}			
		default:{
			return $carrier;
		}
	}
}

function getDetailURL($td, $date, $campid, $sdate, $edate){
	
	$tord = substr($td,0,1); 
	//echo "substr(td,0,1) = ".$tord."<br/>";	

	$r = 'r'.substr($td,1);// get id.	
	//echo "r = ".$r."<br/>";	
	
	//echo "date = ".$date."<br/>";	

	$detailPage = "detail.php";	

	$url = $detailPage."?campid=".$campid."&date=".$date."&tord=".$tord."&sdate=".$sdate."&edate=".$edate;
	//echo "url = ".$url."<br/>";		
	 
	return $url;
}

function getUGCDetailURL($td, $date, $campid, $sdate, $edate, $cID, $localdb){
	
	$tord = substr($td,0,1); 
	//echo "substr(td,0,1) = ".$tord."<br/>";	

	$r = 'r'.substr($td,1);// get id.	
	//echo "r = ".$r."<br/>";	
	
	//echo "date = ".$date."<br/>";	

	$detailPage = "ugcentries.php";	

	//$url = $detailPage."?aID=".$campid."&date=".$date."&tord=".$tord."&sdate=".$sdate."&edate=".$edate."&coid=".$cID."&ldb=".$localdb;
	$url = $detailPage."?cID=".$campid."&sdate=".$sdate."&edate=".$edate."&coID=".$cID;
	//echo "url = ".$url."<br/>";		
	 
	return $url;
}

function getVoteDetailURL($td, $date, $campid, $sdate, $edate, $cID, $localdb){
	
	$tord = substr($td,0,1); 
	//echo "substr(td,0,1) = ".$tord."<br/>";	

	$r = 'r'.substr($td,1);// get id.	
	//echo "r = ".$r."<br/>";	
	
	//echo "date = ".$date."<br/>";	

	$detailPage = "dailyvotesdetails.php";	

	//$url = $detailPage."?aID=".$campid."&date=".$date."&tord=".$tord."&sdate=".$sdate."&edate=".$edate."&coid=".$cID."&ldb=".$localdb;
	$url = $detailPage."?cID=".$campid."&sdate=".$sdate."&edate=".$edate."&coID=".$cID."&ldb=".$localdb;
	//echo "url = ".$url."<br/>";		
	 
	return $url;
}

?>
<?php

/* Load and clear sessions */
session_start();
if ( ! isset ( $_SESSION['companyid'] ) ) {
	header ( 'Location: http://platform.3tierlogic.com/tabbuilder/index1_3.php' );
	exit();
} else {
	if (isset($_REQUEST['customerID'])) {    ///**** This value is set when logged in as a superuser and selecting a company from the drop down to view
		$currentid = $_REQUEST['customerID'];
	} else {
		$currentid = $_SESSION['companyid'];
	}
	$issuperuser = $_SESSION['superaccount'];
}

// Connect to database
require_once("db.php");
$con = mysql_connect($dbhost, $dbuser, $dbpasswd);		// connect to database
if (!$con) {											// error checking and handling
    die('Could not connect: ' . mysql_error());
}
mysql_select_db($dbname);

$con_3tl = mysql_connect($dbhost_3tl, $dbuser_3tl, $dbpasswd_3tl);		// connect to database
if (!$con_3tl) {											// error checking and handling
    die('Could not connect to 3TL: ' . mysql_error());
}
mysql_select_db($dbname_3tl);






/*if (isset($_REQUEST['customerID'])) {
	$currentid = $_REQUEST['customerID'];
	$_SESSION['companyid'] = $currentid;
} else {
	$currentid = $_SESSION['companyid'];
}
$issuperuser = $_SESSION['superaccount'];

//echo "ID: ".$currentid.":super: ".$issuperuser;
if ((!isset($currentid)) && (!isset($issuperuser))) {
	echo "<script language = 'javascript'> alert('Your session has expire. Please login again');</script>" ;
	echo "<script language = 'javascript'> window.location.href = 'index.php'; </script>" ;
}
*/

?>
<!doctype html>

<html lang="en-US">
<head>
<meta charset="UTF-8" />
<title>3 Tier Logic - Platform Tracker V1.3</title>
 <link href="style.css" rel="stylesheet" type="text/css" />
 <!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if IE 6]>
<script src="js/belatedPNG.js"></script>
<script>
	DD_belatedPNG.fix('*');
</script>
<![endif]-->
<link rel="stylesheet" href="colorbox.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="colorbox/jquery.colorbox.js"></script>
<!--<script src="js/jquery-1.4.min.js" type="text/javascript" charset="utf-8"></script>-->
<script src="js/loopedslider.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" charset="utf-8">
	$(function(){
		$('#slider').loopedSlider({
			autoStart: 6000,
			restart: 5000
		});
		
	});
</script>

<script language = "javascript">

	function myformsubmit(ind) {
   		document.myform.submit();
	}
</script>

<script>
	$(document).ready(function(){
		$(".picspopup").colorbox({
			onOpen:function(){  },
			onLoad:function(){  },
			onComplete:function(){  },
			onCleanup:function(){  },
			onClosed:function(){  }
		});
	});
</script>

</head>

<body>
<section id="page">
<div id="bodywrap">
<section id="top">
<nav>
<h1 id="sitename">
  <a href="#">3 Tier Logic Inc.</a></h1>
  
  <ul id="sitenav">
  	<li class="current"><a href="../editor/dashboard.php">Back to Tab Editor</a></li>
  </ul>
  
</nav>
<header id="homeheader">

 <?
 	//**** Get the last 4 articles published on the website to display on the top header
	$publishnews_ok = true;
	$sql = "SELECT ID, post_date, post_content, post_title FROM 3tierweb.tier_posts WHERE post_type='post' and post_parent='0' and post_status='publish' order by post_date desc Limit 4";
	$res = mysql_query($sql, $con_3tl);
	if (mysql_num_rows($res) > 0)  {
		/*while(list($pid, $pdate, $pcontent, $ptitle) = mysql_fetch_array($res))  {
			echo $pid."-".$pdate."-".$pcontent."-".$ptitle."<br>";
		}*/
		$publishnews_ok = true;
	} else {
		//echo "No Posts found";
		$publishnews_ok = false;
	}
?>
<div id="slider">	
	<div class="container">
	<ul class="slides">
    	<?
			if ($publishnews_ok) {
				$latestCounter=0;
				while(list($pid, $pdate, $pcontent, $ptitle) = mysql_fetch_array($res))  {
					//echo $pid."-".$pdate."-".$pcontent."-".$ptitle."<br>";
					$sql_icon = "SELECT guid FROM 3tierweb.tier_posts where post_type='attachment' and (post_mime_type='image/jpeg' or post_mime_type='image/png')  and post_parent='".$pid."' order by post_date desc";
					$res_icon = mysql_query($sql_icon, $con_3tl);
					if (mysql_num_rows($res_icon) > 0)  {
						list($picon) = mysql_fetch_array($res_icon);	
					} else {
						$picon = "";	
					}
					echo "<li><div class='thumbholder'>";
					echo "<img src='".$picon."' width='200' height='200' /></div><div class='txtholder'>";
					echo "<h2>".$ptitle."</h2>";
					$pcontent = substr($pcontent, 0, 180);
					//$pcontent = $pcontent."... <a href='http://www5.3tierlogic.com/?p=".$pid."' target='_blank'>More</a>";
					$pcontent = $pcontent."...";
					if ($latestCounter == 0) {
						$latestImage = $picon;
						$latestTitle = $ptitle;
						$latestLink = "http://www.3tierlogic.com/?p=".$pid;
					}
					$latestCounter++;
					echo "<p>".$pcontent."</p>";
					echo "</div></li>";
				}
			}
			
		?>
			
			
		</ul>
	
	<ul class="pagination">
		<li><a href="#">1</a></li>
		<li><a href="#">2</a></li>
		<li><a href="#">3</a></li>
		<li><a href="#">4</a></li>

	</ul>	
    </div>
</div>


</header>
</section>
<section id="contentwrap">
<div id="contents">

<section id="normalpage">

<section id="left">

<h2>Report</h2>
<article>
		<?
			if ($issuperuser) {
       			$sql = "SELECT cp_companyID, cp_companyName FROM sp_company_info WHERE cp_status=1 order by cp_companyName";
      			$res = mysql_query($sql, $con);
				
		?>
      			<form name="myform" method="POST" action="<?=$_SERVER['PHP_SELF']?>">
					<table width="100%">
						<tr><td>
							<b>Company:</b> <select name = "customerID" onChange = "myformsubmit(1);"> 
                                    <option value = ""> select </option>                                   
                                    <?
                                      while(list($cid,$cname) = mysql_fetch_array($res))  {
									     if($currentid == $cid) {
										  	echo "<option value = '$cid' selected> $cname </option>";
										 } else {
									      	echo "<option value = '$cid'> $cname </option>";
									  	 }								  
									  }
									
									?>
                                   </select>
                                  
						</td></tr>				
					</table>
         		</form>
               
         <?
			}
		 	//echo "companyID: ".$currentid;
			//**** Get the company Name for the selected companyID
			if ($currentid != "") {
				$sql_company = "SELECT cp_companyName FROM sp_company_info WHERE cp_companyID = '".$currentid."'";
       			$res_company = mysql_query($sql_company, $con);
				if (mysql_num_rows($res_company) > 0)  {
					list($currentCompanyName) = mysql_fetch_array($res_company);
				} else {
					$currentCompanyName = "Missing company name";
				}
		 ?>
      			<table cellspacing="0" cellpadding="0" border="0">
       				<tr>
                		<td align="left">
        					<?
								//***** Select all the campaigns associated with the company
	    						$sql = "select campaign_sid, campaign_name, campaign_is_check_reentry FROM sp_campaign_info WHERE partner_sid = '".$currentid."'";
	    						$res = mysql_query($sql, $con);
	    						if (mysql_num_rows($res) > 0)  {
									echo "<b>Select the campaign report you want to view:</b><br><br>";
		 				    		while(list($campaignID,$campaignName, $singleentry) = mysql_fetch_array($res)) {
										if ($singleentry == 1) {
											$multipleentry = "images/singleentry.png";
										} else {
											$multipleentry = "images/multipleentries.png";
										}
							?>
                    					<a href="dailyentries.php?cID=<?=$campaignID?>"><?=$campaignName?> Report</a>
										<img src="<?=$multipleentry?>"></a>
                                        <br /><br>
                    		<?
										
									}  //** WHILE
		  						} else {
		 			 				echo "No reports!";
						 		}
		 					?>
		 				</td>
          			</tr>   
                   
	  			</table>
		 
		<?
	    	}
		?>
        
       
</article>


</section>
<section id="sidebar">
<h2>Twitter</h2>
<article class="testimonials">

<script src="http://widgets.twimg.com/j/2/widget.js"></script>
<script>
new TWTR.Widget({
  version: 2,
  type: 'search',
  search: 'from:3tierlogicrobc OR from:3tierlogicmarie OR from:3tierlogiccarlo OR from:3TierLogicABat OR from:3tierlogicDOM OR from:3TierLogicBlack OR from:3tierlogicJILL OR from:3tierlogicPaul OR from:3tierlogicsandi',
  interval: 6000,
  title: '3 Tier Logic Twitter Feeds',
  subject: 'LIVE',
  width: 280,
  height: 300,
  theme: {
    shell: {
       background: '#333333',
       color: '#ffffff'
    },
    tweets: {
      background: '#000000',
      color: '#ffffff',
      links: '#4aed05'
    }
  },
  features: {
    scrollbar: false,
    loop: true,
    live: true,
    hashtags: true,
    timestamp: false,
    avatars: true,
    toptweets: true,
    behavior: 'default'
  }
}).render().start();
</script>

</article>
</section>

<div class="clear"></div>
</section>
</div>
</section>
</div>
<footer id="pagefooter">
<div id="bottom">
<div class="block1">
<h2>Meet The Team</h2>

<div class="teamimg">

<? include("team.php")?>
<!--<div class="imgthmb"><img src="images/team5.jpg" width="65" height="65" alt="team"></div>
 <div class="imgthmb"><img src="images/team1.jpg" width="65" height="65" alt="team"></div>
  <div class="imgthmb"><img src="images/team2.jpg" width="65" height="65" alt="team"></div>
  <div class="imgthmb"><img src="images/team4.jpg" width="65" height="65" alt="team"></div>
  <div class="imgthmb"><img src="images/team2.jpg" width="65" height="65" alt="team"></div>
  <div class="imgthmb"><img src="images/team1.jpg" width="65" height="65" alt="team"></div>-->
</div>
</div>
<div class="block2">
<h2>Latest Project</h2>
<div class="projectthmb"> <a href="<?=$latestLink?>"><img src="<?=$latestImage?>" width="240" height="150" alt="project"></a><br><p style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#FFF"><?=$latestTitle?></p></div>

</div>
<div class="block3">

<h2>About</h2>
<p>3 Tier Logic is a full service digital marketing and technology services company that enables organizations to grow their customer and prospect communities through anywhere-anytime communications. </p>

</div>
<div class="clear"></div>
</div>
<div id="credits">
<p>
<span class="copyright">
&copy; 2012 | 3 Tier Logic Inc |  All Rights Reserved </span>

</div>
</footer>

</section>
</body>
</html>

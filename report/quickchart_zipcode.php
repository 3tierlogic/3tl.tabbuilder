<?php

require_once("db.php");
$con = mysql_connect($dbhost, $dbuser, $dbpasswd);		// connect to database
if (!$con) {											// error checking and handling
    die('Could not connect: ' . mysql_error());
}
mysql_select_db($dbname);


// add where condition to support date range.
function addCon($original_sql, $s, $e){

	// detect where;
	$pos = strpos($original_sql, 'where', 1); 
	if($pos == false){ // try capital again.
		$pos = strpos($original_sql, 'WHERE', 1);
	}
	
	$sub1 = substr($original_sql,0,$pos+1+4); // "select...where"
	//print "sub1: ".$sub1."<br/>";
	$sub2 = " cpresult_create_time between '".$s."' and '".$e."' and ";
	//print "sub2: ".$sub2."<br/>";
	$sub3 = substr($original_sql, $pos+1+4+1); // " ..."
	//print "sub3: ".$sub3."<br/>";
	
	$original_sql = "";
	$original_sql = $original_sql.$sub1;
	$original_sql = $original_sql.$sub2;
	$original_sql = $original_sql.$sub3;

	return $original_sql;
}

function nichi($raw){
	// 11/16 to 2011-11-16
	$done = str_replace('/',"-",$raw);
	//echo "done = ".$done."<br/>";
	$done = "2011-".$done;
	//echo "done = ".$done."<br/>";

	return $done;
}


//*** Constant
$checkboxStr = "OpenCheckBox";		
$dropdownStr = "OpenDropDown";
$google_maps_key='AIzaSyCX9oTRIVEhJGpDQGgQBNVJ_rg-ljKp6EQ'; //*** Google Map API key

// start of code.
$campaignID = $_REQUEST['cid'];
$sDate = $_REQUEST['sdate'];
$eDate = $_REQUEST['edate'];
$chartType = $_REQUEST['ctype'];
$countryCode = $_REQUEST['c'];

echo $chartType."<br>";
//*** Check chart type to produce
$pos_checkbox = strpos($chartType, $checkboxStr);
$pos_dropdown = strpos($chartType, $dropdownStr);

	if ($pos_checkbox === 0) {
		$chartType = $checkboxStr;
		echo "here";	
	}
	if ($pos_dropdown === 0) {
		$chartType = $dropdownStr;
		echo "here2";	
	}



switch ($chartType) {
	case "ZipCode":
		if ($countryCode == "CA") {
			//$sql_zipcode = "SELECT UCASE(SUBSTR(cpresult_zip_code, 1,3)) as location, count(*) as num FROM sp_campaign_result where campaign_sid ='".$campaignID."' group by SUBSTR(cpresult_zip_code, 1,3) order by num desc";
			$sql_zipcode = "SELECT UCASE(REPLACE(cpresult_zip_code, ' ','')) as location, count(*) as num FROM sp_campaign_result where campaign_sid ='".$campaignID."' group by UCASE(REPLACE(cpresult_zip_code,' ' ,'')) order by num desc";
		} else {
			$sql_zipcode = "SELECT UCASE(SUBSTR(cpresult_zip_code, 1,5)) as location, count(*) as num FROM sp_campaign_result where campaign_sid ='".$campaignID."' group by SUBSTR(cpresult_zip_code, 1,5) order by num desc";
		}
		$sql_zipcode = addCon($sql_zipcode, $sDate, $eDate); //*** Add the start and end dates to the SQL
		//echo "sql zipcode = ".$sql_zipcode."<br/>";
		$res_zipcode = mysql_query($sql_zipcode, $con);
		if(mysql_num_rows($res_zipcode) > 0){
			$firstZipCoord1 = "";
			$firstZipCoord2 = "";
			$count10 = 1; // initial var to count top 10.
			$counter = 0;
			$sumOthers = 0; // num of the rest.
			$sumTotal = 0; // total zip.
			$zipArray = array();
			$zipTotalArray = array();
			$zipcodestr_data = "";
			$zipcodestr_datapercent = "";
			$mapData = "";
			$colorArray = array("FF0000","FF4D4D","FF7070","FF8989","FFB0B0","FFC3C3","FFD4D4","FFDDDD","FFCBCB","FFF2F2");
			$tableHeight = mysql_num_rows($res_zipcode) * 160; 
			if ($tableHeight > 1000) {
				$tableHeight = 1100;
			}
			while(list($digit3, $num) = mysql_fetch_array($res_zipcode)){
				if($count10 <= 10){ // still in top 10.
					$zipcodestr_data = $zipcodestr_data."['".$digit3."', ".$num."],";
					$count10++;
					
					//*** Generate the map data to use with Google Map - START
					$url = "http://maps.google.com/maps/geo?q=".$digit3."&output=xml&key=".$google_maps_key;
					//echo $url."<br>";
					$xml = simplexml_load_file($url);
					$status = $xml->Response->Status->code;
					if ($status='200') { //address geocoded correct, show results
						foreach ($xml->Response->Placemark as $node) { // loop through the responses
							$address = $node->address;
							$quality = $node->AddressDetails['Accuracy'];
							$coordinates = $node->Point->coordinates;
							$coordinatesArray = explode(",", $coordinates );
							//echo ("Quality: $quality. $address. $coordinatesArray[1].$coordinatesArray[0]<br/>");
							$mapData = $mapData."['".$address."',".$coordinatesArray[1].",".$coordinatesArray[0].",".$num.",'".$colorArray[$counter]."'],";
						}
						if ($counter == 0) {
							$firstZipCoord1 = $coordinatesArray[1];	
							$firstZipCoord2 = $coordinatesArray[0];	
						}
					} else {
						$address = $digit3;
					} //*** Generate the map data to use with Google Map - END
				} else { // sum up offsides.
					$sumOthers += $num;
				}
				$sumTotal += $num;
				$zipArray[$counter] = $address;
				$zipTotalArray[$counter] = $num;
				$counter++;
			}
			if ($count10 > 10){
				$zipcodestr_data = $zipcodestr_data."{name:'Other', y:".$sumOthers.", sliced:true, selected:true},"; // add the rest to string.
			}
			$zipcodestr_data = substr($zipcodestr_data, 0, -1);
			$mapData = substr($mapData, 0, -1);

			
			//*** Get the % to plot
			/*$counter = 0;
			foreach ($zipTotalArray as $currTotalEntries) {
  				if ($counter < 10) {
					$zipcodestr_datapercent = $zipcodestr_datapercent."['".$zipArray[$counter]."', ".round((($currTotalEntries/$sumTotal)*100),2)."],";
					$counter++;
				}
			}
			$zipcodestr_datapercent = substr($zipcodestr_datapercent, 0, -1);*/
			
			/*echo $zipcodestr_data."<br />".$sumTotal."<br>";
			echo $zipcodestr_datapercent."<br />";
			echo "<pre>";
			print_r($zipArray);
			print_r($zipTotalArray);
			echo "</pre>";  */
			$displayzipcode_div=1;
			
			
		}
		break;
}



?>

<html>

<head>
<link href="style.css" rel="stylesheet" type="text/css" />
<style>
	div.panel{
		height:auto;
		display:none;
	}

	table#user-list tr.hover {
		color: #fff;
		background-color: #4c95e6;
	}	
	#containergraph {
		float: left
    	width: 33.3%;
		/* Min-height: */
		min-height: 400px;
		height: auto !important; 
		height: 400px;
	}
</style>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>
 <!-- Charts JS library: START -->
<script type="text/javascript" src="js/highcharts.js"></script>
<script type="text/javascript" src="js/themes/gray.js"></script>
<script type="text/javascript" src="js/modules/exporting.js"></script>
<!-- Charts JS library: END -->
 
 <style type="text/css">
      @import "DTmedia/css/demo_page.css";
      @import "DTmedia/css/demo_table.css";
</style>
<script class="jsbin" src="http://datatables.net/download/build/jquery.dataTables.js"></script>

<!-- ***** Google Maps:Start ***** -->
<!--<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCX9oTRIVEhJGpDQGgQBNVJ_rg-ljKp6EQ&sensor=false"></script>-->
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>

<script type="text/javascript"> 
function initialize() {
	// create the map
    var locations = [
      //['Bondi Beach', -33.890542, 151.274856, 4],
//      ['Coogee Beach', -33.923036, 151.259052, 5],
//      ['Cronulla Beach', -34.028249, 151.157507, 3],
//      ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
//      ['Maroubra Beach', -33.950198, 151.259302, 1]
		<?=$mapData?>
	];

    var map = new google.maps.Map(document.getElementById('map_canvas'), {
      zoom: 6,
      center: new google.maps.LatLng(<?=$firstZipCoord1?>,<?=$firstZipCoord2?>),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();
	var marker, i;

    for (i = 0; i < locations.length; i++) {  
			marker = new google.maps.Marker({
        	position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        	map: map,
			//icon: new google.maps.MarkerImage("http://chart.googleapis.com/chart?chst=d_bubble_text_small&chld=bb|"+locations[i][3]+"|FF8080|000000", null, null, new google.maps.Point(0, 42)),
      		//shadow: new google.maps.MarkerImage( "http://chart.googleapis.com/chart?chst=d_bubble_text_small_shadow&chld=bb|"+locations[i][3],null, null, new google.maps.Point(0, 45))
			icon: new google.maps.MarkerImage("http://chart.googleapis.com/chart?chst=d_map_pin_letter&chld="+locations[i][3]+"|"+locations[i][4]+"|000000", null, null, new google.maps.Point(0, 42)),
      		shadow: new google.maps.MarkerImage( "http://chart.googleapis.com/chart?chst=d_map_pin_letter&chld="+locations[i][3],null, null, new google.maps.Point(0, 45))
    	 });
	  
     	 google.maps.event.addListener(marker, 'click', (function(marker, i) {
        	return function() {
          		infowindow.setContent("<b>"+locations[i][0]+"</b> ("+locations[i][3]+")");
          		infowindow.open(map, marker);
        	}
      	})(marker, i));
    }
}

</script> 

    
<!-- ***** Google Maps:End ***** -->
    
<script>
var chart;
var field="<?=$chartType?>";
$(document).ready(function() {
	$('#example').dataTable({
        "bJQueryUI": true,
        <!--"sPaginationType": "full_numbers",-->
		"bPaginate": false,
		"bFilter": false,
		"bLengthChange": false,
		"bScrollInfinite": true,
        "bScrollCollapse": true,
        "sScrollY": "<?php echo $tableHeight ?>px"
    });
	
	
	switch(field){
		case "ZipCode":{	
  			chart = new Highcharts.Chart({
      				chart: {
         				renderTo: 'container',
         				plotBackgroundColor: null,
         				plotBorderWidth: null,
         				plotShadow: false
     				 },
      				title: {
         				text: 'zipcode breakdown'
      				},
      				tooltip: {
         				formatter: function() {
           				 	return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
							//return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
        				 }
      				},
      				plotOptions: {
         				pie: {
           				 allowPointSelect: true,
           				 cursor: 'pointer',
           				 dataLabels: {
              				enabled: true,
              				color: Highcharts.theme.textColor || '#000000',
               				connectorColor: Highcharts.theme.textColor || '#000000',
               				formatter: function() {
                  				//return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
								return '<b>'+ this.point.name +'</b>: '+ Highcharts.numberFormat(this.percentage, 2) +' %';
								
               				}
            			}
         			  }
      				},
       				series: [{
         				type: 'pie',
         				name: 'zipcode breakdown',
         				data: [
            				<?php echo $zipcodestr_data;?> 
         				]
      				}]
   			});
			break;
		}
		case "OpenCheckBox":{
			alert("here");
			chart = new Highcharts.Chart({
      				chart: {
        				renderTo: 'container',
         				plotBackgroundColor: null,
         				plotBorderWidth: null,
         				plotShadow: false
      				},
      				title: {
         				text: 'test checkbox'
      				},
      				tooltip: {
         				formatter: function() {
            				return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
         				}
      				},
      				plotOptions: {
         				pie: {
            				allowPointSelect: true,
            				cursor: 'pointer',
           					dataLabels: {
               					enabled: true,
               					color: Highcharts.theme.textColor || '#000000',
               					connectorColor: Highcharts.theme.textColor || '#000000',
               					formatter: function() {
                  					return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
               					}
            				}
         				}
      				},
       				series: [{
         				type: 'pie',
         				name: 'Browser share',
         				data: [
            				['Firefox',   45.0],
            				['IE',       26.8],
            					{
               						name: 'Chrome',    
               						y: 12.8,
               						sliced: true,
               						selected: true
            					},
            				['Safari',    8.5],
            				['Opera',     6.2],
            				['Others',   0.7]
         				]
      				}]
  			 });
			 break;	
		}
		default: {
			chart = new Highcharts.Chart({
      				chart: {
        				renderTo: 'container',
         				plotBackgroundColor: null,
         				plotBorderWidth: null,
         				plotShadow: false
      				},
      				title: {
         				text: 'Browser market shares at a specific website, 2010'
      				},
      				tooltip: {
         				formatter: function() {
            				return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
         				}
      				},
      				plotOptions: {
         				pie: {
            				allowPointSelect: true,
            				cursor: 'pointer',
           					dataLabels: {
               					enabled: true,
               					color: Highcharts.theme.textColor || '#000000',
               					connectorColor: Highcharts.theme.textColor || '#000000',
               					formatter: function() {
                  					return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
               					}
            				}
         				}
      				},
       				series: [{
         				type: 'pie',
         				name: 'Browser share',
         				data: [
            				['Firefox',   45.0],
            				['IE',       26.8],
            					{
               						name: 'Chrome',    
               						y: 12.8,
               						sliced: true,
               						selected: true
            					},
            				['Safari',    8.5],
            				['Opera',     6.2],
            				['Others',   0.7]
         				]
      				}]
  			 });	
		}
	} //*** Switch
}); //**** function
   


</script>



</head>
<body onLoad="initialize()">
<div id="container" style="width: 900px; height: 400px; margin: auto;"></div>
<div id="map_canvas" style="width: 900px; height: 400px; margin: auto; border-width: 2px; border-style: ridge; border-color: #666"></div>
<div id="tableContainer" style="width: 900px; height: 400px; margin: auto;" >
<br><br>
<?php

											
							
							echo "<h2>Campaign summary from ".$sDate." to ".$eDate." (Top 10 only)</h2>";
							echo "<table cellpadding='0' cellspacing='0' border='2' class='display' id='example'>"; //id='user-list'>"; 
							echo "<thead><tr><th>Item</th><th>Postal Code</th><th>Total Entries</th></tr></thead>";
							echo "<tbody>";
							
							$counter = 0;
							$t = 0;
						   	foreach ($zipArray as $ziplabel) {
								if ($counter < 10) { //** Display max 10 rows
									if($counter % 2 != 0){
							  			echo "<tr class='odd gradeX'>";
								 	}else{
							   			echo "<tr class='odd gradeA'>";
							 		 }
								 	echo "<td class='center'>".($counter+1)."</td>";
									echo "<td class='center'>".$ziplabel."</td>";
								 	echo "<td class='center'>".$zipTotalArray[$counter]."</a></td>";
								 	echo "</tr>";
									$t = $t + $zipTotalArray[$counter];
								}
								$counter++;
							}
							echo "<tr class='odd gradeA'>";
							echo "<td class='center'>11</td>";
							echo "<td class='center'><b>Total entries (top 10)</b></td>";
							echo "<td class='center'><b>".$t."</b></td>";
							echo "</tr>";
							echo "</tbody>";
							echo "</table>";
						     
						
						   
					?>
</div>
</body>

</html>
<?php

require_once("db.php");
$con = mysql_connect($dbhost, $dbuser, $dbpasswd);		// connect to database
if (!$con) {											// error checking and handling
    die('Could not connect: ' . mysql_error());
}
mysql_select_db($dbname);


// add where condition to support date range.
function addCon($original_sql, $s, $e){

	// detect where;
	$pos = strpos($original_sql, 'where', 1); 
	if($pos == false){ // try capital again.
		$pos = strpos($original_sql, 'WHERE', 1);
	}
	
	$sub1 = substr($original_sql,0,$pos+1+4); // "select...where"
	//print "sub1: ".$sub1."<br/>";
	$sub2 = " cpresult_create_time between '".$s."' and '".$e."' and ";
	//print "sub2: ".$sub2."<br/>";
	$sub3 = substr($original_sql, $pos+1+4+1); // " ..."
	//print "sub3: ".$sub3."<br/>";
	
	$original_sql = "";
	$original_sql = $original_sql.$sub1;
	$original_sql = $original_sql.$sub2;
	$original_sql = $original_sql.$sub3;

	return $original_sql;
}

function nichi($raw){
	// 11/16 to 2011-11-16
	$done = str_replace('/',"-",$raw);
	//echo "done = ".$done."<br/>";
	$done = "2011-".$done;
	//echo "done = ".$done."<br/>";

	return $done;
}

function getCACarrierName($carrier){
						
	switch($carrier){
		case "302651":{
			return "BELL";
		}
		case "302720":{
			return "ROGERS";
		}
		case "302655":{
			return "MTS";
		}
		case "302701":{
			return "NB TEL";
		}
		case "302657":{
			return "QUEBECTEL";
		}
		case "302654":{
			return "SASK TEL";
		}
		case "302653":{
			return "TELUS";
		}
		case "302001":{
			return "VIRGIN";
		}
		case "302370":{
			return "FIDO";
		}			
		default:{
			return $carrier;
		}
	}
}

function getUSCarrierName($carrier){ 
			
	switch($carrier){
		case "310980":{
			return "AT&T";
		}
		case "310190":{
			return "ALASKA WIRELESS";
		}
		case "310500":{
			return "ALLTEL";
		}
		case "310420":{
			return "CINCINNATI BELL";
		}
		case "310180":{
			return "CINGULAR";
		}
		case "310016":{
			return "CRICKET";
		}
		case "310560":{
			return "DOBSON";
		}
		case "316010":{
			return "NEXTEL";
		}
		case "316110":{
			return "SPRINT";
		}	
		case "310660":{
			return "TMOBILE USA";
		}
		case "310012":{
			return "VERIZON";
		}			
		default:{
			return $carrier;
		}
	}
}

// start of code.
$campaignID = $_REQUEST['cid'];
$sDate = $_REQUEST['sdate'];
$eDate = $_REQUEST['edate'];
$chartType = $_REQUEST['ctype'];
$countryCode = $_REQUEST['c'];

$sql_carrier = "SELECT cpresult_carrier as carrier, count(*) as num FROM sp_campaign_result where campaign_sid ='".$campaignID."' and cpresult_carrier != '' group by cpresult_carrier";
$sql_carrier = addCon($sql_carrier, $sDate, $eDate);
$res_carrier = mysql_query($sql_carrier, $con);						
if(mysql_num_rows($res_carrier) > 0){
	$carrierArray = array();
	$carrierTotalArray = array();
	$counter = 0;
	while(list($car, $num) = mysql_fetch_array($res_carrier)){
		if (strtoupper($countryCode) == "CA") {
			$car = getCACarrierName($car);
		} else {
			$car = getUSCarrierName($car);
		}
		$carrierstr_data = $carrierstr_data."['".$car."', ".$num."],";
		$carrierArray[$counter] = $car;
		$carrierTotalArray[$counter] = $num;
		$counter++;
	}
	$carrierstr_data = substr($carrierstr_data, 0, -1);
	
}
										
	



?>

<html>

<head>
<link href="style.css" rel="stylesheet" type="text/css" />
<style>
	div.panel{
		height:auto;
		display:none;
	}

	table#user-list tr.hover {
		color: #fff;
		background-color: #4c95e6;
	}	
	#containergraph {
		float: left
    	width: 33.3%;
		/* Min-height: */
		min-height: 400px;
		height: auto !important; 
		height: 400px;
	}
</style>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>
 <!-- Charts JS library: START -->
<script type="text/javascript" src="js/highcharts.js"></script>
<script type="text/javascript" src="js/themes/gray.js"></script>
<script type="text/javascript" src="js/modules/exporting.js"></script>
<!-- Charts JS library: END -->
 
 <style type="text/css">
      @import "DTmedia/css/demo_page.css";
      @import "DTmedia/css/demo_table.css";
</style>
<script class="jsbin" src="http://datatables.net/download/build/jquery.dataTables.js"></script>


    
<script>
var chart;
var field="<?=$chartType?>";
$(document).ready(function() {
	$('#example').dataTable({
        "bJQueryUI": true,
        <!--"sPaginationType": "full_numbers",-->
		"bPaginate": false,
		"bFilter": false,
		"bLengthChange": false,
		"bScrollInfinite": true,
        "bScrollCollapse": true,
        "sScrollY": "<?php echo $tableHeight ?>px"
    });
	
	
		
  			chart = new Highcharts.Chart({
      				chart: {
         				renderTo: 'container',
         				plotBackgroundColor: null,
         				plotBorderWidth: null,
         				plotShadow: false
     				 },
      				title: {
         				text: 'Mobile Carrier Breakdown'
      				},
      				tooltip: {
         				formatter: function() {
           				 	return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
							//return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
        				 }
      				},
      				plotOptions: {
         				pie: {
           				 allowPointSelect: true,
           				 cursor: 'pointer',
           				 dataLabels: {
              				enabled: true,
              				color: Highcharts.theme.textColor || '#000000',
               				connectorColor: Highcharts.theme.textColor || '#000000',
               				formatter: function() {
                  				//return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
								return '<b>'+ this.point.name +'</b>: '+ Highcharts.numberFormat(this.percentage, 2) +' %';
								
               				}
            			}
         			  }
      				},
       				series: [{
         				type: 'pie',
         				name: 'Mobile Carrier Breakdown',
         				data: [
            				<?php echo $carrierstr_data;?> 
         				]
      				}]
   			});
	
		
		
}); //**** function
   


</script>



</head>
<body>
<div id="container" style="width: 900px; height: 400px; margin: auto;"></div>
<div id="tableContainer" style="width: 900px; height: 400px; margin: auto;" >
<br><br><br><br>
<?php

											
							
							echo "<h2>Campaign summary from ".$sDate." to ".$eDate."</h2>";
							echo "<table cellpadding='0' cellspacing='0' border='2' class='display' id='example'>"; //id='user-list'>"; 
							echo "<thead><tr><th>Mobile Carrier</th><th>Option</th><th>Total Entries</th></tr></thead>";
							echo "<tbody>";
							
							$counter = 0;
							$t = 0;
						   	foreach ($carrierArray as $carrierlabel) {
								if($counter % 2 != 0){
							  		echo "<tr class='odd gradeX'>";
								 }else{
							   		echo "<tr class='odd gradeA'>";
							 	}
								echo "<td class='center'>".($counter+1)."</td>";
								echo "<td class='center'>".$carrierlabel."</td>";
								echo "<td class='center'>".$carrierTotalArray[$counter]."</td>";
								echo "</tr>";
								$t = $t + $carrierTotalArray[$counter];
								$counter++;
							}
							echo "<tr class='odd gradeA'>";
							echo "<td class='center'>".($counter+1)."</td>";
							echo "<td class='center'><b>Total entries</b></td>";
							echo "<td class='center'><b>".$t."</b></td>";
							echo "</tr>";
							echo "</tbody>";
							echo "</table>";
						     
						
						   
					?>
</div>
</body>

</html>
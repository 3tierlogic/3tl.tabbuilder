<?php

require_once("db.php");
$con = mysql_connect($dbhost, $dbuser, $dbpasswd);		// connect to database
if (!$con) {											// error checking and handling
    die('Could not connect: ' . mysql_error());
}
mysql_select_db($dbname);


// add where condition to support date range.
function addCon($original_sql, $s, $e){

	// detect where;
	$pos = strpos($original_sql, 'where', 1); 
	if($pos == false){ // try capital again.
		$pos = strpos($original_sql, 'WHERE', 1);
	}
	
	$sub1 = substr($original_sql,0,$pos+1+4); // "select...where"
	//print "sub1: ".$sub1."<br/>";
	$sub2 = " cpresult_create_time between '".$s."' and '".$e."' and ";
	//print "sub2: ".$sub2."<br/>";
	$sub3 = substr($original_sql, $pos+1+4+1); // " ..."
	//print "sub3: ".$sub3."<br/>";
	
	$original_sql = "";
	$original_sql = $original_sql.$sub1;
	$original_sql = $original_sql.$sub2;
	$original_sql = $original_sql.$sub3;

	return $original_sql;
}

function nichi($raw){
	// 11/16 to 2011-11-16
	$done = str_replace('/',"-",$raw);
	//echo "done = ".$done."<br/>";
	$done = "2011-".$done;
	//echo "done = ".$done."<br/>";

	return $done;
}


// start of code.
$campaignID = $_REQUEST['cid'];
$sDate = $_REQUEST['sdate'];
$eDate = $_REQUEST['edate'];
$chartType = $_REQUEST['ctype'];
$countryCode = $_REQUEST['c'];
$fieldLabel="";

//*** get the checkbox label
$sql = "SELECT cpfldcfg_label FROM sp_campaign_config where campaign_sid='".$campaignID."' and cpfldcfg_is_valid='1' AND cpfldcfg_var_name = '".$chartType."'";
$res = mysql_query($sql, $con);	
list($fieldLabel) = mysql_fetch_array($res);

$sql_dyn = "SELECT cpresult_".$chartType.", count(*) FROM sp_campaign_result s where campaign_sid ='".$campaignID."' group by cpresult_".$chartType;
$sql_dyn = addCon($sql_dyn, $sDate, $eDate);
//echo $sql_dyn;
$res_dyn = mysql_query($sql_dyn, $con);						
if(mysql_num_rows($res_dyn) > 0) {
	$checkboxArray = array();
	$checkboxTotalArray = array();
	$counter = 0;
	while(list($dyn, $num) = mysql_fetch_array($res_dyn)){
		if ($dyn == 1){
			$dyn = "Yes";
		} else if ($dyn == 0) {
			$dyn = "No";
		} else {
			$dyn = "No response";	
		}
		$dynstr_data = $dynstr_data."['".$dyn."', ".$num."],";
		$checkboxArray[$counter] = $dyn;
		$checkboxTotalArray[$counter] = $num;
		$counter++;
	}
	$dynstr_data = substr($dynstr_data, 0, -1);
}
echo $dynstr_data;							
	



?>

<html>

<head>
<link href="style.css" rel="stylesheet" type="text/css" />
<style>
	div.panel{
		height:auto;
		display:none;
	}

	table#user-list tr.hover {
		color: #fff;
		background-color: #4c95e6;
	}	
	#containergraph {
		float: left
    	width: 33.3%;
		/* Min-height: */
		min-height: 400px;
		height: auto !important; 
		height: 400px;
	}
</style>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>
 <!-- Charts JS library: START -->
<script type="text/javascript" src="js/highcharts.js"></script>
<script type="text/javascript" src="js/themes/gray.js"></script>
<script type="text/javascript" src="js/modules/exporting.js"></script>
<!-- Charts JS library: END -->
 
 <style type="text/css">
      @import "DTmedia/css/demo_page.css";
      @import "DTmedia/css/demo_table.css";
</style>
<script class="jsbin" src="http://datatables.net/download/build/jquery.dataTables.js"></script>


    
<script>
var chart;
var field="<?=$chartType?>";
$(document).ready(function() {
	$('#example').dataTable({
        "bJQueryUI": true,
        <!--"sPaginationType": "full_numbers",-->
		"bPaginate": false,
		"bFilter": false,
		"bLengthChange": false,
		"bScrollInfinite": true,
        "bScrollCollapse": true,
        "sScrollY": "<?php echo $tableHeight ?>px"
    });
	
	
		
  			chart = new Highcharts.Chart({
      				chart: {
         				renderTo: 'container',
         				plotBackgroundColor: null,
         				plotBorderWidth: null,
         				plotShadow: false
     				 },
      				title: {
         				text: '<?=$fieldLabel?>'
      				},
      				tooltip: {
         				formatter: function() {
           				 	return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
							//return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
        				 }
      				},
      				plotOptions: {
         				pie: {
           				 allowPointSelect: true,
           				 cursor: 'pointer',
           				 dataLabels: {
              				enabled: true,
              				color: Highcharts.theme.textColor || '#000000',
               				connectorColor: Highcharts.theme.textColor || '#000000',
               				formatter: function() {
                  				//return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
								return '<b>'+ this.point.name +'</b>: '+ Highcharts.numberFormat(this.percentage, 2) +' %';
								
               				}
            			}
         			  }
      				},
       				series: [{
         				type: 'pie',
         				name: '<?=$fieldLabel?>',
         				data: [
            				<?php echo $dynstr_data;?> 
         				]
      				}]
   			});
	
		
		
}); //**** function
   


</script>



</head>
<body>
<div id="container" style="width: 900px; height: 400px; margin: auto;"></div>
<div id="tableContainer" style="width: 900px; height: 400px; margin: auto;" >
<br><br><br><br>
<?php

											
							
							echo "<h2>Campaign summary from ".$sDate." to ".$eDate."</h2>";
							echo "<table cellpadding='0' cellspacing='0' border='2' class='display' id='example'>"; //id='user-list'>"; 
							echo "<thead><tr><th>Item</th><th>Option</th><th>Total Entries</th></tr></thead>";
							echo "<tbody>";
							
							$counter = 0;
							$t = 0;
						   	foreach ($checkboxArray as $checkboxlabel) {
								if($counter % 2 != 0){
							  		echo "<tr class='odd gradeX'>";
								 }else{
							   		echo "<tr class='odd gradeA'>";
							 	}
								echo "<td class='center'>".($counter+1)."</td>";
								echo "<td class='center'>".$checkboxlabel."</td>";
								echo "<td class='center'>".$checkboxTotalArray[$counter]."</td>";
								echo "</tr>";
								$t = $t + $checkboxTotalArray[$counter];
								$counter++;
							}
							echo "<tr class='odd gradeA'>";
							echo "<td class='center'>".($counter+1)."</td>";
							echo "<td class='center'><b>Total entries</b></td>";
							echo "<td class='center'><b>".$t."</b></td>";
							echo "</tr>";
							echo "</tbody>";
							echo "</table>";
						     
						
						   
					?>
</div>
</body>

</html>

function showchart(field){
	alert(field);
	
	switch(field){
		default:{			
			$(function() {
				var chart_gender;
				chart_gender = new Highcharts.Chart({
							chart: {
								renderTo: 'containergraph',
								plotBackgroundColor: null,
								plotBorderWidth: null,
								plotShadow: false
							 },
							title: {
								text: 'Gender breakdown'
							},
							tooltip: {
								formatter: function() {
									return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
									//return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
								 }
							},
							plotOptions: {
								pie: {
								 allowPointSelect: true,
								 cursor: 'pointer',
								 dataLabels: {
									enabled: true,
									color: Highcharts.theme.textColor || '#000000',
									connectorColor: Highcharts.theme.textColor || '#000000',
									formatter: function() {
										// return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
										return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
									}
								}
							  }
							},
							series: [{
								type: 'pie',
								name: 'Gender breakdown',
								data: [
									<?php echo $genderstr_data;?>
								]
							}]
				});
				
			});
			break;
		}
		
	}
	
}
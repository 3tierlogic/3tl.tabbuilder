<?php
require_once("db.php");
$con = mysql_connect($dbhost, $dbuser, $dbpasswd);		// connect to database
if (!$con) {											// error checking and handling
    die('Could not connect: ' . mysql_error());
}
mysql_select_db($dbname);


// add where condition to support date range.
function addCon($original_sql, $s, $e){

	// detect where;
	$pos = strpos($original_sql, 'where', 1); 
	if($pos == false){ // try capital again.
		$pos = strpos($original_sql, 'WHERE', 1);
	}
	
	$sub1 = substr($original_sql,0,$pos+1+4); // "select...where"
	//print "sub1: ".$sub1."<br/>";
	$sub2 = " cpresult_create_time between '".$s."' and '".$e."' and ";
	//print "sub2: ".$sub2."<br/>";
	$sub3 = substr($original_sql, $pos+1+4+1); // " ..."
	//print "sub3: ".$sub3."<br/>";
	
	$original_sql = "";
	$original_sql = $original_sql.$sub1;
	$original_sql = $original_sql.$sub2;
	$original_sql = $original_sql.$sub3;

	return $original_sql;
}

function nichi($raw){
	// 11/16 to 2011-11-16
	$done = str_replace('/',"-",$raw);
	//echo "done = ".$done."<br/>";
	$done = "2011-".$done;
	//echo "done = ".$done."<br/>";

	return $done;
}

function codeToCountry($currentCountryArray, $currentCode) {
	$codeFound = false;
	$counter = 0;
	$totalcountry = count($currentCountryArray);
	$thisCode = "Not specified";
	
	if ($currentCode !='') {
		while (($codeFound == false) && ($counter <= $totalcountry)) {
			if ($currentCountryArray[$counter][0] == $currentCode) {
				$codeFound = true;
				$thisCode = $currentCountryArray[$counter][1];
				//echo $counter."-".$thisCode."<br>";
			} 
			$counter++;
		}
	}
	return $thisCode;
	
}

//***** Constants declaration
$google_maps_key='AIzaSyCX9oTRIVEhJGpDQGgQBNVJ_rg-ljKp6EQ'; //*** Google Map API key

/*$arrayStates_us[] = array( '001', 'EN','US', 'Alabama');
$arrayStates_us[] = array( '002', 'EN','US', 'Alaska');
$arrayStates_us[] = array( '003', 'EN','US', 'Arizona');
$arrayStates_us[] = array( '004', 'EN','US', 'Arkansas');
$arrayStates_us[] = array( '005', 'EN','US', 'California');
$arrayStates_us[] = array( '006', 'EN','US', 'Colorado');
$arrayStates_us[] = array( '007', 'EN','US', 'Connecticut');
$arrayStates_us[] = array( '008', 'EN','US', 'Delaware');
$arrayStates_us[] = array( '009', 'EN','US', 'District of Columbia');
$arrayStates_us[] = array( '010', 'EN','US', 'Florida');
$arrayStates_us[] = array( '011', 'EN','US', 'Georgia');
$arrayStates_us[] = array( '012', 'EN','US', 'Hawaii');
$arrayStates_us[] = array( '013', 'EN','US', 'Idaho');
$arrayStates_us[] = array( '014', 'EN','US', 'Illinois');
$arrayStates_us[] = array( '015', 'EN','US', 'Indiana');
$arrayStates_us[] = array( '016', 'EN','US', 'Iowa');
$arrayStates_us[] = array( '017', 'EN','US', 'Kansas');
$arrayStates_us[] = array( '018', 'EN','US', 'Kentucky');
$arrayStates_us[] = array( '019', 'EN','US', 'Louisiana');
$arrayStates_us[] = array( '020', 'EN','US', 'Maine');
$arrayStates_us[] = array( '021', 'EN','US', 'Maryland');
$arrayStates_us[] = array( '022', 'EN','US', 'Massachusetts');
$arrayStates_us[] = array( '023', 'EN','US', 'Michigan');
$arrayStates_us[] = array( '024', 'EN','US', 'Minnesota');
$arrayStates_us[] = array( '025', 'EN','US', 'Mississippi');
$arrayStates_us[] = array( '026', 'EN','US', 'Missouri');
$arrayStates_us[] = array( '027', 'EN','US', 'Montana');
$arrayStates_us[] = array( '028', 'EN','US', 'Nebraska');
$arrayStates_us[] = array( '029', 'EN','US', 'Nevada');
$arrayStates_us[] = array( '030', 'EN','US', 'New Hampshire');
$arrayStates_us[] = array( '031', 'EN','US', 'New Jersey');
$arrayStates_us[] = array( '032', 'EN','US', 'New Mexico');
$arrayStates_us[] = array( '033', 'EN','US', 'New York');
$arrayStates_us[] = array( '034', 'EN','US', 'North Carolina');
$arrayStates_us[] = array( '035', 'EN','US', 'North Dakota');
$arrayStates_us[] = array( '036', 'EN','US', 'Ohio');
$arrayStates_us[] = array( '037', 'EN','US', 'Oklahoma');
$arrayStates_us[] = array( '038', 'EN','US', 'Oregon');
$arrayStates_us[] = array( '039', 'EN','US', 'Pennsylvania');
$arrayStates_us[] = array( '040', 'EN','US', 'Rhode Island');
$arrayStates_us[] = array( '041', 'EN','US', 'South Carolina');
$arrayStates_us[] = array( '042', 'EN','US', 'South Dakota');
$arrayStates_us[] = array( '043', 'EN','US', 'Tennessee');
$arrayStates_us[] = array( '044', 'EN','US', 'Texas');
$arrayStates_us[] = array( '045', 'EN','US', 'Utah');
$arrayStates_us[] = array( '046', 'EN','US', 'Vermont');
$arrayStates_us[] = array( '047', 'EN','US', 'Virginia');
$arrayStates_us[] = array( '048', 'EN','US', 'Washington');
$arrayStates_us[] = array( '049', 'EN','US', 'West Virginia');
$arrayStates_us[] = array( '050', 'EN','US', 'Wisconsin');
$arrayStates_us[] = array( '051', 'EN','US', 'Wyoming');

$arrayStates_ca[] = array( '001', 'EN', 'CA', 'Alberta');
$arrayStates_ca[] = array( '002', 'EN', 'CA', 'British Columbia');
$arrayStates_ca[] = array( '003', 'EN', 'CA', 'Manitoba');
$arrayStates_ca[] = array( '004', 'EN', 'CA', 'New Brunswick');
$arrayStates_ca[] = array( '005', 'EN', 'CA', 'Newfoundland and Labrador');
$arrayStates_ca[] = array( '006', 'EN', 'CA', 'Nova Scotia');
$arrayStates_ca[] = array( '007', 'EN', 'CA', 'Northwest Territories');
$arrayStates_ca[] = array( '008', 'EN', 'CA', 'Nunavut');
$arrayStates_ca[] = array( '009', 'EN', 'CA', 'Ontario');
$arrayStates_ca[] = array( '010', 'EN', 'CA', 'Prince Edward Island');
$arrayStates_ca[] = array( '011', 'EN', 'CA', 'Quebec');
$arrayStates_ca[] = array( '012', 'EN', 'CA', 'Saskatchewan');
$arrayStates_ca[] = array( '013', 'EN', 'CA', 'Yukon');*/


$arrayStates_us[] = array( 'AF','Afghanistan');
					$arrayStates_us[] = array( 'AL','Albania');
					$arrayStates_us[] = array( 'DZ','Algeria');
					$arrayStates_us[] = array( 'AS','American Samoa');
					$arrayStates_us[] = array( 'AD','Andorra');
					$arrayStates_us[] = array( 'AO','Angola');
					$arrayStates_us[] = array( 'AI','Anguilla');
					$arrayStates_us[] = array( 'AQ','Antarctica');
					$arrayStates_us[] = array( 'AG','Antigua and Barbuda');
					$arrayStates_us[] = array( 'AR','Argentina');
					$arrayStates_us[] = array( 'AM','Armenia');
					$arrayStates_us[] = array( 'AW','Aruba');
					$arrayStates_us[] = array( 'AU','Australia');
					$arrayStates_us[] = array( 'AT','Austria');
					$arrayStates_us[] = array( 'AZ','Azerbaijan');
					$arrayStates_us[] = array( 'BS','Bahamas');
					$arrayStates_us[] = array( 'BH','Bahrain');
					$arrayStates_us[] = array( 'BD','Bangladesh');
					$arrayStates_us[] = array( 'BB','Barbados');
					$arrayStates_us[] = array( 'BY','Belarus');
					$arrayStates_us[] = array( 'BE','Belgium');
					$arrayStates_us[] = array( 'BZ','Belize');
					$arrayStates_us[] = array( 'BJ','Benin');
					$arrayStates_us[] = array( 'BM','Bermuda');
					$arrayStates_us[] = array( 'BT','Bhutan');
					$arrayStates_us[] = array( 'BO','Bolivia');
					$arrayStates_us[] = array( 'BA','Bosnia and Herzegovina');
					$arrayStates_us[] = array( 'BW','Botswana');
					$arrayStates_us[] = array( 'BV','Bouvet Island');
					$arrayStates_us[] = array( 'BR','Brazil');
					$arrayStates_us[] = array( 'IO','British Indian Ocean Territory');
					$arrayStates_us[] = array( 'BN','Brunei Darussalam');
					$arrayStates_us[] = array( 'BG','Bulgaria');
					$arrayStates_us[] = array( 'BF','Burkina Faso');
					$arrayStates_us[] = array( 'BI','Burundi');
					$arrayStates_us[] = array( 'KH','Cambodia');
					$arrayStates_us[] = array( 'CM','Cameroon');
					$arrayStates_us[] = array( 'CA','Canada');
					$arrayStates_us[] = array( 'CV','Cape Verde');
					$arrayStates_us[] = array( 'KY','Cayman Islands');
					$arrayStates_us[] = array( 'CF','Central African Republic');
					$arrayStates_us[] = array( 'TD','Chad');
					$arrayStates_us[] = array( 'CL','Chile');
					$arrayStates_us[] = array( 'CN','China');
					$arrayStates_us[] = array( 'CX','Christmas Island');
					$arrayStates_us[] = array( 'CC','Cocos (Keeling) Islands');
					$arrayStates_us[] = array( 'CO','Colombia');
					$arrayStates_us[] = array( 'KM','Comoros');
					$arrayStates_us[] = array( 'CG','Congo');
					$arrayStates_us[] = array( 'CD','Congo, the Democratic Republic of the');
					$arrayStates_us[] = array( 'CK','Cook Islands');
					$arrayStates_us[] = array( 'CR','Costa Rica');
					$arrayStates_us[] = array( 'CI','Cote DIvoire');
					$arrayStates_us[] = array( 'HR','Croatia');
					$arrayStates_us[] = array( 'CU','Cuba');
					$arrayStates_us[] = array( 'CY','Cyprus');
					$arrayStates_us[] = array( 'CZ','Czech Republic');
					$arrayStates_us[] = array( 'DK','Denmark');
					$arrayStates_us[] = array( 'DJ','Djibouti');
					$arrayStates_us[] = array( 'DM','Dominica');
					$arrayStates_us[] = array( 'DO','Dominican Republic');
					$arrayStates_us[] = array( 'EC','Ecuador');
					$arrayStates_us[] = array( 'EG','Egypt');
					$arrayStates_us[] = array( 'SV','El Salvador');
					$arrayStates_us[] = array( 'GQ','Equatorial Guinea');
					$arrayStates_us[] = array( 'ER','Eritrea');
					$arrayStates_us[] = array( 'EE','Estonia');
					$arrayStates_us[] = array( 'ET','Ethiopia');
					$arrayStates_us[] = array( 'FK','Falkland Islands (Malvinas)');
					$arrayStates_us[] = array( 'FO','Faroe Islands');
					$arrayStates_us[] = array( 'FJ','Fiji');
					$arrayStates_us[] = array( 'FI','Finland');
					$arrayStates_us[] = array( 'FR','France');
					$arrayStates_us[] = array( 'GF','French Guiana');
					$arrayStates_us[] = array( 'PF','French Polynesia');
					$arrayStates_us[] = array( 'TF','French Southern Territories');
					$arrayStates_us[] = array( 'GA','Gabon');
					$arrayStates_us[] = array( 'GM','Gambia');
					$arrayStates_us[] = array( 'GE','Georgia');
					$arrayStates_us[] = array( 'DE','Germany');
					$arrayStates_us[] = array( 'GH','Ghana');
					$arrayStates_us[] = array( 'GI','Gibraltar');
					$arrayStates_us[] = array( 'GR','Greece');
					$arrayStates_us[] = array( 'GL','Greenland');
					$arrayStates_us[] = array( 'GD','Grenada');
					$arrayStates_us[] = array( 'GP','Guadeloupe');
					$arrayStates_us[] = array( 'GU','Guam');
					$arrayStates_us[] = array( 'GT','Guatemala');
					$arrayStates_us[] = array( 'GN','Guinea');
					$arrayStates_us[] = array( 'GW','Guinea-Bissau');
					$arrayStates_us[] = array( 'GY','Guyana');
					$arrayStates_us[] = array( 'HT','Haiti');
					$arrayStates_us[] = array( 'HM','Heard Island and Mcdonald Islands');
					$arrayStates_us[] = array( 'VA','Holy See (Vatican City State)');
					$arrayStates_us[] = array( 'HN','Honduras');
					$arrayStates_us[] = array( 'HK','Hong Kong');
					$arrayStates_us[] = array( 'HU','Hungary');
					$arrayStates_us[] = array( 'IS','Iceland');
					$arrayStates_us[] = array( 'IN','India');
					$arrayStates_us[] = array( 'ID','Indonesia');
					$arrayStates_us[] = array( 'IR','Iran, Islamic Republic of');
					$arrayStates_us[] = array( 'IQ','Iraq');
					$arrayStates_us[] = array( 'IE','Ireland');
					$arrayStates_us[] = array( 'IL','Israel');
					$arrayStates_us[] = array( 'IT','Italy');
					$arrayStates_us[] = array( 'JM','Jamaica');
					$arrayStates_us[] = array( 'JP','Japan');
					$arrayStates_us[] = array( 'JO','Jordan');
					$arrayStates_us[] = array( 'KZ','Kazakhstan');
					$arrayStates_us[] = array( 'KE','Kenya');
					$arrayStates_us[] = array( 'KI','Kiribati');
					$arrayStates_us[] = array( 'KP','Korea, Democratic People Republic of');
					$arrayStates_us[] = array( 'KR','Korea, Republic of');
					$arrayStates_us[] = array( 'KW','Kuwait');
					$arrayStates_us[] = array( 'KG','Kyrgyzstan');
					$arrayStates_us[] = array( 'LA','Lao People Democratic Republic');
					$arrayStates_us[] = array( 'LV','Latvia');
					$arrayStates_us[] = array( 'LB','Lebanon');
					$arrayStates_us[] = array( 'LS','Lesotho');
					$arrayStates_us[] = array( 'LR','Liberia');
					$arrayStates_us[] = array( 'LY','Libyan Arab Jamahiriya');
					$arrayStates_us[] = array( 'LI','Liechtenstein');
					$arrayStates_us[] = array( 'LT','Lithuania');
					$arrayStates_us[] = array( 'LU','Luxembourg');
					$arrayStates_us[] = array( 'MO','Macao');
					$arrayStates_us[] = array( 'MK','Macedonia, the Former Yugoslav Republic of');
					$arrayStates_us[] = array( 'MG','Madagascar');
					$arrayStates_us[] = array( 'MW','Malawi');
					$arrayStates_us[] = array( 'MY','Malaysia');
					$arrayStates_us[] = array( 'MV','Maldives');
					$arrayStates_us[] = array( 'ML','Mali');
					$arrayStates_us[] = array( 'MT','Malta');
					$arrayStates_us[] = array( 'MH','Marshall Islands');
					$arrayStates_us[] = array( 'MQ','Martinique');
					$arrayStates_us[] = array( 'MR','Mauritania');
					$arrayStates_us[] = array( 'MU','Mauritius');
					$arrayStates_us[] = array( 'YT','Mayotte');
					$arrayStates_us[] = array( 'MX','Mexico');
					$arrayStates_us[] = array( 'FM','Micronesia, Federated States of');
					$arrayStates_us[] = array( 'MD','Moldova, Republic of');
					$arrayStates_us[] = array( 'MC','Monaco');
					$arrayStates_us[] = array( 'MN','Mongolia');
					$arrayStates_us[] = array( 'MS','Montserrat');
					$arrayStates_us[] = array( 'MA','Morocco');
					$arrayStates_us[] = array( 'MZ','Mozambique');
					$arrayStates_us[] = array( 'MM','Myanmar');
					$arrayStates_us[] = array( 'NA','Namibia');
					$arrayStates_us[] = array( 'NR','Nauru');
					$arrayStates_us[] = array( 'NP','Nepal');
					$arrayStates_us[] = array( 'NL','Netherlands');
					$arrayStates_us[] = array( 'AN','Netherlands Antilles');
					$arrayStates_us[] = array( 'NC','New Caledonia');
					$arrayStates_us[] = array( 'NZ','New Zealand');
					$arrayStates_us[] = array( 'NI','Nicaragua');
					$arrayStates_us[] = array( 'NE','Niger');
					$arrayStates_us[] = array( 'NG','Nigeria');
					$arrayStates_us[] = array( 'NU','Niue');
					$arrayStates_us[] = array( 'NF','Norfolk Island');
					$arrayStates_us[] = array( 'MP','Northern Mariana Islands');
					$arrayStates_us[] = array( 'NO','Norway');
					$arrayStates_us[] = array( 'OM','Oman');
					$arrayStates_us[] = array( 'PK','Pakistan');
					$arrayStates_us[] = array( 'PW','Palau');
					$arrayStates_us[] = array( 'PS','Palestinian Territory, Occupied');
					$arrayStates_us[] = array( 'PA','Panama');
					$arrayStates_us[] = array( 'PG','Papua New Guinea');
					$arrayStates_us[] = array( 'PY','Paraguay');
					$arrayStates_us[] = array( 'PE','Peru');
					$arrayStates_us[] = array( 'PH','Philippines');
					$arrayStates_us[] = array( 'PN','Pitcairn');
					$arrayStates_us[] = array( 'PL','Poland');
					$arrayStates_us[] = array( 'PT','Portugal');
					$arrayStates_us[] = array( 'PR','Puerto Rico');
					$arrayStates_us[] = array( 'QA','Qatar');
					$arrayStates_us[] = array( 'RE','Reunion');
					$arrayStates_us[] = array( 'RO','Romania');
					$arrayStates_us[] = array( 'RU','Russian Federation');
					$arrayStates_us[] = array( 'RW','Rwanda');
					$arrayStates_us[] = array( 'SH','Saint Helena');
					$arrayStates_us[] = array( 'KN','Saint Kitts and Nevis');
					$arrayStates_us[] = array( 'LC','Saint Lucia');
					$arrayStates_us[] = array( 'PM','Saint Pierre and Miquelon');
					$arrayStates_us[] = array( 'VC','Saint Vincent and the Grenadines');
					$arrayStates_us[] = array( 'WS','Samoa');
					$arrayStates_us[] = array( 'SM','San Marino');
					$arrayStates_us[] = array( 'ST','Sao Tome and Principe');
					$arrayStates_us[] = array( 'SA','Saudi Arabia');
					$arrayStates_us[] = array( 'SN','Senegal');
					$arrayStates_us[] = array( 'CS','Serbia and Montenegro');
					$arrayStates_us[] = array( 'SC','Seychelles');
					$arrayStates_us[] = array( 'SL','Sierra Leone');
					$arrayStates_us[] = array( 'SG','Singapore');
					$arrayStates_us[] = array( 'SK','Slovakia');
					$arrayStates_us[] = array( 'SI','Slovenia');
					$arrayStates_us[] = array( 'SB','Solomon Islands');
					$arrayStates_us[] = array( 'SO','Somalia');
					$arrayStates_us[] = array( 'ZA','South Africa');
					$arrayStates_us[] = array( 'GS','South Georgia and the South Sandwich Islands');
					$arrayStates_us[] = array( 'ES','Spain');
					$arrayStates_us[] = array( 'LK','Sri Lanka');
					$arrayStates_us[] = array( 'SD','Sudan');
					$arrayStates_us[] = array( 'SR','Suriname');
					$arrayStates_us[] = array( 'SJ','Svalbard and Jan Mayen');
					$arrayStates_us[] = array( 'SZ','Swaziland');
					$arrayStates_us[] = array( 'SE','Sweden');
					$arrayStates_us[] = array( 'CH','Switzerland');
					$arrayStates_us[] = array( 'SY','Syrian Arab Republic');
					$arrayStates_us[] = array( 'TW','Taiwan, Province of China');
					$arrayStates_us[] = array( 'TJ','Tajikistan');
					$arrayStates_us[] = array( 'TZ','Tanzania, United Republic of');
					$arrayStates_us[] = array( 'TH','Thailand');
					$arrayStates_us[] = array( 'TL','Timor-Leste');
					$arrayStates_us[] = array( 'TG','Togo');
					$arrayStates_us[] = array( 'TK','Tokelau');
					$arrayStates_us[] = array( 'TO','Tonga');
					$arrayStates_us[] = array( 'TT','Trinidad and Tobago');
					$arrayStates_us[] = array( 'TN','Tunisia');
					$arrayStates_us[] = array( 'TR','Turkey');
					$arrayStates_us[] = array( 'TM','Turkmenistan');
					$arrayStates_us[] = array( 'TC','Turks and Caicos Islands');
					$arrayStates_us[] = array( 'TV','Tuvalu');
					$arrayStates_us[] = array( 'UG','Uganda');
					$arrayStates_us[] = array( 'UA','Ukraine');
					$arrayStates_us[] = array( 'AE','United Arab Emirates');
					$arrayStates_us[] = array( 'GB','United Kingdom');
					$arrayStates_us[] = array( 'US','United States');
					$arrayStates_us[] = array( 'UM','United States Minor Outlying Islands');
					$arrayStates_us[] = array( 'UY','Uruguay');
					$arrayStates_us[] = array( 'UZ','Uzbekistan');
					$arrayStates_us[] = array( 'VU','Vanuatu');
					$arrayStates_us[] = array( 'VE','Venezuela');
					$arrayStates_us[] = array( 'VN','Viet Nam');
					$arrayStates_us[] = array( 'VG','Virgin Islands, British');
					$arrayStates_us[] = array( 'VI','Virgin Islands, U.s.');
					$arrayStates_us[] = array( 'WF','Wallis and Futuna');
					$arrayStates_us[] = array( 'EH','Western Sahara');
					$arrayStates_us[] = array( 'YE','Yemen');
					$arrayStates_us[] = array( 'ZM','Zambia');
					$arrayStates_us[] = array( 'ZW','Zimbabwe');
// start of code.
$campaignID = $_REQUEST['cid'];
$sDate = $_REQUEST['sdate'];
$eDate = $_REQUEST['edate'];
$chartType = $_REQUEST['ctype'];
/*$countryCode = $_REQUEST['c'];

if (strtoupper($countryCode) == "CA") {
	$arrayStates = $arrayStates_ca;
	$countryLabel = "Canada";
} else {
	$arrayStates = $arrayStates_us;
	$countryLabel = "United States";
}*/

/*echo "<pre>";
print_r($arrayStates);
echo "</pre>";*/

switch ($chartType) {
	case "country":
		$countrystr_data="";				
		$sql_country = "SELECT cpresult_country_sid, count(*) as num FROM sp_campaign_result where campaign_sid='".$campaignID."' group by cpresult_country_sid order by num desc";
		$sql_country = addCon($sql_country, $sDate, $eDate); //*** Add the start and end dates to the SQL
		//echo $sql_country;
		$res_country = mysql_query($sql_country, $con); 
		if (mysql_num_rows($res_country) > 0) {
			$countryArray = array();
			$countryTotalArray = array();
			$counter = 0;
			while (list($country, $totalcountry) = mysql_fetch_array($res_country)) {
				//echo $country.":".$totalcountry."<br>";
				//$currcountry = codeToState($arrayStates, $country);
				$currcountry = codeToCountry($arrayStates_us, $country);
				//echo $currcountry."<br>";								
				$countrystr_data = $countrystr_data."['".$currcountry."', ".$totalcountry."],";
				$countryArray[$counter] = $currcountry;
				$countryTotalArray[$counter] = $totalcountry;
				
				//*** Generate the map data to use with Google Map - START
				$url = "http://maps.google.com/maps/geo?q=".$currcountry."&output=xml&key=".$google_maps_key;
				//echo $url;
				$xml = simplexml_load_file($url);
				$status = $xml->Response->Status->code;
				if ($status='200') { //address geocoded correct, show results
					$placemarkcounter = 0;
					foreach ($xml->Response->Placemark as $node) { // loop through the responses
						if ($placemarkcounter == 0) {
							$address = $node->address;
							$quality = $node->AddressDetails['Accuracy'];
							$coordinates = $node->Point->coordinates;
							$coordinatesArray = explode(",", $coordinates );
							$mapData = $mapData."['".$address."',".$coordinatesArray[1].",".$coordinatesArray[0].",".$totalcountry."],";
							$placemarkcounter++;
						} else {
							break;// quit. we don't need more than one placemark.
						}
					}
					if ($counter == 0) {
						$firstZipCoord1 = $coordinatesArray[1];	
						$firstZipCoord2 = $coordinatesArray[0];	
					}
				} else {
					$address = "Cannot find Country";
				} //*** Generate the map data to use with Google Map - END
					
					
				$counter++;
			}
		}						
		$countrystr_data = substr($countrystr_data, 0, -1);
		//echo $mapData;
	break;
}



?>

<html>

<head>
<link href="style.css" rel="stylesheet" type="text/css" />
<style>
	div.panel{
		height:auto;
		display:none;
	}

	table#user-list tr.hover {
		color: #fff;
		background-color: #4c95e6;
	}	
	#containergraph {
		float: left
    	width: 33.3%;
		/* Min-height: */
		min-height: 400px;
		height: auto !important; 
		height: 400px;
	}
</style>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>
 <!-- Charts JS library: START -->
<script type="text/javascript" src="js/highcharts.js"></script>
<script type="text/javascript" src="js/themes/gray.js"></script>
<script type="text/javascript" src="js/modules/exporting.js"></script>
<!-- Charts JS library: END -->
 
 <style type="text/css">
      @import "DTmedia/css/demo_page.css";
      @import "DTmedia/css/demo_table.css";
</style>
<script class="jsbin" src="http://datatables.net/download/build/jquery.dataTables.js"></script>

<!-- ***** Google Maps:Start ***** -->
<!--<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCX9oTRIVEhJGpDQGgQBNVJ_rg-ljKp6EQ&sensor=false"></script>-->
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>

<script type="text/javascript"> 
function initialize() {
	// create the map
    var locations = [
      //['Bondi Beach', -33.890542, 151.274856, 4],
//      ['Coogee Beach', -33.923036, 151.259052, 5],
//      ['Cronulla Beach', -34.028249, 151.157507, 3],
//      ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
//      ['Maroubra Beach', -33.950198, 151.259302, 1]
		<?=$mapData?>
	];

    var map = new google.maps.Map(document.getElementById('map_canvas'), {
      zoom: 3,
      center: new google.maps.LatLng(<?=$firstZipCoord1?>,<?=$firstZipCoord2?>),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();
	var marker, i;

    for (i = 0; i < locations.length; i++) {  
			marker = new google.maps.Marker({
        	position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        	map: map
    	 });
	  
     	 google.maps.event.addListener(marker, 'click', (function(marker, i) {
        	return function() {
          		infowindow.setContent("<b>"+locations[i][0]+"</b> ("+locations[i][3]+")");
          		infowindow.open(map, marker);
        	}
      	})(marker, i));
    }
}

</script> 

    
<!-- ***** Google Maps:End ***** -->
    
<script>
var chart;
var field="<?=$chartType?>";
$(document).ready(function() {
	$('#example').dataTable({
        "bJQueryUI": true,
        <!--"sPaginationType": "full_numbers",-->
		"bPaginate": false,
		"bFilter": false,
		"bLengthChange": false,
		"bScrollInfinite": true,
        "bScrollCollapse": true,
        "sScrollY": "<?php echo $tableHeight ?>px"
    });
	
	
	switch(field){
		case "country":{	
  			chart = new Highcharts.Chart({
      				chart: {
         				renderTo: 'container',
         				plotBackgroundColor: null,
         				plotBorderWidth: null,
         				plotShadow: false
     				 },
      				title: {
         				text: 'Country breakdown'
      				},
      				tooltip: {
         				formatter: function() {
           				 	return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
							//return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
        				 }
      				},
      				plotOptions: {
         				pie: {
           				 allowPointSelect: true,
           				 cursor: 'pointer',
           				 dataLabels: {
              				enabled: true,
              				color: Highcharts.theme.textColor || '#000000',
               				connectorColor: Highcharts.theme.textColor || '#000000',
               				formatter: function() {
                  				//return '<b>'+ this.point.name +'</b>: '+ this.y +' entries';
								return '<b>'+ this.point.name +'</b>: '+ Highcharts.numberFormat(this.percentage, 2) +' %';
								
               				}
            			}
         			  }
      				},
       				series: [{
         				type: 'pie',
         				name: 'Country breakdown',
         				data: [
            				<?php echo $countrystr_data;?> 
         				]
      				}]
   			});
			break;
		}
		
		default: {
			chart = new Highcharts.Chart({
      				chart: {
        				renderTo: 'container',
         				plotBackgroundColor: null,
         				plotBorderWidth: null,
         				plotShadow: false
      				},
      				title: {
         				text: 'Browser market shares at a specific website, 2010'
      				},
      				tooltip: {
         				formatter: function() {
            				return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
         				}
      				},
      				plotOptions: {
         				pie: {
            				allowPointSelect: true,
            				cursor: 'pointer',
           					dataLabels: {
               					enabled: true,
               					color: Highcharts.theme.textColor || '#000000',
               					connectorColor: Highcharts.theme.textColor || '#000000',
               					formatter: function() {
                  					return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
               					}
            				}
         				}
      				},
       				series: [{
         				type: 'pie',
         				name: 'Browser share',
         				data: [
            				['Firefox',   45.0],
            				['IE',       26.8],
            					{
               						name: 'Chrome',    
               						y: 12.8,
               						sliced: true,
               						selected: true
            					},
            				['Safari',    8.5],
            				['Opera',     6.2],
            				['Others',   0.7]
         				]
      				}]
  			 });	
		}
	} //*** Switch
}); //**** function
   


</script>



</head>
<body onLoad="initialize()">
<div id="container" style="width: 900px; height: 400px; margin: auto;"></div>
<div id="map_canvas" style="width: 900px; height: 400px; margin: auto; border-width: 2px; border-style: ridge; border-color: #666"></div>
<div id="tableContainer" style="width: 900px; height: 400px; margin: auto;" >
<br><br>
<?php

											
							
							echo "<h2>Campaign summary from ".$sDate." to ".$eDate."</h2>";
							echo "<table cellpadding='0' cellspacing='0' border='2' class='display' id='example'>"; //id='user-list'>"; 
							echo "<thead><tr><th>Item</th><th>State/Province</th><th>Total Entries</th></tr></thead>";
							echo "<tbody>";
							
							$counter = 0;
							$t = 0;
						   	foreach ($countryArray as $countrylabel) {
																	if($counter % 2 != 0){
							  			echo "<tr class='odd gradeX'>";
								 	}else{
							   			echo "<tr class='odd gradeA'>";
							 		 }
								 	echo "<td class='center'>".($counter+1)."</td>";
									echo "<td class='center'>".$countrylabel."</td>";
								 	echo "<td class='center'>".$countryTotalArray[$counter]."</a></td>";
								 	echo "</tr>";
									$t = $t + $countryTotalArray[$counter];
								
  								$counter++;
							}
							echo "<tr class='odd gradeA'>";
							echo "<td class='center'>".($counter+1)."</td>";
							echo "<td class='center'><b>Total entries</b></td>";
							echo "<td class='center'><b>".$t."</b></td>";
							echo "</tr>";
							echo "</tbody>";
							echo "</table>";
					?>
</div>
</body>

</html>
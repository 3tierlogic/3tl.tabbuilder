<?php

require_once("timlib.php");
// Connect to database
require_once("db.php");
$con = mysql_connect($dbhost, $dbuser, $dbpasswd);		// connect to database
if (!$con) {											// error checking and handling
    die('Could not connect: ' . mysql_error());
}
mysql_select_db($dbname);

$con_3tl = mysql_connect($dbhost_3tl, $dbuser_3tl, $dbpasswd_3tl);		// connect to database
if (!$con_3tl) {											// error checking and handling
    die('Could not connect to 3TL: ' . mysql_error());
}
mysql_select_db($dbname_3tl);

/* Load and clear sessions */
session_start();

$currentid = $_SESSION['companyid'];
$campaignID = $_REQUEST['cID']; // get
//$campaignName = $_REQUEST['cName']; // get
//$//companyName = $_REQUEST['coName']; // get

$campaignDesc = "";
$campaignSdate = "";
$campaignEdate = "";
$campaignReentryPic = "";
$maxDefaultDates = 7;
$recentStartDate = "";
$recentEndDate = "";

// run a sql to get camp info for later html use. 
$sql_companyInfo = "SELECT cp_companyName FROM sp_company_info WHERE cp_companyID='".$currentid."'";
$res_companyInfo = mysql_query($sql_companyInfo, $con);
if (mysql_num_rows($res_companyInfo) == 1) {
	list($companyName) = mysql_fetch_array($res_companyInfo);	
}

$sql_campInfo = "SELECT campaign_sid as cid, campaign_name as cname, campaign_description as cdesc, campaign_start_time as sdate, campaign_end_time as edate, campaign_is_check_reentry as creentry FROM sp_campaign_info s where campaign_sid ='".$campaignID."'";
$res0 = mysql_query($sql_campInfo, $con);
if (mysql_num_rows($res0) == 1) {
	list($camp_id, $camp_name, $camp_desc, $camp_sdate, $camp_edate, $camp_reentry) = mysql_fetch_array($res0);	
	$campaignName = $camp_name;
	$campaignDesc = $camp_desc;
	if (strlen($campaignDesc) <= 0) {
		$campaignDesc ="*** No description available ***";
	} 
	$campaignSdate = $camp_sdate;
	$campaignEdate = $camp_edate;
	//$campaignReentry = $camp_reentry;
	if ($camp_reentry == 1) {
		$campaignReentryPic = "images/singleentry.png";
	} else {
		$campaignReentryPic = "images/multipleentries.png";
	}	
}
else{
	echo "Err: could not pull out campaign info for cid: ".$campaignID."<br/>";
}

// import from getprizereport.php
//$sqlEndDate = $_REQUEST['edate']; //post
// to control if to show all dates or the specified period.
$default_date = 1; // defaults tolast few days
if ( (!isset($_REQUEST['edate'])) || ($_REQUEST['edate'] == "") || (!isset($_REQUEST['sdate'])) || ($_REQUEST['sdate'] == "") ){
	$default_date = 1;
} else {
	$default_date = 0;
}

if ( (!isset($_REQUEST['edate'])) || ($_REQUEST['edate'] == "") ){
	$sqlEndDate = "2100-12-31";
} else {
	$sqlEndDate = $_REQUEST['edate'];	
}
if( (!isset($_REQUEST['sdate'])) || ($_REQUEST['sdate'] == "") ){
	//$_POST['sdate'] = "";
	$_POST['sdate'] = "2000-01-01";
	$sqlStartDate = "2000-01-01";
}
else{
	$_POST['sdate'] = $_REQUEST['sdate'];
	$sqlStartDate = $_REQUEST['sdate'];
}
$sqlStartDate = $sqlStartDate." 00:00:00";
$sqlEndDate = $sqlEndDate." 23:59:59";

//$x_axis = "";
//$y_axis = "";
$displaygender_div = 0;
$displayemail_div=0;
$displayagerange_div=0;
$displayzipcode_div=0;
$displaycarrier_div = 0;

$countDyn = 0;
$displaydyn_div[$countDyn]=0;
$dynstr_data[$countDyn] = "";
$labels[$countDyn] = "";

$zipCodeCountry = "CA"; //by default Canada 


// end of import from getprizereport.php


//echo "ID: ".$currentid;
if (!isset($currentid)) {
	/*echo "<script language = 'javascript'> alert('Your session has expire. Please login again');</script>" ;
	echo "<script language = 'javascript'> window.location.href = 'index.php'; </script>" ;*/
	header ( 'Location: http://platform.3tierlogic.com/tabbuilder/index1_3.php' );
	exit();
}

//*****  Daily Visit chart
$x_axis = "";
$y_axis = "";
$y2_axis = ""; // for distinct entries.

$x_axis_last5 = "";
$y_axis_last5 = "";
$y2_axis_last5 = ""; //** Distinct entries

$total_entry_last5 = 0;
$total_uniqueentry_last5 = 0;

$sql = "SELECT date_format(cpresult_create_time,'%Y-%m-%d'), count(*) as num FROM sp_campaign_result where campaign_sid='".$campaignID."' and cpresult_create_time between '".$sqlStartDate."' and '".$sqlEndDate."' group by date_format(cpresult_create_time,'%Y-%m-%d') order by date_format(cpresult_create_time,'%Y-%m-%d') ASC";
$sql2 = "select dates, count(*) as num from ( SELECT date_format(cpresult_create_time,'%Y-%m-%d') as dates, cpresult_user_id as uid FROM sp_campaign_result f where f.campaign_sid='".$campaignID."' and cpresult_create_time between '".$sqlStartDate."' and '".$sqlEndDate."' group by date_format(f.cpresult_create_time,'%Y-%m-%d'), f.cpresult_user_id) s group by s.dates order by s.dates asc"; 
/*
	echo "sql = ".$sql."<br/>";
	echo "sql2 = ".$sql2."<br/>";*/

$res = mysql_query($sql, $con);
$res2 = mysql_query($sql2, $con);
$totalRecords = mysql_num_rows($res);
//*** Calculate the height of the table. Keep up to 1000px
$tableHeight = $totalRecords * 160; 
if ($tableHeight > 1000) {
	$tableHeight = 1100;
}
if ($totalRecords  > 0) {
	//echo "total rows: ".$totalRecords."<br>";
	
	$index = 0;
	$total_entry = 0;
	$total_distinct_entry = 0;
	// get Y.
	while (list($currdate, $totalentries) = mysql_fetch_array($res)) {
		//echo "date:".$currdate."- entries: ".$totalentries."<br>";
		//$newcurrdate = date('m/d', strtotime($currdate));
		$newcurrdate = $currdate;
		$x_axis = $x_axis . "'".$newcurrdate."',";
		$y_axis = $y_axis . $totalentries.",";
		
		//***** Get the last x days data defined by maxDefaultDates above
		if ((($totalRecords - $index) - $maxDefaultDates) <= 0) {
			$x_axis_last5 = $x_axis_last5 . "'".$newcurrdate."',";
			$y_axis_last5 = $y_axis_last5 . $totalentries.",";	
			$total_entry_last5 += $totalentries;
			//** Capture the start date 
			if ($recentStartDate == "") {
				$recentStartDate = $currdate;
			}
			//** Capture the end date
			if (($recentEndDate == "") && (($index+1) == $totalRecords)) {
				$recentEndDate = $currdate;
			}
		}
		
		// create array to store sql and sql2 results.
		$array_sql[$index][0] = $newcurrdate;
		$array_sql[$index][1] = $totalentries;
		$total_entry += $totalentries;
		$index++;
	}
	$array_sql[$index][0] = "Total";
	$array_sql[$index][1] = $total_entry;
	
	
	$index = 0;
	// get y2.
	while ( list($d, $dis) = mysql_fetch_array($res2)){
		$y2_axis = $y2_axis . $dis.",";
		//***** Get the last x days data defined by maxDefaultDates above
		if ((($totalRecords - $index) - $maxDefaultDates) <= 0) {
			$y2_axis_last5 = $y2_axis_last5 . $dis.",";
			$total_uniqueentry_last5 += $dis;	
		}
		
		$array_sql[$index][2] = $dis;
		$total_distinct_entry += $dis;
		$index++;
	}
	$array_sql[$index][2] = $total_distinct_entry;

	$x_axis = substr($x_axis, 0, -1);
	$y_axis = substr($y_axis, 0, -1);
	$y2_axis = substr($y2_axis, 0, -1);
	$x_axis_last5 = substr($x_axis_last5, 0, -1);
	$y_axis_last5 = substr($y_axis_last5, 0, -1);
	$y2_axis_last5 = substr($y2_axis_last5, 0, -1);
	
	//** By default, if no dates are specified, display last x dates defined by maxDefaultDates
	if ($default_date == 1) {
		$x_axis = $x_axis_last5;
		$y_axis = $y_axis_last5;
		$y2_axis = $y2_axis_last5;
	}
	//echo $recentStartDate.":".$recentEndDate."<br>";
}




?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>3 Tier Logic - Report V1.3</title>
 <link href="style.css" rel="stylesheet" type="text/css" />
 <!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if IE 6]>
<script src="js/belatedPNG.js"></script>
<script>
	DD_belatedPNG.fix('*');
</script>
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>
 <!-- Charts JS library: START -->
<script type="text/javascript" src="js/highcharts.js"></script>
<script type="text/javascript" src="js/themes/gray.js"></script>
<script type="text/javascript" src="js/modules/exporting.js"></script>
<!-- Charts JS library: END -->

<!-- date Picker start -->
<!--<style type="text/css">@import "css/redmond.datepick.css";</style>
<script src="js/CalendarControl/CalendarControl.js" language="javascript"></script>
<script type="text/javascript" src="js/jquery.datepick.js"></script>-->
<!-- date picker end-->

<script src="js/jquery.bxSlider.min.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#slider1').bxSlider({
    		auto: true,
    		pager: false,
			controls: false,
			pause: 5000
  		});
  });
</script>

<script type="text/javascript">

function viewEnties(td){
	var tord = td.substring(0,1);
	//alert(tord);

	var r = "r"+td.substring(1);
	var date = document.getElementById(r).innerHTML;
	//alert(date);

	var campid = "<?php echo $campaignID; ?>";
	//alert(campid);

	var detailPage = "detail.php";
	
	var sdate = <?php echo '\''.$_POST['sdate'].'\''; ?>;
	var edate = <?php echo '\''.$sqlEndDate.'\''; ?>;

	//var url = detailPage + "?campid=" + campid + "&date=" + date + "&tord=" + tord;
	var url = detailPage + "?campid=" + campid + "&date=" + date + "&tord=" + tord + "&sdate=" + sdate + "&edate=" + edate + "";
	//alert(url);
	 
	// window.open(url);   
	window.location.href = url; // to try colorbox.
}


		var chart;
		var chart_gender;
$(document).ready(function() {

$('tr').hover(
        function () {
            $(this).addClass('hover');
        },
        function () {
            $(this).removeClass('hover');
        }
    );
 
 
   chart = new Highcharts.Chart({
      chart: {
         renderTo: 'containergraph',
         defaultSeriesType: 'line',
         marginRight: 130,
         marginBottom: 25,
		 zoomType: 'x'
      },
      title: {
         text: '<?=$campaignName?>',
         x: -20 //center
      },
      subtitle: {
         text: 'Prepared for: <?=$companyName?>',
         x: -20
      },
      xAxis: {
         //categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		 categories: [<?=$x_axis?>],
		
		 
      },
      yAxis: {
         title: {
            text: 'Total Entries'
         },
         plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
         }]
      },
      tooltip: {
         formatter: function() {
                   return '<b>'+ this.series.name +'</b><br/>'+
               this.x +': '+ this.y +' entries';
         }
      },
      legend: {
         layout: 'vertical',
         align: 'right',
         verticalAlign: 'top',
         x: -10,
         y: 100,
         borderWidth: 0
      },
      series: [
      {
         name: 'Total Entries',
         data: [<?=$y_axis?>]
       },
      /*{
	name: 'Distinct Entries',
        data: [<?=$y2_axis?>]
      }*/]
   });
   
   
   
   
  });
</script>      

<script type="text/javascript">

$(function() {
	$('#popupDatepicker_start').datepicker({dateFormat: 'yy-mm-dd'});
	$('#popupDatepicker_end').datepicker({dateFormat: 'yy-mm-dd'});
});




function showDate(state) {
	//alert(state);

	var sdate = "";
	var edate = "";
	
	switch(state) {
		case 0:
			sdate = document.getElementById('popupDatepicker_start').value;
			edate = document.getElementById('popupDatepicker_end').value;
			break;
		case 1:	
			sdate = '2000-01-01';
			edate = '2100-12-31';
			break;
		case 2:
			sdate = '<?php echo $recentStartDate ?>';
			edate = '<?php echo $recentEndDate ?>';
			break;
		default:
			sdate = '2000-01-01';
			edate = '2100-12-31';
			
	}
	
	//alert('The date chosen is ' + sdate + " " + edate);


	// get every necessary param.
	var cid = "<?php echo $campaignID; ?>";
	var caname = "<?php echo $campaignName; ?>";
	var coname = "<?php echo $companyName; ?>";
	
	// build url.
	var url = '';
	url += 'dailyentries.php';
	var params = '?cID='+cid+'&cName='+caname+'&coName='+coname+'&sdate='+sdate+'&edate='+edate;
	url += params; 
	//alert(url);
	// go to self with new params.
	window.location.href = url; 

}

</script>

<style type="text/css">
      @import "DTmedia/css/demo_page.css";
      @import "DTmedia/css/demo_table.css";
</style>

<script class="jsbin" src="http://datatables.net/download/build/jquery.dataTables.js"></script>


<script type="text/javascript">
$(document).ready(function(){
	$('#example').dataTable({
        "bJQueryUI": true,
        <!--"sPaginationType": "full_numbers",-->
		"bPaginate": false,
		"bFilter": false,
		"bLengthChange": false,
		"bScrollInfinite": true,
        "bScrollCollapse": true,
        "sScrollY": "<?php echo $tableHeight ?>px"
    });
	
	$(".panel").slideToggle("slow");
	
	$(".flip").click(function(){
		$(".panel").slideToggle("slow");
	});
});
</script>

<link rel="stylesheet" href="colorbox.css" />
<script src="colorbox/jquery.colorbox.js"></script>
<script>
	$(document).ready(function(){	
		$(".picspopup").colorbox({iframe:true, width:"90%", height:"90%"});
	});
</script>


<style>
	div.panel{
		height:auto;
		display:none;
	}

	table#user-list tr.hover {
		color: #fff;
		background-color: #4c95e6;
	}	
	#containergraph {
		float: left
    	width: 33.3%;
		/* Min-height: */
		min-height: 400px;
		height: auto !important; 
		height: 400px;
	}
</style>
    

</head>



<body class="normalpage">
	<section id="page">
		<div id="bodywrap">
			<section id="top">
				<nav>
					<h1 id="sitename">
  					<a href="#">3 Tier Logic Inc.</a></h1>
  
 					<ul id="sitenav">
                    	<li class="current"><a href="#">Entries Summary</a></li>
  						<li><a href="main.php">Report Dashboard</a></li>
  						<li><a href="../editor/dashboard.php">Back to Tab Editor</a></li>
  					</ul>
  
				</nav>
				<header id="normalheader"></header>
			</section>
            
			<section id="contentwrap">
				<div id="contents">
					<div id="topcolumns">
						<div class="col">
							<!-- <img src="../images/design.png" width="61" height="60" alt="graph" class="imgright"> --> 
							<h2>Campaign Info</h2>
                            <p>
							<b>Company Name: </b> <?=$companyName?><br> 
							<b>Campaign Name: </b> <?=$campaignName?><br>   
							<b>Start date: </b> <?=$campaignSdate?><br> 
							<b>End date: </b><?=$campaignEdate?><br> 
							<b>Entry requirement: </b><img src="<?=$campaignReentryPic?>" /><br> 
                            <p class="flip"><b>Description: (Click to view/hide)</b></p><div class="panel"><?=$campaignDesc?></div><br>
							</p>
						</div>
						<div class="col">							
							<h2>Filter by date</h2>
							<table cellpadding="0" cellspacing="0">
                            	<tr>
                      				<td><b>Start Date:</b>&nbsp;</td> 
                                    <td><input name='startdate'  type='text' id='popupDatepicker_start'></input></td>
								</tr>
                                <tr>
									<td><b>End Date:</b>&nbsp;</td>
                                    <td><input name='enddate' type='text' id='popupDatepicker_end'></input></td>
								</tr>
                                <tr>
                                	<td colspan="2" align="right"><br /><input type='button' value='submit' onclick='showDate(0)'></input></td>
                                </tr>
							</table>
						</div>
						<div class="col">							
							<h2>Quick Links</h2>
							<p>
                            <ul>
								<li><img src="images/rightarrow.png" border="0" width="10" height="10" />&nbsp;&nbsp;<a href="#" onclick='showDate(1)'>View all entries</a></li><br/>
                            <?php if ($totalRecords > 0) { ?>	
                                    <li><img src="images/rightarrow.png" border="0" width="10" height="10" />&nbsp;&nbsp;<a href="#" onclick="showDate(2)">View last <?php echo $maxDefaultDates ?> days' entries</a></li><br/>	
									<?php 
										$checkbox = "OpenCheckBox";		
										$dropdown = "OpenDropDown";
										$opentext = "OpenText";
										$pos_opentext = 1;
										$cType = "";
										for ($i = 0; $i < $count_fields; $i++){
											$pos_checkbox = strpos($fieldNames[$i], $checkbox);
											$pos_dropdown = strpos($fieldNames[$i], $dropdown);
											if (($pos_checkbox === 0) || ($pos_dropdown === 0)) {
												if ($pos_checkbox === 0) $cType = $checkbox;
												if ($pos_dropdown === 0) $cType = $dropdown;
											} else {
												$cType = $fieldNames[$i];	
											}
											$pos_opentext = strpos($fieldNames[$i], $opentext);
											//echo "pos_opentext: ".$pos_opentext."-".$fieldNames[$i]."<br>";
											if( (strtolower($fieldNames[$i]) == "firstname") || (strtolower($fieldNames[$i]) == "lastname") || (strtolower($fieldNames[$i]) == "email") || (strtolower($fieldNames[$i]) == "mobilephone") || (strtolower($fieldNames[$i]) == "phonenumber") || (strtolower($fieldNames[$i]) == "address") || (strtolower($fieldNames[$i]) == "city") || (strtolower($fieldNames[$i]) == "state") || (strtolower($fieldNames[$i]) == "zipcode") ||  (strtolower($fieldNames[$i]) == "birthdaymonth") || (strtolower($fieldNames[$i]) == "birthdayday") || ($pos_opentext === 0)  ){
												continue;
											}
											echo '<li><img src="images/rightarrow.png" border="0" width="10" height="10" />&nbsp;&nbsp;<a class="picspopup" href="quickchart_'.$cType.'.php?cid='.$campaignID.'&sdate='.$sqlStartDate.'&edate='.$sqlEndDate.'&ctype='.$fieldNames[$i].'&c='.$zipCodeCountry.'">View "'.$fieldLabelNames[$i].'" stats</a></li><br>';
										}
								}
							?>	
                            	<li><img src="images/rightarrow.png" border="0" width="10" height="10" />&nbsp;&nbsp;<a href="dailyugcentries.php?cID=<?=$campaignID?>">View UGC Report</a></li><br/>
                                <li><img src="images/rightarrow.png" border="0" width="10" height="10" />&nbsp;&nbsp;<a href="dailyvotes.php?cID=<?=$campaignID?>">View UGC Vote Report</a></li><br/>
                            </ul>
							</p>
						</div>	
						<div class="clear"></div>
                        <!-- ****** Display Charts here **** -->
                    	<div id="containergraph" style="width: 900px; height: 400px; margin: auto;"></div>
						
						
						<br /><br />
					<?php

						echo "<div id='tableContainer'>";					
						if($index > 0){
							echo "<h2>Campaign summary from ".$sqlStartDate." to ".$sqlEndDate." </h2>";
							echo "<table cellpadding='0' cellspacing='0' width='60%'>";
							echo "<tr>";
							//echo "<h4>total days: ".mysql_num_rows($res)."</h4>";
							if ($index <= 1) {
								echo "<td width='60%'><h4>Campaign duration: </h4></td><td width='40%'>".$index." day</td>";
							} else {
								echo "<td width='60%'><h4>Campaign duration: </h4></td><td width='40%'>".$index." days</td>";
							}
							echo "</tr>";
							echo "<tr><td width='60%'><h4>Total entries: </h4></td><td width='40%'>".$total_entry."</td></tr>";
							//echo "<tr><td width='60%'><h4>Total unique entries: </h4></td><td width='40%'>".$total_distinct_entry."</td></tr>";
							echo "<tr><td width='60%'><h4>Total entries in the ".$maxDefaultDates." last days: </h4></td><td width='40%'>".$total_entry_last5."</td></tr>";
							//echo "<tr><td width='60%'><h4>Total unique entries in the last ".$maxDefaultDates." days: </h4></td><td width='40%'>".$total_uniqueentry_last5."</td></tr>";
							echo "<table>";
							
							echo "<br><br>";
							echo "<table cellpadding='0' cellspacing='0' border='2' class='display' id='example'>"; //id='user-list'>"; 
							//echo "<thead><tr><th>Day</th><th>Date</th><th>Total Entries</th><th>Distinct Entries</th></tr></thead>";
							echo "<thead><tr><th>Day</th><th>Date</th><th>Total Entries</th></tr></thead>";
							echo "<tbody>";
							
							$i = 0;
						   	//while (list($currdate, $totalentries) = mysql_fetch_array($res)) {
						   	while($i <= $index){
								//echo "date:".$currdate."- entries: ".$totalentries."<br>";
							 
							 	if($i % 2 != 0){
							  		echo "<tr class='odd gradeX'>";
								 }else{
							   		echo "<tr class='odd gradeA'>";
							 	 }
							 	//echo "<td class='center'>".$currdate."</td>";
								echo "<td class='center'>".($i+1)."</td>";
							 	echo "<td class='center'> ".$array_sql[$i][0]."</td>";
						
								//echo '<td class="center"> <a id="t'.$i.'" href="javascript: viewEnties(\'t'.$i.'\')" >'.$array_sql[$i][1].'</a></td>';	
								echo '<td class="center"> <a class="picspopup" id="t'.$i.'" href='.getDetailURL("t".$i, $array_sql[$i][0], $campaignID, $_POST['sdate'], $sqlEndDate).'>'.$array_sql[$i][1].'</a></td>'; 
								//echo "postsdate = ".$_POST['sdate']." sqlenddate = ".$sqlEndDate."<br/>";
						
								//echo '<td class="center"> <a id="d'.$i.'" href="javascript: viewEnties(\'d'.$i.'\')" >'.$array_sql[$i][2].'</a></td>';
								//echo '<td class="center"> <a class="picspopup" id="d'.$i.'" href="javascript: viewEnties(\'d'.$i.'\')" >'.$array_sql[$i][2].'</a></td>';
								//echo '<td class="center"> <a class="picspopup" id="t'.$i.'" href='.getDetailURL("d".$i, $array_sql[$i][0], $campaignID, $_POST['sdate'], $sqlEndDate).'>'.$array_sql[$i][2].'</a></td>'; 
								echo "</tr>";
							 	$i++;
						   	}
							echo "</tbody>";
							echo "</table>";
						}       
						echo "</div>";
					
						if ($displaygender_div == 1) { 
							echo "<div id='container2' style='width: 900px; height: 400px; margin: 0 auto'></div>";
						 }
					?>
						
						<div class="clear"></div>
					</div>
				</div>
			</section>
		</div>

				<footer id="pagefooter">
					<div id="bottom">
						<div class="block1">
							<h2>Meet The Team</h2>
							<div class="teamimg">
                            <? include("team.php")?>
							
							</div>
						</div>
						<div class="block2">
							<h2>Latest Project</h2>
							<div class="projectthmb"> 
                            	
                                <?
                                	$sql = "SELECT ID, post_date, post_content, post_title FROM 3tierweb.tier_posts WHERE post_type='post' and post_parent='0' and post_status='publish' order by post_date desc Limit 4";
									$res = mysql_query($sql, $con_3tl);
									if (mysql_num_rows($res) > 0)  {
								?>
                                			
                                        
                                        	<ul id="slider1">
                                            	<?
												$picon = "";
												while(list($pid, $pdate, $pcontent, $ptitle) = mysql_fetch_array($res))  {
													$sql_icon = "SELECT guid FROM 3tierweb.tier_posts where post_type='attachment' and (post_mime_type='image/jpeg' or post_mime_type='image/png')  and post_parent='".$pid."' order by post_date desc";
													$res_icon = mysql_query($sql_icon, $con_3tl);
													if (mysql_num_rows($res_icon) > 0)  {
														list($picon) = mysql_fetch_array($res_icon);	
													}
													echo "<li><a href='http://www5.3tierlogic.com/?p=".$pid."' target='_blank'><img src='".$picon."' width='240' height='150' /></a><p style='font:Verdana, Geneva, sans-serif; color:#FFF; font-size:10px '>".$ptitle."</p></li>";
													//echo "<li><a href='http://www5.3tierlogic.com/?p=".$pid."' target='_blank'>".$ptitle."</a></li>";
												}
												?>
                                               
                                            </ul>
                                        
                                        
                                <?
									} else {
								?>
                                		<a href="portfolio"><img src="images/latstproject.jpg" width="240" height="150" alt="project"></a>
                                <?
									}
								?>
                            </div>

						</div>
						<div class="block3">
							<h2>About</h2>
							<p>3 Tier Logic is a full service digital marketing and technology services company that enables organizations to grow their customer and prospect communities through anywhere-anytime communications. </p>

						</div>
						<div class="clear"></div>
				</div>
				<div id="credits">
					<p>
					<span class="copyright">
					&copy; 2012 | 3 Tier Logic Inc |  All Rights Reserved </span></p>
				</div>
			</footer>

</section>

</body>
</html>

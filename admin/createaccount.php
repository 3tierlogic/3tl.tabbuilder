<?
session_start();
if (isset($_SESSION['superaccount'])) {
	$isSuperUser = 	$_SESSION['superaccount'];
}
	
// Connect to database
require_once("../editor/config.php");
$con = mysql_connect(DB_HOST, DB_USER, DB_PASS);		// connect to database
if (!$con) {											// error checking and handling
    die('Could not connect: ' . mysql_error());
}
mysql_select_db(DB_NAME);
?>
<!doctype html>

<html lang="en-US">
<head>
<meta charset="UTF-8" />
<title>Tab Editor - Administration Section </title>
<link href="../style.css" rel="stylesheet" type="text/css" />
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if IE 6]>
<script src="../js/belatedPNG.js"></script>
<script>
	DD_belatedPNG.fix('*');
</script>
<![endif]-->
</head>

<body class="normalpage">
<section id="page">
<div id="bodywrap">
<section id="top">
<nav>
<h1 id="sitename">
  <a href="#">Portfolio | Premium CSS Template </a></h1>
  
  	<ul id="sitenav">
		<li><a href="../editor/dashboard.php">Back to dashboard</a></li>
        <li class="current"><a href="">Admin</a></li>
	</ul>
  
</nav>
<header id="normalheader"></header>
</section>
<section id="contentwrap">
<div id="contents" class="normalcontents"><section id="normalpage">

<section id="left">

<h2>Administration Page</h2>
<article>

<h3>Your account has been created with the following information:</h3>


<?
$currentCompanyID = $_REQUEST['customerID'];
$createCompany = false;
if ($currentCompanyID == '0') {
	$createCompany = true;	
}


echo "Company Name: ".$_REQUEST['cname']."<br>";
echo "Company Address: ".$_REQUEST['caddress']."<br>";
echo "City: ".$_REQUEST['ccity']."<br>";
echo "Postal Code: ".$_REQUEST['cpostal']."<br>";
echo "URL: ".$_REQUEST['curl']."<br>";

$totalwidgets = count($_REQUEST['cwidget']);
$selectedwidgets = $_REQUEST['cwidget'];
echo "Total widgets: ".$totalwidgets."<br>";
for ($i=0; $i<$totalwidgets; $i++) {
	echo ($i+1) . '- ' . $selectedwidgets[$i] . '<br>';
}

echo "User Name: ".$_REQUEST['user1name']."<br>";
echo "Login ID: ".$_REQUEST['user1loginid']."<br>";
echo "Password: ".$_REQUEST['user1password']."<br>";

if (!$createCompany) { //*** Company already 
	$new_companyID = $currentCompanyID;
} else { //*** Create a new company 
	
	$sql_create_company = "INSERT INTO sp_company_info (cp_companyName, cp_companyHeadAddress, cp_city, cp_postalCode, cp_createDate, cp_url, cp_status) VALUES ('".
					  addslashes($_REQUEST['cname'])."','".
					  addslashes($_REQUEST['caddress'])."','".
					  addslashes($_REQUEST['ccity'])."','".
					  addslashes($_REQUEST['cpostal'])."','".
					  date("Y-m-d H:i:s")."','".
					  addslashes($_REQUEST['curl'])."','1')";
	$res_create_company = mysql_query($sql_create_company, $con);
	$new_companyID= mysql_insert_id();
	echo "Record created in the company table with id: ".$new_companyID."<br>";		

	
}

$sql_create_wdigets = "INSERT INTO sp_partner_widgets SET partner_sid ='".$new_companyID."', ";
for ($i=0; $i<$totalwidgets; $i++) {
	$sql_create_wdigets = $sql_create_wdigets.$selectedwidgets[$i]."=1,";
}

$sql_create_wdigets = substr($sql_create_wdigets, 0, -1);
echo $sql_create_wdigets;
$res_create_wdigets = mysql_query($sql_create_wdigets, $con);
$new_widgetsID= mysql_insert_id();
echo "Record created in the widgets table with id: ".$new_widgetsID."<br>";
	
	
$sql_create_user = "INSERT INTO sp_partner_info SET partner_company_sid ='".$new_companyID."', partner_code='0', partner_login_account='".
					$_REQUEST['user1loginid']."', partner_login_password='".$_REQUEST['user1password']."', partner_contact_name='".$_REQUEST['user1name']."', partner_create_time='".date("Y-m-d H:i:s")."'";
$res_create_user = mysql_query($sql_create_user, $con);
$new_userID= mysql_insert_id();
echo "<br>".$sql_create_user."<br>";
echo "Record created in the users table with id: ".$new_userID."<br>";	

?>

<br><br><br>    
         
</article>

</section>
<section id="sidebar">
<h2>Testimonials</h2>
<article class="testimonials">
<blockquote>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer egestas purus bibendum neque aliquam ut posuere elit semper. Fusce sagittis pharetra eros, sit amet consequat sem mollis vitae. </p>
<cite>John Doe, New York City</cite>
</blockquote>

</article>
</section>

<div class="clear"></div>
</section>
</div>
</section>
</div>
<footer id="pagefooter">
<div id="bottom">
<div class="block1">
<h2>Meet The Team</h2>

<div class="teamimg">
	<? include("../editor/team.php")?>
	<!--<div class="imgthmb"><img src="../images/team5.jpg" width="65" height="65" alt="team"></div>
  	<div class="imgthmb"><img src="../images/team1.jpg" width="65" height="65" alt="team"></div>
  	<div class="imgthmb"><img src="../images/team2.jpg" width="65" height="65" alt="team"></div>
  	<div class="imgthmb"><img src="../images/team4.jpg" width="65" height="65" alt="team"></div>
  	<div class="imgthmb"><img src="../images/team2.jpg" width="65" height="65" alt="team"></div>
  	<div class="imgthmb"><img src="../images/team1.jpg" width="65" height="65" alt="team"></div>-->
</div>
</div>
<div class="block2">
<h2>Latest Project</h2>
<div class="projectthmb"><? include("../editor/latestproject.php")?> <!--<a href="../portfolio"><img src="../images/latstproject.jpg" width="240" height="150" alt="project"></a>--></div>

</div>
<div class="block3">

<h2>More About</h2>
<p>3 Tier Logic is a full service digital marketing and technology services company that enables organizations to grow their customer and prospect communities through anywhere-anytime communications. </p>

</div>
<div class="clear"></div>
</div>
<div id="credits">
<p>
<span class="copyright">&copy; 2012 | 3 Tier Logic Inc |  All Rights Reserved </span>
<span id="designcredit">Powered by <a href="http://www.3tierlogic.com" title="Powered by 3 Tier Logic Inc" target="_blank">3 Tier Logic Inc.</a></span>
</p>
</div>
</footer>

</section>
</body>
</html>

<?
session_start();
if (isset($_SESSION['superaccount'])) {
	$isSuperUser = 	$_SESSION['superaccount'];
}

if (isset($_REQUEST['customerID'])) {
	$currentid = $_REQUEST['customerID'];
} else {
	$currentid = 0;
}

	
// Connect to database
require_once("../editor/config.php");
$con = mysql_connect(DB_HOST, DB_USER, DB_PASS);		// connect to database
if (!$con) {											// error checking and handling
    die('Could not connect: ' . mysql_error());
}
mysql_select_db(DB_NAME);
?>
<!doctype html>

<html lang="en-US">
<head>
<meta charset="UTF-8" />
<title>Tab Editor - Administration Section </title>
<link href="../style.css" rel="stylesheet" type="text/css" />
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if IE 6]>
<script src="../js/belatedPNG.js"></script>
<script>
	DD_belatedPNG.fix('*');
</script>
<![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script language = "javascript">

	function myformsubmit() {
   		document.myform.submit();
	}
</script>
</head>

<body class="normalpage">
<section id="page">
<div id="bodywrap">
<section id="top">
<nav>
<h1 id="sitename">
  <a href="#">Portfolio | Premium CSS Template </a></h1>
  
  	<ul id="sitenav">
		<li><a href="../editor/dashboard.php">Back to dashboard</a></li>
        <li class="current"><a href="">Admin</a></li>
		
	</ul>
  
</nav>
<header id="normalheader"></header>
</section>
<section id="contentwrap">
<div id="contents" class="normalcontents"><section id="normalpage">

<section id="left">

<h2>Administration Page</h2>
<article>
<?
	//**** Display the list of companies - Available only for super user account
	if ($isSuperUser) {
       	$sql = "SELECT cp_companyID, cp_companyName FROM sp_company_info where cp_status='1' order by cp_companyName";
      	$res = mysql_query($sql, $con);
?>
      	<form name="myform" method="POST" action="<?=$_SERVER['PHP_SELF']?>">
			<table width="100%">
				<tr><td>
					<h3>Select a Company from the list below to view</h3> 
                    <select name = "customerID" onChange = "myformsubmit();"> 
                    	<option value = ""> select </option> 
                        <option value = "0"> Create a new company </option>                                  
                        <?
                                      while(list($cid,$cname) = mysql_fetch_array($res))  {
									     if($currentid == $cid) {
										  	echo "<option value = '$cid' selected> $cname </option>";
										 } else {
									      	echo "<option value = '$cid'> $cname </option>";
									  	 }								  
									  }
									
						?>
                    </select>
                </td></tr>				
			</table>
         </form>
<?
	}
?>

<form name="newform" method="POST" action="confirm.php">

<? if ($currentid != 0) { 
		$divParams = "id='companyinfo' style='display: none;'";
	} else {
		$divParams = "id='companyinfo'";
	}
?>

<div <?=$divParams?> >

<br><br>
<h3>or, fill out the form below to add a new company profile</h3>



<table width="100%">
	<tr>
    	<td>Company Name:</td>
        <td>&nbsp;</td>
        <td><input type="text" name="cname" maxlength="100" size="40"></td>		 
    </tr>		
    <tr>
    	<td>Address:</td>
        <td>&nbsp;</td>
        <td><input type="text" name="caddress" maxlength="100" size="40"></td>		 
    </tr>	
    <tr>
    	<td>City:</td>
        <td>&nbsp;</td>
        <td><input type="text" name="ccity" maxlength="100" size="40"></td>		 
    </tr>
    <tr>
    	<td>Postal Code:</td>
        <td>&nbsp;</td>
        <td><input type="text" name="cpostal" maxlength="100" size="40"></td>		 
    </tr>
     <tr>
    	<td>URL:</td>
        <td>&nbsp;</td>
        <td><input type="text" name="curl" maxlength="100" size="40"></td>		 
    </tr>
</table>
</div>
<table width="100%">
    <tr>
    	<td colspan="3"><br><br><hr width="100%"><br><br></td>	 
    </tr>	
     <tr>
    	<td valign="top">Widgets:</td>
        <td>&nbsp;</td>
        <td><input type="checkbox" name="cwidget[]" value="w_text" checked>&nbsp;&nbsp;Text<br><br>
        	<input type="checkbox" name="cwidget[]" value="w_link" checked>&nbsp;&nbsp;Hotspot<br><br>
            <input type="checkbox" name="cwidget[]" value="w_countdown" checked>&nbsp;&nbsp;Countdown<br><br>
            <input type="checkbox" name="cwidget[]" value="w_contact_form" checked>&nbsp;&nbsp;Contact Form<br><br>
            <input type="checkbox" name="cwidget[]" value="w_email" checked>&nbsp;&nbsp;Email<br><br>
            <input type="checkbox" name="cwidget[]" value="w_like" checked>&nbsp;&nbsp;Like<br><br>
            <input type="checkbox" name="cwidget[]" value="w_comment" checked>&nbsp;&nbsp;Comment<br><br>
            <input type="checkbox" name="cwidget[]" value="w_share" checked>&nbsp;&nbsp;Facebook Share<br><br>
            <input type="checkbox" name="cwidget[]" value="w_invitefriend" checked>&nbsp;&nbsp;Facebook Invite a Friend<br><br>
            <input type="checkbox" name="cwidget[]" value="w_send" checked>&nbsp;&nbsp;Send<br><br>
            <input type="checkbox" name="cwidget[]" value="w_twitter_stream" checked>&nbsp;&nbsp;Twitter Stream<br><br>
            <input type="checkbox" name="cwidget[]" value="w_sharetwitter" checked>&nbsp;&nbsp;Twitter Share<br><br>
            <input type="checkbox" name="cwidget[]" value="w_pinterestpinit" checked>&nbsp;&nbsp;Pinterest Pin It<br><br>
            <input type="checkbox" name="cwidget[]" value="w_pinterestfollow" checked>&nbsp;&nbsp;Pinterest Follow Me<br><br>
            <input type="checkbox" name="cwidget[]" value="w_google_plus" checked>&nbsp;&nbsp;Google+<br><br>
            <input type="checkbox" name="cwidget[]" value="w_rss" checked>&nbsp;&nbsp;RSS<br><br>
            <input type="checkbox" name="cwidget[]" value="w_google_maps" checked>&nbsp;&nbsp;Google Maps<br><br>
            <input type="checkbox" name="cwidget[]" value="w_youtube" checked>&nbsp;&nbsp;YouTube<br><br>
            <input type="checkbox" name="cwidget[]" value="w_vimeo" checked>&nbsp;&nbsp;Vimeo<br><br>
            <input type="checkbox" name="cwidget[]" value="w_flickr" checked>&nbsp;&nbsp;Flickr<br><br>
            <input type="checkbox" name="cwidget[]" value="w_image" checked>&nbsp;&nbsp;Single Image<br><br>
            <input type="checkbox" name="cwidget[]" value="w_imageslider" checked>&nbsp;&nbsp;Image Slider<br><br>
            <input type="checkbox" name="cwidget[]" value="w_html" checked>&nbsp;&nbsp;HTML<br><br>
            <input type="checkbox" name="cwidget[]" value="w_iframe" checked>&nbsp;&nbsp;iFrame<br><br>
            <input type="checkbox" name="cwidget[]" value="w_contest" checked>&nbsp;&nbsp;Sweepstakes<br><br>
            <input type="checkbox" name="cwidget[]" value="w_ugc" checked>&nbsp;&nbsp;UGC<br><br>
        </td>		 
	</tr>	
    <tr>
    	<td colspan="3"><br><br><hr width="100%"><br><br></td>	  
    </tr>	
     <tr>
    	<td>User 1 - Name:</td>
        <td>&nbsp;</td>
        <td><input type="text" name="user1name" maxlength="100" size="40"></td>		 
    </tr>
     <tr>
    	<td>User 1 - Login ID:</td>
        <td>&nbsp;</td>
        <td><input type="text" name="user1loginid" maxlength="100" size="40"></td>		 
    </tr>
    <tr>
    	<td>User 1 - Password:</td>
        <td>&nbsp;</td>
        <td><input type="text" name="user1password" maxlength="100" size="40"></td>		 
    </tr>
    <tr>
    	<td colspan="3"><br><br><hr width="100%"><br><br></td>	  
    </tr>
    <tr>
    	<td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><input type="hidden" value="<?=$currentid?>" name="customerID"><input type="submit" value="Create Account"></td>		 
    </tr>	
</table>
</form>
<br><br><br>    
         
</article>

</section>
<section id="sidebar">
<h2>Testimonials</h2>
<article class="testimonials">
<blockquote>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer egestas purus bibendum neque aliquam ut posuere elit semper. Fusce sagittis pharetra eros, sit amet consequat sem mollis vitae. </p>
<cite>John Doe, New York City</cite>
</blockquote>

</article>
</section>

<div class="clear"></div>
</section>
</div>
</section>
</div>
<footer id="pagefooter">
<div id="bottom">
<div class="block1">
<h2>Meet The Team</h2>

<div class="teamimg">
	<? include("../editor/team.php")?>
	<!--<div class="imgthmb"><img src="../images/team5.jpg" width="65" height="65" alt="team"></div>
  	<div class="imgthmb"><img src="../images/team1.jpg" width="65" height="65" alt="team"></div>
  	<div class="imgthmb"><img src="../images/team2.jpg" width="65" height="65" alt="team"></div>
  	<div class="imgthmb"><img src="../images/team4.jpg" width="65" height="65" alt="team"></div>
  	<div class="imgthmb"><img src="../images/team2.jpg" width="65" height="65" alt="team"></div>
  	<div class="imgthmb"><img src="../images/team1.jpg" width="65" height="65" alt="team"></div>-->
</div>
</div>
<div class="block2">
<h2>Latest Project</h2>
<div class="projectthmb"><? include("../editor/latestproject.php")?> <!--<a href="../portfolio"><img src="../images/latstproject.jpg" width="240" height="150" alt="project"></a>--></div>

</div>
<div class="block3">

<h2>More About</h2>
<p>3 Tier Logic is a full service digital marketing and technology services company that enables organizations to grow their customer and prospect communities through anywhere-anytime communications. </p>

</div>
<div class="clear"></div>
</div>
<div id="credits">
<p>
<span class="copyright">&copy; 2012 | 3 Tier Logic Inc |  All Rights Reserved </span>
<span id="designcredit">Powered by <a href="http://www.3tierlogic.com" title="Powered by 3 Tier Logic Inc" target="_blank">3 Tier Logic Inc.</a></span>
</p>
</div>
</footer>

</section>
</body>
</html>
